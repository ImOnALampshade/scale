import os
import shutil
import subprocess
import sys
import tempfile
import time
import nvtools

imgtype = "tga"

def generate_mips(count, fulltexture, miplist):
  prev = fulltexture

  for mip in miplist:
    nvtools.zoom(prev, output=mip, filter='kaiser')
    prev = mip

def make_kto(compression, output, files):
  print ('Generating KTO file...')

  args = ['ktomaker', output, compression] + files
  subprocess.call(args)

def main():

  if len(sys.argv) != 5:
    print('Usage: create-kto <input> <output> <mip count> <compression>')
    return None

  start_time = time.time()

  inputimg    = sys.argv[1]
  outputimg   = sys.argv[2]
  mipcount    = int(sys.argv[3])
  compression = sys.argv[4]

  # Make a temporary directory (For us to process asset files with)
  work_dir    = tempfile.mkdtemp()

  try:
    fulltexture = os.path.join(work_dir, 'full.{}'.format(imgtype))
    miplist = [os.path.join(work_dir, 'mip{}.{}'.format(i, imgtype)) for i in range(1, mipcount + 1)]

    print ('Converting to TGA...')
    nvtools.convertimg(inputimg, fulltexture)

    print ('Generating mip maps...')
    generate_mips(mipcount, fulltexture, miplist)

    if compression != 'none':
      print ('Compressing images...')

      images = []

      for img in [fulltexture] + miplist:
        compressed = '{}.dds'.format(os.path.splitext(img)[0])
        nvtools.compress(compression, img, compressed)
        images.append(compressed)

      print ('Generating final KTO')
      make_kto(compression, outputimg, images)

    else:
      print ('Generating final KTO')
      make_kto(compression, outputimg, [fulltexture] + miplist)

  finally:
    # Delete the working directory now that we're done with it
    print('Removing temporary directory:', work_dir)
    shutil.rmtree(work_dir)

  end_time = time.time()
  print('Finished in {:0.4} seconds'.format(end_time - start_time))

if __name__ == '__main__': main()
