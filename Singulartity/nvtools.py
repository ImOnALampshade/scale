import sys
from subprocess import call

def convertimg(src, dest):
  call(['imgconv', src, dest])

compress_modes = { 'dxt1': '-bc1', 'dxt3': '-bc2', 'dxt5': '-bc3' }

def compress(mode, infile, outfile, type='color', genmips=False):
  args = ['nvcompress', '-{}'.format(type)]

  if not genmips:
    args.append('-nomips')

  args.append(compress_modes[mode])
  args.append(infile)
  args.append(outfile)

  call(args)

def decompress(file):
  call(['nvdecompress', file])

def ddsinfo(file):
  call(['nvddsinfo', file])

def imgdiff(img1, img2, normal=False, alpha=False):
  args = ['nvimgdiff']

  if normal:
    args.append('-normal')
  if alpha:
    args.append('-alpha')

  args.append(img1)
  args.append(img2)

  call(args)

def assemble(mode, files):
  args = ['nvassemble', '-{}'.format(mode)] + files
  call(args)

def zoom(input, output=None, scale=0.5, gamma=1, filter='box'):
  args = ['nvzoom', '-s', str(scale), '-g', str(gamma), '-f', filter, input]

  if output is not None:
    args.append(output)

  call(args)
