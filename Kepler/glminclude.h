// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Include file for GLM for defining preprocessor stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once
#ifndef KEPLER_GLM_INCLUDE_H
#define KEPLER_GLM_INCLUDE_H

#ifndef GLM_SWIZZLE
#define GLM_SWIZZLE
#endif

#ifndef GLM_FORCE_RADIANS
#define GLM_FORCE_RADIANS
#endif

#pragma warning(push,1)
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/simd_vec4.hpp>
#include "KeplerMath/KeplerMath.h"
#pragma warning(pop)

#ifndef PI
#define PI (3.141592f)
#endif

#ifndef DEGREE
#define DEGREE (PI/180)
#endif

#ifndef RAD_TO_DEG
#define RAD_TO_DEG(deg) ((deg) * (180.f / PI))
#endif

#ifndef DEG_TO_RAD
#define DEG_TO_RAD(rad) ((rad) * (PI / 180.f))
#endif

#endif
