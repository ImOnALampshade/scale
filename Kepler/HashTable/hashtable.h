// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Closed-chaining hash table implementation
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <cassert>
#include <cstdint>
#include <utility>
#include "../ObjectAllocator/objectallocator.h"
#include "../AllocatorManager/allocatormanager.h"
#include "hashfn.h"
#include "primes.h"
#include "../Logger/logger.h"

// -----------------------------------------------------------------------------

template<typename Key, typename Value, typename HashFunctionType = HashFunction<Key>::type>
class HashTable
{
public:
  struct Payload
  {
    Payload (const Key &key, const Value &val);

    Key   key;
    Value value;
  };

private:
  struct TableNode
  {
    TableNode (const Key &key, const Value &val, std::uint32_t hash);

    TableNode    *next;
    Payload       data;
    std::uint32_t fullHash;
  };

  struct TableSlot
  {
    TableSlot();

    TableNode *first;
  };

  TableSlot        *m_buckets;
  unsigned          m_bucketCount;
  unsigned          m_nodeCount;
  HashFunctionType  m_hash;
  ObjectAllocator  *m_allocator;

  static const float    GROWTH_FACTOR;
  static const float    MAX_LOAD_FACTOR;
  static const unsigned SIZE_START;

  void GrowTable();

public:

  class iterator
  {
  private:
    HashTable *m_table;
    unsigned   m_bucket;
    TableNode *m_node;
  public:
    iterator();
    iterator (HashTable *table);
    iterator (HashTable *table, unsigned bucket, TableNode *node);

    iterator operator++ (int);
    iterator &operator++ ();

    Payload &operator*() const;
    Payload *operator->() const;

    bool operator== (const iterator &it) const;
    bool operator!= (const iterator &it) const;
  };

  class const_iterator
  {
  private:
    const HashTable *m_table;
    unsigned   m_bucket;
    TableNode *m_node;
  public:
    const_iterator();
    const_iterator (const HashTable *table);
    const_iterator (const HashTable *table, unsigned bucket, TableNode *node);

    const_iterator operator++ (int);
    const_iterator &operator++ ();

    const Payload &operator*() const;
    const Payload *operator->() const;

    bool operator== (const const_iterator &it) const;
    bool operator!= (const const_iterator &it) const;
  };

  HashTable (HashFunctionType hash);
  HashTable (const HashTable &table);
  ~HashTable();

  HashTable &operator= (const HashTable &);

  void Clear();
  bool IsEmpty() const;

  unsigned Size() const;

  iterator begin();
  iterator end();
  const_iterator cbegin() const;
  const_iterator cend() const;
  const_iterator begin() const;
  const_iterator end() const;

  Value &operator[] (const Key &);

  iterator Find (const Key &);
  const_iterator Find (const Key &) const;

  bool Contains (const Key &) const;

  std::pair<bool, iterator> Insert (const Key &, const Value &);
  bool Erase (const Key &key);
  void Erase (iterator it);

  template<typename Callable>
  Callable EraseIf (Callable call);

  template <typename Callable>
  void ForEach (Callable call);

  template <typename Callable>
  void ForEach (Callable call) const;

  template<typename Callable>
  bool CallOn (const Key &key, Callable call);

  template<typename Callable>
  bool CallOn (const Key &key, Callable call) const;

  template<typename Callable>
  bool CallOnValue (const Key &key, Callable call);

  template<typename Callable>
  bool CallOnValue (const Key &vkey, Callable call) const;

  template<typename CallFound, typename CallLost>
  bool CallOnValue (const Key &key, CallFound found, CallLost lost);

  template<typename CallFound, typename CallLost>
  bool CallOnValue (const Key &key, CallFound found, CallLost lost) const;


  template<typename... Args>
  Value Locate (const Key &, Args... constructorArgs) const;
};

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::Payload::Payload (const Key &key_, const Val &val_) :
  key (key_),
  value (val_)
{
}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::TableNode::TableNode (const Key &key, const Val &val,
                                                 std::uint32_t hash) :
  next (nullptr),
  data (key, val),
  fullHash (hash)
{
}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::TableSlot::TableSlot() :
  first (nullptr)
{
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
const float HashTable<Key, Val, Hash>::GROWTH_FACTOR = 3.0;

template<typename Key, typename Val, typename Hash>
const float HashTable<Key, Val, Hash>::MAX_LOAD_FACTOR = 3.0;

template<typename Key, typename Val, typename Hash>
const unsigned HashTable<Key, Val, Hash>::SIZE_START = 13;

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::iterator::iterator() :
  m_table (nullptr),
  m_bucket (0),
  m_node (nullptr)
{
}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::iterator::iterator (HashTable *table) :
  m_table (table),
  m_bucket (0),
  m_node (table->m_buckets[0].first)
{
  if (m_table->m_nodeCount == 0)
  {
    m_node = nullptr;
    return;
  }
  while (m_node == nullptr)
  {
    ++m_bucket;
    m_node = table->m_buckets[m_bucket].first;
  }

  ERROR_IF(unsigned(m_node) == 0xfdfdfdfd, "Invalid iterator");
}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::iterator::iterator (HashTable *table, unsigned bucket, TableNode *node) :
  m_table (table),
  m_bucket (bucket),
  m_node (node)
{
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::iterator HashTable<Key, Val, Hash>::iterator::operator++ (int)
{
  iterator temp = *this;
  ++ (*this);
  return temp;
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::iterator &HashTable<Key, Val, Hash>::iterator::operator++ ()
{
  // Promote the next node
  m_node = m_node->next;

  if (m_node == nullptr)
  {
    do
    {
      // End of bucket, go to next one
      ++m_bucket;

      // If there are no more buckets, set node to 0
      if (m_bucket == m_table->m_bucketCount)
      {
        m_node = nullptr;
        return *this;
      }
    }
    while (m_table->m_buckets[m_bucket].first == nullptr);

    m_node = m_table->m_buckets[m_bucket].first;
  }

  ERROR_IF((unsigned int)m_node == 0xfdfdfdfd, "Bad iterator walk");
  return *this;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::Payload &HashTable<Key, Val, Hash>::iterator::operator*() const
{
  return m_node->data;
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::Payload *
HashTable<Key, Val, Hash>::iterator::operator->() const
{
  return &m_node->data;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
bool HashTable<Key, Val, Hash>::iterator::operator== (const iterator &it) const
{
  return m_node == it.m_node;
}

template<typename Key, typename Val, typename Hash>
bool HashTable<Key, Val, Hash>::iterator::operator!= (const iterator &it) const
{
  return m_node != it.m_node;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::const_iterator::const_iterator() :
  m_table (nullptr),
  m_bucket (0),
  m_node (nullptr)
{
}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::const_iterator::const_iterator (const HashTable *table) :
  m_table (table),
  m_bucket (0),
  m_node (m_table->m_buckets[0].first)
{
  if (m_table->m_nodeCount == 0)
  {
    m_node = nullptr;
    return;
  }
  while (m_node == nullptr)
  {
    ++m_bucket;
    m_node = table->m_buckets[m_bucket].first;
  }

}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::const_iterator::const_iterator (const HashTable *table, unsigned bucket,
                                                           TableNode *node) :
  m_table (table),
  m_bucket (bucket),
  m_node (node)
{
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::const_iterator
HashTable<Key, Val, Hash>::const_iterator::operator++
(int)
{
  iterator temp = *this;
  ++ (*this);
  return temp;
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::const_iterator
&HashTable<Key, Val, Hash>::const_iterator::operator++ ()
{
  // Promote the next node
  m_node = m_node->next;

  if (m_node == nullptr)
  {
    do
    {
      // End of bucket, go to next one
      ++m_bucket;

      // If there are no more buckets, set node to 0
      if (m_bucket == m_table->m_bucketCount)
      {
        m_node = nullptr;
        return *this;
      }
    }
    while (m_table->m_buckets[m_bucket].first == nullptr);

    m_node = m_table->m_buckets[m_bucket].first;
  }

  ERROR_IF((unsigned int)m_node == 0xfdfdfdfd, "Bad iterator walk");
  return *this;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
const typename HashTable<Key, Val, Hash>::Payload
&HashTable<Key, Val, Hash>::const_iterator::operator*() const
{
  return m_node->data;
}

template<typename Key, typename Val, typename Hash>
const typename HashTable<Key, Val, Hash>::Payload
*HashTable<Key, Val, Hash>::const_iterator::operator->() const
{
  return &m_node->data;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
bool HashTable<Key, Val, Hash>::const_iterator::operator== (const const_iterator &it) const
{
  return m_node == it.m_node;
}

template<typename Key, typename Val, typename Hash>
bool HashTable<Key, Val, Hash>::const_iterator::operator!= (const const_iterator &it) const
{
  return m_node != it.m_node;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
void HashTable<Key, Val, Hash>::GrowTable()
{
  unsigned newSize = unsigned (m_bucketCount * GROWTH_FACTOR);
  newSize = Utilities::GetClosestPrime (newSize);

  TableSlot *newTable = new TableSlot[newSize];

  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    while (m_buckets[i].first)
    {
      TableNode *moving = m_buckets[i].first;
      m_buckets[i].first = m_buckets[i].first->next;

      unsigned hash = moving->fullHash % newSize;
      moving->next = newTable[hash].first;
      newTable[hash].first = moving;
    }
  }

  delete[] m_buckets;

  m_buckets = newTable;
  m_bucketCount = newSize;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::HashTable (Hash hash) :
  m_buckets (new TableSlot[SIZE_START]),
  m_bucketCount (SIZE_START),
  m_nodeCount (0),
  m_hash (hash)
{
  m_allocator = Utilities::MemoryManagement::AllocatorManager::GiveAllocator (sizeof TableNode, 0);
}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::HashTable (const HashTable &table) :
  m_buckets (new TableSlot[table.m_bucketCount]),
  m_bucketCount (table.m_bucketCount),
  m_nodeCount (table.m_nodeCount),
  m_hash (table.m_hash)
{
  m_allocator = Utilities::MemoryManagement::AllocatorManager::GiveAllocator (sizeof TableNode, 0);

  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    TableNode **addTo = &m_buckets[i].first;

    for (TableNode *node = table.m_buckets[i].first; node; node = node->next)
    {
      void *mem = m_allocator->OA_ALLOCATE();

      *addTo = new (mem) TableNode (node->data.key, node->data.value, node->fullHash);
      addTo = & (*addTo)->next;
    }
  }
}

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash>::~HashTable()
{
  Clear();
  delete[] m_buckets;
  Utilities::MemoryManagement::AllocatorManager::ReleaseAllocator (m_allocator);
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
HashTable<Key, Val, Hash> &HashTable<Key, Val, Hash>::operator= (const HashTable &that)
{
  if (this != &that)
  {
    Clear();

    if (m_bucketCount != that.m_bucketCount)
    {
      delete[] m_buckets;
      m_buckets = new TableSlot[that.m_bucketCount];
      m_bucketCount = that.m_bucketCount;
    }

    that.ForEach ([this] (const Payload &data) {
      Insert (data.key, data.value);
    });

    m_nodeCount = that.m_nodeCount;
  }

  return *this;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
void HashTable<Key, Val, Hash>::Clear()
{
  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    while (m_buckets[i].first)
    {
      TableNode *destroying = m_buckets[i].first;
      m_buckets[i].first = destroying->next;

      destroying->~TableNode();
      m_allocator->OA_DEALLOCATE (destroying);
    }
  }

  m_nodeCount = 0;
}

template<typename Key, typename Val, typename Hash>
bool HashTable<Key, Val, Hash>::IsEmpty() const
{
  return m_nodeCount == 0;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
unsigned HashTable<Key, Val, Hash>::Size() const
{
  return m_nodeCount;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::iterator HashTable<Key, Val, Hash>::begin()
{
  return iterator (this);
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::iterator HashTable<Key, Val, Hash>::end()
{
  return iterator (this, 0, nullptr);
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::const_iterator HashTable<Key, Val, Hash>::cbegin() const
{
  return const_iterator (this);
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::const_iterator HashTable<Key, Val, Hash>::cend() const
{
  return const_iterator (this, 0, nullptr);
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::const_iterator HashTable<Key, Val, Hash>::begin() const
{
  return const_iterator (this);
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::const_iterator HashTable<Key, Val, Hash>::end() const
{
  return const_iterator (this, 0, nullptr);
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
Val &HashTable<Key, Val, Hash>::operator[] (const Key &key)
{
  std::uint32_t fullHash = m_hash (key);
  std::uint32_t hash = fullHash % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return node->data.value;
  }

  void *mem = m_allocator->OA_ALLOCATE();
  TableNode *node = new (mem) TableNode (key, Val(), fullHash);

  ++m_nodeCount;

  if ( (float (m_nodeCount)) / float (m_bucketCount) > MAX_LOAD_FACTOR)
  {
    GrowTable();
    hash = fullHash % m_bucketCount;
  }

  node->next = m_buckets[hash].first;
  m_buckets[hash].first = node;

  return node->data.value;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::iterator HashTable<Key, Val, Hash>::Find (const Key &key)
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return iterator (this, hash, node);
  }

  return end();
}

template<typename Key, typename Val, typename Hash>
typename HashTable<Key, Val, Hash>::const_iterator HashTable<Key, Val, Hash>::Find (
  const Key &key) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return const_iterator (this, hash, node);
  }

  return end();
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
bool HashTable<Key, Val, Hash>::Contains (const Key &key) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return true;
  }

  return false;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
std::pair<bool, typename HashTable<Key, Val, Hash>::iterator> HashTable<Key, Val, Hash>::Insert (
  const Key &key, const Val &val)
{
  std::uint32_t fullHash = m_hash (key);
  std::uint32_t hash = fullHash % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return std::make_pair (false, iterator (this, hash, node));
  }

  void *mem = m_allocator->OA_ALLOCATE();
  TableNode *node = new (mem) TableNode (key, val, fullHash);

  ++m_nodeCount;

  if ( (float (m_nodeCount)) / float (m_bucketCount) > MAX_LOAD_FACTOR)
  {
    GrowTable();
    hash = fullHash % m_bucketCount;
  }

  node->next = m_buckets[hash].first;
  m_buckets[hash].first = node;

  return std::make_pair (true, iterator (this, hash, node));
}

template<typename Key, typename Val, typename Hash>
bool HashTable<Key, Val, Hash>::Erase (const Key &key)
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode **node = &m_buckets[hash].first; *node; node = & (*node)->next)
  {
    if ( (*node)->data.key == key)
    {
      TableNode *remove = *node;
      *node = (*node)->next;

      remove->~TableNode();
      m_allocator->OA_DEALLOCATE (remove);

      --m_nodeCount;

      return true;
    }
  }

  return false;
}

// -----------------------------------------------------------------------------


template<typename Key, typename Val, typename Hash>
template<typename Callable>
Callable HashTable<Key, Val, Hash>::EraseIf (Callable call)
{
  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    for (TableNode **node = &m_buckets[i].first; node && *node; node = & (*node)->next)
    {
      Payload &payload = (*node)->data;

      if (call (payload))
      {
        TableNode *remove = *node;
        *node = (*node)->next;

        remove->~TableNode();
        m_allocator->OA_DEALLOCATE (remove);

        --m_nodeCount;
      }
    }
  }

  return call;
}

template<typename Key, typename Val, typename Hash>
template <typename Callable>
void HashTable<Key, Val, Hash>::ForEach (Callable call)
{
  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    for (TableNode *node = m_buckets[i].first; node; node = node->next)
      call (node->data);
  }
}

template<typename Key, typename Val, typename Hash>
template <typename Callable>
void HashTable<Key, Val, Hash>::ForEach (Callable call) const
{
  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    for (const TableNode *node = m_buckets[i].first; node; node = node->next)
      call (node->data);
  }
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
template<typename Callable>
bool HashTable<Key, Val, Hash>::CallOn (const Key &key, Callable call)
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
    {
      call (node->data);
      return true;
    }
  }

  return false;
}

template<typename Key, typename Val, typename Hash>
template<typename Callable>
bool HashTable<Key, Val, Hash>::CallOn (const Key &key, Callable call) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (const TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
    {
      call (node->data);
      return true;
    }
  }

  return false;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
template<typename Callable>
bool HashTable<Key, Val, Hash>::CallOnValue (const Key &key, Callable call)
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
    {
      call (node->data.value);
      return true;
    }
  }

  return false;
}

template<typename Key, typename Val, typename Hash>
template<typename Callable>
bool HashTable<Key, Val, Hash>::CallOnValue (const Key &key, Callable call) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (const TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
    {
      call (node->data.value);
      return true;
    }
  }

  return false;
}

template<typename Key, typename Val, typename Hash>
template<typename CallFound, typename CallLost>
bool HashTable<Key, Val, Hash>::CallOnValue (const Key &key, CallFound found, CallLost lost)
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
    {
      found (node->data.value);
      return true;
    }
  }

  lost();
  return false;
}

template<typename Key, typename Val, typename Hash>
template<typename CallFound, typename CallLost>
bool HashTable<Key, Val, Hash>::CallOnValue (const Key &key, CallFound found, CallLost lost) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (const TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
    {
      found (node->data.value);
      return true;
    }
  }

  lost();
  return false;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
template<typename... Args>
Val HashTable<Key, Val, Hash>::Locate (const Key &key, Args... constructorArgs) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (const TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return node->data.value;
  }

  return Val (constructorArgs...);
}

// -----------------------------------------------------------------------------

#endif
