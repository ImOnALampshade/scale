// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Hash function template definition
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef HASH_FUNCTION_H
#define HASH_FUNCTION_H

// -----------------------------------------------------------------------------

template<typename Key>
struct HashFunction
{
  typedef std::uint32_t (*type) (const Key &);
};

// -----------------------------------------------------------------------------

#endif