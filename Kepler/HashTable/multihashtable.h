// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Closed-chaining hash table implementation
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef MUTLIHASHTABLE_H
#define MUTLIHASHTABLE_H

#include <cstdint>
#include <utility>
#include "../ObjectAllocator/objectallocator.h"
#include "../AllocatorManager/allocatormanager.h"
#include "hashfn.h"
#include "primes.h"

// -----------------------------------------------------------------------------

template<typename Key, typename Value, typename HashFunctionType = HashFunction<Key>::type>
class MultiHashTable
{
public:
  struct Payload
  {
    Payload (const Key &key, const Value &val);

    Key   key;
    Value value;
  };

private:
  struct TableNode
  {
    TableNode (const Key &key, const Value &val, std::uint32_t hash);

    TableNode    *next;
    Payload       data;
    std::uint32_t fullHash;
  };

  struct TableSlot
  {
    TableSlot();

    TableNode *first;
  };

  TableSlot        *m_buckets;
  unsigned          m_bucketCount;
  unsigned          m_nodeCount;
  HashFunctionType  m_hash;
  ObjectAllocator  *m_allocator;

  static const float    GROWTH_FACTOR;
  static const float    MAX_LOAD_FACTOR;
  static const unsigned SIZE_START;

  void GrowTable();

public:

  class iterator
  {
  private:
    MultiHashTable *m_table;
    unsigned   m_bucket;
    TableNode *m_node;
  public:
    iterator();
    iterator (MultiHashTable *table);
    iterator (MultiHashTable *table, unsigned bucket, TableNode *node);

    iterator operator++ (int);
    iterator &operator++ ();

    Payload &operator*() const;
    Payload *operator->() const;

    bool operator== (const iterator &it) const;
    bool operator!= (const iterator &it) const;

    friend class MultiHashTable < Key, Value, HashFunctionType > ;
  };

  class const_iterator
  {
  private:
    MultiHashTable *m_table;
    unsigned   m_bucket;
    TableNode *m_node;
  public:
    const_iterator();
    const_iterator (MultiHashTable *table);
    const_iterator (MultiHashTable *table, unsigned bucket, TableNode *node);

    const_iterator operator++ (int);
    const_iterator &operator++ ();

    const Payload &operator*() const;
    const Payload *operator->() const;

    bool operator== (const const_iterator &it) const;
    bool operator!= (const const_iterator &it) const;
  };

  MultiHashTable (HashFunctionType hash);
  MultiHashTable (const MultiHashTable &table);
  ~MultiHashTable();

  MultiHashTable &operator= (const MultiHashTable &);

  void Clear();
  bool IsEmpty() const;

  unsigned Size() const;

  iterator begin();
  iterator end();
  const_iterator cbegin() const;
  const_iterator cend() const;
  const_iterator begin() const;
  const_iterator end() const;

  iterator Find (const Key &);
  const_iterator Find (const Key &) const;

  unsigned Contains (const Key &) const;

  std::pair<bool, iterator> Insert (const Key &, const Value &);
  unsigned Erase (const Key &key);
  void Erase (iterator it);
};

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::Payload::Payload (const Key &key_, const Val &val_) :
  key (key_),
  value (val_)
{
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::TableNode::TableNode (const Key &key, const Val &val,
                                                      std::uint32_t hash) :
  next (nullptr),
  data (key, val),
  fullHash (hash)
{
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::TableSlot::TableSlot() :
  first (nullptr)
{
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
const float MultiHashTable<Key, Val, Hash>::GROWTH_FACTOR = 3.0;

template<typename Key, typename Val, typename Hash>
const float MultiHashTable<Key, Val, Hash>::MAX_LOAD_FACTOR = 3.0;

template<typename Key, typename Val, typename Hash>
const unsigned MultiHashTable<Key, Val, Hash>::SIZE_START = 13;

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::iterator::iterator() :
  m_table (nullptr),
  m_bucket (0),
  m_node (nullptr)
{
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::iterator::iterator (MultiHashTable *table) :
  m_table (table),
  m_bucket (0),
  m_node (table->m_buckets[0].first)
{
  while (m_node == nullptr)
  {
    ++m_bucket;
    m_node = table->m_buckets[m_bucket].first;
  }
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::iterator::iterator (MultiHashTable *table, unsigned bucket,
                                                    TableNode *node) :
  m_table (table),
  m_bucket (bucket),
  m_node (node)
{
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::iterator
MultiHashTable<Key, Val, Hash>::iterator::operator++ (int)
{
  iterator temp = *this;
  ++ (*this);
  return temp;
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::iterator
&MultiHashTable<Key, Val, Hash>::iterator::operator++ ()
{
  // Promote the next node
  m_node = m_node->next;

  if (m_node == nullptr)
  {
    do
    {
      // End of bucket, go to next one
      ++m_bucket;

      // If there are no more buckets, set node to 0
      if (m_bucket == m_table->m_bucketCount)
      {
        m_node = nullptr;
        return *this;
      }
    }
    while (m_table->m_buckets[m_bucket].first == nullptr);

    m_node = m_table->m_buckets[m_bucket].first;
  }

  return *this;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::Payload
&MultiHashTable<Key, Val, Hash>::iterator::operator*() const
{
  return m_node->data;
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::Payload *
MultiHashTable<Key, Val, Hash>::iterator::operator->() const
{
  return &m_node->data;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
bool MultiHashTable<Key, Val, Hash>::iterator::operator== (const iterator &it) const
{
  return m_node == it.m_node;
}

template<typename Key, typename Val, typename Hash>
bool MultiHashTable<Key, Val, Hash>::iterator::operator!= (const iterator &it) const
{
  return m_node != it.m_node;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::const_iterator::const_iterator() :
  m_table (nullptr),
  m_bucket (0),
  m_node (nullptr)
{
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::const_iterator::const_iterator (MultiHashTable *table) :
  m_table (nullptr),
  m_bucket (0),
  m_node (m_table->m_buckets[0].first)
{
  while (m_node == nullptr)
  {
    ++m_bucket;
    m_node = table->m_buckets[m_bucket].first;
  }
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::const_iterator::const_iterator (MultiHashTable *table,
                                                                unsigned bucket,
                                                                TableNode *node) :
  m_table (table),
  m_bucket (bucket),
  m_node (node)
{
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::const_iterator
MultiHashTable<Key, Val, Hash>::const_iterator::operator++
(int)
{
  iterator temp = *this;
  ++ (*this);
  return temp;
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::const_iterator
&MultiHashTable<Key, Val, Hash>::const_iterator::operator++ ()
{
  // Promote the next node
  m_node = m_node->next;

  if (m_node == nullptr)
  {
    do
    {
      // End of bucket, go to next one
      ++m_bucket;

      // If there are no more buckets, set node to 0
      if (m_bucket == m_table->m_bucketCount)
      {
        m_node = nullptr;
        return *this;
      }
    }
    while (m_table->m_buckets[m_bucket].first == nullptr);

    m_node = m_table->m_buckets[m_bucket].first;
  }

  return *this;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
const typename MultiHashTable<Key, Val, Hash>::Payload
&MultiHashTable<Key, Val, Hash>::const_iterator::operator*() const
{
  return m_node->data;
}

template<typename Key, typename Val, typename Hash>
const typename MultiHashTable<Key, Val, Hash>::Payload
*MultiHashTable<Key, Val, Hash>::const_iterator::operator->() const
{
  return &m_node->data;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
bool MultiHashTable<Key, Val, Hash>::const_iterator::operator== (const const_iterator &it) const
{
  return m_node == it.m_node;
}

template<typename Key, typename Val, typename Hash>
bool MultiHashTable<Key, Val, Hash>::const_iterator::operator!= (const const_iterator &it) const
{
  return m_node != it.m_node;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
void MultiHashTable<Key, Val, Hash>::GrowTable()
{
  unsigned newSize = unsigned (m_bucketCount * GROWTH_FACTOR);
  newSize = Utilities::GetClosestPrime (newSize);

  TableSlot *newTable = new TableSlot[newSize];

  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    while (m_buckets[i].first)
    {
      TableNode *moving = m_buckets[i].first;
      m_buckets[i].first = m_buckets[i].first->next;

      unsigned hash = moving->fullHash % newSize;
      moving->next = newTable[i].first;
      newTable[i].first = moving;
    }
  }

  delete[] m_buckets;

  m_buckets = newTable;
  m_bucketCount = newSize;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::MultiHashTable (Hash hash) :
  m_buckets (new TableSlot[SIZE_START]),
  m_bucketCount (SIZE_START),
  m_nodeCount (0),
  m_hash (hash)
{
  m_allocator = Utilities::MemoryManagement::AllocatorManager::GiveAllocator (sizeof TableNode, 0);
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::MultiHashTable (const MultiHashTable &table) :
  m_buckets (new TableSlot[table.m_bucketCount]),
  m_bucketCount (table.m_bucketCount),
  m_nodeCount (table.m_nodeCount),
  m_hash (table.m_hash)
{
  m_allocator = Utilities::MemoryManagement::AllocatorManager::GiveAllocator (sizeof TableNode, 0);

  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    TableNode **addTo = &m_buckets[i].first;

    for (TableNode *node = table.m_buckets[i].first; node; node = node->next)
    {
      void *mem = m_allocator.OA_ALLOCATE();

      *addTo = new (mem) TableNode (node->data.key, node->data.value, node->fullHash);
      addTo = & (*addTo)->next;
    }
  }
}

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash>::~MultiHashTable()
{
  Clear();
  delete[] m_buckets;
  Utilities::MemoryManagement::AllocatorManager::ReleaseAllocator (m_allocator);
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
MultiHashTable<Key, Val, Hash> &MultiHashTable<Key, Val, Hash>::operator=
(const MultiHashTable &that)
{
  if (this != &that)
  {
    Clear();

    if (m_bucketCount != that.m_bucketCount)
    {
      delete[] m_buckets;
      m_buckets = new TableSlot[that.m_buckets];
      m_bucketCount = that.m_bucketCount;
    }

    for (unsigned i = 0; i < m_bucketCount; ++i)
    {
      TableNode **addTo = &m_buckets[i].first;

      for (TableNode *node = table.m_buckets[i].first; node; node = node->next)
      {
        void *mem = m_allocator.OA_ALLOCATE();

        *addTo = new (mem) TableNode (node->data.key, node->data.value, node->fullHash);
        addTo = & (*addTo)->next;
      }
    }
  }

  return *this;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
void MultiHashTable<Key, Val, Hash>::Clear()
{
  for (unsigned i = 0; i < m_bucketCount; ++i)
  {
    while (m_buckets[i].first)
    {
      TableNode *destroying = m_buckets[i].first;
      m_buckets[i].first = destroying->next;

      destroying->~TableNode();
      m_allocator->OA_DEALLOCATE (destroying);
    }
  }

  m_nodeCount = 0;
}

template<typename Key, typename Val, typename Hash>
bool MultiHashTable<Key, Val, Hash>::IsEmpty() const
{
  return m_nodeCount == 0;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
unsigned MultiHashTable<Key, Val, Hash>::Size() const
{
  return m_nodeCount;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::iterator MultiHashTable<Key, Val, Hash>::begin()
{
  return iterator (this);
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::iterator MultiHashTable<Key, Val, Hash>::end()
{
  return iterator (this, 0, nullptr);
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::const_iterator MultiHashTable<Key, Val, Hash>::cbegin()
const
{
  return const_iterator (this);
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::const_iterator MultiHashTable<Key, Val, Hash>::cend() const
{
  return const_iterator (this, 0, nullptr);
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::const_iterator MultiHashTable<Key, Val, Hash>::begin()
const
{
  return const_iterator (this);
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::const_iterator MultiHashTable<Key, Val, Hash>::end() const
{
  return const_iterator (this, 0, nullptr);
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::iterator MultiHashTable<Key, Val, Hash>::Find (
  const Key &key)
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return iterator (this, hash, node);
  }

  return end();
}

template<typename Key, typename Val, typename Hash>
typename MultiHashTable<Key, Val, Hash>::const_iterator MultiHashTable<Key, Val, Hash>::Find (
  const Key &key) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      return const_iterator (this, hash, node);
  }

  return end();
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
unsigned MultiHashTable<Key, Val, Hash>::Contains (const Key &key) const
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;
  unsigned total = 0;

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    if (node->data.key == key)
      ++total;
  }

  return total;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
std::pair<bool, typename MultiHashTable<Key, Val, Hash>::iterator>
MultiHashTable<Key, Val, Hash>::Insert (
  const Key &key, const Val &val)
{
  std::uint32_t fullHash = m_hash (key);
  std::uint32_t hash = fullHash % m_bucketCount;


  void *mem = m_allocator->OA_ALLOCATE();
  TableNode *newNode = new (mem) TableNode (key, val, fullHash);

  ++m_nodeCount;

  if ( (float (m_nodeCount)) / float (m_bucketCount) > MAX_LOAD_FACTOR)
  {
    GrowTable();
    hash = fullHash % m_bucketCount;
  }

  for (TableNode *node = m_buckets[hash].first; node; node = node->next)
  {
    // Put the new node after the first node with the same key
    if (node->data.key == key)
    {
      newNode->next = node->next;
      node->next = newNode;
      return std::make_pair (true, iterator (this, hash, newNode));
    }
  }

  // No node with the same key - add it to the front of the bucket
  newNode->next = m_buckets[hash].first;
  m_buckets[hash].first = newNode;
  return std::make_pair (true, iterator (this, hash, newNode));
}

template<typename Key, typename Val, typename Hash>
unsigned MultiHashTable<Key, Val, Hash>::Erase (const Key &key)
{
  std::uint32_t hash = m_hash (key) % m_bucketCount;
  unsigned total = 0;

  TableNode **node = &m_buckets[hash].first;

  while (*node)
  {
    if ( (*node)->data.key == key)
    {
      TableNode *remove = *node;
      *node = (*node)->next;

      remove->~TableNode();
      m_allocator->OA_DEALLOCATE (remove);

      --m_nodeCount;

      ++total;
    }

    else
      node = & (*node)->next;
  }

  return total;
}

// -----------------------------------------------------------------------------

template<typename Key, typename Val, typename Hash>
void MultiHashTable<Key, Val, Hash>::Erase (iterator it)
{
  if (m_buckets[it.m_bucket].first == it.m_node)
    m_buckets[it.m_bucket].first = it.m_node->next;

  else
  {
    for (TableNode *node = m_buckets[it.m_bucket].first; node; node = node->next)
    {
      if (node->next == it.m_node)
        node->next = it.m_node->next;
    }
  }

  it.m_node->~TableNode();
  m_allocator->OA_DEALLOCATE (it.m_node);
}

// -----------------------------------------------------------------------------

#endif
