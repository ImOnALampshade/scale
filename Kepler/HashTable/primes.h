// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Prime number generator for hashin functions to use
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PRIMES_H
#define PRIMES_H

namespace Utilities
{
  unsigned GetClosestPrime (unsigned value);
}

#endif
