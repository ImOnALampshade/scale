// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Kepler Engine core entry point
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_MAIN_H
#define KEPLER_MAIN_H

void KeplerInit();
void KeplerRun();

void KeplerUseAppDataDirectory();
void KeplerUseGameDirectory();

#endif
