// -----------------------------------------------------------------------------
//  Author: Howard Hughes, Reese Jones
//
//  Thread managerment system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef THREAD_MANAGER_COMPONENT_H
#define THREAD_MANAGER_COMPONENT_H

#include "component.h"
#include "semaphor.h"
#include "..\TypePuns\variant.h"
#include <future>
#include <atomic>
#include <condition_variable>
#include <thread>
#include <queue>


class ThreadManagerComponent : public Component
{
private:
  typedef std::function<void(void) > Task;
  typedef std::packaged_task<void(void) > PackagedTask;

public:
  ThreadManagerComponent();

  virtual void Initialize(const Json::Object &jsonData);
  virtual void Free();

  //void RemoveConsumer( unsigned int Count = 1 );
  void AddConsumer(unsigned int Count = 1);
  //add fire and forget job
  void AddJob(Task fn);

  //add job that you want an object back from.
  template<typename T>
  std::future<T> AddJobReturning(std::function<T(void) > fn)
  {
    //get the lock to the job buffer and lock everybody else out
    m_JobBufferLock.lock();

    //put function into packaged_task
    std::packaged_task<T(void) > newTask(fn);
    //get future from packaged task to give back to user...
    std::future<T> future(newTask.get_future());
    ReturningPackagedAdapter<T> pa(newTask);

    //actually put it into the task list.
    m_consumerQueue.emplace< Task >(Task(pa));

    m_TotalJobsReceived += 1; // count total jobs we have pushed onto the queue.

    //release lock on the job buffer so others can have it.
    m_JobBufferLock.unlock();

    //we unlock the job buffer first so that when we signal there is
    //items consumers will not still be waiting on the buffer to free up.

    //add one to available item count. (signaling that there is available
    //items to take)
    m_Items.release();

    //automatically release lock on job buffer. Others may use it now.

    return future;
  }

  COMPONENT_META

private:
  std::vector<std::thread> m_consumers;
  //lock for job buffer. Only one thread may use it at a time.
  std::mutex               m_JobBufferLock;
  //This semaphore sort of keeps track of how many items are
  //there for the taking. Consumers block when there
  //are no more items to take.
  Semaphore                m_Items;
  //Lock to add and remove consumers during runtime in many threads.
  std::mutex               m_ConsumerBufferLock;

  std::queue<Task> m_consumerQueue;
  std::atomic_bool       m_running;

  unsigned m_TotalJobsReceived;
  unsigned m_TotalJobsStarted;

  void Consumer();

  //------------------------------------------------------------------------------
  class PackagedAdapter
  {
  public:
    PackagedAdapter(PackagedTask& func);
    PackagedAdapter(PackagedAdapter const& other);
    void operator()(void);

  private:
    mutable PackagedTask m_Task;

  };

  //------------------------------------------------------------------------------
  template<typename ReturnType>
  class ReturningPackagedAdapter
  {
  public:
    ReturningPackagedAdapter(std::packaged_task<ReturnType(void)>& func)
    {
      m_Task = std::move(func);
    }

    ReturningPackagedAdapter(ReturningPackagedAdapter<ReturnType> const& other)
    {
      m_Task = std::move(other.m_Task);
    }

    void operator()(void)
    {
      m_Task();
    }

  private:
    mutable std::packaged_task<ReturnType(void)> m_Task;

  };

};

#endif
