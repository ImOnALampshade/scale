// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Kepler Engine core entry point
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../Serializer/jsonparser.h"
#include "compositefactory.h"

// Windows only. Need this for installer to work.
#include <ShlObj.h>
static char s_appData[MAX_PATH] ={ 0 };
static char s_gameDir[MAX_PATH] ={ 0 };

using namespace std;

void KeplerInit()
{
  CompositeManagement::Factory = new CompositeFactory;
}


void KeplerRun()
{
  ERROR_IF (!CoreEngine::Game.IsValid(), "Game was never created");
  CoreEngine::Game->Dispatch ("Start");

  while (CoreEngine::Running)
  {
    CoreEngine::Game->Dispatch ("Update");
    CompositeManagement::Factory->PurgeObjects();
  }

  CoreEngine::Game->Destroy();
  CompositeManagement::Factory->PurgeObjects();

  delete CompositeManagement::Factory;

  CompositeManagement::Factory = nullptr;
}

void KeplerUseAppDataDirectory()
{
  //CoreEngine::Game->Dispatch("Chdir_AppData");
  if (s_appData[0] == '\0')
  {
    int err;
    err = SHGetFolderPath
      (NULL, CSIDL_APPDATA | CSIDL_FLAG_CREATE, NULL, SHGFP_TYPE_CURRENT, s_appData);
    strcat(s_appData, "\\DigiPen");
    err = CreateDirectory(s_appData, NULL);
    strcat(s_appData, "\\Scale");
    err = CreateDirectory(s_appData, NULL);
  }
  SetCurrentDirectory(s_appData);
}

void KeplerUseGameDirectory()
{
  //CoreEngine::Game->Dispatch("Chdir_Game");
  if (s_gameDir[0] == '\0')
  {
    int length, left, right;
    length = GetModuleFileName(NULL, s_gameDir, FILENAME_MAX);
    for (right = 0; right < length - 1; ++right)
    {
      if (s_gameDir[right] == '\\')
        left = right;
    }
    s_gameDir[left] = '\0';
  }

  SetCurrentDirectory(s_gameDir);
}
