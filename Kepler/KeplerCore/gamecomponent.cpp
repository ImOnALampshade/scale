// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "gamecomponent.h"
#include "game.h"
#include "../HashFunctions/hashfunctions.h"

// -----------------------------------------------------------------------------

void GameComponent::Initialize (const Json::Object &jsonData)
{
  RegisterMessageProc ("Update", std::function<void() > (
  [this] () {
    std::vector<::Handle<Composite>> &children = Owner()->GetChildren();

    for (size_t i = 0; i < children.size(); ++i)
    {
      ::Handle<Composite> child = children[i];

      if (child.IsValid())
        child->Dispatch ("SpaceUpdate");
    }

    Owner()->Dispatch ("UIUpdate");
    Owner()->Dispatch ("GraphicsUpdate");
  }));
}

META_REGISTER_FUNCTION (Game)
{
  META_INHERIT (GameComponent, "Kepler", "Component");

  META_HANDLE (GameComponent);
  META_ADD_METHOD (GameComponent, ExitGame);

  META_FINALIZE_PTR (GameComponent, MetaPtr);
}

void GameComponent::ExitGame()
{
  CoreEngine::Running = false;
}

// -----------------------------------------------------------------------------
