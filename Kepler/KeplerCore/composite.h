// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Composite type
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef COMPOSITE_H
#define COMPOSITE_H

#include <functional>
#include <random>
#include <vector>
#include <stack>
#include "handle.h"
#include "../HashFunctions/hashfunctions.h"
#include "../HashTable/multihashtable.h"
#include "../Serializer/jsonparser.h"
#include "../Logger/logger.h"
#include "../MetaTyping/tuple.h"
#include "sharedmutex.h"

// -----------------------------------------------------------------------------

class Component;

class Composite
{
public:
  struct Archetype
  {
    String       TypeName;
    Json::Object JsonData;

    Archetype() : TypeName(), JsonData (Utilities::Hash::SuperFast) {}
  };

  enum ObserverMode
  {
    OBSERVER_PASS_ARGS,
    OBSERVER_PASS_TUPLE
  };

private:
  struct Observer
  {
    Variant<sizeof std::function<void() > > m_varFn;
    ObserverMode m_mode;

    #ifdef _DEBUG
    std::vector<const type_info *> m_types;
    #endif
  };

  struct RegisteredObserver
  {
    ::Handle<Composite> handle;
    String              name;
    std::uint32_t       id;
  };

  HandleInner<Composite>                  *m_handle;
  String                                   m_typeName;
  String                                   m_instanceName;
  std::uint64_t                            m_guid;
  ::Handle<Composite>                      m_space;
  ::Handle<Composite>                      m_owner;
  std::vector<Handle<Composite> >          m_children;
  HashTable<String, std::vector<Observer>> m_observers;
  std::vector<Component *>                 m_componentTable;
  const Json::Object                      *m_params;
  std::vector<RegisteredObserver>          m_registered;
  bool                                     m_initialized;
  bool                                     m_destroyed;

  static const float LOAD_FACTOR;
  static std::mt19937_64 m_random;

  static const StringHasher m_primaryHash;
  static const StringHasher m_secondaryHash;

  void InsertComponent (const String &name);

  // Private - only the factory can create new composites
  Composite (const Archetype &arch, Handle<Composite> owner);
  void InitializeComponents (const Archetype &arch);

  void SetDestroyedFlag();
  void RemoveChild (::Handle<Composite> toRemove);

public:
  ~Composite();

  Composite (const Composite &) = delete;
  Composite &operator= (const Composite &) = delete;

  ::Handle<Component> GetComponentUninitialized (String name);
  ::Handle<Component> GetComponent (String name);
  ::Handle<Composite> Handle();

  template<typename ComponentType>
  ::Handle<ComponentType> GetComponent();

  String TypeName();
  String InstanceName() const;
  void InstanceName (String value);

  template<typename... Args> void     Dispatch (String name, Args... payload);
  template<typename... Args> void DispatchDown (String name, Args... payload);
  template<typename... Args> void   DispatchUp (String name, Args... payload);

  void     DispatchTuple (String name, const Tuple &args);
  void DispatchDownTuple (String name, const Tuple &args);
  void   DispatchUpTuple (String name, const Tuple &args);

  template<typename... Args>
  std::uint32_t RegisterObserver (String name, std::function<void (Args...) > fn, ::Handle<Composite> context = nullptr, ObserverMode mode = OBSERVER_PASS_ARGS);
  void        UnregisterObserver (String name, std::uint32_t id);

  std::vector <Component *> &ComponentTable();

  void Destroy();

  std::uint64_t GetGuid();

  std::vector<::Handle<Composite>> &GetChildren();
  ::Handle<Composite> Parent();
  ::Handle<Composite> Space();

  bool IsAlive() const;

  friend class CompositeFactory;
};

// -----------------------------------------------------------------------------

template<typename ComponentType>
::Handle<ComponentType> Composite::GetComponent()
{
  return GetComponent (ComponentType::Name).as<ComponentType>();
}

// -----------------------------------------------------------------------------

#ifdef _DEBUG
namespace CompositeMessagingDebugging
{
  template <int N>
  bool CheckTypes (const std::vector<const type_info *> &types);
  template <int N,typename T>
  bool CheckTypes (const std::vector<const type_info *> &types, T arg);
  template <int N, typename T, typename... Args>
  bool CheckTypes (const std::vector<const type_info *> &types, T arg, Args... args);

  template <typename T1>
  bool CompareTypes (const type_info *T2)
  {
    // This means that a pointer type was passed in, and the function expects a
    // void *. Which is the old messaging system.
    if (std::is_pointer<T1>::value && typeid (void *) == *T2)
      return true;

    else
      return typeid (T1) == *T2;
  }

  template<int N>
  bool CheckTypes (const std::vector<const type_info *> &types)
  {
    return types.size() == 0 || (types.size() == 1 && *types[0] == typeid (void *));
  }

  template <int N, typename T>
  bool CheckTypes (const std::vector<const type_info *> &types, T arg)
  {
    return CompareTypes<T> (types[N]);
  }

  template<int N, typename T, typename... Args>
  bool CheckTypes (const std::vector<const type_info *> &types, T arg, Args... args)
  {
    if (!CompareTypes<T> (types[N]))
      return false;

    else
      return CheckTypes<N + 1> (types, args...);
  }

  template<typename...>
  struct TypeAdder;

  template<typename T, typename... Args>
  struct TypeAdder<T, Args...>
  {
    static void AddTypes (std::vector<const type_info *> &types);
  };

  template<>
  struct TypeAdder<>
  {
    static void AddTypes (std::vector<const type_info *> &types);
  };


  template<typename T, typename... Args>
  void TypeAdder<T, Args...>::AddTypes (std::vector<const type_info *> &types)
  {
    types.push_back (&typeid (T));

    TypeAdder<Args...>::AddTypes (types);
  }
}
#endif

// -----------------------------------------------------------------------------

template<typename... Args>
void Composite::Dispatch (String name, Args... payload)
{ 
  auto found = m_observers.Find (name);

  if (found == m_observers.end())
    return;

  std::vector<Observer> &observers = found->value;

  for (size_t i = 0; i < observers.size(); ++i)
  {
    Observer &observer = observers[i];

    // Skip unconstructed observers
    if (!observer.m_varFn.IsConstructed())
      continue;

    switch (observer.m_mode)
    {
    case OBSERVER_PASS_ARGS:
      {
        #ifdef _DEBUG
        using namespace CompositeMessagingDebugging;

        if (!CheckTypes<0> (observer.m_types, payload...))
        {
          BREAK (String::Format ("Invalid message parameters in message type %s in composite %s", name.c_str(), m_typeName.c_str()));
          continue;
        }

        #endif

        auto &fn = observer.m_varFn.as < std::function<void (Args...) > >();

        fn (payload...);
      }
      break;

    case OBSERVER_PASS_TUPLE:
      {
        auto &fn = observer.m_varFn.as < std::function<void (const Tuple &) > >();

        std::tuple<Args...> cppTuple (payload...);

        fn (FromStlTuple (cppTuple));
      }
      break;
    }
  }
}

template<typename... Args>
void Composite::DispatchDown (String name, Args... payload)
{
  Dispatch<Args...> (name, payload...);

  for (size_t i = 0; i < m_children.size(); ++i)
    if (m_children[i].IsValid())
      m_children[i]->DispatchDown (name, payload...);
}

template<typename... Args>
void Composite::DispatchUp (String name, Args... payload)
{
  Dispatch<Args...> (name, payload...);

  if (m_owner.IsValid())
    m_owner->DispatchUp (name, payload...);
}

// -----------------------------------------------------------------------------

template<typename... Args>
std::uint32_t Composite::RegisterObserver (String name, std::function<void (Args...) > fn, ::Handle<Composite> context, ObserverMode mode)
{
  Observer observer;
  observer.m_varFn = fn;
  observer.m_mode = mode;

  #ifdef _DEBUG
  using namespace CompositeMessagingDebugging;

  TypeAdder<Args...>::AddTypes (observer.m_types);
  #endif

  std::vector<Observer> &list = m_observers[name];

  if (list.empty())
    list.resize (32);

  std::uint32_t id = 0;

  // Search through the table looking for an empty slot.
  std::uint32_t i = 0;

  for (i = 0; i < list.size(); ++i)
    if (!list[i].m_varFn.IsConstructed())
    {
      list[i] = observer;
      id = i;
      break;
    }

  // If we passed the end of the table without finding a slot.
  if (i >= list.size())
  {
    list.push_back (observer);
    id = std::uint32_t (list.size() - 1);
  }

  if (context.IsValid())
  {
    RegisteredObserver registered;
    registered.handle = Handle();
    registered.id = id;
    registered.name = name;
    context->m_registered.push_back (registered);
  }

  return id;
}

// -----------------------------------------------------------------------------

#endif
