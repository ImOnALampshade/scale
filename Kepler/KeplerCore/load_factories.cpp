// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Composite factory intializer
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "load_factories.h"
#include "compositefactory.h"

#include "transformcomponent.h"
#include "objecttrackercomponent.h"
#include "gamecomponent.h"

void KeplerCore::LoadMeta()
{
  Component::CreateMeta();

  TransformComponent::CreateMeta();
  ObjectTrackerComponent::CreateMeta();
  GameComponent::CreateMeta();
}

void KeplerCore::LoadFactories()
{
  CREATE_FACTORY (Transform);
  CREATE_FACTORY (ObjectTracker);
  CREATE_FACTORY (Game);
}

