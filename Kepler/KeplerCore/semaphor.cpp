// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Semaphor implementaton
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "semaphor.h"

// -----------------------------------------------------------------------------

Semaphore::Semaphore() : m_lock(), m_condition(), m_count (0)
{
}

Semaphore::Semaphore(unsigned long InitialCount) : m_lock(), m_condition(),
   m_count(InitialCount)
{

}

// -----------------------------------------------------------------------------

void Semaphore::aquire()
{
  std::unique_lock<std::mutex> lck (m_lock);

  while (m_count == 0)
    m_condition.wait (lck);

  --m_count;
}

void Semaphore::release()
{
  std::lock_guard<std::mutex> lck (m_lock);
  ++m_count;
  m_condition.notify_one();
}

// -----------------------------------------------------------------------------
