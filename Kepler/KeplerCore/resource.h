// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Resource class for the resource manager to use
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef RESOURCE_H
#define RESOURCE_H

// -----------------------------------------------------------------------------

class BasicResource
{
public:
  virtual ~BasicResource() { }
};

// -----------------------------------------------------------------------------

#endif
