// -----------------------------------------------------------------------------
//  Author: Howard Hughes, Reese Jones
//
//  Thread managerment system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "threadmanagercomponent.h"

using namespace std;


// -----------------------------------------------------------------------------
ThreadManagerComponent::ThreadManagerComponent() : m_TotalJobsReceived (0),
  m_TotalJobsStarted (0), m_Items (0)
{

}

// -----------------------------------------------------------------------------

void ThreadManagerComponent::Consumer()
{
  while (m_running)
  {


    //wait until jobs available
    m_Items.aquire(); //decrements from available items.

    //aquire JobBuffer Lock and keep otheres out.
    m_JobBufferLock.lock();

    //when the thread manager shuts down, there is a good chance their is no jobs
    //left. That causes this thread to block. In order to shutdown this thread
    //the threadmanager must wake it up by signaling that there is items available
    //(even though there is no jobs)
    //how ever this thread will aquire the JobBufferLock and then check it
    //to find out there is no jobs. It will then continue, find out that m_running
    //is false, and then complete execution of the thread.
    if (m_consumerQueue.size() < 1)
    {
      m_JobBufferLock.unlock();
      continue;
    }

    //look at job next in queue
    Task fn = std::move (m_consumerQueue.front());
    //take job off the queue
    m_consumerQueue.pop();
    //count it as a job that we have started...
    m_TotalJobsStarted += 1;

    //release lock on job buffer so others may use it.
    m_JobBufferLock.unlock();

    //execute the job.
    fn();

    //packaged_task finished. should i let people know about this with a message?

  }
}

// -----------------------------------------------------------------------------

void ThreadManagerComponent::Initialize (const Json::Object &jsonData)
{
  jsonData.CallOn ("thread_count",
                   [this] (const Json::Object::Payload &data)
  {
    //get total threads in pool.
    unsigned threadCount = unsigned (data.value.as<Json::Number>());
    AddConsumer (threadCount);
  }
                  );

  RegisterMessageProc ("AddJob", function<void (Task) > (
                         bind (&ThreadManagerComponent::AddJob, this, placeholders::_1)));

}

void ThreadManagerComponent::Free()
{
  //turn off thread manager
  m_running = false;


  unsigned int consumersCount = m_consumers.size();

  //wake up all threads waiting on items(jobs).
  //there may not actually be any jobs, but theyll deal with it.
  for (unsigned int i = 0; i < consumersCount; ++i)
  {
    m_Items.release();
  }

  //rejoin with threads
  for (thread &t : m_consumers)
    t.join();
}

// -----------------------------------------------------------------------------

void ThreadManagerComponent::AddJob (Task fn)
{
  //get the lock to the job buffer and lock everybody else out
  m_JobBufferLock.lock();

  //put job into queueeuueueuueueueuueueueu (how do you spell queue?)
  m_consumerQueue.emplace (fn);

  //m_consumerQueue.emplace<std::function<void(void) > >( std::function<void(void)>(vc) );
  m_TotalJobsReceived += 1; // count total jobs we have pushed onto the queue.

  //release lock on the job buffer so others can have it.
  m_JobBufferLock.unlock();

  //we unlock the job buffer first so that when we signal there is
  //items consumers will not still be waiting on the buffer to free up.

  //add one to available item count. (signaling that there is available
  //items to take)
  m_Items.release();

  //automatically release lock on job buffer. Others may use it now.
}

//std::future< ThreadManagerComponent::ReturnValue > ThreadManagerComponent::AddJobReturning(ReturningTask fn)
//{
//  //get the lock to the job buffer and lock everybody else out
//  m_JobBufferLock.lock();
//
//  //put function into packaged_task
//  ReturningPackagedTask newTask(fn);
//  //get future from packaged task to give back to user...
//  std::future<ReturnValue> future( newTask.get_future() );
//  //put this in an adapater so we can through it into the task list
//  ReturningPackagedAdapter vc(newTask);
//  //actually put it into the task list.
//  m_consumerQueue.emplace< Task >( Task(vc) );
//
//  m_TotalJobsReceived += 1; // count total jobs we have pushed onto the queue.
//
//  //release lock on the job buffer so others can have it.
//  m_JobBufferLock.unlock();
//
//  //we unlock the job buffer first so that when we signal there is
//  //items consumers will not still be waiting on the buffer to free up.
//
//  //add one to available item count. (signaling that there is available
//  //items to take)
//  m_Items.release();
//
//  //automatically release lock on job buffer. Others may use it now.
//
//  return future;
//}

// -----------------------------------------------------------------------------
//Add consumers at runtime!
void ThreadManagerComponent::AddConsumer (unsigned int Count)
{
  //set the consumer lock so only this thread can conccurently access it.
  lock_guard<mutex> _ (m_ConsumerBufferLock);

  //add threads to thread pool.
  for (unsigned int i = 0; i < Count; ++i)
  {
    m_consumers.emplace_back (&ThreadManagerComponent::Consumer, this);
  }

//implicitly release consumer lock
}


META_REGISTER_FUNCTION (ThreadManager)
{
  META_INHERIT (ThreadManagerComponent, "Kepler", "Component");

  META_HANDLE (ThreadManagerComponent);

  META_ADD_METHOD (ThreadManagerComponent, AddJob);

  META_FINALIZE_PTR (ThreadManagerComponent, MetaPtr);
}

// -----------------------------------------------------------------------------
//This doesnt work unless we keep track of when each individual thread is
//supposed to shutdown. Right now we dont, but it could be added...
/*void ThreadManagerComponent::RemoveConsumer(unsigned int Count)
{
  //aquire the consumer buffer lock. Only this thread will be able to modify
  //the consumer array at this time.
  std::lock_guard<mutex> _(m_ConsumerBufferLock);

  //Will we ever want to remove all Consumers?(besides on clean up?)
  //anyway, this line will make people double check that. (enforces 1+ consumer)
  assert( Count < m_consumers.size() );

  //remove Count number of threads
  for(unsigned int i = 0; i < Count; i += 1)
  {
    //Join with thread at the end of the consumer vector.
    m_consumers[ m_consumers.size() - 1 ].join();
    //take them off the consumer vector
    m_consumers.pop_back();
  }

  //implicitly release consumer buffer lock
}*/
// -----------------------------------------------------------------------------



//------------------------------------------------------------------------------

ThreadManagerComponent::PackagedAdapter::PackagedAdapter (PackagedTask &func)
{
  //move packaged_task into void to variant functor
  m_Task = std::move (func);
}

ThreadManagerComponent::PackagedAdapter::PackagedAdapter (PackagedAdapter const &other)
{
  //the new copy takes the packaged_task
  m_Task = std::move (other.m_Task);
}


void ThreadManagerComponent::PackagedAdapter::operator() (void)
{
  //execute the task
  m_Task();
}

//------------------------------------------------------------------------------