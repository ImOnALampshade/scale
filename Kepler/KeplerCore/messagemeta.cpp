// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "messagemeta.h"

// -----------------------------------------------------------------------------

MessageMeta *MessageMeta::Get (String name)
{
  if (!m_messages.IsCreated())
    return nullptr;

  auto found = m_messages->Find (name);

  if (found != m_messages->end())
    return &found->value;

  else
    return nullptr;
}

// -----------------------------------------------------------------------------

void MessageMeta::Dispatch (Handle<Composite> composite, const Tuple &args) const
{
  m_dispatch (composite, args, m_name);
}

void MessageMeta::DispatchUp (Handle<Composite> composite, const Tuple &args) const
{
  m_dispatchUp (composite, args, m_name);
}

void MessageMeta::DispatchDown (Handle<Composite> composite, const Tuple &args) const
{
  m_dispatchDown (composite, args, m_name);
}

// -----------------------------------------------------------------------------

DelayedConstructor<MessageMeta::MessageTable> MessageMeta::m_messages;
