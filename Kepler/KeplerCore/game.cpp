// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  The global scoped game composite
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "game.h"

namespace CoreEngine
{
  bool              Running = true;
  Handle<Composite> Game;
}
