// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Header file with definitions for engine version numbers and crap like that
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_VERSION_INFO_H
#define KEPLER_VERSION_INFO_H

#ifdef _DEBUG
#define KEPLER_VERSION_NAME "0.1_dev DEBUG BUILD"
#else
#define KEPLER_VERSION_NAME "0.1_dev"
#endif

#define KEPLER_VERSION_NUM  1

#define KEPLER_NAME_OFFICIAL "Kepler Engine - " KEPLER_VERSION_NAME

#endif
