// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Until C++ 14, we have to make our own shared mutex :(
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------


// A NOTE TO ANYONE READING THIS
// This file is some black magic I wrote. I borrowed a few things from a book on
// posix threading. THIS IS REALLY HARD TO UNDERSTAND. DO NOT TRY TO WRAP YOUR
// HEAD AROUND THIS. Down that road lies madness. This could probably do with some
// commenting, but to be honest, I don't think I could explain the logic that is
// going on here with any words that exist in the english language.

// A NOTE TO ANYONE MAINTAINING THIS
// https://www.youtube.com/watch?v=_s7qgNMqDJI


#include "sharedmutex.h"
#include <thread>

using namespace std;


SharedMutex::SharedMutex() :
  m_lock(),
  m_gate1(),
  m_gate2(),
  m_state (0)
{
}

// -----------------------------------------------------------------------------

void SharedMutex::lock()
{
  unique_lock<mutex> lk (m_lock);

  while (m_state & WRITE_ENTERED)
    m_gate1.wait (lk);

  m_state |= WRITE_ENTERED;

  while (m_state & NO_READERS)
    m_gate2.wait (lk);
}

void SharedMutex::unlock()
{
  {
    lock_guard<mutex> lk (m_lock);
    m_state = 0;
  }
  m_gate1.notify_all();
}

// -----------------------------------------------------------------------------

void SharedMutex::shared_lock()
{
  unique_lock<mutex> lk (m_lock);

  while ( (m_state & WRITE_ENTERED) || (m_state & NO_READERS) == NO_READERS)
    m_gate1.wait (lk);

  unsigned numReaders = (m_state & NO_READERS) + 1;
  m_state &= ~NO_READERS;
  m_state |= numReaders;
}

void SharedMutex::shared_unlock()
{
  lock_guard<mutex> lk (m_lock);

  unsigned numReaders = (m_state & NO_READERS) - 1;
  m_state &= ~NO_READERS;
  m_state |= numReaders;

  if (m_state & WRITE_ENTERED)
  {
    if (numReaders == 0)
      m_gate2.notify_one();
  }

  else if (numReaders == NO_READERS - 1)
    m_gate1.notify_one();
}
