// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  A tracker for components and composites used by selectors
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "objecttrackercomponent.h"
#include "../HashFunctions/hashfunctions.h"
#include <algorithm> // https://i.chzbgr.com/maxW500/4209089792/hA74576A1/
#include <iostream>

using namespace std;
using Utilities::Hash::SuperFast;

// -----------------------------------------------------------------------------

void ObjectTrackerComponent::CleanTable (ComponentTable &table)
{
  table.EraseIf ([] (ComponentTable::Payload &payload) {
    return !payload.value.IsValid();
  });
}

void ObjectTrackerComponent::CleanTable (CompositeTable &table)
{
  table.EraseIf ([] (CompositeTable::Payload &payload) {
    return !payload.value.IsValid();
  });
}

template<typename T>
static bool ValidateHandle (Handle<T> handle)
{
  return handle.IsValid();
}

void ObjectTrackerComponent::CleanTable (ComponentListTable &table)
{
  table.ForEach ([] (ComponentListTable::Payload &payload) {
    auto &list = payload.value;
    list.erase (
      remove_if (list.begin(), list.end(), ValidateHandle<Component>),
      list.end()
    );
  });

  table.EraseIf ([] (const ComponentListTable::Payload &payload) {
    return payload.value.empty();
  });
}

void ObjectTrackerComponent::CleanTable (CompositeListTable &table)
{
  table.ForEach ([] (CompositeListTable::Payload &payload) {
    auto &list = payload.value;
    list.erase (
      remove_if (list.begin(), list.end(), ValidateHandle<Composite>),
      list.end()
    );
  });

  table.EraseIf ([] (const CompositeListTable::Payload &payload) {
    return payload.value.empty();
  });
}

void ObjectTrackerComponent::GarbageCollect()
{
  CleanTable (m_componentLists);
  CleanTable (m_compositeLists);
  CleanTable (m_groups);
  CleanTable (m_namedObjects);

  m_ticksToGC = GC_TICK_COUNT;
}

// -----------------------------------------------------------------------------

ObjectTrackerComponent::ObjectTrackerComponent() :
  m_componentLists (SuperFast),
  m_compositeLists (SuperFast),
  m_groups (SuperFast),
  m_namedObjects (SuperFast)
{
}

// -----------------------------------------------------------------------------

void ObjectTrackerComponent::Initialize (const Json::Object &jsonData)
{
  m_ticksToGC = GC_TICK_COUNT;

  RegisterMessageProc ("RegisterComponent", [this] (void *ptr) {
    RegisterComponent *reg = reinterpret_cast<RegisterComponent *> (ptr);
    String name = reg->component->GetName();
    m_componentLists[name].emplace_back (reg->component);
  });

  RegisterMessageProc ("RegisterComposite", [this] (void *ptr) {
    RegisterComposite *reg = reinterpret_cast<RegisterComposite *> (ptr);
    String name = reg->composite->TypeName();
    m_compositeLists[name].emplace_back (reg->composite);
  });

  RegisterMessageProc ("AddNamedObject", [this] (void *ptr) {
    AddNamedObject *obj = reinterpret_cast<AddNamedObject *> (ptr);
    m_namedObjects[obj->name] = obj->object;
  });

  RegisterMessageProc ("AddToGroup", [this] (void *ptr) {
    AddToGroup *obj = reinterpret_cast<AddToGroup *> (ptr);
    m_groups[obj->name].emplace_back (obj->object);
  });

  RegisterMessageProc ("PostSync", [this] (void *) {
    --m_ticksToGC;

    if (m_ticksToGC == 0)
      GarbageCollect();
  });
}

void ObjectTrackerComponent::Free()
{
}

// -----------------------------------------------------------------------------

const vector<Handle<Component>> *ObjectTrackerComponent::GetComponentList (String name)
{
  auto found = m_componentLists.Find (name);

  if (found == m_componentLists.end())
    return nullptr;

  else
    return &found->value;
}

const vector<Handle<Composite>> *ObjectTrackerComponent::GetCompositeList (String name)
{
  auto found = m_compositeLists.Find (name);

  if (found == m_compositeLists.end())
    return nullptr;

  else
    return &found->value;
}

const vector<Handle<Composite>> *ObjectTrackerComponent::GetGroup (String name)
{
  auto found = m_groups.Find (name);

  if (found == m_groups.end())
    return nullptr;

  else
    return &found->value;
}

Handle<Composite> ObjectTrackerComponent::GetNamedObject (String name)
{
  auto found = m_namedObjects.Find (name);

  if (found == m_namedObjects.end())
    return ::Handle<Composite>();

  else
    return found->value;
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (ObjectTracker)
{
}
