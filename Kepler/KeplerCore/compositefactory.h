// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Factory for creating components and composites
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef COMPOSITE_FACTORY_H
#define COMPOSITE_FACTORY_H

#include "../HashTable/hashtable.h"
#include "../ObjectAllocator/objectallocator.h"
#include "../Serializer/jsonparser.h"
#include "component.h"
#include "composite.h"
#include "game.h"
#include <map>
#include <queue>

// -----------------------------------------------------------------------------

class CompositeFactory
{
private:
  typedef std::queue<std::pair<Handle<Composite>, const Composite::Archetype *>> InitializerQueue;
  typedef std::vector<std::pair<Handle<Composite>, Composite::Archetype>> InitQueue_ByCopy;
  typedef std::vector< std::unique_ptr<Composite::Archetype> > ListofArchPtrs;

  HashTable<String, Component::Archetype>    m_componentArchetypes;
  HashTable<String, Composite::Archetype>    m_compositeArchetypes;
  std::queue<Handle<Composite>>              m_purgeQueue;
  std::map<std::uint64_t, Handle<Composite>> m_guids;
  ObjectAllocator                           *m_allocator;
  InitializerQueue                           m_initQueue;

  void DestroyObject (Handle<Composite> object);
  Handle<Composite> CreateCompositeUninitialized (String name, Handle<Composite> owner);

  void FillInMissingData(Json::Object& Instance,const Json::Object& archetype);
  Handle<Composite> CreateCompositeUninitialized(const Composite::Archetype& InstanceArch, Handle<Composite> owner, InitQueue_ByCopy& InitQueue, ListofArchPtrs& TempArchs);
  Handle<Composite> CreateCompositeUninitialized(String name, Handle<Composite> owner, InitQueue_ByCopy& InitQueue);

  Handle<Composite> CreateInstanceHelper(Json::List& InstanceList, Handle<Composite> Owner, InitQueue_ByCopy& InitQueue, ListofArchPtrs& TempArchs );
public:
  CompositeFactory ();
  CompositeFactory(const CompositeFactory &copyFrom) = delete;
  ~CompositeFactory();

  const Component::Archetype *ComponentArchetype (String name);
  const Composite::Archetype *CompositeArchetype (String name);

  void ComponentArchetype (Component::Archetype &arch);
  void CompositeArchetype (Composite::Archetype &arch);

  void LoadRecipeFile (String name);
  void PurgeObjects();

  Handle<Composite> CreateGame();
  Handle<Composite> CreateSpace (String archName);
  Handle<Composite> CreateComposite (String name, Handle<Composite> owner);

  Handle<Composite> CreateInstance(String instance_file, Handle<Composite> owner);

  Handle<Composite> GetObjectFromGuid (std::uint64_t guid);

  friend class Composite;
};

template<typename ComponentType>
void CreateComponentFactory (const char *name)
{
  Component::Archetype arch = Component::Archetype::Create<ComponentType> (name);
  CompositeManagement::Factory->ComponentArchetype (arch);
}

#define CREATE_FACTORY(type) CreateComponentFactory<type##Component>(#type);

// -----------------------------------------------------------------------------

namespace CompositeManagement
{
  extern CompositeFactory *Factory;
}

// -----------------------------------------------------------------------------

#endif
