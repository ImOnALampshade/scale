// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Structure for adding to run queue
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef RUN_QUEUE_STRUCT_H
#define RUN_QUEUE_STRUCT_H

#include "../String/string.h"
#include <functional>

struct RunOnThread
{
  std::function<void (void) > Runnable;
};

#endif
