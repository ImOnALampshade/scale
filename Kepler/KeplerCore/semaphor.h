// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Semaphor implementaton
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SEMAPHOR_H
#define SEMAPHOR_H

#include <condition_variable>
#include <mutex>

class Semaphore
{
private:
  std::mutex m_lock;
  std::condition_variable m_condition;
  unsigned long m_count;

public:
  Semaphore();
  Semaphore(unsigned long InitialCount );

  void aquire();
  void release();
};

#endif