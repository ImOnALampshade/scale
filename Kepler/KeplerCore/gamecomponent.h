// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef GAME_COMPONENT_H
#define GAME_COMPONENT_H

#include "component.h"
#include "sharedmutex.h"

// -----------------------------------------------------------------------------

class GameComponent : public Component
{
public:
  virtual void Initialize (const Json::Object &jsonData);

  void ExitGame();

  COMPONENT_META
};

// -----------------------------------------------------------------------------

#endif
