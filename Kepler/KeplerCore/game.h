// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  The global scoped game composite
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "composite.h"

namespace CoreEngine
{
  extern bool              Running;
  extern Handle<Composite> Game;
}
