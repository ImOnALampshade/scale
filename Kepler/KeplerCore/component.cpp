// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component type (Component based architecture duh)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "component.h"
#include "composite.h"
#include "../MetaTyping/bindhelpers.h"

// -----------------------------------------------------------------------------

void Component::RegisterMessageProc (String msgName, std::function<void (void *) >fn)
{
  m_owner->RegisterObserver (msgName, fn);
}

// -----------------------------------------------------------------------------

Component::Component () :
  m_initialized (false),
  m_owner(),
  m_arch (nullptr),
  m_handle (new HandleInner<Component> (this))
{
}

Component::~Component()
{
  m_handle->ptr = nullptr;

  --m_handle->refcount;

  if (m_handle->refcount == 0)
    delete m_handle;
}

// -----------------------------------------------------------------------------

Handle<Composite> Component::Owner() const
{
  return m_owner;
}

String Component::GetName()
{
  return m_arch->TypeName;
}

Handle<Component> Component::Handle()
{
  return m_handle;
}

MetaType *Component::Meta()
{
  return m_arch->Meta;
}

void Component::CreateMeta()
{
  META_CREATE (Component, "Kepler");
  META_HANDLE (Component);
  META_ADD_PROP_GET (Component, Owner);
  META_ADD_PROP_GET (Component, Space);
  META_FINALIZE (Component);
}

::Handle<Composite> Component::Space() const
{
  return Owner()->Space();
}


// -----------------------------------------------------------------------------
