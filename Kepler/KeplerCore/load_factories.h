// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Composite factory intializer
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef FACTORY_INIT_H
#define FACTORY_INIT_H

namespace KeplerCore
{
  void LoadMeta();
  void LoadFactories();
}

#endif
