// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Until C++ 14, we have to make our own shared mutex :(
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SHARED_MUTEX_H
#define SHARED_MUTEX_H

#include <mutex>
#include <condition_variable>

class SharedMutex
{
private:
  std::mutex              m_lock;
  std::condition_variable m_gate1;
  std::condition_variable m_gate2;
  unsigned int            m_state;

  static const unsigned WRITE_ENTERED = 1U << (sizeof (unsigned) * 8 - 1);
  static const unsigned NO_READERS    = ~WRITE_ENTERED;

public:

  SharedMutex();

  SharedMutex (const SharedMutex &) = delete;
  SharedMutex &operator= (const SharedMutex &) = delete;

  // Unique (write) ownership

  void lock();
  void unlock();

  // Shared (read) ownership

  void shared_lock();
  void shared_unlock();
};

// Version of std::lock_gaurd that calls shared_{lock,unlock} instead
template<typename MutType>
class SharedLockGaurd
{
private:
  MutType &m_lock;
public:
  SharedLockGaurd (MutType &lock) : m_lock (lock)
  {
    m_lock.shared_lock();
  }

  ~SharedLockGaurd()
  {
    m_lock.shared_unlock();
  }
};

#endif
