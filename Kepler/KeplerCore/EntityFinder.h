// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Helper methods for navigating from one composite to another by relative path
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../String/string.h"
#include "composite.h"

Handle<Composite> FindComposite(Handle<Composite> origin, String path);