// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef MESSAGE_META_H
#define MESSAGE_META_H

#include "../MetaTyping/tuple.h"
#include "../TypePuns/delayedconstructor.h"
#include "composite.h"

// -----------------------------------------------------------------------------

template<typename... Args>
struct InvokeMessageFromTuple
{
  template <unsigned N>
  struct ApplyFunc
  {
    template <typename... ArgsPack, typename... ArgsUnpack>
    static void ApplyTuple (Composite *composite,
                            void (Composite::*DispatchMode) (String, Args...),
                            String name,
                            const std::tuple<ArgsPack...> &t,
                            ArgsUnpack... unpacked)
    {
      return ApplyFunc<N - 1>::ApplyTuple (composite, DispatchMode, name, t, std::get<N - 1> (t), unpacked...);
    }
  };

  template<>
  struct ApplyFunc < 0 >
  {
    template <typename... ArgsPack, typename... ArgsUnpack>
    static void ApplyTuple (Composite *composite,
                            void (Composite::*DispatchMode) (String, Args...),
                            String name,
                            const std::tuple<ArgsPack...> &t,
                            ArgsUnpack... unpacked)
    {
      return (composite->*DispatchMode) (name, unpacked...);
    }
  };

  template<void (Composite::*DispatchMode) (String, Args...) >
  static void CallFunction (Composite *composite, String name, const std::tuple<Args...> &args)
  {
    ApplyFunc<sizeof... (Args) >::ApplyTuple (composite, DispatchMode, name, args);
  }
};

// -----------------------------------------------------------------------------

class MessageMeta
{
public:
  template<typename... Args>
  static void Dispatcher (Handle<Composite> composite, const Tuple &args, String name)
  {
    if (!composite.IsValid())
      return;

    std::tuple<Args...> cppArgs = ToStlTuple<Args...> (args);
    InvokeMessageFromTuple<Args...>::CallFunction<&Composite::Dispatch> (composite.GetInner()->ptr, name, cppArgs);
  }

  template<typename... Args>
  static void DispatcherUp (Handle<Composite> composite, const Tuple &args, String name)
  {
    if (!composite.IsValid())
      return;

    std::tuple<Args...> cppArgs = ToStlTuple<Args...> (args);
    InvokeMessageFromTuple<Args...>::CallFunction<&Composite::DispatchUp> (composite.GetInner()->ptr, name, cppArgs);
  }

  template<typename... Args>
  static void DispatcherDown (Handle<Composite> composite, const Tuple &args, String name)
  {
    if (!composite.IsValid())
      return;

    std::tuple<Args...> cppArgs = ToStlTuple<Args...> (args);
    InvokeMessageFromTuple<Args...>::CallFunction<&Composite::DispatchDown> (composite.GetInner()->ptr, name, cppArgs);
  }

  static MessageMeta *Get (String name);

  void     Dispatch (Handle<Composite> composite, const Tuple &args) const;
  void   DispatchUp (Handle<Composite> composite, const Tuple &args) const;
  void DispatchDown (Handle<Composite> composite, const Tuple &args) const;

  template<typename... Args>
  static void Create (String name)
  {
    MessageMeta meta;
    meta.m_dispatch = Dispatcher < Args... > ;
    meta.m_dispatchDown = DispatcherDown < Args... > ;
    meta.m_dispatchUp = DispatcherUp < Args ... > ;

    if (!m_messages.IsCreated())
      m_messages.Construct (Utilities::Hash::SuperFast);

    meta.m_name = name;
    m_messages->Insert (name, meta);
  }

private:
  void (*m_dispatch) (Handle<Composite> composite, const Tuple &args, String name);
  void (*m_dispatchDown) (Handle<Composite> composite, const Tuple &args, String name);
  void (*m_dispatchUp) (Handle<Composite> composite, const Tuple &args, String name);
  String m_name;

  typedef HashTable<String, MessageMeta> MessageTable;
  static DelayedConstructor<MessageTable> m_messages;
};

#define META_MESSAGE(name, ...) \
  do \
  {  \
    MessageMeta::Create<__VA_ARGS__>(name); \
  }  \
  while(false)

#endif
