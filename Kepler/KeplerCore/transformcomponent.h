// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Scale entry poin
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef TRANSFORM_COMPONENT_H
#define TRANSFORM_COMPONENT_H 

#include "../glminclude.h"
#include "component.h"

// -----------------------------------------------------------------------------

class TransformComponent : public Component
{
private:
  Vec3 m_pos;
  Vec3 m_scale;
  Quat m_rotation;
  bool m_inherits;
  glm::mat4 m_localTransform;
  glm::mat4 m_worldTransform;

  glm::mat4 getParentMatrix() const;
  glm::vec3 getParentScale() const;
  glm::vec3 getParentPosition() const;
  Quat getParentRotation() const;
  void computeLocalTransform();
public:
  TransformComponent();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  void Position (const Vec3 &);
  void    Scale (const Vec3 &);
  void Rotation (const Quat &);

  const Vec3 &Position() const;
  const Vec3    &Scale() const;
  const Quat &Rotation() const;

  void WorldPosition (const Vec3 &);
  void    WorldScale (const Vec3 &);
  void WorldRotation (const Quat &);

  Vec3 WorldPosition() const;
  Vec3    WorldScale() const;
  Quat WorldRotation() const;

  bool InheritsFromParent() const;
  void InheritsFromParent (bool value);

  glm::mat4 &GetMatrix();

  COMPONENT_META
};

// -----------------------------------------------------------------------------

#endif
