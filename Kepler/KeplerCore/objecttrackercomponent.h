// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  A tracker for components and composites used by selectors
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef COMPOSITE_TRACKER_H
#define COMPOSITE_TRACKER_H

#include <map>
#include "component.h"
#include "composite.h"
#include "../HashTable/hashtable.h"
#include "../HashFunctions/hashfunctions.h"
#include "../Serializer/jsonparser.h"

// -----------------------------------------------------------------------------

class ObjectTrackerComponent : public Component
{
private:
  typedef HashTable<String, ::Handle<Component>>              ComponentTable;
  typedef HashTable<String, ::Handle<Composite>>              CompositeTable;
  typedef HashTable<String, std::vector<::Handle<Component>>> ComponentListTable;
  typedef HashTable<String, std::vector<::Handle<Composite>>> CompositeListTable;

  ComponentListTable m_componentLists;
  CompositeListTable m_compositeLists;
  CompositeListTable m_groups;
  CompositeTable     m_namedObjects;
  unsigned int       m_ticksToGC;

  static const unsigned int GC_TICK_COUNT = 60;

  static void CleanTable (ComponentTable &table);
  static void CleanTable (CompositeTable &table);
  static void CleanTable (ComponentListTable &table);
  static void CleanTable (CompositeListTable &table);
  void GarbageCollect();

public:
  ObjectTrackerComponent();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  const std::vector<::Handle<Component>> *GetComponentList (String name);
  const std::vector<::Handle<Composite>> *GetCompositeList (String name);
  const std::vector<::Handle<Composite>> *GetGroup (String name);
  ::Handle<Composite>                     GetNamedObject (String name);

  COMPONENT_META
};

// -----------------------------------------------------------------------------

struct RegisterComponent { Handle<Component> component; };
struct RegisterComposite { Handle<Composite> composite; };

typedef struct
{
  String            name;
  Handle<Composite> object;
} AddNamedObject, AddToGroup;

// -----------------------------------------------------------------------------

#endif
