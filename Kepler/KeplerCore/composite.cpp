// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Composite type
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "component.h"
#include "composite.h"
#include "compositefactory.h"
#include "objecttrackercomponent.h"
#include "../Logger/logger.h"
#include "../HashTable/primes.h"
#include <algorithm>
#include <cassert>

// -----------------------------------------------------------------------------

using namespace std;

const float Composite::LOAD_FACTOR = 0.95f;
mt19937_64 Composite::m_random (47);

const StringHasher Composite::m_primaryHash = Utilities::Hash::SuperFast;
const StringHasher Composite::m_secondaryHash = Utilities::Hash::FNV1A;

#ifdef _DEBUG
void CompositeMessagingDebugging::TypeAdder<>::AddTypes (std::vector<const type_info *> &types)
{
}
#endif


// -----------------------------------------------------------------------------

void Composite::InsertComponent (const String &name)
{
  const Component::Archetype *arch = CompositeManagement::Factory->ComponentArchetype (name);
  BREAK_IF (!arch, String::Format ("Invalid archetype name: %s", name.CStr()));

  Component *component = arch->Constructor (malloc (arch->ComponentSize));
  component->m_owner    = Handle();
  component->m_arch     = arch;

  if (m_owner.IsValid())
  {
    RegisterComponent reg;
    reg.component = component->Handle();
    m_owner->DispatchUp ("RegisterComponent", &reg);
  }

  uint32_t hash1 = m_primaryHash (name) % m_componentTable.size();
  uint32_t hash = hash1;

  if (m_componentTable[hash])
  {
    uint32_t hash2 = m_secondaryHash (name) % (m_componentTable.size() - 1) + 1;

    for (;;)
    {
      hash = (hash + hash2) % m_componentTable.size();

      if (m_componentTable[hash] == nullptr)
      {
        m_componentTable[hash] = component;
        break;
      }

      else if (hash == hash1)
        abort();
    }
  }

  else
    m_componentTable[hash] = component;
}

// -----------------------------------------------------------------------------

Composite::Composite (const Archetype &arch, ::Handle<Composite> owner) :
  m_handle (new HandleInner<Composite> (this)),
  m_instanceName ("[Unnamed]"),
  m_typeName (arch.TypeName),
  m_guid (m_random()),
  m_space(),
  m_owner (owner),
  m_children (),
  m_observers (m_primaryHash),
  m_componentTable (max (2u, Utilities::GetClosestPrime
                         (unsigned (arch.JsonData.Locate ("Components").as<Json::Object>().Size() / LOAD_FACTOR) + 2))),
  m_params (&arch.JsonData),
  m_registered(),
  m_initialized (false),
  m_destroyed (false)
{
  // If the game has not been made yet, this must be the game
  if (!CoreEngine::Game.IsValid())
    CoreEngine::Game = Handle();

  CompositeManagement::Factory->m_guids[m_guid] = Handle();

  BREAK_IF (m_params == nullptr, "Cannot serialize an object from no data");

  m_instanceName = arch.JsonData.Locate ("Name", Json::String()).as<Json::String>();
  BREAK_IF(m_instanceName.CStr() == nullptr, "Created invalid string");

  // Insert components into the table
  arch.JsonData.Locate ("Components").as<Json::Object>().ForEach ([this] (const Json::Object::Payload &component) {
    InsertComponent (component.key);
  });

  if (m_owner.IsValid())
  {
    RegisterComposite reg;
    reg.composite = Handle();
    m_owner->DispatchUp ("RegisterComposite", &reg);
  }

  // Set space handle (Could just be a pointer to this object)
  for (::Handle<Composite> curr = Handle(); curr->Parent().IsValid(); curr = curr->Parent())
  {
    // If the owner of the current object is the game, then we found the space
    if (curr->Parent()->GetGuid() == CoreEngine::Game->GetGuid())
    {
      m_space = curr;
      break;
    }
  }

  // If this object has an owner, add this component to the owner's list of children
  if (m_owner.IsValid())
    m_owner->m_children.emplace_back (m_handle);
}

Composite::~Composite()
{
  // Erease our GUID from the factory's GUID map
  CompositeManagement::Factory->m_guids.erase (m_guid);

  // Free all the components
  for (Component *component : m_componentTable)
  {
    if (component)
      component->Free();
  }

  // Set the handle's pointer to null (No one can use it anymore) and
  // release our reference to it
  m_handle->ptr = nullptr;
  --m_handle->refcount;

  if (m_handle->refcount == 0)
    delete m_handle;

  // Call the destructor on the components
  for (Component *component : m_componentTable)
  {
    if (component)
    {
      component->~Component();
      free (component);
    }
  }

  for (RegisteredObserver &registered : m_registered)
  {
    if (registered.handle.IsValid())
      registered.handle->UnregisterObserver (registered.name, registered.id);
  }

  if (m_owner.IsValid() && m_owner->IsAlive())
  {
    m_owner->RemoveChild(this->Handle());
  }
}

// -----------------------------------------------------------------------------

::Handle<Component> Composite::GetComponentUninitialized (String name)
{
  // Calculate the first hash
  uint32_t hash1 = m_primaryHash (name) % m_componentTable.size();
  uint32_t hash = hash1;

  // If there isn't a component there, then that component isn't in the table
  if (m_componentTable[hash] == nullptr)
    return nullptr;

  // Otherwise, if the names are the same, then we found it
  if (m_componentTable[hash]->GetName() == name)
  {
    Component *comp = m_componentTable[hash];
    return comp->Handle();
  }

  // Still looking, run second hash
  uint32_t hash2 = m_secondaryHash (name) % (m_componentTable.size() - 1) + 1;

  do
  {
    hash = (hash + hash2) % m_componentTable.size();

    // If there isn't a component there, then again, not in the tale
    if (m_componentTable[hash] == nullptr)
      return nullptr;

    // If the names match, we found it
    if (m_componentTable[hash]->GetName() == name)
    {
      Component *comp = m_componentTable[hash];
      return comp->Handle();
    }
  }
  while (hash != hash1);

  // Once we're back to the first hash value, then everything has been checked
  // and its not here

  return nullptr;
}

// -----------------------------------------------------------------------------

::Handle<Component> Composite::GetComponent (String name)
{
  ::Handle<Component> comp = GetComponentUninitialized (name);

  if (comp.IsValid() && !comp->m_initialized)
  {
    m_params->Locate ("Components").as<Json::Object>().CallOnValue (name, [&comp] (const Json::Object &param) {
      comp->Initialize (param);
      comp->m_initialized = true;
    });
  }

  return comp;
}

// -----------------------------------------------------------------------------

Handle<Composite> Composite::Handle()
{
  return ::Handle<Composite> (m_handle);
}

// -----------------------------------------------------------------------------

String Composite::TypeName()
{
  return m_typeName;
}

// -----------------------------------------------------------------------------

String Composite::InstanceName() const
{
  return m_instanceName;
}

void Composite::InstanceName (String value)
{
  m_instanceName = value;
  BREAK_IF(m_instanceName.CStr() == nullptr, "Set instance name to invalid string.");
}

// -----------------------------------------------------------------------------

vector<Component *> &Composite::ComponentTable()
{
  return m_componentTable;
}

// -----------------------------------------------------------------------------

void Composite::Destroy()
{
  this->SetDestroyedFlag();
  CompositeManagement::Factory->m_purgeQueue.push(m_handle);
}

void Composite::SetDestroyedFlag()
{
  m_destroyed = true;
  for(::Handle<Composite> cmp : m_children)
    cmp->SetDestroyedFlag();
}

void Composite::RemoveChild(::Handle<Composite> toRemove)
{
  for (size_t i = 0; i < m_children.size(); ++i)
  {
    if (m_children[i] == toRemove)
    {
      m_children.erase(m_children.begin() + i);
      return;
    }
  }

  ERROR("Attempted to remove a nonexistant child.");
}

// -----------------------------------------------------------------------------

uint64_t Composite::GetGuid()
{
  return m_guid;
}

// -----------------------------------------------------------------------------

vector<::Handle<Composite>> &Composite::GetChildren()
{
  return m_children;
}

::Handle<Composite> Composite::Parent()
{
  return m_owner;
}

::Handle<Composite> Composite::Space()
{
  return m_space;
}

// -----------------------------------------------------------------------------

void Composite::DispatchTuple (String name, const Tuple &args)
{
  auto found = m_observers.Find (name);

  if (found == m_observers.end())
    return;

  for (size_t i = 0; i < found->value.size(); ++i)
  {
    Observer &observer = found->value[i];

    // If we can't pass a tuple to this observer, we can't call it like this
    if (observer.m_mode != OBSERVER_PASS_TUPLE)
      continue;

    // If the observer doesn't exist, skip it
    if (!observer.m_varFn.IsConstructed())
      continue;

    auto &fn = observer.m_varFn.as<function<void (const Tuple &) >>();

    fn (args);
  }
}

void Composite::DispatchDownTuple (String name, const Tuple &args)
{
  DispatchTuple (name, args);

  for (size_t i = 0; i < m_children.size(); ++i)
    if (m_children[i].IsValid()) m_children[i]->DispatchDownTuple (name, args);
}

void Composite::DispatchUpTuple (String name, const Tuple &args)
{
  DispatchTuple (name, args);

  if (m_owner.IsValid())
    m_owner->DispatchUpTuple (name, args);
}

// -----------------------------------------------------------------------------

void Composite::UnregisterObserver (String msgName, uint32_t id)
{
  auto found = m_observers.Find (msgName);

  if (found != m_observers.end())
  {
    found->value[id].m_varFn.Destroy();
    #ifdef _DEBUG
    found->value[id].m_types.clear();
    #endif
  }
}

void Composite::InitializeComponents (const Archetype &arch)
{
  BREAK_IF (m_initialized, "Composite being initialized twice.");
  m_initialized = true;

  // Initialize components
  for (Component *component : m_componentTable)
  {
    if (!component || component->m_initialized)
      continue;

    Json::Object param = arch.JsonData.Locate ("Components").as<Json::Object>().Locate (component->GetName());

    component->Initialize (param);
    component->m_initialized = true;
  }

  Dispatch ("Awake");

  m_params = nullptr;
}

bool Composite::IsAlive() const
{
  return !m_destroyed;
}

// -----------------------------------------------------------------------------
