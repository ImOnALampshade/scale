// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Factory for creating components and composites
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../AllocatorManager/allocatormanager.h"
#include "../Serializer/jsonparser.h"
#include "../Logger/logger.h"
#include "compositefactory.h"
#include <cassert>

// -----------------------------------------------------------------------------

using namespace Utilities::MemoryManagement;

// -----------------------------------------------------------------------------

CompositeFactory::CompositeFactory () :
  m_componentArchetypes (Utilities::Hash::SuperFast),
  m_compositeArchetypes (Utilities::Hash::SuperFast),
  m_purgeQueue(),
  m_guids(),
  m_allocator (AllocatorManager::GiveAllocator (sizeof Composite, 0)),
  m_initQueue()
{
  Composite::Archetype defaultArchetype;
  defaultArchetype.TypeName = "";
  CompositeArchetype (defaultArchetype);
}

CompositeFactory::~CompositeFactory()
{
  AllocatorManager::ReleaseAllocator (m_allocator);
}

// -----------------------------------------------------------------------------

const Component::Archetype *CompositeFactory::ComponentArchetype (String name)
{
  auto found = m_componentArchetypes.Find (name);

  if (found == m_componentArchetypes.end())
    return nullptr;

  else return &found->value;
}

const Composite::Archetype *CompositeFactory::CompositeArchetype (String name)
{
  auto found = m_compositeArchetypes.Find (name);

  if (found == m_compositeArchetypes.end())
    return nullptr;

  else return &found->value;
}

// -----------------------------------------------------------------------------

void CompositeFactory::ComponentArchetype (Component::Archetype &arch)
{
  std::pair<bool, HashTable<String, Component::Archetype>::iterator> result =
    m_componentArchetypes.Insert (arch.TypeName, arch);

  BREAK_IF (!result.first, String::Format ("Component archetype name already taken %s", arch.TypeName.CStr()));
}

void CompositeFactory::CompositeArchetype (Composite::Archetype &arch)
{
  std::pair<bool, HashTable<String, Composite::Archetype>::iterator> result =
    m_compositeArchetypes.Insert (arch.TypeName, arch);

  BREAK_IF (!result.first, String::Format ("Composite archetype name already taken %s", arch.TypeName.CStr()));
}

// -----------------------------------------------------------------------------

void CompositeFactory::LoadRecipeFile (String name)
{
  Serializer::Value v = Json::ReadFile (name);

  BREAK_IF (!v.IsConstructed(), "Could not load or parse recipe file " + name);
  BREAK_IF (!v.IsType<Json::Object>(), "Malformed recipe file (" + name + "), root value must be object");

  LOG (String::Format ("Loaded recipe file %s", name.CStr()));

  Json::Object &jsonData = v;

  jsonData.ForEach ([this] (const Json::Object::Payload &recipe) {
    Composite::Archetype arch;
    arch.TypeName = recipe.key;
    arch.JsonData = recipe.value;
    CompositeArchetype (arch);

    LOG (String::Format ("Loaded archetype %s", arch.TypeName.CStr()));
  });
}

// -----------------------------------------------------------------------------

void CompositeFactory::DestroyObject (Handle<Composite> object)
{
  if (!object.IsValid())
    return;

  // Destroy all the children of this object first
  for (Handle<Composite> child : object->GetChildren())
    DestroyObject (child);

  // Get a pointer to the actual memory before destroying the object
  void *actual_ptr = object.GetInner()->ptr;

  object->~Composite();

  m_allocator->OA_DEALLOCATE (actual_ptr);
}

void CompositeFactory::PurgeObjects()
{
  // Destroy each object in the queue
  while (!m_purgeQueue.empty())
  {
    DestroyObject (m_purgeQueue.front());
    m_purgeQueue.pop();
  }
}

// -----------------------------------------------------------------------------

Handle<Composite> CompositeFactory::CreateGame()
{
  CreateComposite ("__game__", nullptr);
  BREAK_IF (CoreEngine::Game == nullptr, "Game was not created.");
  return CoreEngine::Game;
}

Handle<Composite> CompositeFactory::CreateSpace (String archName)
{
  return CreateComposite (archName, CoreEngine::Game);
}

// -----------------------------------------------------------------------------

Handle<Composite> CompositeFactory::CreateComposite (String name,
                                                     Handle<Composite> owner)
{
  const Composite::Archetype *archetype = CompositeArchetype (name);
  InitQueue_ByCopy InitQ;
  ListofArchPtrs Stored_Archetypes;

  //calls recursive helper (not directly recursice, create uninitialized component has recursion
  Handle<Composite> composite = CreateCompositeUninitialized (*archetype, owner, InitQ, Stored_Archetypes);

  for (auto Iter : InitQ)
  {
    Iter.first->InitializeComponents (Iter.second);
  }

  for (auto Iter : InitQ)
  {
    Iter.first->Dispatch ("Awake");
  }

  return composite;
}

Handle<Composite> CompositeFactory::CreateCompositeUninitialized (String name, Handle<Composite> owner)
{
  const Composite::Archetype *archetype = CompositeArchetype (name);

  if (!archetype)
  {
    LOG ("Could not find archetype " + name);
    return Handle<Composite> (nullptr);
  }

  void *memory = m_allocator->OA_ALLOCATE();
  Composite *composite = new (memory) Composite (*archetype, owner);

  archetype->JsonData.CallOnValue ("Children", [this, composite] (const Json::Object &children) {
    children.ForEach ([this, composite] (const Json::Object::Payload &childInfo) {
      Handle<Composite> child = CreateCompositeUninitialized (childInfo.key, composite->Handle());
      child->InstanceName (childInfo.value.as<String>());
    });
  });

  LOG ("Created new " + composite->TypeName() +" composite");
  m_initQueue.push (std::make_pair (composite->Handle(), archetype));
  return composite->Handle();
}
//------------------------------------------------------------------------------

Handle<Composite> CompositeFactory::CreateCompositeUninitialized (const Composite::Archetype &InstanceArch, Handle<Composite> owner, InitQueue_ByCopy &InitQueue, ListofArchPtrs &TempArchs)
{
  void *memory = m_allocator->OA_ALLOCATE();
  Composite *composite = new (memory) Composite (InstanceArch, owner);

  //recursive call, base case, no children
  if (InstanceArch.JsonData.Contains ("Children"))
  {
    Json::List children = InstanceArch.JsonData.Find ("Children")->value.as<Json::List>();
    CreateInstanceHelper (children, composite->Handle(), InitQueue, TempArchs);
  }

  InitQueue.push_back (std::make_pair (composite->Handle(), InstanceArch));
  return composite->Handle();
}

Handle<Composite> CompositeFactory::CreateCompositeUninitialized (String name, Handle<Composite> owner, InitQueue_ByCopy &InitQueue)
{

  const Composite::Archetype *archetype = CompositeArchetype (name);

  if (!archetype)
  {
    LOG ("Could not find archetype " + name);
    return Handle<Composite> (nullptr);
  }

  void *memory = m_allocator->OA_ALLOCATE();
  Composite *composite = new (memory) Composite (*archetype, owner);

  archetype->JsonData.CallOnValue ("Children", [this, composite, &InitQueue] (const Json::Object &children) {
    children.ForEach ([this, composite, &InitQueue] (const Json::Object::Payload &childInfo) {
      Handle<Composite> child = CreateCompositeUninitialized (childInfo.key, composite->Handle(), InitQueue);
      child->InstanceName (childInfo.value.as<String>());
    });
  });

  LOG ("Created new " + composite->TypeName() + " composite");
  InitQueue.push_back (std::make_pair (composite->Handle(), *archetype));
  return composite->Handle();
}
Handle<Composite> CompositeFactory::CreateInstanceHelper (Json::List &InstanceList, Handle<Composite> Owner,
                                                          InitQueue_ByCopy &InitQueue, ListofArchPtrs &TempArchs)
{
  for (auto &recipe : InstanceList)
  {
    //retrieve name and object
    String name = "";


    Json::Object Instance = recipe;
    name = Instance.Locate ("Name", Json::String()).as<Json::String>();

    //retrieve archetype name
    const Composite::Archetype *archetype = nullptr;
    String archetypeName;
    auto archetypeIter = Instance.Find ("Archetype");

    if (archetypeIter == Instance.end())
    {
      //create custom object
      archetypeName = "";
      archetype = CompositeArchetype ("");
    }

    else
    {
      //load archetype
      archetypeName = String (archetypeIter->value);
      archetype = CompositeArchetype (archetypeName);

      if (!archetype)
      {
        LOG ("Could not find archetype " + archetypeName);
        return nullptr;
      }
    }


    FillInMissingData (Instance, archetype->JsonData);

    std::unique_ptr<Composite::Archetype> ConstructedArchetype = std::make_unique<Composite::Archetype>();
    ConstructedArchetype->JsonData = Instance;
    ConstructedArchetype->TypeName = archetypeName;

    //Create Composite
    Handle<Composite> composite = CreateCompositeUninitialized (*ConstructedArchetype, Owner, InitQueue, TempArchs);
    //Initialize Composite
    TempArchs.push_back (std::move (ConstructedArchetype));

    //need to add queue

    composite->InstanceName (name);
    //Give Composite name
  };

  return nullptr;
}
Handle<Composite> CompositeFactory::CreateInstance (String instance_file, Handle<Composite> owner)
{
  //load instance file into json object

  Serializer::Value v = Json::ReadFile (instance_file);
  BREAK_IF (!v.IsConstructed(), "Could not load or parse instnace file " + instance_file);
  BREAK_IF (!v.IsType<Json::List>(), "Malformed instance file (" + instance_file + "), root value must be list");
  LOG ("Loaded Instance file " + instance_file);
  Json::List jsonData = v;

  InitQueue_ByCopy InitQ;
  ListofArchPtrs Stored_Archetypes;

  //calls recursive helper (not directly recursice, create uninitialized component has recursion
  CreateInstanceHelper (jsonData, owner, InitQ, Stored_Archetypes);

  for (auto Iter : InitQ)
  {
    Iter.first->InitializeComponents (Iter.second);
  }

  for (auto Iter : InitQ)
  {
    Iter.first->Dispatch ("Awake");
  }

  return nullptr;
// for (int i = 0; i < Stored_Archetypes.size(); ++i)
//   Stored_Archetypes[i].

  //LOG(String::Format("Loaded archetype %s\n", arch.TypeName.CStr()));
}
//----------------------------------------------------------------------------
void CompositeFactory::FillInMissingData (Json::Object &Instance, const Json::Object &archetype)
{
  //if no components, grab the ones from the archetype
  if (!Instance.Contains ("Components") && !archetype.Contains ("Components"))
  {
    BREAK ("Either instance or archetype must have components.");
  }

  else if (archetype.Contains ("Components") && !Instance.Contains ("Components"))
  {
    Instance["Components"] = archetype.Find ("Components")->value;
  }

  else if (Instance.Contains ("Components") && archetype.Contains ("Components"))
  {

    Json::Object &InstanceComponents = Instance["Components"];

    const Json::Object &ArcheTypeComponents = archetype.Find ("Components")->value.as<Json::Object>();

    for (auto &archetypeComponent : ArcheTypeComponents)
    {
      //if a component doesn't exist in the insance, add it
      if (!InstanceComponents.Contains (archetypeComponent.key))
      {
        InstanceComponents[archetypeComponent.key] = archetypeComponent.value;
      }

      else
      {
        Json::Object InstComponentArgs = InstanceComponents[archetypeComponent.key];

        //if an arguement doesn't exist, add it
        //for (auto archCompArgs : archetypeComponent.value.as<Json::Object>())
        auto &range = archetypeComponent.value.as<Json::Object>();
        unsigned int loopcounter = 0;
        for (auto itr = range.begin(); itr!= range.end(); ++itr) 
        {
          auto &archCompArgs = *itr;
          if (!InstComponentArgs.Contains (archCompArgs.key))
          {
            InstComponentArgs[archCompArgs.key] = archCompArgs.value;
          }
          loopcounter++;
        }
      }
    }
  }

  if (archetype.Contains ("Children"))
  {
    //if no children, add them from archetype
    if (!Instance.Contains ("Children"))
    {
      if (archetype.Contains ("Children"))
        Instance["Children"] = archetype.Find ("Children")->value;
    }

    else
    {
      ////otherwise append to list
      Json::List &InstanceChildren = Instance["Children"];
      const Json::List &archetypeChildren = archetype.Find ("Children")->value.as<Json::List>();
      InstanceChildren.insert (InstanceChildren.begin(), archetypeChildren.begin(), archetypeChildren.end());
    }
  }
}

// -----------------------------------------------------------------------------

Handle<Composite> CompositeFactory::GetObjectFromGuid (std::uint64_t guid)
{
  auto found = m_guids.find (guid);
  return (found == m_guids.end()) ? Handle<Composite>() : found->second;
}

// -----------------------------------------------------------------------------

CompositeFactory *CompositeManagement::Factory;

// -----------------------------------------------------------------------------
