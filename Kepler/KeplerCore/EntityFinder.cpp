// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Helper methods for navigating from one composite to another by relative path
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "EntityFinder.h"
#include "../KeplerCore/game.h"

static void preprocess(String &path, std::vector<String> &tokens, char &origin);
static Handle<Composite> followToken(Handle<Composite> current, String token);

Handle<Composite> FindComposite(Handle<Composite> origin, String path)
{
  std::vector<String> tokens;
  char originType;
  preprocess(path, tokens, originType);

  switch (originType)
  {
  case '/':
    origin = CoreEngine::Game;
    break;
  case '~':
    origin = origin->Space();
    break;
  }

  Handle<Composite> current = origin;
  for (String token : tokens)
  {
    current = followToken(current, token);
  }

  return current;
}

static void preprocess(String &path, std::vector<String> &tokens, char &origin)
{
  size_t startIndex = 0;
  
  // If we start with a specifier, this is easy
  switch (path[0])
  {
  case '/':
  case '~':
    origin = path[0];
    if (path[1] != '/')
      startIndex = 1;
    else
      startIndex = 2;
    break;

    // . is it's own case because of ".."
  case '.':
    origin = '.';
    if (path[1] == '.')
    {
      // ".." is a relative path. We'll "solve" it by making it "./.." which is totally valid
      startIndex = 0;
    }
    else if(path[1] == '/')
    {
      startIndex = 2;
    }
    else
    {
      startIndex = 1;
    }
    break;

  default:
    // If they don't specify, we'll assume they want to start at the space.
    // For example, the path, "Player" is equivalent to "~/Player"
    origin = '~';
    startIndex = 0;
  }

  tokens = path.SubString(startIndex).Split('/');
}

Handle<Composite> followToken(Handle<Composite> current, String token)
{
  if (!current.IsValid())
    return current;

  if (token == "." || token == "")
    return current;

  if (token == "..")
    return current->Parent();

  for (Handle<Composite> child : current->GetChildren())
  {
    if (child->InstanceName() == token)
      return child;
  }

  return nullptr;
}
