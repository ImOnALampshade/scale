// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component type (Component based architecture duh)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef COMPONENT_H
#define COMPONENT_H

#include <functional>
#include <type_traits>
#include "composite.h"
#include "handle.h"
#include "messagemeta.h"
#include "../MetaTyping/metatype.h"
#include "../MetaTyping/bindhelpers.h"
#include "../Serializer/jsonparser.h"

// -----------------------------------------------------------------------------

class Composite;

class Component
{
public:
  struct Archetype
  {
    String       TypeName;
    std::size_t  ComponentSize;
    MetaType    *Meta;

    std::function<Component * (void *) > Constructor;

    template<typename T>
    static Archetype Create (const char *name);
  };

private:
  bool                      m_initialized;
  Handle<Composite>         m_owner;
  const Archetype          *m_arch;
  ::HandleInner<Component> *m_handle;

protected:
  template<typename... Args>
  void RegisterMessageProc (String msgName, std::function<void (Args...) > fn);

  void RegisterMessageProc (String msgName, std::function<void (void *) >fn);

public:

  Component ();
  virtual ~Component();

  virtual void Initialize (const Json::Object &jsonData) = 0;
  virtual void Free() { }

  ::Handle<Composite> Owner() const;
  ::Handle<Composite> Space() const;
  String GetName();
  ::Handle<Component> Handle();
  MetaType *Meta();

  friend class Composite;

  static void CreateMeta();
};

// -----------------------------------------------------------------------------

template<typename T>
Component::Archetype Component::Archetype::Create (const char *cname)
{
  static_assert (std::is_base_of<Component, T>::value,
                 "Cannot create a component archetype for a non-component type");

  String name = cname;

  Component::Archetype arch;
  arch.TypeName      = name;
  arch.ComponentSize = sizeof T;
  arch.Meta          = T::MetaPtr;

  arch.Constructor = [name] (void *mem) {
    T *component = new (mem) T();
    return component;
  };

  return arch;
}

template<typename... Args>
void Component::RegisterMessageProc (String msgName, std::function<void (Args...) > fn)
{
  m_owner->RegisterObserver<Args...> (msgName, fn);
}

#define COMPONENT_META \
  static const String Name; \
  static void CreateMeta(); \
  static MetaType *MetaPtr;

#define META_REGISTER_FUNCTION(name_) \
  const String name_##Component::Name (#name_); \
  MetaType *name_##Component::MetaPtr = nullptr; \
  void name_##Component::CreateMeta()

// -----------------------------------------------------------------------------

#endif
