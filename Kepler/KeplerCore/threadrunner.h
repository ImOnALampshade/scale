// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Convinience functions for running an arbitrary callable object on a given
//  thread
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "runqueuestruct.h"
#include "compositefactory.h"

#ifndef THREAD_RUNNER_H
#define THREAD_RUNNER_H

template<typename Callable>
void RunOnLogicThread (Callable fn)
{
  RunOnThread run;
  run.Runnable = fn;

  CoreEngine::Game->Dispatch (L"RunOnLogicThread", &run);
}

#endif