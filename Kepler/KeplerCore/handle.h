// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Easy-to-use handle thing
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef HANDLE_H
#define HANDLE_H

#include <atomic>
#include <new>

// -----------------------------------------------------------------------------

class Component;
class Composite;

template<typename HandleType>
struct HandleInner
{
  HandleType      *ptr;
  std::atomic_uint refcount;

  // Refcount is 2 becuase the object has a reference, as well as Python
  HandleInner (HandleType *ptr) : ptr (ptr) { refcount = 2; }
  ~HandleInner() { assert (refcount == 0); }
};

// -----------------------------------------------------------------------------

template<typename HandleType>
class Handle
{
private:
  HandleInner<HandleType> *m_inner;

  void Aquire()
  {
    if (m_inner)
      ++m_inner->refcount;
  }

  void Release()
  {
    if (!m_inner)
      return;

    --m_inner->refcount;

    if (m_inner->refcount == 0)
      delete m_inner;
  }
public:
  Handle (HandleInner<HandleType> *inner = nullptr) : m_inner (inner)
  {
    Aquire();
  }

  Handle (const Handle &handle) : m_inner (handle.m_inner)
  {
    Aquire();
  }

  Handle (Handle &&other) : m_inner (other.m_inner)
  {
    other.m_inner = nullptr;
  }

  ~Handle()
  {
    Release();
  }

  Handle &operator= (const Handle &handle)
  {
    if (this != &handle)
    {
      Release();
      m_inner = handle.m_inner;
      Aquire();
    }

    return *this;
  }

  Handle &operator= (Handle &&other)
  {
    if (this != &other)
    {
      Release();

      m_inner = other.m_inner;
      other.m_inner = nullptr;
    }

    return *this;
  }

  HandleType &operator*()
  {
    return *m_inner->ptr;
  }

  HandleType *operator->()
  {
    return m_inner->ptr;
  }

  bool IsValid()
  {
    return m_inner != nullptr && m_inner->ptr != nullptr;
  }

  HandleInner<HandleType> *GetInner()
  {
    return m_inner;
  }

  bool operator== (const Handle<HandleType> &handle) const
  {
    // Iff both handles point to the same inner pointer, they are the same handle
    return m_inner == handle.m_inner;
  }

  bool operator!= (const Handle<HandleType> &handle) const
  {
    return ! ( (*this) == handle);
  }
};

// -----------------------------------------------------------------------------

template<>
struct Handle<Component>
{
private:
  HandleInner<Component> *m_inner;

  void Aquire()
  {
    if (m_inner)
      ++m_inner->refcount;
  }

  void Release()
  {
    if (!m_inner)
      return;

    --m_inner->refcount;

    if (m_inner->refcount == 0)
      delete m_inner;
  }
public:
  Handle (HandleInner<Component> *inner = nullptr) : m_inner (inner)
  {
    Aquire();
  }

  Handle (const Handle &handle) : m_inner (handle.m_inner)
  {
    Aquire();
  }

  Handle (Handle &&other) : m_inner (other.m_inner)
  {
    other.m_inner = nullptr;
  }

  ~Handle()
  {
    Release();
  }

  Handle &operator= (const Handle &handle)
  {
    if (this != &handle)
    {
      Release();
      m_inner = handle.m_inner;
      Aquire();
    }

    return *this;
  }

  Handle &operator= (Handle &&other)
  {
    if (this != &other)
    {
      Release();

      m_inner = other.m_inner;
      other.m_inner = nullptr;
    }

    return *this;
  }

  Component &operator*()
  {
    return *m_inner->ptr;
  }

  Component *operator->()
  {
    return m_inner->ptr;
  }

  bool IsValid()
  {
    return m_inner != nullptr && m_inner->ptr != nullptr;
  }

  HandleInner<Component> *GetInner()
  {
    return m_inner;
  }

  bool operator== (const Handle<Component> &handle) const
  {
    // Iff both handles point to the same inner pointer, they are the same handle
    return m_inner == handle.m_inner;
  }

  bool operator!= (const Handle<Component> &handle) const
  {
    return ! ( (*this) == handle);
  }

  template<typename ComponentType>
  Handle<ComponentType> &as()
  {
    return *reinterpret_cast<Handle<ComponentType> *> (this);
  }
};

// -----------------------------------------------------------------------------

#endif