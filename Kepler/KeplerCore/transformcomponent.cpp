// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Scale entry poin
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "composite.h"
#include "transformcomponent.h"
#include "../OpenGLHelpers/glinclude.h"
#include "../MetaTyping/bindhelpers.h"

using namespace glm;



// -----------------------------------------------------------------------------

void TransformComponent::Initialize (const Json::Object &jsonData)
{
  bool found;

  found = jsonData.CallOnValue ("position", [this] (const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_pos[i] = float (l[i].as<Json::Number>());
  });

  if (!found)
    m_pos = vec3 (0);

  found = jsonData.CallOnValue ("scale", [this] (const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_scale[i] = float (l[i].as<Json::Number>());
  });

  if (!found)
    m_scale = vec3 (1);

  found = jsonData.CallOnValue ("rotation", [this] (const Json::List &l) {
    vec3 eulerAngle;

    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      eulerAngle[i] = float(l[i].as<Json::Number>()) * DEGREE;

    quat q1 = angleAxis (eulerAngle.x, vec3 (1, 0, 0));
    quat q2 = angleAxis (eulerAngle.y, vec3 (0, 1, 0));
    quat q3 = angleAxis (eulerAngle.z, vec3 (0, 0, 1));

    m_rotation = q1 * q2 * q3;
  });

  computeLocalTransform();
}

void TransformComponent::Free()
{
}

// -----------------------------------------------------------------------------

void TransformComponent::Position (const Vec3 &pos)
{
  static String msgName ("PositionChanged");

  m_pos = pos;
  computeLocalTransform();
  Owner()->Dispatch (msgName, m_pos);
}

void TransformComponent::Scale (const Vec3 &scale)
{
  static String msgName ("ScaleChanged");

  m_scale = scale;
  computeLocalTransform();
  Owner()->Dispatch (msgName, m_scale);
}

void TransformComponent::Rotation (const Quat &rotation)
{
  static String msgName ("RotationChanged");

  m_rotation = rotation;
  computeLocalTransform();
  Owner()->Dispatch (msgName, m_rotation);
}

void TransformComponent::WorldPosition (const Vec3 &value)
{
  glm::vec3 scale = getParentScale();
  scale = 1.0f / scale;
  Position (scale * value - getParentPosition());
}

void TransformComponent::WorldRotation (const Quat &value)
{
  Rotation (getParentRotation().inverted() * value);
}

// -----------------------------------------------------------------------------

const Vec3 &TransformComponent::Position() const
{
  return m_pos;
}

const Vec3 &TransformComponent::Scale() const
{
  return m_scale;
}

const Quat &TransformComponent::Rotation() const
{
  return m_rotation;
}

Vec3 TransformComponent::WorldPosition() const
{
  Vec4 homogeneous = glm::vec4 (m_pos, 1);
  homogeneous = getParentMatrix() * homogeneous;
  glm::vec3 temp = homogeneous.xyz;
  return temp;
}
Quat TransformComponent::WorldRotation() const
{
  return getParentRotation() * m_rotation;
}

// -----------------------------------------------------------------------------

void TransformComponent::computeLocalTransform()
{
  m_localTransform = mat4 (1);

  m_localTransform = translate (m_localTransform, m_pos);
  m_localTransform = m_localTransform * mat4_cast (m_rotation);
  m_localTransform = scale (m_localTransform, m_scale);
}

mat4 &TransformComponent::GetMatrix()
{
  if (m_inherits && Owner()->Parent().IsValid())
  {
    ::Handle<TransformComponent> parentTransform = Owner()->Parent()->GetComponent<TransformComponent>();

    if (parentTransform.IsValid())
    {
      m_worldTransform = parentTransform->GetMatrix() * m_localTransform;
      return m_worldTransform;
    }
  }

  return m_localTransform;
}

// -----------------------------------------------------------------------------

glm::vec3 TransformComponent::getParentScale() const
{
  if (m_inherits && Owner()->Parent().IsValid())
  {
    ::Handle<TransformComponent> parentTransform = Owner()->Parent()->GetComponent<TransformComponent>();

    if (parentTransform.IsValid())
    {
      return parentTransform->Scale() * parentTransform->getParentScale();
    }
  }

  return Vec3 (1, 1, 1);
}

glm::vec3 TransformComponent::getParentPosition() const
{
  if (m_inherits && Owner()->Parent().IsValid())
  {
    ::Handle<TransformComponent> parentTransform = Owner()->Parent()->GetComponent<TransformComponent>();

    if (parentTransform.IsValid())
    {
      return parentTransform->WorldPosition();
    }
  }

  return Vec3 (0, 0, 0);
}

Quat TransformComponent::getParentRotation() const
{
  if (m_inherits && Owner()->Parent().IsValid())
  {
    ::Handle<TransformComponent> parentTransform = Owner()->Parent()->GetComponent<TransformComponent>();

    if (parentTransform.IsValid())
    {
      return parentTransform->WorldRotation();
    }
  }

  return Quat();
}

glm::mat4 TransformComponent::getParentMatrix() const
{
  if (m_inherits && Owner()->Parent().IsValid())
  {
    ::Handle<TransformComponent> parentTransform = Owner()->Parent()->GetComponent<TransformComponent>();

    if (parentTransform.IsValid())
    {
      return parentTransform->GetMatrix();
    }
  }

  return glm::mat4 (1);
}

// -----------------------------------------------------------------------------

bool TransformComponent::InheritsFromParent() const
{
  return m_inherits;
}

void TransformComponent::InheritsFromParent (bool value)
{
  m_inherits = value;
  computeLocalTransform();
}

TransformComponent::TransformComponent()
  : m_pos(0, 0, 0), m_scale(1, 1, 1), m_rotation(), m_inherits(true)
{

}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (Transform)
{
  // Inherit from Component to get Parent and Space.
  // The second  argument is the module. This should be "Kepler" for all components.
  META_INHERIT (TransformComponent, "Kepler", "Component");

  // META_HANDLE means that we are binding a pointer to this object, not the object itself
  META_HANDLE (TransformComponent);

  // Properties can be bound with META_ADD_PROP
  META_ADD_PROP (TransformComponent, Position);
  META_ADD_PROP (TransformComponent, Scale);
  META_ADD_PROP (TransformComponent, Rotation);

  META_ADD_PROP (TransformComponent, WorldPosition);
  META_ADD_PROP (TransformComponent, WorldRotation);

  META_ADD_PROP (TransformComponent, InheritsFromParent);

  // Methods can be bound with META_ADD_METHOD
  // But in this case, we don't have any that we want to bind, since mat4 is not bound to python.
  //META_ADD_METHOD (TransformComponent, GetMatrix);

  // Call this to finalize the metatype for a component.
  META_FINALIZE_PTR (TransformComponent, MetaPtr);
}


// -----------------------------------------------------------------------------

