// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  An object that can hold any type of object that does NOT have to fit within
//  a fixed size (Unlike a normal variant)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef DYNAMIC_VARIANT_H
#define DYNAMIC_VARIANT_H

#include "dynamicvariantfunctions.h"
#include <new>
#include <typeinfo>
#include <functional>

// -----------------------------------------------------------------------------

class DynamicVariant
{
private:
  typedef void * (*ctor_type) (const void *);
  typedef void (*dtor_type) (void *);

  void *m_obj;
  const std::type_info *m_type;
  size_t m_size;

  ctor_type m_copy;
  dtor_type m_dtor;

public:
  template<typename T>
  static void *Copy (const void *src)
  {
    const T *obj = reinterpret_cast<const T *> (src);
    void *dest = malloc (sizeof T);
    return new (dest) T (*obj);
  }

  template<typename T>
  static void Destroy (void *obj)
  {
    reinterpret_cast<T *> (obj)->~T();
    free (obj);
  }

  DynamicVariant();
  DynamicVariant (const DynamicVariant &var);
  DynamicVariant &operator= (const DynamicVariant &var);

  template<typename T>
  DynamicVariant (const T &var);

  template<typename T>
  DynamicVariant &operator= (const T &var);

  ~DynamicVariant();

  template <typename T>
  T &as();

  template <typename T>
  const T &as() const;

  template<typename T>
  operator T &();

  template<typename T>
  operator const T &() const;

  template<typename T>
  bool IsType() const;

  bool IsConstructed() const;

  void AllocateSpace (size_t size, const std::type_info *typeInfo, ctor_type copy, dtor_type destroy);

  template<typename T, typename... Args>
  T &Construct (Args... args);

  void Destroy();

  void *Addr() const;

  const type_info *Type() const;

  friend struct MetaType;
};

// -----------------------------------------------------------------------------

template<typename T>
DynamicVariant::DynamicVariant (const T &var) :
  m_obj (new T (var)),
  m_type (&typeid (T)),
  m_copy (Copy<T>),
  m_dtor (Destroy<T>),
  m_size (sizeof (T))
{
}

template<typename T>
DynamicVariant &DynamicVariant::operator = (const T &var)
{
  if (m_dtor)
    m_dtor (m_obj);

  m_obj = malloc (sizeof (T));
  new (m_obj) T (var);
  m_type = &typeid (T);
  m_copy = Copy < T > ;
  m_dtor = Destroy < T > ;
  m_size = sizeof (T);

  return *this;
}

// -----------------------------------------------------------------------------

template <typename T>
T &DynamicVariant::as()
{
  return *reinterpret_cast<T *> (m_obj);
}

template <typename T>
const T &DynamicVariant::as() const
{
  return *reinterpret_cast<const T *> (m_obj);
}

template<typename T>
DynamicVariant::operator T &()
{
  return as<T>();
}

template<typename T>
DynamicVariant::operator const T &() const
{
  return as<T>();
}

template<typename T>
bool DynamicVariant::IsType() const
{
  return m_type && (*m_type == typeid (T));
}

// -----------------------------------------------------------------------------

template<typename T, typename... Args>
T &DynamicVariant::Construct (Args... args)
{
  if (m_dtor)
    m_dtor (m_obj);

  m_obj = malloc (sizeof (T));
  new (m_obj) T (args...);

  m_type = &typeid (T);
  m_copy = Copy < T > ;
  m_dtor = Destroy < T > ;
  m_size = sizeof (T);

  return as<T>();
}

// -----------------------------------------------------------------------------

#endif
