// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  An object that can hold any type of object that does NOT have to fit within
//  a fixed size (Unlike a normal variant)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "dynamicvariant.h"

// -----------------------------------------------------------------------------

DynamicVariant::DynamicVariant() :
  m_obj (nullptr),
  m_type (nullptr),
  m_copy (nullptr),
  m_dtor (nullptr),
  m_size (0)
{
}

DynamicVariant::DynamicVariant (const DynamicVariant &var) :
  m_obj(),
  m_type (var.m_type),
  m_copy (var.m_copy),
  m_dtor (var.m_dtor),
  m_size (var.m_size)
{
  if (m_copy)
    m_obj = m_copy (var.m_obj);
}

DynamicVariant &DynamicVariant::operator = (const DynamicVariant &var)
{
  if (this != &var)
  {
    if (m_dtor)
      m_dtor (m_obj);

    m_type = var.m_type;
    m_copy = var.m_copy;
    m_dtor = var.m_dtor;

    if (m_copy)
      m_obj = m_copy (var.m_obj);
  }

  return *this;
}

// -----------------------------------------------------------------------------

DynamicVariant::~DynamicVariant()
{
  if (m_dtor)
    m_dtor (m_obj);
}

bool DynamicVariant::IsConstructed() const
{
  return m_obj != nullptr;
}

void DynamicVariant::Destroy()
{
  if (m_dtor)
  {
    m_dtor (m_obj);

    m_obj = nullptr;
    m_type = nullptr;
    m_copy = nullptr;
    m_dtor = nullptr;
  }
}

// -----------------------------------------------------------------------------

void *DynamicVariant::Addr() const
{
  return m_obj;
}

// -----------------------------------------------------------------------------

const type_info *DynamicVariant::Type() const
{
  return m_type;
}

void DynamicVariant::AllocateSpace (size_t size, const std::type_info *typeInfo, ctor_type copy, dtor_type destroy)
{
  this->m_obj = malloc (size);
  this->m_type = typeInfo;
  this->m_size = size;
  this->m_copy = copy;
  this->m_dtor = destroy;
}
