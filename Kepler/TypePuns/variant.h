// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  An object that can hold any type of object that fits within a fixed size
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef VARIANT_H
#define VARIANT_H

#include <cstddef>
#include <exception>
#include <typeinfo>

// -----------------------------------------------------------------------------

template <size_t SIZE>
class Variant
{
private:
  unsigned char         m_var[SIZE];
  const std::type_info *m_type;

  void (*m_copy) (void *dest, const void *src);
  void (*m_dtor) (void *var);

  template<typename T>
  static void Copy (void *dest, const void *src_)
  {
    const T &src = *reinterpret_cast<const T *> (src_);
    new (dest) T (src);
  }

  template <typename T>
  static void Destroy (void *var)
  {
    reinterpret_cast<T *> (var)->~T();
  }

public:
  Variant();
  Variant (const Variant &var);
  Variant &operator= (const Variant &var);

  template<typename T>
  Variant (const T &var);

  template <typename T>
  Variant &operator= (const T &var);

  ~Variant();

  template <typename T>
  T &as();

  template <typename T>
  const T &as() const;

  template<typename T>
  operator T &();

  template<typename T>
  operator const T &() const;

  template<typename T>
  bool IsType () const;

  bool IsConstructed() const;

  template<typename T, typename... Args>
  T &Construct (Args... args);

  void Destroy();

  const type_info *Type() const;
};

// -----------------------------------------------------------------------------

template<size_t SIZE>
Variant<SIZE>::Variant() :
  m_type (nullptr),
  m_copy (nullptr),
  m_dtor (nullptr)
{
  for (size_t i = 0; i < SIZE; ++i)
    m_var[i] = 0;
}

template<size_t SIZE>
Variant<SIZE>::Variant (const Variant &var) :
  m_type (var.m_type),
  m_copy (var.m_copy),
  m_dtor (var.m_dtor)
{
  if (m_copy)
    m_copy (m_var, var.m_var);
}

template<size_t SIZE>
Variant<SIZE> &Variant<SIZE>::operator= (const Variant &var)
{
  if (&var != this)
  {
    Destroy();

    m_type = var.m_type;
    m_copy = var.m_copy;
    m_dtor = var.m_dtor;

    if (m_copy)
      m_copy (m_var, var.m_var);
  }

  return *this;
}

// -----------------------------------------------------------------------------

template<size_t SIZE>
template<typename T>
Variant<SIZE>::Variant (const T &var) :
  m_type (&typeid (T)),
  m_copy (Copy<T>),
  m_dtor (Destroy<T>)
{
  static_assert (SIZE >= sizeof T, "Variant is too small for value!");

  new (m_var) T (var);
}

template<size_t SIZE>
template <typename T>
Variant<SIZE> &Variant<SIZE>::operator= (const T &var)
{
  static_assert (SIZE >= sizeof T, "Variant is too small for value!");

  Destroy();

  m_type = &typeid (T);
  m_copy = Copy < T > ;
  m_dtor = Destroy < T > ;

  new (m_var) T (var);

  return *this;
}

// -----------------------------------------------------------------------------

template<size_t SIZE>
Variant<SIZE>::~Variant()
{
  Destroy();
}

// -----------------------------------------------------------------------------

template<size_t SIZE>
template <typename T>
T &Variant<SIZE>::as()
{
  return *reinterpret_cast<T *> (m_var);
}

template<size_t SIZE>
template <typename T>
const T &Variant<SIZE>::as() const
{
  return *reinterpret_cast<const T *> (m_var);
}

template<size_t SIZE>
template<typename T>
Variant<SIZE>::operator T &()
{
  return as<T>();
}

template<size_t SIZE>
template<typename T>
Variant<SIZE>::operator const T &() const
{
  return as<T>();
}

// -----------------------------------------------------------------------------

template<size_t SIZE>
template<typename T>
bool Variant<SIZE>::IsType() const
{
  return m_type ? typeid (T) == *m_type : false;
}

template<size_t SIZE>
bool Variant<SIZE>::IsConstructed() const
{
  return m_type != nullptr;
}

// -----------------------------------------------------------------------------

template<size_t SIZE>
template<typename T, typename... Args>
T &Variant<SIZE>::Construct (Args... args)
{
  static_assert (SIZE >= sizeof T, "Variant is too small for value!");
  Destroy();

  m_type = &typeid (T);
  m_copy = Copy < T > ;
  m_dtor = Destroy < T > ;

  new (m_var) T (args...);

  return as<T>();
}

template<size_t SIZE>
void Variant<SIZE>::Destroy()
{
  if (m_dtor)
  {
    m_dtor (m_var);
    m_type = nullptr;
    m_copy = nullptr;
    m_dtor = nullptr;
  }
}

// -----------------------------------------------------------------------------

template<size_t SIZE>
const type_info *Variant<SIZE>::Type() const
{
  return m_type;
}

// -----------------------------------------------------------------------------

#endif
