// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  An object that can hold any type of object that fits within a fixed size
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef DELAYED_CONSTRUCTOR_OBJECT_H
#define DELAYED_CONSTRUCTOR_OBJECT_H

// -----------------------------------------------------------------------------

template<typename T>
class DelayedConstructor
{
private:
  bool          m_created;
  unsigned char m_obj[sizeof T];
public:
  typedef T ObjectType;

  DelayedConstructor() :
    m_created (false)
  {
  }

  DelayedConstructor (const DelayedConstructor &that) :
    m_created (that.m_created),
    m_obj()
  {
    if (m_created)
      new (m_obj) T (*that);
  }

  ~DelayedConstructor()
  {
    Destroy();
  }

  DelayedConstructor &operator= (const DelayedConstructor &that)
  {
    if (this != &that)
    {
      Destroy();

      if (m_created = that.m_created)
        new (m_obj) T (*that);
    }

    return *this;
  }

  bool IsCreated()
  {
    return m_created;
  }

  template<typename... Args>
  T &Construct (Args... args)
  {
    Destroy();

    m_created = true;
    new (m_obj) T (args...);

    return *reinterpret_cast<T *> (m_obj);
  }

  void Destroy()
  {
    if (m_created)
      reinterpret_cast<T *> (m_obj)->~T();

    m_created = false;
  }

  operator T *() { return reinterpret_cast<T *> (m_obj); }
  T *operator->() { return reinterpret_cast<T *> (m_obj); }
  T &operator*() { return *reinterpret_cast<T *> (m_obj); }

  operator const T *() const { return reinterpret_cast<const T *> (m_obj); }
  const T *operator->() const { return reinterpret_cast<const T *> (m_obj); }
  const T &operator*() const { return *reinterpret_cast<const T *> (m_obj); }
};

// -----------------------------------------------------------------------------

#endif
