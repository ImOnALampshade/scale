//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <python\Python.h>
#include <iostream>

int main()
{
  Py_Initialize();

  PyRun_SimpleString ("print('Hello, world!')\n");

  Py_Finalize();

  std::cin.get();

  return 0;
}