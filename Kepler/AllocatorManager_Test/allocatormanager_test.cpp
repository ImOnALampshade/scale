// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  AllocatorManager Test Cases
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include <catch.hpp>
#include <cstdlib>
#include <cstdio>
#include <ctime>



#include "../AllocatorManager/allocatormanager.h"
#include "../ObjectAllocator/objectallocator.h"


using Utilities::MemoryManagement::AllocatorManager;

TEST_CASE("Allocator", "[Give]")
{

  SECTION("Test Single Allocator")
  {
    

    ObjectAllocator* myOA;

    void* val = nullptr;

    myOA = AllocatorManager::GiveAllocator( 32, 4 );
    CHECK( myOA != val);
    CHECK( myOA->Configuration().Alignment == 4 );
    CHECK( myOA->Configuration().ObjectSize == 32);

    AllocatorManager::ReleaseAllocator( myOA );
  
  }

  SECTION("Test Multiple Allocations, with Power Of 2")
  {

    ObjectAllocator* myOA;
    ObjectAllocator* myOA2;
    ObjectAllocator* myOA3;

    void* val = nullptr;

    myOA = AllocatorManager::GiveAllocator(32, 4);
    CHECK(myOA != val);
    CHECK(myOA->Configuration().Alignment == 4);
    CHECK(myOA->Configuration().ObjectSize == 32);
    myOA2 = AllocatorManager::GiveAllocator(32, 0);
    myOA3 = AllocatorManager::GiveAllocator(52, 0);

    AllocatorManager::ReleaseAllocator(myOA);
    AllocatorManager::ReleaseAllocator(myOA2);
    AllocatorManager::ReleaseAllocator(myOA3);

  }

  SECTION("Test Multiple Allocations, with threshold")
  {

    ObjectAllocator* myOA;
    ObjectAllocator* myOA2;
    ObjectAllocator* myOA3;

    void* val = nullptr;

    myOA = AllocatorManager::GiveAllocator(64, 4);
    CHECK(myOA != val);
    CHECK(myOA->Configuration().Alignment == 4);
    CHECK(myOA->Configuration().ObjectSize == 64);
    myOA2 = AllocatorManager::GiveAllocator( 32, 2 );
    myOA3 = AllocatorManager::GiveAllocator( 52, 4 ); 

    myOA3->OA_DEALLOCATE( myOA3->OA_ALLOCATE() );
    //myOA3->OA_ALLOCATE(); // uncommenting this causes an error when Releasing OA3
    //because of releasing OA that still has objects allocated out.

    AllocatorManager::ReleaseAllocator(myOA);
    AllocatorManager::ReleaseAllocator(myOA2);
    AllocatorManager::ReleaseAllocator(myOA3);

  }

  SECTION("Test Multiple Intializes and Cleanups")
  {
    unsigned testsToRun = 10;
    

    for(unsigned i = 0; i <  testsToRun; i += 1)
    {
      std::vector< void* > allocatedObjects;
      allocatedObjects.reserve( 8000 );

      ObjectAllocator* myOA;
      myOA = AllocatorManager::GiveAllocator(64, 4);

      for( unsigned j = 0; j < 8000; ++j )
      {
        allocatedObjects.push_back( myOA->OA_ALLOCATE() );
      }

      for (unsigned k = 0; k < allocatedObjects.size(); ++k)
      {
          myOA->OA_DEALLOCATE( allocatedObjects[k] );
      }

      
      AllocatorManager::ReleaseAllocator(myOA);

    }

  }


  SECTION("Try to free objects in wrong allocator")
  {

    ObjectAllocator* myOA;
    ObjectAllocator* myOA2;

    myOA = AllocatorManager::GiveAllocator(64, 4); //same size, but different alignment.
    myOA2 = AllocatorManager::GiveAllocator(64, 8);

    void* obj1 = myOA->OA_ALLOCATE();
    void* obj2 = myOA2->OA_ALLOCATE();

    //attempt to free objects from correct allocator.
    myOA->OA_DEALLOCATE( obj1 );
    myOA2->OA_DEALLOCATE( obj2 );

    AllocatorManager::ReleaseAllocator(myOA);
    AllocatorManager::ReleaseAllocator(myOA2);

  }

  std::printf("End of Test. Press Enter to Continue...\n");
  std::getchar();


}