//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#define CATCH_CONFIG_RUNNER
#include <catch.hpp>
#include "AllocatorManager\allocatormanager.h"

int main (int argc, char **const argv)
{
  using Utilities::MemoryManagement::AllocatorManager;
  AllocatorManager::Config config;
  config.ChooseMethod = AllocatorManager::Exact;
  config.DebugOn = true;
  config.PadBytes = 4;
  config.SizeAccuracy = 0.0f;

  AllocatorManager::Initialize(config);

  return Catch::Session().run (argc, argv);
}
