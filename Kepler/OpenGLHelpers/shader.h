// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Shader helper class
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SHADER_HELPER_H
#define SHADER_HELPER_H

#include "../String/string.h"
#include "../HashTable/hashtable.h"
#include "../glminclude.h"
#include "glinclude.h"

// -----------------------------------------------------------------------------

namespace OpenGL
{
  bool CompileShader (GLuint shader, String filename);
  bool LinkProgram (GLuint program);

  class Shader
  {
  public:
    struct UniformInfo
    {
      GLenum type;
      GLint  location;

      UniformInfo () : type (0), location (-1) { }
      UniformInfo (GLenum t, GLint l) : type (t), location (l) { }
    };

    struct BlockInfo
    {
      GLint index;

      BlockInfo () : index (GL_INVALID_INDEX) { }
      BlockInfo (GLint i) : index (i) { }
    };

    struct BufferInfo
    {
      GLint index;

      BufferInfo () : index (GL_INVALID_INDEX) { }
      BufferInfo (GLint i) : index (i) { }
    };

    struct OutputInfo
    {
      GLint index;

      OutputInfo () : index (-1) { }
      OutputInfo (GLint i) : index (i) { }
    };

    Shader ();
    Shader (unsigned program);

    Shader &operator= (const Shader &);
    Shader &operator= (unsigned program);

    UniformInfo Uniform (String name) const;
    BlockInfo     Block (String name) const;
    BufferInfo   Buffer (String name) const;
    OutputInfo   Output (String name) const;

    const HashTable<String, UniformInfo> &Uniforms() const;
    const HashTable<String, BlockInfo>     &Blocks() const;
    const HashTable<String, BufferInfo>   &Buffers() const;
    const HashTable<String, OutputInfo>   &Outputs() const;

    void SetUniform (String name, float);
    void SetUniform (String name, glm::vec2);
    void SetUniform (String name, glm::vec3);
    void SetUniform (String name, glm::vec4);

    void SetUniform (String name, double);
    void SetUniform (String name, glm::dvec2);
    void SetUniform (String name, glm::dvec3);
    void SetUniform (String name, glm::dvec4);

    void SetUniform (String name, int);
    void SetUniform (String name, glm::ivec2);
    void SetUniform (String name, glm::ivec3);
    void SetUniform (String name, glm::ivec4);

    void SetUniform (String name, unsigned);
    void SetUniform (String name, glm::uvec2);
    void SetUniform (String name, glm::uvec3);
    void SetUniform (String name, glm::uvec4);

    void SetUniform (String name, glm::mat2);
    void SetUniform (String name, glm::mat3);
    void SetUniform (String name, glm::mat4);

    void SetUniform (String name, glm::mat2x3);
    void SetUniform (String name, glm::mat2x4);
    void SetUniform (String name, glm::mat3x2);
    void SetUniform (String name, glm::mat3x4);
    void SetUniform (String name, glm::mat4x2);
    void SetUniform (String name, glm::mat4x3);

    void SetBlockBinding (String name, GLuint index);
    void SetBufferBinding (String name, GLuint index);

    unsigned GetProgram() const;
  private:

    void FindUniforms();
    void FindBlocks();
    void FindBuffers();
    void FindOuputs();

    GLuint                              m_program;
    HashTable<String, UniformInfo> m_uniforms;
    HashTable<String, BlockInfo>   m_blocks;
    HashTable<String, BufferInfo>  m_buffers;
    HashTable<String, OutputInfo>  m_outputs;
  };

}

// -----------------------------------------------------------------------------

#endif
