// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Texture helper class
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../Logger/logger.h"
#include "../kto/header.h"
#include "texture.h"
#include "glinclude.h"
#include <cstdint>
#include <memory>

using namespace std;

// -----------------------------------------------------------------------------

namespace OpenGL
{
  // ---------------------------------------------------------------------------

  template<typename HeaderCallback, typename MipCallback>
  bool LoadKTO (String filename, HeaderCallback headerCall, MipCallback mipCall)
  {
    FILE *file = fopen (filename.c_str(), "rb");

    if (!file)
      return false;

    Kto::Header header;

    fread (&header, sizeof header, 1, file);

    headerCall (header);

    for (unsigned i = 0; i < header.mipmaps; ++i)
    {
      uint32_t size[2];
      fread (size, sizeof uint32_t, 2, file);

      auto data = make_unique<uint8_t[]> (size[0] * size[1] * 4);
      fread (data.get(), 4, size[0] * size[1], file);

      mipCall (size[0], size[1], i, data.get());
    }

    fclose (file);
    return true;
  }

  // ---------------------------------------------------------------------------

  bool LoadTextureArray (std::vector<String> filenames, GLenum format, GLenum target)
  {
    bool loaded;


    loaded = LoadKTO (filenames[0],
    [&] (Kto::Header &header) {
      // Create the storage for the texture array
      // Assume all textures in the array have the same of the following parameters as the first one:
      // - Mipmaps
      // - Width
      // - Height
      // Depth is the number of textures we are loading (Size of the array)
      glTexStorage3D (target, header.mipmaps, format, header.width, header.height, filenames.size());
    },
    [&] (uint32_t w, uint32_t h, unsigned level, uint8_t *pixels) {
      // Load the first texture
      glTexSubImage3D (target,
                       level,   // Whatever level is given
                       0, 0, 0, // The first texel of the first image in the array
                       w, h, 1, // dimmensions were passed in, depth is 1 texel (Flat image)
                       GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    });

    if (!loaded)
      return false;

    for (size_t i = 1; i < filenames.size(); ++i)
    {
      loaded = LoadKTO (filenames[i],
      [] (Kto::Header &header) { },
      [&] (uint32_t w, uint32_t h, unsigned level, uint8_t *pixels) {
        glTexSubImage3D (target,
                         level,   // Whatever level is given
                         0, 0, i, // The first texel of the ith image in the array
                         w, h, 1, // The dimmensions were passed in, depth is 1 texel (Flat image)
                         GL_RGBA, GL_UNSIGNED_BYTE, pixels);
      });

      if (!loaded)
        return false;
    }

    return true;
  }

  // ---------------------------------------------------------------------------

  bool LoadSingleTextureSub3D (String filename, GLuint z, GLenum target)
  {
    return LoadKTO (filename,
    [] (Kto::Header &) {},
    [&] (uint32_t w, uint32_t h, unsigned level, uint8_t *pixels) {
      glTexSubImage3D (target, level, 0, 0, z, w, h, 1, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    });
  }

  // ---------------------------------------------------------------------------
}

// -----------------------------------------------------------------------------
