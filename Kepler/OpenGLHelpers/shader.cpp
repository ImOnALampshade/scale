// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Shader helper class
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "shader.h"
#include "glinclude.h"
#include "../HashFunctions/hashfunctions.h"
#include <iostream>
#include <memory>

using namespace std;
using namespace glm;

// -------------------------------------------------------------------------------

namespace OpenGL
{
  // -----------------------------------------------------------------------------

  bool OpenGL::CompileShader (GLuint shader, String filename)
  {
    String source = String::LoadFile (filename);

    if (source.Empty())
      return false;

    const char *csrc = source.CStr();

    glShaderSource (shader, 1, &csrc, nullptr);
    glCompileShader (shader);

    int compiled;
    glGetShaderiv (shader, GL_COMPILE_STATUS, &compiled);

    if (compiled == GL_FALSE)
    {
      int logsize;
      glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &logsize);
      unique_ptr<char> log (new char[logsize]);

      glGetShaderInfoLog (shader, logsize, &logsize, log.get());

      fprintf (stderr, "Error compiling shader %s:\n%s", filename.CStr(), log.get());
      return false;
    }

    else
      return true;
  }

  bool OpenGL::LinkProgram (GLuint program)
  {
    int linked;

    glLinkProgram (program);
    glGetObjectParameterivARB (program, GL_LINK_STATUS, &linked);

    if (linked == GL_FALSE)
    {
      int logsize;
      glGetProgramiv (program, GL_INFO_LOG_LENGTH, &logsize);
      unique_ptr<char> log (new char[logsize]);

      glGetProgramInfoLog (program, logsize, &logsize, log.get());

      fprintf (stderr, "Error linking shaders:\n%s\n", log.get());
      return false;
    }

    else
      return true;
  }

  // -----------------------------------------------------------------------------

  using Utilities::Hash::SuperFast;

  Shader::Shader() :
    m_program (0),
    m_uniforms (SuperFast),
    m_blocks (SuperFast),
    m_buffers (SuperFast),
    m_outputs (SuperFast)
  {
  }

  Shader::Shader (unsigned program) :
    m_program (program),
    m_uniforms (SuperFast),
    m_blocks (SuperFast),
    m_buffers (SuperFast),
    m_outputs (SuperFast)
  {
    FindUniforms();
    FindBlocks();
    FindBuffers();
    FindOuputs();
  }

  Shader &Shader::operator= (const Shader &shader)
  {
    if (this != &shader)
    {
      m_program = shader.m_program;

      m_uniforms.Clear();
      m_blocks.Clear();
      m_buffers.Clear();
      m_outputs.Clear();

      FindUniforms();
      FindBlocks();
      FindBuffers();
      FindOuputs();
    }

    return *this;
  }

  Shader &Shader::operator= (unsigned program)
  {
    if (program != m_program)
    {
      m_program = program;

      m_uniforms.Clear();
      m_blocks.Clear();
      m_buffers.Clear();
      m_outputs.Clear();

      FindUniforms();
      FindBlocks();
      FindBuffers();
      FindOuputs();
    }

    return *this;
  }

  // -----------------------------------------------------------------------------

  Shader::UniformInfo Shader::Uniform (String name) const
  {
    return m_uniforms.Locate (name);
  }

  Shader::BlockInfo Shader::Block (String name) const
  {
    return m_blocks.Locate (name);
  }

  Shader::BufferInfo Shader::Buffer (String name) const
  {
    return m_buffers.Locate (name);
  }

  Shader::OutputInfo Shader::Output (String name) const
  {
    return m_outputs.Locate (name);
  }

  // -----------------------------------------------------------------------------

  const HashTable<String, Shader::UniformInfo> &Shader::Uniforms() const
  {
    return m_uniforms;
  }

  const HashTable<String, Shader::BlockInfo> &Shader::Blocks() const
  {
    return m_blocks;
  }

  const HashTable<String, Shader::BufferInfo> &Shader::Buffers() const
  {
    return m_buffers;
  }

  const HashTable<String, Shader::OutputInfo> &Shader::Outputs() const
  {
    return m_outputs;
  }

  // -----------------------------------------------------------------------------

  void Shader::SetUniform (String name, float v)
  {
    glUniform1f (Uniform (name).location, v);
  }

  void Shader::SetUniform (String name, vec2 v)
  {
    glUniform2f (Uniform (name).location, v.x, v.y);
  }

  void Shader::SetUniform (String name, vec3 v)
  {
    glUniform3f (Uniform (name).location, v.x, v.y, v.z);
  }

  void Shader::SetUniform (String name, vec4 v)
  {
    glUniform4f (Uniform (name).location, v.x, v.y, v.z, v.w);
  }

  void Shader::SetUniform (String name, double v)
  {
    glUniform1d (Uniform (name).location, v);
  }

  void Shader::SetUniform (String name, dvec2 v)
  {
    glUniform2d (Uniform (name).location, v.x, v.y);
  }

  void Shader::SetUniform (String name, dvec3 v)
  {
    glUniform3d (Uniform (name).location, v.x, v.y, v.z);
  }

  void Shader::SetUniform (String name, dvec4 v)
  {
    glUniform4d (Uniform (name).location, v.x, v.y, v.z, v.w);
  }

  void Shader::SetUniform (String name, int v)
  {
    glUniform1i (Uniform (name).location, v);
  }

  void Shader::SetUniform (String name, ivec2 v)
  {
    glUniform2i (Uniform (name).location, v.x, v.y);
  }

  void Shader::SetUniform (String name, ivec3 v)
  {
    glUniform3i (Uniform (name).location, v.x, v.y, v.z);
  }

  void Shader::SetUniform (String name, ivec4 v)
  {
    glUniform4i (Uniform (name).location, v.x, v.y, v.z, v.w);
  }

  void Shader::SetUniform (String name, unsigned v)
  {
    glUniform1ui (Uniform (name).location, v);
  }

  void Shader::SetUniform (String name, uvec2 v)
  {
    glUniform2ui (Uniform (name).location, v.x, v.y);
  }

  void Shader::SetUniform (String name, uvec3 v)
  {
    glUniform3ui (Uniform (name).location, v.x, v.y, v.z);
  }

  void Shader::SetUniform (String name, uvec4 v)
  {
    glUniform4ui (Uniform (name).location, v.x, v.y, v.z, v.w);
  }

  void Shader::SetUniform (String name, mat2 v)
  {
    glUniformMatrix2fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat3 v)
  {
    glUniformMatrix3fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat4 v)
  {
    glUniformMatrix4fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat2x3 v)
  {
    glUniformMatrix2x3fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat2x4 v)
  {
    glUniformMatrix2x4fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat3x2 v)
  {
    glUniformMatrix3x2fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat3x4 v)
  {
    glUniformMatrix3x4fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat4x2 v)
  {
    glUniformMatrix4x2fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetUniform (String name, mat4x3 v)
  {
    glUniformMatrix4x3fv (Uniform (name).location, 1, false, value_ptr (v));
  }

  void Shader::SetBlockBinding (String name, GLuint index)
  {
    glUniformBlockBinding (m_program, index, Block (name).index);
  }

  void Shader::SetBufferBinding (String name, GLuint index)
  {
    glShaderStorageBlockBinding (m_program, index, Buffer (name).index);
  }

  // -----------------------------------------------------------------------------

  unsigned Shader::GetProgram() const
  {
    return m_program;
  }

  // -----------------------------------------------------------------------------

  void Shader::FindUniforms()
  {
    static const GLenum PROPS[] = { GL_BLOCK_INDEX, GL_NAME_LENGTH, GL_TYPE, GL_LOCATION };

    GLint n;
    glGetProgramInterfaceiv (m_program, GL_UNIFORM, GL_ACTIVE_RESOURCES, &n);

    for (GLint i = 0; i < n; ++i)
    {
      GLint values[4];
      glGetProgramResourceiv (m_program, GL_UNIFORM, i, 4, PROPS, 4, nullptr, values);

      unique_ptr<char> str (new char[values[1]]);
      glGetProgramResourceName (m_program, GL_UNIFORM, i, values[1], nullptr, str.get());

      m_uniforms.Insert (str.get(), UniformInfo (values[2], values[3]));
    }
  }

  void Shader::FindBlocks()
  {
    static const GLenum PROPS[] = { GL_NAME_LENGTH, GL_BUFFER_BINDING };

    GLint n;
    glGetProgramInterfaceiv (m_program, GL_UNIFORM_BLOCK, GL_ACTIVE_RESOURCES, &n);

    for (GLint i = 0; i < n; ++i)
    {
      GLint values[2];
      glGetProgramResourceiv (m_program, GL_UNIFORM_BLOCK, i, 2, PROPS, 2, nullptr, values);

      unique_ptr<char> str (new char[values[0]]);
      glGetProgramResourceName (m_program, GL_UNIFORM_BLOCK, i, values[0], nullptr, str.get());

      m_blocks.Insert (str.get(), BlockInfo (values[1]));
    }
  }

  void Shader::FindBuffers()
  {
    static const GLenum PROPS[] = { GL_NAME_LENGTH, GL_BUFFER_BINDING };

    GLint n;
    glGetProgramInterfaceiv (m_program, GL_SHADER_STORAGE_BLOCK, GL_ACTIVE_RESOURCES, &n);

    for (GLint i = 0; i < n; ++i)
    {
      GLint values[2];
      glGetProgramResourceiv (m_program, GL_SHADER_STORAGE_BLOCK, i, 2, PROPS, 2, nullptr, values);

      unique_ptr<char> str (new char[values[0]]);
      glGetProgramResourceName (m_program, GL_SHADER_STORAGE_BLOCK, i, values[0], nullptr, str.get());

      m_buffers.Insert (str.get(), BufferInfo (values[1]));
    }
  }

  void Shader::FindOuputs()
  {
    static const GLenum PROPS[] = { GL_NAME_LENGTH, GL_LOCATION_INDEX };

    GLint n;
    glGetProgramInterfaceiv (m_program, GL_PROGRAM_OUTPUT, GL_ACTIVE_RESOURCES, &n);

    for (GLint i = 0; i < n; ++i)
    {
      GLint values[2];
      glGetProgramResourceiv (m_program, GL_PROGRAM_OUTPUT, i, 2, PROPS, 2, nullptr, values);

      unique_ptr<char> str (new char[values[0]]);
      glGetProgramResourceName (m_program, GL_PROGRAM_OUTPUT, i, values[0], nullptr, str.get());

      m_outputs.Insert (str.get(), OutputInfo (values[1]));
    }
  }

  // -----------------------------------------------------------------------------
}

// -------------------------------------------------------------------------------
