// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  OpenGL indirect buffer data structures
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef OPENGL_INDIRECT_HELPERS_H
#define OPENGL_INDIRECT_HELPERS_H

#include "glinclude.h"

#pragma pack(push,1)
struct IndirectDraw
{
  GLuint count;
  GLuint primCount;
  GLuint firstIndex;
  GLuint baseVertex;
  GLuint baseInstance;
};
#pragma pack(pop)

// For consitancy with OpenGL documentation
typedef IndirectDraw DrawElementsIndirectCommand;

#endif
