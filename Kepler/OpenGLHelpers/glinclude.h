// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Include stuff for OpenGL
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef GL_INCLUDE_H
#define GL_INCLUDE_H

//#define NSIGHT_BUILD

#ifndef GLEW_STATIC
#define GLEW_STATIC
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

// Comment this out to enable OpenGL error checking
#define GL_ERROR_CHECK

#ifdef GL_ERROR_CHECK
#define GL_CHECK_ERRORS()                                                       \
    do {                                                                        \
      GLenum err = glGetError();                                                \
      if (err != GL_NO_ERROR)                                                   \
      {                                                                         \
         printf ("%s line %i: %s\n", __FILE__, __LINE__, gluErrorString (err)); \
         fflush(stdout);                                                        \
      }                                                                         \
    } while (false)
#else
#define GL_CHECK_ERRORS()
#endif

#endif
