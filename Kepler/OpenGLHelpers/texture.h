// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Texture helper class
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef TEXTURE_HELPER_H
#define TEXTURE_HELPER_H

#include "glinclude.h"
#include "../String/string.h"
#include <vector>

// -----------------------------------------------------------------------------

namespace OpenGL
{
  bool LoadTextureArray (std::vector<String> filenames, GLenum format = GL_RGBA8, GLenum target = GL_TEXTURE_2D_ARRAY);
  bool LoadSingleTextureSub3D (String filename, GLuint z, GLenum target = GL_TEXTURE_2D_ARRAY);
}

// -----------------------------------------------------------------------------

#endif
