#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import os, re

# match every ".py" file, but not anything starting with an underscore
py_regex = re.compile('(?!_.*).*\.py$')

# Set __all__ to empty list, we'll add to it later
__all__ = []

# Walk the directory of this file
for root, dirs, filenames in os.walk(os.path.dirname(__file__)):

  if root == os.path.dirname(__file__):
    for f in filenames:
      # Math all .py files in the directory that are not __init__.py
      if py_regex.match(f) is not None:
        # append the file to __all__, but strip the extension
        __all__.append(f[0:f.find('.py')])
