#version 440

uniform sampler2DArray diffuse_maps;
uniform sampler2DArray specular_maps;

uniform material
{
  uint diffuse_index;
  uint specular_index;
};

in VertexData
{
  vec2 texcoord;
  vec3 normal;
  vec3 world;
} VertexIn;

out vec3 out_diffuse;

void main()
{
  out_diffuse = vec3(VertexIn.texcoord, 1);
  //out_diffuse = texture(diffuse_maps,  vec3(VertexIn.texcoord, diffuse_index)).rgb;
}
