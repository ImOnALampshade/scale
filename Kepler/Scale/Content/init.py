#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
from Components import *

print ('Creating game')
KeplerComposite.LoadRecipeFile('game.json')
KeplerComposite.LoadRecipeFile('Archetypes/ship_parts.json')
KeplerComposite.LoadRecipeFile('Archetypes/objects.json')
KeplerComposite.LoadRecipeFile('Archetypes/player.json')
KeplerComposite.LoadRecipeFile('Archetypes/testobjects.json')
KeplerComposite.LoadRecipeFile('Archetypes/bullet.json')
KeplerComposite.LoadRecipeFile('Archetypes/EnemyBullet.json')
KeplerComposite.LoadRecipeFile('Archetypes/weapon_collectables.json')
KeplerComposite.LoadRecipeFile('Archetypes/CometEnemy.json')
KeplerComposite.LoadRecipeFile('Archetypes/KitingEnemy.json')
KeplerComposite.LoadRecipeFile('Archetypes/HomingEnemy.json')
KeplerComposite.LoadRecipeFile('Archetypes/SpinneyDoo.json')
KeplerComposite.LoadRecipeFile('Archetypes/waveatron.json')
KeplerComposite.LoadRecipeFile('Archetypes/PauseMenu.json')
KeplerComposite.LoadRecipeFile('Archetypes/Particles.json')
KeplerComposite.LoadRecipeFile('Archetypes/Explosions.json')
KeplerComposite.LoadRecipeFile('Archetypes/DJComposite.json')
KeplerComposite.LoadRecipeFile('Archetypes/BackgroundPlane.json')
KeplerComposite.LoadRecipeFile('Archetypes/CreditsScreen.json');
KeplerComposite.LoadRecipeFile('Archetypes/HUD.json');
KeplerComposite.LoadRecipeFile('Archetypes/LoadingScreen.json');

KeplerComposite.CreateGame()
