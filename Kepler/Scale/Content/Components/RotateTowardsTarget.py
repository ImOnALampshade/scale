#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
from KeplerComposite import FindEntity
import math

class RotateTowardsTarget:
    # Serialized Properties:
    TargetName = "PlayerShip";
    MaxRotation = 100.0;

    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        
        self.Target = None;
        pass

    def OnLogicUpdate(self, Dt):
        
        if (self.Target != None):
            self.RotateSelf(Dt);
            pass
        else:
            pathString = "~/" + self.TargetName;
            self.Target = FindEntity(self.Owner, pathString);
            pass
        
        pass
        
    def RotateSelf(self, dt):
        targetLocation = self.Target.Transform.Position;
        
        # Get the rotation needed to look at the point
        targetRotation = self.Owner.Orientation.GetLookAtPointRotation(targetLocation);

        currentRotation = self.Owner.Transform.Rotation;

        # Move towards the needed rotation at a max rate
        partialRotation = currentRotation.toward(targetRotation, math.radians(self.MaxRotation) * dt)

        self.Owner.Transform.Rotation = partialRotation;
        
        pass

KeplerComposite.RegisterComponent(RotateTowardsTarget, "RotateTowardsTarget")