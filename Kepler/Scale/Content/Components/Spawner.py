#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#properties:
# Archetype: String
# Active: 0 or 1

class SpawnerComponent:

    def Initialize(self):
        try:
            if self.SpawnOnAwake:
                KeplerComposite.Connect(self.Owner, "Awake", self.SpawnObject)
        except:
            pass
        KeplerComposite.Connect(self.Owner, "Spawn", self.SpawnObject)
        self.SpawnedObject = None;
        pass
        
    def SpawnObject(self):
        if(self.Active != 0):
            if(self.LoadFromStore != ""):
                spawn = Zero.ObjectStore.RestoreOrArchetype(self.LoadFromStore, self.Spawn, self.Space);
            else:
                spawn = self.Space.Create(self.Spawn);
            print(spawn);
            spawn.Transform.Translation = self.Owner.Transform.Translation
            spawn.Transform.Rotation = self.Owner.Transform.Rotation;
            
            spawn.DispatchDown(MyEvents.PostInit, Zero.ScriptEvent());
            spawn.DispatchDown(MyEvents.Spawn, Zero.ScriptEvent());
            
            self.SpawnedObject = spawn;

KeplerComposite.RegisterComponent(SpawnerComponent, "Spawner")