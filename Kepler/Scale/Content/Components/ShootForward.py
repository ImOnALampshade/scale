import KeplerComposite, time, Keys
from KeplerMath import *

class ShootForward:
    # Serialized Properties
    Bullet = "EnemyBullet" # Bullet Archtetype
    BulletSpeed = 20.0
    FireRate = 3
    OffsetDist = 1.0

    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        
        self.FireDelay = 1/self.FireRate;
        self.Timer = self.FireDelay;

        print(self.Bullet)
        pass

    def OnLogicUpdate(self, Dt):
        if (self.Timer > 0.0):
            self.Timer -= Dt;
            
            if (self.Timer <= 0.0):
                #print("Before Shoot")
                self.Shoot();
                #print("After Shoot")
                self.Timer = self.FireDelay;
                pass
            pass
        
        pass
        
    def Shoot(self):
        #print("In Shoot")
        forward = self.Owner.Orientation.WorldForward;
        
        spawnPosition = self.Owner.Transform.Position;
        spawnPosition = spawnPosition.add( forward.mults(self.OffsetDist) );
        
        createdProjectile = KeplerComposite.CreateComposite(self.Bullet, self.Space);
        createdProjectile.Transform.Position = spawnPosition;
        createdProjectile.Transform.Rotation = self.Owner.Transform.Rotation;
        
        createdProjectile.RigidBody.Velocity = forward.mults(self.BulletSpeed);
        pass

KeplerComposite.RegisterComponent(ShootForward, "ShootForward")