#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#Serialized Properties:
# DeadZoneRadius
# TurnZoneRadius
# TurnSmoothFactor
# TurnRate
# LookAheadFactor

#Other Properties:
# Forward
# MouseVector
# TurnVector
# lastLookAhead
# lookAheadVeloc

class CameraSteererComponent:
  def Initialize(self):
    KeplerComposite.Connect(KeplerComposite.Game(), "MouseMove", self.OnMouseMove)
    KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
    KeplerComposite.Connect(self.Owner, "Awake", self.OnStart)

    self.MouseVector = Vec2(0,0)
    self.TurnVector = Vec2(0,0)
    self.Forward = self.Owner.Transform.Rotation;
    self.lastLookAhead = Vec3(0,0,0)
    self.lookAheadVeloc = Vec3(0,0,0)
    pass

  def OnStart(self):
    self.SetTargets();


  def SetTargets(self):
    self.Camera = KeplerComposite.FindEntity(self.Owner, "./CameraStand/CameraTripod/CameraScope")
    self.Constraints = KeplerComposite.FindEntity(self.Owner, ".").PlayerConstraints
    self.Window = KeplerComposite.Game().Window
    pass

  def OnMouseMove(self, mouseX, mouseY):
    

    # Move everything into aspect space (-1 to 1 in y, -aspect to aspect in x)
    mouseVec    = KeplerComposite.Game().Window.GetMousePos()
    mouseVec.x *= self.Window.AspectRatio;

    # Now constraint to the turnzone - deadzone donut and normalize
    if(mouseVec.length() < self.DeadZoneRadius):
      mouseVec = Vec2(0, 0)
    elif(mouseVec.length() < self.TurnZoneRadius):
      length = (mouseVec.length() - self.DeadZoneRadius) / (self.TurnZoneRadius - self.DeadZoneRadius)
      mouseVec = mouseVec.normalized().mults(length);
    else:
      mouseVec = mouseVec.normalized()
    self.MouseVector = mouseVec;

  def OnLogicUpdate(self, Dt):
    self.Constraints.ApplyConstraints()
    if(self.IsLocked(Dt)):
      self.ControlLockedUpdate(Dt);
    else:
      self.HandleTurning(Dt);
      self.ApplyLookAhead();

    self.Forward = self.Owner.Transform.Rotation;
    self.Constraints.ApplyConstraints()

    pass

  def IsLocked(self, Dt):
    return self.Constraints.Ship.Stunnable.IsStunned()

  def ControlLockedUpdate(self, Dt):
    self.Constraints.CameraStandQuat = self.Constraints.CameraStandQuat.slerp(Quat(), 0.07)
    veloc = self.Constraints.Ship.RigidBody.Velocity
    direction = self.Constraints.Ship.Orientation.GetLookAtDirectionRotation(veloc)
    self.Constraints.Owner.Transform.Rotation = self.Constraints.Owner.Transform.Rotation.slerp(direction, 0.07)
    pass

  def HandleTurning(self, Dt):
    self.TurnVector = self.TurnVector.lerp(self.MouseVector, self.TurnSmoothFactor)
    turning_Euler = Vec3(-self.TurnVector.y, -self.TurnVector.x, 0);
    turning_Quat = QuatFromEulerAngles(turning_Euler.mults(self.GetTurnSpeed() * Dt))

    originPos = self.Constraints.Tripod.Transform.WorldPosition
    self.Constraints.RotateAroundPoint(originPos, turning_Quat)

  def ApplyLookAhead(self):
    aspectRatio = KeplerComposite.Game().Window.AspectRatio;
    turning_Euler = Vec3(self.TurnVector.y * .33, -self.TurnVector.x * -1 * aspectRatio, 0)
    delta_Euler = turning_Euler.subtr(self.lastLookAhead)
    if(self.lookAheadVeloc.dot(delta_Euler) < 0):
        self.lookAheadVeloc = Vec3(0,0,0)
    self.lookAheadVeloc = self.lookAheadVeloc.lerp(delta_Euler, .15);
    self.lastLookAhead = self.lastLookAhead.add(self.lookAheadVeloc)
    turning_Euler = self.lastLookAhead;

    turning_Quat = QuatFromEulerAngles(turning_Euler.mults(self.LookAheadFactor));
    turning_Quat = self.Constraints.CameraStandQuat.toward(turning_Quat, 0.08);
    turning_Quat = self.Constraints.CameraStandQuat.slerp(turning_Quat, 0.24);
    self.Constraints.CameraStandQuat = turning_Quat;

  def GetTurnSpeed(self):
    return self.TurnRate
    #turnPenalty = (self.Throttle - self.OptimalTurningSpeed)
    #turnPenalty = abs(max(-turnPenalty/self.OptimalTurningSpeed/2, turnPenalty/(1-self.OptimalTurningSpeed)));
    #turnPenalty *= turnPenalty;
    #return self.MinTurnRate*turnPenalty + self.MaxTurnRate*(1-turnPenalty);

  def GetDriveThrust(self):
    return self.Owner.CameraDriver.Throttle

KeplerComposite.RegisterComponent(CameraSteererComponent, 'CameraSteerer')
