#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
from KeplerComposite import FindEntity
import math

class KiteTarget:
    # Serialized Properties:
    TargetName = "PlayerShip"
    AdvanceSpeed = 5.0
    RetreatSpeed = 2.0
    KiteDist = 5.0
    KiteBuffer = 1.0
    
    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        
        self.Target = None;
        pass

    def OnLogicUpdate(self, Dt):
        
        if (self.Target != None):
            self.Kite();
            pass
        else:
            pathString = "~/" + self.TargetName;
            self.Target = FindEntity(self.Owner, pathString);
            pass
        
        
        pass
        
    def Kite(self):
        vecToTarget = self.Target.Transform.Position.subtr(self.Owner.Transform.Position);
        distToTarget = vecToTarget.length();
        
        if (distToTarget > self.KiteDist + self.KiteBuffer):
            forward = self.Owner.Orientation.WorldForward;
            self.Owner.RigidBody.Velocity = forward.mults(self.AdvanceSpeed);
            pass
        elif (distToTarget < self.KiteDist - self.KiteBuffer):
            backward = self.Owner.Orientation.WorldForward.mults(-1);
            self.Owner.RigidBody.Velocity = backward.mults(self.RetreatSpeed);
            pass
        else:
            self.Owner.RigidBody.Velocity = Vec3(0,0,0);
        pass

KeplerComposite.RegisterComponent(KiteTarget, "KiteTarget")