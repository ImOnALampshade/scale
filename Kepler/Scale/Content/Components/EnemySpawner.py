#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#Serialized Properties:
# "SpawnRate": 5.0
# "MaxEnemies": 50
# "EnemyToSpawn": "WaspEnemy"
# "SpawnDelay": 5.0

class EnemySpawnerComponent:
    def Initialize(self):
        self.CanSpawn = False
        self.EnemiesSpawned = 0
        self.Timer = self.SpawnDelay
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        self.EnemyToSpawn = 'WaspEnemy'
        print(1/self.SpawnRate)
        pass

    def OnLogicUpdate(self, Dt):
        if(self.CanSpawn and self.EnemiesSpawned < self.MaxEnemies):
            print("SpawnEnemy")
            self.SpawnEnemy(Dt)
        else:
            #enemyTimer = self.Space.FindObjectByName("CapShip").WaveTimer.CurrentTime
            #if(enemyTimer <= 0):
            #   self.CanSpawn = True

            # replace with above when we have CapShip, etc.
            self.Timer -= Dt
            print("Timer: ", self.Timer)
            if (self.Timer < 0.0):
                self.CanSpawn = True
        pass
    
    def SpawnEnemy(self, dt):
        if(self.Timer < 0.0):
            #self.Space.CreateAtPosition(self.EnemyToSpawn, self.Owner.Transform.WorldTranslation)
            print("Trying to spawn enemy")
            enemy = KeplerComposite.CreateComposite(self.EnemyToSpawn, self.Space)
            print("Spawned an enemy")
            enemy.Transform.Position = self.Owner.Transform.WorldPosition
            self.EnemiesSpawned += 1
            self.Timer = 1/self.SpawnRate
        else:
            self.Timer -= dt

KeplerComposite.RegisterComponent(EnemySpawnerComponent, 'EnemySpawner')