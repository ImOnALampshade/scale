#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
import csv
import random
import math
from KeplerMath import *

class RippleData:
  Archetype = 'none'
  Delay = 0
  EnemyCount = 0
  SpawnDirection = 0
  DirectionOffset = 0
  StaggerTime = 0
  RotationAngle = 0
  AngleWiggle = 0
  AngleAddition = 0
  pass
  


class Ripple:
  Started = False #Has this ripple started spawning enemies yet?
  SpawnedEnemies = list(); #list of enemies spawned and alive
  CurrentSpawnCount = 0 #number of enemies physicaly spawned
  Data = None #Data pertaining to the ripple
  TimeSinceWaveStart = 0 #time since the wave with this ripple has started
  StaggerDelay = 0 #StaggerDelay must be zero before enemy can be spawned. Data.StaggerTime is added everytime an enemy is spawned
  InitialAngle = 0 #Initial angle offset that enemies are spawned at
  CurrentSpawnDirection = 0 #inital spawn direction around circle enemies will spawn.
  DirectionIncrement = 0 #An angle from the center of the map to the ege, indicating an increment around the map.
  SpawnWidth = 16 #the width of the map area
  SpawnHeight = 9#the height of the map area.
  Waveatron = None #
  
  def __init__(self, WaveatronParent):
    self.SpawnedEnemies = list()
    self.Waveatron = WaveatronParent
    self.EnemiesLeft = 0 #number of enemies left before ripple is defeated
  
  def Update(self, Dt ):
    if( self.Started ):
      self.TimeSinceWaveStart += Dt
      #manage spawning of enemies
      if(self.Data.Delay < self.TimeSinceWaveStart):
        if( self.CurrentSpawnCount < self.Data.EnemyCount ):
          if( self.Data.StaggerTime == 0 ):
            for index in range(0, self.Data.EnemyCount):
              self.SpawnNextEnemy();
          elif( self.StaggerDelay < 0 ):
            self.SpawnNextEnemy()
          else:
            self.StaggerDelay -= Dt
            
      #check self to see if enemies are killed
      objectRemoveList = list()
      for i in range(len( self.SpawnedEnemies ) ):
        #check if the enemy is alive, if not add it to the remove list
        if( self.SpawnedEnemies[i].IsValid() == False ):
          objectRemoveList.append( self.SpawnedEnemies[i] )
      for i in range( len( objectRemoveList) ):
        self.EnemiesLeft -= 1
        self.SpawnedEnemies.remove( objectRemoveList[i] )
        #print("Enemies Left: " + str(self.EnemiesLeft))
        #print("Enemes In Action: " +str(len(self.SpawnedEnemies ) ) )
        
    pass
    
  def Start(self):
    self.Started = True
    self.EnemiesLeft = self.Data.EnemyCount #intialize total enemies left to kill
    self.InitialAngle = -(self.Data.AngleAddition * self.Data.EnemyCount / 2)
    self.CurrentSpawnDirection = 0
    self.DirectionIncrement = 360 / self.Data.EnemyCount
    
  
  def SpawnNextEnemy(self):
    self.CurrentSpawnCount += 1
    self.StaggerDelay += self.Data.StaggerTime
    self.InitialAngle += self.Data.AngleAddition
    
    spawnDirection = 0
    if( self.Data.SpawnDirection == None ):
      spawnDirection = self.CurrentSpawnDirection
      self.CurrentSpawnDirection += self.DirectionIncrement
    else:
      spawnDirection = self.Data.SpawnDirection
      
    spawnDirection += self.Data.DirectionOffset
      
    spawnPosition = Vec3( math.cos( math.radians( spawnDirection ) ) * self.SpawnWidth , math.sin( math.radians( spawnDirection ) ) * self.SpawnHeight , 0)
    
    newEnemy = KeplerComposite.CreateComposite(self.Data.Archetype, self.Waveatron.Space)
    #Set position of enemy
    newEnemy.Transform.Position = spawnPosition
    #get direction enemy needs to face
    directionToCenter = Vec3(0,0,0).subtr(spawnPosition)
    directionToCenter = directionToCenter.normalized()
    angleToCenter = math.atan2( directionToCenter.y, directionToCenter.x )
    if(self.Data.RotationAngle != None ):
      angleToCenter = math.radians(self.Data.RotationAngle)
    angleToCenter += math.radians(self.InitialAngle)
    angleToCenter += math.radians(random.uniform(-self.Data.AngleWiggle, self.Data.AngleWiggle))
    #angleToCenter = 3.14
    directionTowardsCenter = Vec3( math.cos(angleToCenter), math.sin( angleToCenter ), 0 )
    
    newEnemy.Orientation.LookAtDirection( directionTowardsCenter )
    targetScale = newEnemy.Transform.Scale;
    newEnemy.Transform.Scale = Vec3(0,0,0);

    self.Waveatron.Owner.Actions.Sequence().AddPropertyVec3(newEnemy.Transform, "Scale", targetScale, 0.3, 0)
    try:
      newEnemy.DirectionData.SetDirectionAngle( angleToCenter )
      pass
    except:
      pass
      

    
    self.SpawnedEnemies.append(newEnemy)
    #print("Spawned enemy:" + self.Data.Archetype )
    pass
  
  def IsDefeated(self):
    if( self.Started == False):
      return False
      
    if( self.EnemiesLeft == 0 ):
      return True
    else:
      return False
     
    pass
  
  pass

  
#The Wave-A-Tron will take the spawn data and start the waves
#Each wave has a certain set of functionality
# -Create Ripples at appropriate delays
# -Update Ripples
# -Keep track of whether wave is defeated or not
class Wave:
  RippleList = None;
  Started = False
  WaveNumber = 0
  
  def __init__(self):
    self.RippleList = list();
  
  def Start(self):
    self.Started = True
    print("Starting Wave " + str(self.WaveNumber) )
    print("Wave has " + str( len(self.RippleList) ) + " ripples.")
    for ripple in self.RippleList:
      ripple.Start()
    pass
  
  def Update( self, Dt ):
    if(self.Started):
      for ripple in self.RippleList:
        ripple.Update( Dt )
    pass
    
  def IsDefeated(self):
    if(self.Started == False ):
      return False
      
    for ripple in self.RippleList:
      if(ripple.IsDefeated() == False ):
        return False
        
    return True
    pass
    
    #RippleData - I think its a dictionary. At least I access it like one...
  def AddRipple(self, RippleCsvData, WaveatronParent ):
    #pull ripple data out of the dictionary thing we are passed, and create a new ripple and add it to our list.
    rippleData = RippleData();
    if(len(RippleCsvData['Archetype']) > 0):
      rippleData.Archetype = RippleCsvData['Archetype']
    else:
      print("Wave-A-Tron: Added ripple with now archetype!?!? ERROR THIS LINE.")
      
    if(len(RippleCsvData['Delay']) > 0):
      rippleData.Delay = float(RippleCsvData['Delay'])
      
    if(len(RippleCsvData['Count']) > 0):
      rippleData.EnemyCount = int(RippleCsvData['Count'])
    else:
      rippleData.EnemyCount = 1
      
    if(len(RippleCsvData['SpawnDirection']) > 0):
      rippleData.SpawnDirection = float(RippleCsvData['SpawnDirection'])
    else:
      rippleData.SpawnDirection = None
      
    if(len(RippleCsvData['DirectionOffset']) > 0):
      rippleData.DirectionOffset = float(RippleCsvData['DirectionOffset'])
      
    if(len(RippleCsvData['StaggerTime']) > 0):
      rippleData.StaggerTime = float(RippleCsvData['StaggerTime'])

      
    if(len(RippleCsvData['RotationAngle']) > 0):
      rippleData.RotationAngle = float(RippleCsvData['RotationAngle'])
    else:
      rippleData.RotationAngle = None
      
    if(len(RippleCsvData['Wiggle']) > 0):
      rippleData.AngleWiggle = float(RippleCsvData['Wiggle'])
      
    if(len(RippleCsvData['AngleAddition']) > 0):
      rippleData.AngleAddition = float(RippleCsvData['AngleAddition'])    
    
    newRipple = Ripple(WaveatronParent)
    newRipple.Data = rippleData
    
    #add the new ripple into the RippleList
    #print("Adding ripple to wave " + str( self.WaveNumber ) )
    self.RippleList.append( newRipple )
    
    pass
    
  
  
  pass

class WaveATronComponent:
  #values that should be serialized
  WaveFile = 'InvalidFile'
  WaveDelay = 4; # 4 second delay between waves
  WaveStartSound = ""
  
  #dont bother with these
  WaveList = None
  State = 0
  CurrentDelay = 0
  AllWavesDefeated = False
  TotalWaves = 0
  StartNextWave = False

  def Initialize(self):
    KeplerComposite.Connect(self.Owner, 'LogicUpdate', self.OnLogicUpdate )
    print('Created Wave-A-Tron')
    self.LoadFileData( self.WaveFile )
    #initialize wave delay
    self.CurrentDelay = self.WaveDelay
    pass

  def OnLogicUpdate(self, Dt):
    #Update the WaveATron.Waveatron waits delay, starts wave. wait until wave defeated. Reset wave delay.
    #Wave-A-Tron is in two states.
    #1 - Waiting for the waves to be defeated
    #2 - Waiting for the delay until starting the next wave
    if( self.AllWavesDefeated == False ):
      #update current wave
      self.WaveList[0].Update( Dt )
      #manage wave starting and stopping
      if( self.State == 0 ): # if we are waiting for a delay
        self.CurrentDelay -= Dt
        if( self.CurrentDelay < 0 ):
          self.State = 1
        
          self.Owner.Audio.PlayLongSound(self.WaveStartSound)
          
      elif( self.State == 1 ): #we are in the wave start state
        self.WaveList[0].Start()

        HUDSpace = self.Game.FindEntity("/HUD")
        if HUDSpace is None: return
        HUDSpace.Dispatch('UpdateNewWave', str(len(self.WaveList)))
        pass


        self.State = 2
      elif( self.State == 2 ):# if we are waiting for the wave to be defeated
        if( self.WaveList[0].IsDefeated() or self.StartNextWave): #if this wave is defeated, reset wave delay and go back to state 0
          self.StartNextWave = False
          self.CurrentDelay = self.WaveDelay
          self.State = 0

          #and pop this wave off the stack
          self.WaveList.pop(0)
          
          if(len(self.WaveList) == 0 ):
            self.Space.Dispatch("AllWavesDefeated")
            
            self.AllWavesDefeated = True
    pass
  
  def WavesDefeated(self):
    return self.AllWavesDefeated
    pass
    
  def LoadFileData(self, FileName ):
    print("Wave-a-Tron opening file: " + FileName );
    currentWave = None
    self.WaveList = list()
    with open(FileName, 'rt') as f:
      reader = csv.DictReader(f, delimiter=',')
      for line in reader:
        if( line['Wave'] == 'Wave' ):
          if( currentWave is None ):
            print('Creating first wave')
          else:
            print('Finishing wave with ' + str(len(currentWave.RippleList)) + ' ripples.')
          print('New Wave')
          currentWave = Wave();
          print("Starting new wave: " + str(currentWave.WaveNumber) )
          self.WaveList.append( currentWave )
          self.TotalWaves += 1
          currentWave.WaveNumber = self.TotalWaves
          #print("Append Target: " + str(currentWave.WaveNumber) )
        else:
          print('Adding ripple to wave ' + str( currentWave.WaveNumber ) )
          print( line['Wave'] + line['Archetype'] + line['Delay'] + line['Count'] + line['SpawnDirection'] + line['RotationAngle'] + line['StaggerTime'] + line['Wiggle'] + line['AngleAddition'])
          currentWave.AddRipple( line, self );
    print("There are " + str( len( self.WaveList ) ) + " waves.")
    for wave in self.WaveList:
      print("Wave " + str( wave.WaveNumber ) +" has " + str( len(wave.RippleList) ) +" ripples." )
    pass
    
KeplerComposite.RegisterComponent(WaveATronComponent, 'WaveATron')