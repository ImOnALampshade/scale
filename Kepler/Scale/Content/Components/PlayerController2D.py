#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
import math
from KeplerMath import *


#Serialized Properties
    # Acceleration
    # Thrust
    # VelocityAdjustRate
    # CameraPath
    # DamageSound
#Other Properties
    # Throttle
    # CurrentVelocity
    # Speed

class PlayerController2D:
  Outro = False
  Maneuvering = Vec3(0.4, 1, 0.2)


  DamageSound = "Asteroid Tink"
  Acceleration = 0.2
  Thrust = 30.0
  CameraPath = "~/WorldCamera"
  Friction = 0.96
  TurnRate = 4

  MaxInvincibilityTime = 0.8
  CurrTime = 0
  Invincible = False

  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)
    KeplerComposite.Connect(self.Owner, "Awake", self.OnStart)
    KeplerComposite.Connect(self.Space, "OutroStarted", self.OnOutro)
    KeplerComposite.Connect(self.Owner, "TakeDamageEvent", self.OnDamageTaken)
    KeplerComposite.Connect(self.Owner, "DeathEvent", self.OnDeath)
    self.Throttle = Vec3(0,0,0);
    self.LastThrottle = Vec3(0,0,0);
    self.Speed = 0;
    self.RollAngle = 0;
    
    KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollision)
    
    #print(self.MaxInvincibilityTime)
    pass

  def OnStart(self):
    print(self.CameraPath)
    self.SetTargets()
    pass

  def OnDeath(self, destroyer):
    NewSpace = KeplerComposite.CreateComposite('GameOverMenu', self.Game)
    NewSpace.PauseMenu.PausedSpace = self.Space
    self.Game.Dispatch("PauseEvent", NewSpace.Name());

    #FELIPE TEST------------------------------------------------
  def OnCollision(self, Composite):
    if(self.Invincible == False and Composite.EnemyComponent != None):
        self.Owner.Dispatch("TakeDamageEvent",Composite.EnemyComponent.Damage)
        self.Invincible = True
    pass   

  def OnDamageTaken(self, DamageTaken):
    self.Owner.Audio.PlayShortSound(self.DamageSound);
    #self.Owner.Audio.PlayShortSound("PlayerTakesDamage");
    pass
    #------------------------------------------------------------
    
  def OnOutro(self):
    self.Outro = True

  def SetTargets(self):
    self.Camera = KeplerComposite.FindEntity(self.Owner, self.CameraPath)
    self.Ship = self.Owner
    self.Window = self.Game.Window
    pass
    
  def UpdateInvincibility(self,Dt):
    if(self.Invincible == True):
        self.CurrTime += Dt
        if(self.CurrTime >= self.MaxInvincibilityTime):
            self.Invincible = False
            self.CurrTime = 0
    pass

  def ApplyRoll(self, Dt):
    self.Owner.Transform.WorldRotation = self.Owner.Orientation.GetLookAtDirectionWithUpRotation(self.Owner.Orientation.WorldForward, Vec3(0,0,1));
    rollQuat = QuatFromAxisAngle(Vec3(0,0,-1), self.RollAngle*0.5)
    self.Owner.Transform.WorldRotation = self.Owner.Transform.WorldRotation.mult(rollQuat)
    pass

  def OnLogicUpdate(self, Dt):
    #Short invincibility logic
    self.UpdateInvincibility(Dt)
  
    if(not self.IsLocked()):
      self.HandleKeyboardInput(Dt)
      self.ClampToYZero();
      self.ApplyForwardThrust(Dt)
      self.FaceCursor()
      self.ApplyRoll(Dt)
    else:
      self.ControlLockedUpdate(Dt)
    pass

  def ClampToYZero(self):
    pos = self.Owner.Transform.WorldPosition
    pos.z = 0;
    self.Owner.Transform.WorldPosition = pos;
    pass

  def IsLocked(self):
    if self.Outro:
      return True
    return self.Ship.Stunnable.IsStunned()

  def HandleKeyboardInput(self, Dt):
    Keyboard = KeplerComposite.Game().Window
    mod = Vec3(0,0,0)
    self.RollAngle *= 0.97 
    if(Keyboard.IsKeyDown(Keys.KEY_W)):
      mod = mod.add(Vec3(0,0,-1))
    if(Keyboard.IsKeyDown(Keys.KEY_S)):
      mod = mod.add(Vec3(0,0,1))
    if(Keyboard.IsKeyDown(Keys.KEY_Q)):
      mod = mod.add(Vec3(-1,0,0))
      self.RollAngle -= 0.03;
    if(Keyboard.IsKeyDown(Keys.KEY_E)):
      self.RollAngle += 0.03;
      mod = mod.add(Vec3( 1,0,0))
    if(Keyboard.IsKeyDown(Keys.KEY_A)):
      self.Owner.Transform.WorldRotation = self.Owner.Transform.WorldRotation.mult(QuatFromAxisAngle(Vec3(0,1,0), Dt * self.TurnRate))
    if(Keyboard.IsKeyDown(Keys.KEY_D)):
      self.Owner.Transform.WorldRotation = self.Owner.Transform.WorldRotation.mult(QuatFromAxisAngle(Vec3(0,1,0), -Dt * self.TurnRate))

    if(mod.length() > 0):
      mod = mod.normalized();

    if(mod.z < 0):
      mod = mod.mult(Vec3(self.Maneuvering.x, 0, self.Maneuvering.y));
    else:
      mod = mod.mult(Vec3(self.Maneuvering.x, 0, self.Maneuvering.z));
      
    self.Throttle = self.Throttle.lerp(mod, self.Acceleration);

    pass

  def ApplyForwardThrust(self, Dt):
    targetVeloc = self.GetTargetVelocity()
    self.Owner.RigidBody.Velocity = self.Owner.RigidBody.Velocity.mults(self.Friction);
    self.Owner.RigidBody.Velocity = self.Owner.RigidBody.Velocity.add(targetVeloc)

  def GetTargetVelocity(self):
    speed = self.Throttle.mults(self.Thrust)
    speed = self.Owner.Transform.WorldRotation.rotate(speed)
    self.Throttle = Vec3(0,0,0)
    self.LastThrottle = speed;
    return speed
  
  def ControlLockedUpdate(self, Dt):
    pass

  def FaceCursor(self):

    #mousePos = self.Window.GetMousePos();
    #if(math.isinf(mousePos.x)):
    #  return
    #mouseRay = self.Camera.Camera.ScreenToWorld(mousePos)
    #mousePos = mouseRay.SampleAtDistance(-40/mouseRay.Direction.z)
    #self.Owner.Orientation.LookAtPointWithUp(mousePos, Vec3(0,0,1))
    pass

KeplerComposite.RegisterComponent(PlayerController2D, 'PlayerController2D')
