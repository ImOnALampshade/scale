#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class TestActionListComponent:
  def Initialize(self):
    KeplerComposite.Connect(KeplerComposite.Game(), "KeyDown", self.OnKeyDown)
    pass

  def OnKeyDown(self, Key, Mods):
    print("foo")
    if Key == Keys.KEY_0:
      print(self.Owner.Actions)
    elif Key == Keys.KEY_1:
      print(self.Owner.Actions.Set())
    elif Key == Keys.KEY_2:
      print(self.Owner.Actions.Sequence())
    elif Key == Keys.KEY_3:
      self.Owner.Actions.Sequence().AddPropertyVec3(self.Owner.Transform, "Scale", Vec3(2,2,2), 1.0, 0)
    elif Key == Keys.KEY_4:
      self.Owner.Actions.Sequence().AddPropertyVec3(self.Owner.Transform, "Scale", Vec3(1,1,1), 1.0, 0)
    elif Key == Keys.KEY_5:
      self.Owner.Actions.Sequence().Skip()
    elif Key == Keys.KEY_6:
      self.Owner.Actions.Sequence().Cancel()
    elif Key == Keys.KEY_7:
      print(self.Owner.Actions.Sequence().Finished)
    pass

KeplerComposite.RegisterComponent(TestActionListComponent, "TestActionList")
