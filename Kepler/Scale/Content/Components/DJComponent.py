#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys
import KeplerMath

class DJComponent:
  def Initialize(self):
    KeplerComposite.Connect(KeplerComposite.Game(), 'KeyDown', self.OnKeyDown)
    KeplerComposite.Connect(self.Owner, 'Awake', self.OnAwake)
    self.Owner.Audio.MakeMeTheListener()
    print("DJObject")

  def OnAwake(self):
    self.Owner.Audio.PlayLongSound("Zero_Prototype_Music")

  def OnKeyDown(self, key, mods):
    if(key == Keys.KEY_P):
      print ("p was hit, playing sound")
      self.Owner.Audio.PlayLongSound("Zero_Prototype_Music")

KeplerComposite.RegisterComponent(DJComponent, 'DJ')
