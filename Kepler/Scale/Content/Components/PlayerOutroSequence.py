#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class PlayerOutroSequence:
  OutroStarted = False
  OutroFinshed = False
  TargetPoint2 = Vec3(-4, -4, 40)
  Speed = 22
  TurnRate = 0.08

  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)
    #KeplerComposite.Connect(self.Game, "KeyDown", self.OnKeyDown)
    KeplerComposite.Connect(self.Space, "OutroStarted", self.OnOutro)
    pass

  def OnOutro(self):
    self.OutroStarted = True
    if self.Owner.Object2D != None:
      self.Owner.Object2D.Enabled = False

  def OnKeyDown(self, key, mods):
    if(key == Keys.KEY_Q):
      self.Space.Dispatch("OutroStarted")

  def OnLogicUpdate(self, Dt):
    if self.OutroStarted:
      if self.Owner.Transform.Position.z < 35:
        self.TurnTowardTarget(Dt)
        self.MoveForward(Dt)
        self.Owner.RigidBody.Velocity = Vec3(0,0,0)
      if self.Owner.Transform.Position.z > 45:
        self.OutroFinished = True

  def TurnTowardTarget(self, Dt):
    targetPoint1 = Vec3(self.Owner.Transform.Position.x, self.Owner.Transform.Position.y, self.TargetPoint2.z)
    z = min(self.Owner.Transform.Position.z / 35, 1)
    targetPoint = targetPoint1.lerp(self.TargetPoint2, z);
    #qTarget = self.Owner.Orientation.GetLookAtDirectionRotation(targetPoint)
    qTarget = self.Owner.Orientation.GetLookAtPointRotation(targetPoint)
    qTarget = self.Owner.Transform.WorldRotation.slerp(qTarget, self.TurnRate)
    self.Owner.Transform.WorldRotation = qTarget
    qTarget = self.Owner.Orientation.GetLookAtDirectionWithUpRotation(self.Owner.Orientation.WorldForward, Vec3(0,1,0))
    qTarget = self.Owner.Transform.WorldRotation.slerp(qTarget, self.TurnRate)
    self.Owner.Transform.WorldRotation = qTarget
    self.TargetPoint = targetPoint;
    pass

  def MoveForward(self, Dt):
    forward = self.Owner.Orientation.WorldForward.mults(self.Speed * Dt)
    targetDir = self.TargetPoint.subtr(self.Owner.Transform.Position)
    forward = forward.lerp(forward.mults(forward.normalized().dot(targetDir.normalized())), 0.7)
    
    newPos = self.Owner.Transform.Position.add(forward)
    self.Owner.Transform.Position = newPos
    pass

KeplerComposite.RegisterComponent(PlayerOutroSequence, 'PlayerOutroSequence')