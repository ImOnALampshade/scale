#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
import math

#Serialized Properties
# Speed = Property.Float(5.0);

class MoveInDirection:
    Speed = 2

    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        pass

    def OnLogicUpdate(self, Dt):
        self.Owner.RigidBody.Velocity = self.Owner.DirectionData.DirVec.mults(self.Speed)
        pass
        

KeplerComposite.RegisterComponent(MoveInDirection, 'MoveInDirection')