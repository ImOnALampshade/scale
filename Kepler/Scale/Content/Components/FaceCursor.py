#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys
from KeplerComposite import FindEntity
from KeplerMath import *
import math

class FaceCursor:
  CameraPath = "~/WorldCamera"

  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)
    self.Window = self.Game.Window
    self.Camera = KeplerComposite.FindEntity(self.Owner, self.CameraPath)
    pass

  def OnLogicUpdate(self, Dt):
    mousePos = self.Window.GetMousePos();
    if(math.isinf(mousePos.x)):
      return
    mouseRay = self.Camera.Camera.ScreenToWorld(mousePos)
    mousePos = mouseRay.SampleAtDistance(-40/mouseRay.Direction.z)
    self.Owner.Orientation.LookAtPointWithUp(mousePos, Vec3(0,0,1))
    pass

KeplerComposite.RegisterComponent(FaceCursor, 'FaceCursor')
