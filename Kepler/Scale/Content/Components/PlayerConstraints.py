#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
from KeplerComposite import FindEntity
import time

#Serialized Properties:
# CameraStandDistance
# MoveTargetDistance
# CameraTripodHeight
#Other Properties
# CameraStandQuat


class PlayerConstraintsComponent:
  def Initialize(self):
    KeplerComposite.Connect(KeplerComposite.Game(), "Start", self.OnStart)
    self.TargetStretch = 1;
    self.CameraStandQuat = Quat();
    pass


  def OnStart(self):
    self.SetTargets();

  def SetTargets(self):
    self.Stand = FindEntity(self.Owner, "./CameraStand")
    self.Target = FindEntity(self.Owner, "./FacingTarget")
    self.Ship = FindEntity(self.Owner, "../PlayerShip")
    self.Tripod = FindEntity(self.Owner, "./CameraStand/CameraTripod")
    self.Camera = FindEntity(self.Owner, "./CameraStand/CameraTripod/CameraScope")
    pass

  def RotateAroundPoint(self, Origin, quat):
    negOrigin = Origin.mults(-1)
    self.TranslateSystem(negOrigin)
    self.RotateAroundOrigin(quat)
    self.TranslateSystem(Origin)
    pass

  def TranslateSystem(self, Position):
    self.Owner.Transform.Position = self.Owner.Transform.Position.add(Position)
    pass

  def RotateAroundOrigin(self, quat):
    self.Owner.Transform.Position = self.Owner.Transform.Rotation.inverted().rotate(self.Owner.Transform.Position)
    self.Owner.Transform.Rotation = self.Owner.Transform.Rotation.mult (quat)
    self.Owner.Transform.Position = self.Owner.Transform.Rotation.rotate(self.Owner.Transform.Position)
    pass

  def ApplyConstraints(self):
    self.Target.Transform.Position = Vec3(0,0,-1).mults(self.MoveTargetDistance * self.TargetStretch)
    self.Stand.Transform.Position = self.CameraStandQuat.rotate(Vec3(0,0,1)).mults(self.CameraStandDistance)
    self.Tripod.Transform.Position = Vec3(0,1,0).mults(self.CameraTripodHeight)
    pass

  def RotateSystemPositionsAroundPoint(self, Origin, quat):
    negOrigin = Origin.mults(-1)
    self.TranslateSystem(negOrigin)
    self.RotateSystemPositionsAroundOrigin(quat);
    self.TranslateSystem(Origin)
    pass

  def RotateSystemPositionsAroundOrigin(self, quat):
    self.Owner.Transform.Position = self.Owner.Transform.Rotation.inverted().rotate(self.Owner.Transform.Position)
    self.Owner.Transform.Rotation = self.Owner.Transform.Rotation.mult (quat)
    self.Owner.Transform.Position = self.Owner.Transform.Rotation.rotate(self.Owner.Transform.Position)
    self.Owner.Transform.Rotation = self.Owner.Transform.Rotation.mult (quat.inverted())
    pass

KeplerComposite.RegisterComponent(PlayerConstraintsComponent, 'PlayerConstraints')
