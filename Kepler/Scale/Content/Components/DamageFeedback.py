#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#Serialized Properties:
# "Health": 10,
# "DamageOnCollide": 0 or 1

class DamageFeedback:
    SoundCue = None
    FlashDuration = 0.2
    FlashColor = Vec4(1,0.2,0.2,1)

    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "TakeDamageEvent", self.OnTakeDamage)
        pass
    

    def OnTakeDamage(self, Damage):
        if self.FlashColor != None:
            print("Starting damage feedback")
            self.Owner.Actions.Sequence().Skip();
            self.Owner.Actions.Sequence().AddPropertyColor(self.Owner.Renderable, "Tint", self.FlashColor, self.FlashDuration*.2, 0)
            self.Owner.Actions.Sequence().AddPropertyColor(self.Owner.Renderable, "Tint", self.Owner.Renderable.Tint, self.FlashDuration*0.8, 0)
        if self.SoundCue != None:
            self.Owner.Audio.PlayShortSound(self.SoundCue)



KeplerComposite.RegisterComponent(DamageFeedback, 'DamageFeedback')