#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys

from KeplerMath import *

class PresentationComponent:
  def Initialize(self):
    print('xxxxxxxxxxxxxxxxxxxxxxx')
    KeplerComposite.Connect(self.Owner, 'KeyDown', self.KeyDown)
    KeplerComposite.Connect(self.Owner, 'Start', self.Start)
    KeplerComposite.LoadRecipeFile('Archetypes/slides.json')

    self.slides = [
      self.SlideDemo,
      self.Owner.Game.ExitGame,
    ]

    self.slide_num = 0

    self.destroy_list = []

  def Start(self):
    self.slides[self.slide_num]()


  def KeyDown(self, key, mods):
    if key == Keys.KEY_SPACE:
      for obj in self.destroy_list:
        obj.Destroy()

      self.slide_num = (self.slide_num + 1) % len(self.slides)
      self.slides[self.slide_num]()

  def Slide1(self):
    space = KeplerComposite.CreateComposite('space', self.Owner)
    self.destroy_list = [
      space
    ]
    KeplerComposite.CreateComposite('Slide1', space),
    KeplerComposite.CreateComposite('SlideCamera', space)

  def SlideDemo(self):
    space = KeplerComposite.CreateComposite('space', self.Owner)
    self.destroy_list = [
      space
    ]
    KeplerComposite.CreateComposite('PlayerRoot', space)
    print("Im in your base killin your roots")
    we = KeplerComposite.CreateComposite('waspEnemy', space)
    we.Transform.Position = Vec3(0, 5, -8)
    #t1 = KeplerComposite.CreateComposite('test_1', space)
    #t2 = KeplerComposite.CreateComposite('test_1', space)
    #
    #t1.Transform.Position = Vec3(3, 0, -8)
    #t2.Transform.Position = Vec3(-3, 0, -8)
    KeplerComposite.CreateInstance('Instances/TestBoxes.json')


    
    
  def SlidePhysics(self):
    space = KeplerComposite.CreateComposite('physics_sandbox_space', self.Owner)
    self.destroy_list = [
      space
    ] 
    #KeplerComposite.CreateComposite('phys_test_1',  space)
    #KeplerComposite.CreateComposite('phys_test_2',  space)
    #KeplerComposite.CreateComposite('test_box',     space)
    #KeplerComposite.CreateComposite('test_box2',    space)
    #KeplerComposite.CreateComposite('test_plane',   space)
    KeplerComposite.CreateInstance('Instances/Test_Instance.json', space)
    
    

KeplerComposite.RegisterComponent(PresentationComponent, 'Presentation')
