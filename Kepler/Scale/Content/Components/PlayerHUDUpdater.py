#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys

from KeplerMath import *

class PlayerHUDUpdater:
  Outro = False
  counter = 0
  
  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.LogicUpdate)
    KeplerComposite.Connect(self.Game, "KeyDown", self.OnKeyDown)
    pass

  def LogicUpdate(self, dt):
    self.counter = self.counter + 1
    if self.counter % 6 != 0:
     return
    
    HUDSpace = self.Game.FindEntity("/HUD")

    if HUDSpace is None: return

    value = self.Owner.Health.Health
    Weapon = self.Owner.Weapons.CurrentWeaponType
    Ammo = self.Owner.Weapons.Ammo
    if Weapon is "BasicShot":
     Ammo = "infinite"
    
    if (value < 3):
      HUDSpace.Dispatch('MakeTheHUDRed')
    else:
      HUDSpace.Dispatch('MakeTheHUDWhite')

    #HUDSpace.Dispatch('UpdateValues', str(value), str(Weapon), str(Ammo))
    HUDSpace.Dispatch('HealthUpdate', str(value))
    HUDSpace.Dispatch('WeaponTypeUpdate', str(Weapon))
    HUDSpace.Dispatch('AmmoUpdate', str(Ammo))
  
  def OnKeyDown(self, key, mods):
    pass

KeplerComposite.RegisterComponent(PlayerHUDUpdater, 'PlayerHUDUpdater')
