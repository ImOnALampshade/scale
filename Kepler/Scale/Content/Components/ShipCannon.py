#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time
from KeplerMath import *
import time
import random

class ShipCannonComponent:
    WeaponType = "BasicShot"
    BulletType = 'Bullet'
    Inaccuracy = 0;
    RecoilForce = 0;
    IsFiring = False

    def Initialize(self):
        #properties
        self.ShipSubsystem = self.Owner.ShipSubsystem
        self.Ship = self.ShipSubsystem.Ship;

        self.Ammo = self.Ship.Weapons
        self.time = 0;

        KeplerComposite.Connect(self.Owner, "Awake", self.OnAwake)
        KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
        KeplerComposite.Connect(self.Owner, 'StartShooting', self.OnStartShooting)
        KeplerComposite.Connect(self.Owner, 'StopShooting', self.OnStopShooting)

        pass

    def OnAwake(self):
        pass

    def OnStartShooting(self, WeaponType):
        if (self.WeaponType == WeaponType):
            self.IsFiring = True
            #self.Owner.Audio.PlayLongSound("PlayerGun")

    def OnStopShooting(self):
        self.IsFiring = False
        #self.Owner.Audio.StopButLetFinish("PlayerGun");
        pass

    def OnLogicUpdate(self, Dt):
        if(self.ShipSubsystem.Enabled):
            self.time -= Dt
            if(self.IsFiring):
                self.FireWeapon();
            elif(self.time <= self.Ship.Shoot.RefireShift):
                self.time = self.Ship.Shoot.RefireShift;
        pass
        
    def FireWeapon(self):
        while(self.time <= 0):
            directQuat = self.Owner.Transform.WorldRotation
            offset = (random.random()*2 - 1) * self.Inaccuracy
            directQuat = directQuat.mult(QuatFromAxisAngle(Vec3(0,1,0), offset))
            direct = directQuat.rotate(Vec3(0,0,-1))
            bullet = KeplerComposite.CreateComposite(self.BulletType, self.Space)
            bullet.Transform.Position = self.Owner.Transform.WorldPosition.add(direct.mults(self.Ship.Shoot.OriginOffset))
            bullet.Transform.Rotation = directQuat
            bullet.RigidBody.Velocity = direct.mults(self.BulletSpeed)
            self.time += self.RefireRate;

            self.Owner.Audio.PlayShortSound("PlayerGun_SingleShot")

            self.Ship.RigidBody.Velocity = self.Ship.RigidBody.Velocity.add(direct.normalized().mults(-self.RecoilForce))

            if self.Ammo.Ammo > 0 and self.WeaponType != 'BasicShot':
                self.Ammo.Ammo -= 1
            else:
                self.Ship.Dispatch('BasicShot')
        
KeplerComposite.RegisterComponent(ShipCannonComponent, "ShipCannon")
