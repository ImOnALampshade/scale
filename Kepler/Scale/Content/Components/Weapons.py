#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
from KeplerComposite import FindEntity

class WeaponsComponent:
  
  def Initialize(self):
    self.CurrentWeaponType = 'BasicShot'
    self.Ammo = 0
    KeplerComposite.Connect(self.Owner, "SpreadShot", self.OnSpreadShot)
    KeplerComposite.Connect(self.Owner, "BombShot", self.OnBombShot)
    KeplerComposite.Connect(self.Owner, "BasicShot", self.OnBasicShot)
    pass

  def OnSpreadShot(self, Ammo):
    if(self.CurrentWeaponType == 'SpreadShot'):
      self.Ammo += Ammo
    else:
      self.CurrentWeaponType = 'SpreadShot'
      self.Ammo = Ammo
      self.Owner.Dispatch("WeaponChanged")

  def OnBombShot(self, Ammo):
    if(self.CurrentWeaponType == 'BombShot'):
      self.Ammo += Ammo
    else:
      self.CurrentWeaponType = 'BombShot'
      self.Ammo = Ammo
      self.Owner.Dispatch("WeaponChanged")

  def OnBasicShot(self):
    if(self.CurrentWeaponType != 'BasicShot'):
      self.Owner.Dispatch("WeaponChanged")
    self.CurrentWeaponType = 'BasicShot'
    self.Ammo = 0

KeplerComposite.RegisterComponent(WeaponsComponent, "Weapons")