#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#Serialized Properties:
# "Health": 10,
# "DamageOnCollide": 0 or 1

class LifeTime:
    Duration = 2.0
    Active = True

    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)
        pass
    

    def OnLogicUpdate(self, Dt):
        if self.Active:
            self.Duration -= Dt;
            if self.Duration < 0:
                self.Owner.Destroy();



KeplerComposite.RegisterComponent(LifeTime, 'LifeTime')