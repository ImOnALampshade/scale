#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
import math

# RotSpeed = Property.Float(5.0);
# RotateClockwise: 0 or 1

class KeepRotating:
    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        
        self.RotDir = 1;
        if (self.RotateClockwise == True):
            rotDir = -1;
            pass
            
        pass

    def OnLogicUpdate(self, Dt):        
        self.Owner.RigidBody.RotVelocity = Vec3(0,0,1).mults(math.radians(self.RotSpeed) * self.RotDir);
        pass

KeplerComposite.RegisterComponent(KeepRotating, 'KeepRotating')