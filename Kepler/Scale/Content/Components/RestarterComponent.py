#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class RestarterComponent:
  def Initialize(self):
    KeplerComposite.Connect(self.Space, "RestartSpace", self.OnRestart)

  def OnRestart(self):
    
    pass

KeplerComposite.RegisterComponent(RestarterComponent, 'Restarter')
