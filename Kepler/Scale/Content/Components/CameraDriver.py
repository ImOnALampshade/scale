#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys

from KeplerMath import *

#Serialized Properties
# Acceleration
# MaxSpeed
# VelocityAdjustRate
#Other Properties
# Throttle
# CurrentVelocity
# Speed

class CameraDriverComponent:
  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)
    KeplerComposite.Connect(self.Owner, "Awake", self.OnStart)
    self.Throttle = 0;
    self.Velocity = Vec3(0,0,0)
    self.Speed = 0;

  def OnStart(self):
    self.SetTargets();


  def SetTargets(self):
    self.Constraints = KeplerComposite.FindEntity(self.Owner, ".").PlayerConstraints
    pass

  def OnLogicUpdate(self, Dt):
    if(not self.IsLocked()):
      self.HandleKeyboardInput(Dt)
      self.ApplyForwardThrust(Dt)
    else:
      self.ControlLockedUpdate(Dt)
    pass

  def IsLocked(self):
    return self.Constraints.Ship.Stunnable.IsStunned()

  def HandleKeyboardInput(self, Dt):
    Keyboard = KeplerComposite.Game().Window
    if(Keyboard.IsKeyDown(Keys.KEY_W)):
      self.ModThrottle( self.Acceleration * Dt)
   
    if(Keyboard.IsKeyDown(Keys.KEY_S)):
      self.ModThrottle(-self.Acceleration * Dt)
    
    if(Keyboard.IsKeyDown(Keys.KEY_X)):
      self.Constraints.Ship.Stunnable.StunFor(0.5)
    

  def ModThrottle(self, amount):
    self.Throttle += amount
    self.Throttle = min(self.Throttle, 1)
    self.Throttle = max(self.Throttle, 0)

  def ApplyForwardThrust(self, Dt):
    move_Vec3 = self.GetTargetVelocity()
    self.Velocity = self.Velocity.lerp(move_Vec3, self.VelocityAdjustRate)
    self.Owner.Transform.Position = self.Owner.Transform.Position.add(self.Velocity.mults(Dt))
    self.Speed = self.Velocity.length();

  def GetTargetVelocity(self):
    speed = self.Throttle * self.MaxSpeed
    return self.Owner.Transform.Rotation.rotate(Vec3(0,0,-1).mults(speed));
  
  def ControlLockedUpdate(self, Dt):
    self.Constraints.Owner.Transform.WorldPosition = self.Constraints.Owner.Transform.WorldPosition.lerp(self.Constraints.Ship.Transform.WorldPosition, 0.7)
    pass

KeplerComposite.RegisterComponent(CameraDriverComponent, 'CameraDriver')
