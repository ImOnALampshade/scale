#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
import Keys

class MainMenuLoaderComponent:
  def Initialize(self):
    print ('Initializing')
    KeplerComposite.LoadRecipeFile('Archetypes/awesomium_test.json')
    KeplerComposite.CreateComposite('MainMenu', self.Owner)

KeplerComposite.RegisterComponent(MainMenuLoaderComponent, 'MainMenuLoader')
