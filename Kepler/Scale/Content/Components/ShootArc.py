import KeplerComposite, time, Keys
from KeplerMath import *
import math

class ShootArc:
    # Serialized Properties
    Bullet = "EnemyBullet"
    BulletSpeed = 20
    FireRate = 1
    OffsetDist = 1.0
    FireArc = 60.0
    BulletCount = 8

    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        
        self.FireDelay = 1/self.FireRate;
        self.Timer = self.FireDelay;
        
        # bounds check the fire arc
        if (self.FireArc < 0.0):
            self.FireArc = 0.0;
            pass
        if (self.FireArc > 360.0):
            self.FireArc = 360.0;
            pass
        pass

    def OnLogicUpdate(self, Dt):
        if (self.Timer > 0.0):
            self.Timer -= Dt;
            
            if (self.Timer <= 0.0):
                self.Shoot();
                self.Timer = self.FireDelay;
                pass
            pass
            
        pass
        
    def Shoot(self):
        if (self.BulletCount > 1):
            angleStep = 0;
            if (self.FireArc == 360.0):
                angleStep = self.FireArc/self.BulletCount;
                pass
            else:
                angleStep = self.FireArc/(self.BulletCount - 1);
                pass
                
            angleStartOffset = -self.FireArc / 2.0;
            baseShootVec = self.RotateVecByAngle(self.Owner.Orientation.WorldForward, angleStartOffset);
            
            for i in range(0, int(self.BulletCount) ):
                shootVec = self.RotateVecByAngle(baseShootVec, i * angleStep)
                
                self.CreateProjectile(shootVec);
                pass
        else:
            self.CreateProjectile(self.Owner.Orientation.WorldForward);
            pass
        pass
        
    def CreateProjectile(self, shootVec):
        spawnPosition = self.Owner.Transform.Position;
        spawnPosition = spawnPosition.add( shootVec.mults(self.OffsetDist) );
        
        createdProjectile = KeplerComposite.CreateComposite(self.Bullet, self.Space);
        createdProjectile.Transform.Position = spawnPosition;
        #createdProjectile.Orientation.LookAtPoint(self.Owner.Transform.Translation.add(shootVec.mults(self.OffsetDist * 2)));
        
        createdProjectile.RigidBody.Velocity = shootVec.mults(self.BulletSpeed);
        pass

    def RotateVecByAngle(self, vec, angle):
        vecAxis = Vec3(0,0,1);
        quatAxisAngle = QuatFromAxisAngle(vecAxis, math.radians(angle));
        
        return quatAxisAngle.rotate( vec );

KeplerComposite.RegisterComponent(ShootArc, "ShootArc")