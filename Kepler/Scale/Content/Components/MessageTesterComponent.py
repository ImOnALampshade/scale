#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite

class MessageTesterComponent:
  def Initialize(self):
    KeplerComposite.Connect(self.Owner, 'PrintOutSomething', self.Printer)

  def Printer(self, str1, str2):
    print('{}.{}'.format(str1, str2))

KeplerComposite.RegisterComponent(MessageTesterComponent, 'MessageTester')
