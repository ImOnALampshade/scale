#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
import math
from KeplerMath import *

class BombBulletComponent:
  BulletToSpawn = 'Bullet'
  OriginOffset = 3
  BulletSpeed = 16
  NumberToSpawn = 24
  Spawned = 0
  DeltaTheta = 15
  TimeOut = 1
  
  def Initialize(self):
    self.Timer = 0

    KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollision)
    KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
    pass

  def OnCollision(self, Composite):
    #self.Explode()
    pass

  def OnLogicUpdate(self, Dt):
    if(self.Timer > self.TimeOut):
      self.Explode()
    else:
      self.Timer += Dt
    pass

  def Explode(self):
    spawned = 0
    theta = 0
    while(self.Spawned < self.NumberToSpawn):
      direction = Vec3(math.cos(math.radians(theta)), math.sin(math.radians(theta)), 0)
      direction = direction.normalized()

      bullet = KeplerComposite.CreateComposite(self.BulletToSpawn, self.Space)
      bullet.Transform.Position = self.Owner.Transform.WorldPosition.add(direction.mults(self.OriginOffset))
      bullet.RigidBody.Velocity = direction.mults(self.BulletSpeed)

      theta += self.DeltaTheta
      self.Spawned += 1
    self.Owner.Destroy()



KeplerComposite.RegisterComponent(BombBulletComponent, "BombBullet")