#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
from KeplerComposite import FindEntity


#Serialized Properties:
# self.TurnRate = Property.Float(3);
# self.Acceleration = Property.Float(300);

class HomingComponent:
    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        self.Target = None;
        self.TargetPos = Vec3(-3, 0, -8);
        pass

    def OnLogicUpdate(self, Dt):
        #self.Target = self.Space.FindObjectByName("PlayerShip");
        if(self.Target != None):
            self.FaceTowardTarget(Dt);
            self.ApplyDrag(Dt);
            self.Accelerate(Dt);
            #print(self.Target.Transform.Position)
            pass
        else:
            #self.Target = self.Space.FindObjectByName("PlayerShip");
            self.Target = FindEntity(self.Owner, "~/PlayerRoot/PlayerShip")
            print(self.Target)
            pass

        #self.TestHoming(Dt);
        #self.Owner.RigidBody.ApplyForce(Vec3(0,0,1), 1)
        #print("end")
        pass

    def TestHoming(self, Dt):
        self.FaceTowardTarget(Dt);
        self.ApplyDrag(Dt);
        self.Accelerate(Dt);
        
    def FaceTowardTarget(self, Dt):
        target_Quat = self.Owner.Orientation.GetLookAtPointRotation(self.Target.Transform.Position);
        target_Quat = self.Owner.Transform.Rotation.toward(target_Quat, self.TurnRate*Dt);
        self.Owner.Transform.Rotation = self.Owner.Transform.Rotation.slerp(target_Quat, 0.2);
        
    def ApplyDrag(self, Dt):
        #self.Owner.RigidBody.Velocity *= 1-Dt; # original
        self.Owner.RigidBody.Velocity = self.Owner.RigidBody.Velocity.mults(1-Dt);
        #self.Owner.RigidBody.AngularVelocity *= 0.8; # original
        self.Owner.RigidBody.RotVelocity = self.Owner.RigidBody.RotVelocity.mults(0.8);

        
    def Accelerate(self, Dt):
        #idealForward = self.Target.Transform.Translation - self.Owner.Transform.Translation;
        idealForward = self.Target.Transform.Position.subtr(self.Owner.Transform.Position);
        idealForward.normalize();
        currentForward = self.Owner.Orientation.WorldForward;
        #self.Owner.RigidBody.Velocity += self.Owner.Transform.Rotation.rotate(self.Owner.Orientation.LocalForward) * self.Acceleration * Dt * idealForward.dot(currentForward);
        scalar = self.Acceleration * Dt * idealForward.dot(currentForward);
        adder = self.Owner.Transform.Rotation.rotate(self.Owner.Orientation.Forward).mults(scalar)
        self.Owner.RigidBody.Velocity = self.Owner.RigidBody.Velocity.add( adder );

KeplerComposite.RegisterComponent(HomingComponent, 'Homing')