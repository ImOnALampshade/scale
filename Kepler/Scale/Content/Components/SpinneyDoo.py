import KeplerComposite, time, Keys
from KeplerMath import *
import math

#Serialized Properties
# Speed = Property.Float(5.0);

class SpinneyDooComponent:
    Speed = 2
    SpeedOscillationFactor = 5
    SpeedOscillationRate = 1;
    DirectionSpeed = 1
    DirectionOscillationFactor = 1;
    DirectionOscillationRate = 1;
    
    
    
    #non serialize
    TimeElapsedDirection = 0.0
    TimeElapsedSpeed = 0.0
    CurrentSpeed = 0.0
    

    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        pass

    def OnLogicUpdate(self, Dt):
        self.TimeElapsedDirection += Dt * self.DirectionOscillationRate;
        self.TimeElapsedSpeed += Dt * self.SpeedOscillationRate;
        
        self.CurrentSpeed = self.Speed + ( (math.sin( self.TimeElapsedSpeed ) + 1) * self.SpeedOscillationFactor * self.Speed );
        self.Owner.DirectionData.SetDirectionAngle( self.Owner.DirectionData.DirectionAngle + (self.DirectionSpeed + ( (math.sin( self.TimeElapsedDirection ) + 1 ) * self.DirectionOscillationFactor * self.DirectionSpeed) ));
        self.Owner.RigidBody.Velocity = self.Owner.DirectionData.DirVec.mults(self.CurrentSpeed)
        pass
        

KeplerComposite.RegisterComponent(SpinneyDooComponent, 'SpinneyDoo')