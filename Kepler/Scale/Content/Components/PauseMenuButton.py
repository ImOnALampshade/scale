#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys

class PauseTestComponent:
  def Initialize(self):
    #KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
    KeplerComposite.Connect(self.Game, "KeyDown", self.OnKeyDown)
    KeplerComposite.Connect(self.Game, "WindowFocus", self.OnRefocus)
    KeplerComposite.Connect(self.Game, "WindowUnfocus", self.OnUnfocus)
    KeplerComposite.Connect(self.Game, "PauseEvent", self.OnPause)
    KeplerComposite.Connect(self.Game, "Update", self.OnUpdate)
    self.PauseState = 0;
    self.PauseIn = -1
    pass
    
  #def OnLogicUpdate(self, Dt):
  #  pass

  def OnUpdate(self):
    if(self.PauseIn >= 0):
      self.PauseIn -= 1;
    if(self.PauseIn == 0 and self.PauseState == 0):
      self.TryPause()


  def OnRefocus(self):
    self.PauseIn = 30;
    

  def OnUnfocus(self):
    self.Space.SpaceUpdater.Paused = 1
    self.Game.AudioEngine.SetMasterPause(True)
    
  def OnKeyDown(self, key, mods):
    if(key == Keys.KEY_ESCAPE):
      self.TryPause();
    pass

  def OnPause(self, pauseSpace):
    self.PauseState = 2
    pass
  
  def TryPause(self):
    if(self.PauseState == 0):
      self.Space.SpaceUpdater.Paused = 1
      self.Game.AudioEngine.SetMasterPause(True)
      NewSpace = KeplerComposite.CreateComposite('PauseMenu', self.Game)
      NewSpace.PauseMenu.PausedSpace = self.Space
      self.PauseSpace = NewSpace
      self.PauseState = 1
      
    elif(self.PauseState == 1 and self.PauseSpace != None):
      self.PauseSpace.Dispatch('Resume')
      self.PauseSpace = None;
      self.PauseState = 0

    else:
      self.Game.DispatchDown('GoToMainMenu')
      self.PauseSpace = None
      self.PauseState = 0
    
    
KeplerComposite.RegisterComponent(PauseTestComponent, "PauseMenuButton")