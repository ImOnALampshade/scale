#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
import Keys

#Serialized Properties
    # LevelSong
    # MouseOver
    # MouseSelect
#Other Properties


class AudioDJComponent:
    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "Awake", self.OnStart)
        KeplerComposite.Connect(self.Space, "MouseOver", self.OnMouseOver)
        KeplerComposite.Connect(self.Space, "MouseSelect", self.OnMouseSelect)
        print("The DJ is in the house!")
        pass
        
    def OnStart(self):
        if(self.LevelSong != ""):
            self.Owner.Audio.PlayLongSound(self.LevelSong)
        pass 
        
    def OnMouseOver(self):
        if(self.MouseOver != ""):
            self.Owner.Audio.PlayShortSound(self.MouseOver)
        pass
        
    def OnMouseSelect(self):
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        if(self.MouseSelect != ""):
            print(self.MouseSelect)
            self.Owner.Audio.PlayShortSound(self.MouseSelect)
        pass
        
KeplerComposite.RegisterComponent(AudioDJComponent, 'LevelDJ')