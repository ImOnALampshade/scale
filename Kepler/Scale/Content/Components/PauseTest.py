#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys

class PauseTestComponent:
  def Initialize(self):
    #KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
    KeplerComposite.Connect(self.Game, "KeyDown", self.OnKeyDown)
    pass
    
  #def OnLogicUpdate(self, Dt):
  #  pass
    
  def OnKeyDown(self, key, mods):
    if(key == Keys.KEY_M):
      self.Space.SpaceUpdater.Paused = 1
      NewSpace = KeplerComposite.CreateComposite('PauseMenu', self.Game)
      NewSpace.PauseMenu.PausedSpace = self.Space
      
    
    
KeplerComposite.RegisterComponent(PauseTestComponent, "PauseTest")