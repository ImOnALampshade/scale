#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class WorldWrapperComponent:
  height = 10
  width  = 14*(16/12)

  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)

  def OnLogicUpdate(self, Dt):
    pos = self.Owner.Transform.Position
    if(pos.x > self.width):
      pos.x -= 2*self.width
    elif(pos.x < -self.width):
      pos.x += 2*self.width

    if(pos.y > self.height):
      pos.y -= 2*self.height
    elif(pos.y < -self.height):
      pos.y += 2*self.height

    self.Owner.Transform.Position = pos
    pass

KeplerComposite.RegisterComponent(WorldWrapperComponent, 'WorldWrapper')