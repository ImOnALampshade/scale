#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys
import math
from KeplerComposite import FindEntity
from KeplerMath import *


class ShootComponent:

    def Initialize(self):
      self.WeaponType = 'BasicShot'
      self.CurrentBulletSpeed = 100
      self.BasicBulletSpeed = 100
      self.BombBulletSpeed = 3
      self.SpreadBulletSpeed = 75
      self.OriginOffset = 1
      self.RefireRate = .1
      self.RefireShift = 0

      self.CameraPath = "~/WorldCamera"
      self.Target = Vec3(0,0,0);
      KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
      
      KeplerComposite.Connect(self.Game, 'MouseDown', self.OnMouseDown)
      KeplerComposite.Connect(self.Game, 'MouseUp', self.OnMouseUp)
      KeplerComposite.Connect(self.Game, 'KeyDown', self.OnKeyDown)
      KeplerComposite.Connect(self.Game, 'KeyUp',   self.OnKeyUp)
  
      KeplerComposite.Connect(self.Owner, 'WeaponChanged', self.OnWeaponChanged)

    def OnLogicUpdate(self, Dt):
        mouseScreenPos = self.Game.Window.GetMousePos()
        if(math.isinf(mouseScreenPos.x)):
          return
        camera = FindEntity(self.Owner, self.CameraPath)
        worldRay = camera.Camera.ScreenToWorld(mouseScreenPos)
        self.Target = worldRay.SampleAtDistance(-40/worldRay.Direction.z)

    
    def OnMouseDown(self, button, mods):
      if(button == Keys.KEY_SPACE):
        self.Owner.DispatchDown('StartShooting', self.Owner.Weapons.CurrentWeaponType)

    def OnMouseUp(self, button, mods):
      if(button == Keys.KEY_SPACE):
        self.Owner.DispatchDown('StopShooting')

    def OnKeyDown(self, button, mods):
      if(button == Keys.KEY_SPACE):
        self.Owner.DispatchDown('StartShooting', self.Owner.Weapons.CurrentWeaponType)

    def OnKeyUp(self, button, mods):
      if(button == Keys.KEY_SPACE):
        self.Owner.DispatchDown('StopShooting')


    def OnWeaponChanged(self):
      self.Owner.DispatchDown('StopShooting')
        
KeplerComposite.RegisterComponent(ShootComponent, "Shoot")