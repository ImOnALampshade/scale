#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys

from KeplerMath import *

#Serialized
#   self.MaxSpeed = Property.Float(75);
#   self.Maneuvering = Propert

class ShipEnginesComponent:
  def Initialize(self):
    KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
    self.Ship = self.Owner.ShipSubsystem.Ship;
    pass

  def OnLogicUpdate(self, Dt):
    self.UpdateParticleProperties();
    pass
  
  def UpdateParticleProperties(self):
    throttle = self.Ship.PlayerController2D.LastThrottle.length();
    particles = self.Owner.ParticleSystemHack;
    particles.SetParticleLifetime(0.15*throttle, 0.4*throttle);
    particles.SetEmissionRate(120*throttle);
    pass

KeplerComposite.RegisterComponent(ShipEnginesComponent, "ShipEngines")