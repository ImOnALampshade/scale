#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys
from KeplerComposite import FindEntity

class CheaterComponent:

  def Initialize(self):
    self.Player = FindEntity(self.Owner, "~/PlayerShip")
    self.GameWaveManager = FindEntity(self.Owner, "~/WaveATron")
    self.DJ = FindEntity(self.Owner, "~/DJComposite")
    KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)

  def OnLogicUpdate(self, Dt):
    self.Player = FindEntity(self.Owner, "~/PlayerShip")

    if(self.Player):
      Keyboard = KeplerComposite.Game().Window
      if(Keyboard.IsKeyDown(Keys.KEY_F1) and self.Player.Weapons.CurrentWeaponType != 'BombShot'):
        self.Player.Dispatch('BombShot', 50000)
  
      if(Keyboard.IsKeyDown(Keys.KEY_F2) and self.Player.Weapons.CurrentWeaponType != 'SpreadShot'):
        self.Player.Dispatch('SpreadShot', 150000000)
  
      if(Keyboard.IsKeyDown(Keys.KEY_F3)):
        self.Player.Dispatch('Recover', 100000)
  
      if(Keyboard.IsKeyDown(Keys.KEY_F4)):
        self.GameWaveManager.WaveATron.StartNextWave = True;

      if(Keyboard.IsKeyDown(Keys.KEY_F5)):
        self.Player.Dispatch('TakeDamageEvent', 100000)
      pass
    pass
    


KeplerComposite.RegisterComponent(CheaterComponent, 'Cheater')

        #cheat to get bomb bullets
        #Keyboard = KeplerComposite.Game().Window
        #if(Keyboard.IsKeyDown(Keys.KEY_O) and self.Owner.Weapons.CurrentWeaponType != 'BombShot'):
        #  self.Owner.Dispatch('BombShot', 5)
        #if(Keyboard.IsKeyDown(Keys.KEY_I) and self.Owner.Weapons.CurrentWeaponType != 'SpreadShot'):
        #  self.Owner.Dispatch('SpreadShot', 150)
        #pass