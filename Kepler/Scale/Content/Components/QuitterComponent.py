#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class QuitterComponent:
  def Initialize(self):
    KeplerComposite.Connect(KeplerComposite.Game(), "KeyDown", self.OnKeyDown)

  def OnKeyDown(self, key, mods):
    if key == Keys.KEY_ESCAPE:
      KeplerComposite.Game().Game.ExitGame();
    if key == Keys.KEY_P:
      if(KeplerComposite.Game().LogicUpdate.Paused == 0):
        KeplerComposite.Game().LogicUpdate.Paused = 1
      else:
        KeplerComposite.Game().LogicUpdate.Paused = 0
    pass

KeplerComposite.RegisterComponent(QuitterComponent, 'Quitter')
