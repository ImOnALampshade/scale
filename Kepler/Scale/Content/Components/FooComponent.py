#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys
from KeplerMath import *

class FooComponent:
  def Initialize(self):
    print("in foo init")
    #KeplerComposite.Connect(Game.GameSession, 'Start', self.Start)

  def Start(self):
    pass

KeplerComposite.RegisterComponent(FooComponent, 'Foo')
