#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#Serialized:
#   self.Mode = Property.String("User")

class ManeuveringComponent:
  def Initialize(self):
    self.DesiredThrust = Vec3(0, 0, 0);
    self.FacingOffset = Vec3(0, 0, 0);
    self.Mode = "User"
    pass

  def SetFacingTarget(self, target, source = ""):
    if(source == "" or source == self.Mode):
      self.FacingOffset = target.subtr(self.Owner.Transform.WorldPosition);

  def SetFacingOffset(self, target, source = ""):
    if(source == "" or source == self.Mode):
      self.FacingOffset = target;

  def SetDesiredThrust(self, thrust, source = ""):
    if(source == "" or source == self.Mode):
      self.DesiredThrust = thrust;

KeplerComposite.RegisterComponent(ManeuveringComponent, "Maneuvering")
