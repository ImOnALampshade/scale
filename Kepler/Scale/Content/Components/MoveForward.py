#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *


class MoveForward:
    # Serialized Properties:
    Speed = 5.0

    def Initialize(self):
        KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
        pass

    def OnLogicUpdate(self, Dt):
        self.Move();
        pass
        
    def Move(self):
        forward = self.Owner.Orientation.WorldForward;
        self.Owner.RigidBody.Velocity = forward.mults(self.Speed);
        pass

KeplerComposite.RegisterComponent(MoveForward, "MoveForward")