#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite

class PauseMenuComponent:
  def Initialize(self):
    self.PausedSpace = 0
    KeplerComposite.Connect(self.Owner, 'Resume', self.OnResume)
    KeplerComposite.Connect(self.Owner, 'GoToMainMenu', self.OnGoToMainMenu)
    KeplerComposite.Connect(self.Owner, 'Quit', self.OnQuit)
    KeplerComposite.Connect(self.Owner, 'ToggleFullscreen', self.ToggleFullscreen)
    KeplerComposite.Connect(self.Owner, 'RestartGame', self.OnRestartGame)
    
  def ToggleFullscreen(self):
    window = self.Game.Window

    if window.Fullscreen == 1:
      window.Fullscreen = 0
    else:
      window.Fullscreen = 1
  def OnResume(self):
    print("Destroying", self.Space.Name(), "and unpausing", self.PausedSpace.Name())
    self.Game.AudioEngine.SetMasterPause(False)
    self.PausedSpace.SpaceUpdater.Paused = 0
    self.Space.Destroy()
    

  def OnGoToMainMenu(self):
    self.PausedSpace.Destroy()
    self.Game.FindEntity("/HUD").Destroy();
    KeplerComposite.CreateComposite('MainMenu', KeplerComposite.Game())
    self.Space.Destroy()
  
  def OnRestartGame(self):
    self.PausedSpace.Destroy()
    self.Game.FindEntity("/HUD").Destroy();
    self.Space.Destroy()
    KeplerComposite.CreateComposite('LoadingScreenSpace', KeplerComposite.Game())
  
  def OnQuit(self):
    KeplerComposite.Game().Game.ExitGame()


KeplerComposite.RegisterComponent(PauseMenuComponent, "PauseMenu")