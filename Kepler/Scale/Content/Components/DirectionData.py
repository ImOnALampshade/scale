#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
from KeplerMath import *
import math

#Serialized Properties
# DirectionAngle = Property.Float(0.0);

class DirectionDataComponent:
    DirectionAngle = 0.0

    def Initialize(self):
        self.DirectionAngle = math.radians(self.DirectionAngle)
        self.DirVec = self.GetDirVec();
        self.DirVec = self.DirVec.normalized();
        pass
        
    def SetDirectionAngle(self, directionAngle ):
      self.DirectionAngle = directionAngle
      self.DirVec = self.GetDirVec();
        
    def GetDirVec(self):
        vecAxis = Vec3(0,0,1);
        quatAxisAngle = QuatFromAxisAngle(vecAxis, self.DirectionAngle);
        #print(quatAxisAngle);
        
        return quatAxisAngle.rotate( Vec3(1,0,0) );
        pass

KeplerComposite.RegisterComponent(DirectionDataComponent, 'DirectionData')