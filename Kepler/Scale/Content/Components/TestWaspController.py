#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class TestWaspControllerComponent:
    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "DeathEvent", self.OnDeathEvent)
        pass

    def OnDeathEvent(self, destroyer):
        print("I'm Dead!!!!")
        pass


KeplerComposite.RegisterComponent(TestWaspControllerComponent, 'TestWaspController')