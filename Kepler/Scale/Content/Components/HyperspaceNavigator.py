#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
from KeplerComposite import FindEntity

class HyperspaceNavigatorComponent:
  def Initialize(self):
    self.Ship = self.Owner.ShipSubsystem.Ship
    self.Root = KeplerComposite.FindEntity(self.Owner, "~/PlayerRoot")
    self.Data = KeplerComposite.FindEntity(self.Owner, "/PlayerDataSpace").PlayerData

    KeplerComposite.Connect(self.Owner, "Awake", self.OnAwake)
    KeplerComposite.Connect(self.Ship, "HyperspaceContact", self.OnHyperspaceContact)
    pass

  def OnAwake(self):
    print(self.Ship, "\n" , self.Root, "\n", self.Data)

    if self.Data.ExitingHyperspace:
      hyperspaceExit = KeplerComposite.FindEntity(self, "~/" + self.Data.hyperspaceExit)
      player = FindEntity(self.Root, "CameraRoot")
    pass

  def OnHyperspaceContact(self, otherEntity):
    print("Collided with portal")
    pass

KeplerComposite.RegisterComponent(HyperspaceNavigatorComponent, "HyperspaceNavigator")