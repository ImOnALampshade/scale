#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
from KeplerComposite import FindEntity

#Serialized properties
    #WeaponType
    #Ammo
    #PickupSound

class WeaponCollectableComponent:
  def Initialize(self):
    self.MoveSpeed = 1
    KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollision)
    KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
    pass

  #set move speed to a constant
  def OnLogicUpdate(self, Dt):
    direction = self.Owner.RigidBody.Velocity
    direction = direction.normalized()
    self.Owner.RigidBody.Velocity = direction.mults(self.MoveSpeed)
    pass

  def OnCollision(self, Composite):
    player = FindEntity(self.Owner, "~/PlayerShip")
    if(player.Name() == Composite.Name()):
      player.Dispatch(self.WeaponType, self.Ammo)
      self.Owner.Audio.PlayShortSound(self.PickupSound)
      self.Owner.Destroy()
    pass


KeplerComposite.RegisterComponent(WeaponCollectableComponent, "WeaponCollectable")