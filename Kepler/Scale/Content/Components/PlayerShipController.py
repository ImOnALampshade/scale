#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *
import math

#Serialized properties:
#   self.FudgeSpeed       = Property.Float(0.7);
#   self.FudgeThreshold   = Property.Float(0.4);
#   self.RotationFixRate  = Property.Float(2);
#   self.BankRate         = Property.Float(10);
#   self.ConstraintsPath  = Property.String()
#   self.StretchBy        = Property.Float()

class PlayerShipControllerComponent:
  def Initialize(self):
    self.ControllerEntity = KeplerComposite.FindEntity(self.Owner, "../CameraOrigin")

    self.Constraints = self.ControllerEntity.PlayerConstraints;
    #FIXME: Should not have to do this
    self.Constraints.SetTargets()

    self.Steerer = self.ControllerEntity.CameraSteerer
    self.Driver = self.ControllerEntity.CameraDriver
    self.Ship = self.Constraints.Ship;
    self.Maneuvering = self.Ship.Maneuvering;

    KeplerComposite.Connect(self.Space, "LogicUpdate", self.OnLogicUpdate)
    KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollide)

    self.TargetPos = self.Ship.Transform.WorldPosition;
    self.AheadPos = Vec3(0,0,0);
    self.StunTime = 0;
    self.lastRotation = None;
    self.Maneuvering.SetDesiredThrust(Vec3(0, 0, 0));

    self.Stand = self.Constraints.Tripod;
    self.Rest = self.Constraints.Owner;

    self.Ship.Transform.WorldPosition = self.Rest.Transform.WorldPosition
    self.Ship.Transform.WorldRotation = self.Stand.Transform.WorldRotation;
    self.DetermineForwardPoint(0);

    self.Camera = self.Constraints.Camera;

  def OnCollide(self, Other):
    #if(self.Other.Bullet == None):
    #  pass
    self.Owner.Dispatch("Stun", 0.6)

  def OnLogicUpdate(self, Dt):
    if(self.Maneuvering.Mode == "User"):
      targetStandBias = self.GetBias(self.GetDriverThrottle());
      self.ApplyCameraFudge(Dt, self.AheadPos, targetStandBias);
      self.DetermineForwardPoint(targetStandBias);
      forward_Quat = self.Ship.Orientation.GetLookAtPointWithUpRotation(self.AheadPos, self.Ship.Orientation.WorldUp);

      if(self.StunTime <= 0):
        self.RequestThrust(Dt, targetStandBias);
        #forward_Quat = self.Ship.Transform.WorldRotation.toward(forward_Quat, 10*Dt);
        self.ApplyBanking(Dt);
        forward_Quat = self.Ship.Transform.WorldRotation.slerp(forward_Quat, 0.5);
        self.Ship.Transform.WorldRotation = forward_Quat

    else:
      print(self.Maneuvering.Mode)

      #else:
      #  self.StunTime -= Dt;
      #  self.Ship.RigidBody.AngularVelocity *= (1-(Dt * 4));
      #  if(self.StunTime <= 0.1):
      #    self.Constraints.ConstrainStandToTarget(999, 0.9);
      #    self.Ship.Transform.WorldRotation = self.Ship.Transform.WorldRotation.slerp(forward_Quat, 0.1);
      #    self.Ship.Maneuvering.SetDesiredThrust(Vec3(0, 0, 0), Maneuvering.Input.PlayerBasic);

  def RequestThrust(self, Dt, bias):
    max_Vec3 = self.AheadPos.subtr(self.Ship.Transform.WorldPosition);
    maxSpeed = self.GetDriverSpeed() + 1;
    maxSpeed *= (1-bias)
    stretchFactor = (self.GetDriverThrottle() * self.StretchBy) + (1 - self.GetDriverThrottle());

    aspectRatio = KeplerComposite.Game().Window.AspectRatio
    distortion_Vec3 = Vec3(aspectRatio, 1, 1)

    desiredVeloc_Vec3 = self.GetDesiredDeltaPosition(max_Vec3, distortion_Vec3, self.FollowDistance*stretchFactor, maxSpeed);
    thrust_Vec3 = desiredVeloc_Vec3

    self.Ship.Maneuvering.SetDesiredThrust(thrust_Vec3, "User")
    self.Ship.Maneuvering.SetFacingTarget(self.AheadPos)

  def GetDesiredDeltaPosition(self, max_Vec3, distortion_Vec3, maxFollowDistance, maxSpeed):
    delta_Vec3 = Vec3(0,0,0);

    max_Vec3 = self.Stand.Transform.WorldRotation.inverted().rotate(max_Vec3);

    if(max_Vec3.length() < maxFollowDistance):
      delta_Vec3 = Vec3(0,0,0)
    else:
      delta_Vec3 = max_Vec3.subtr(max_Vec3.normalized().mults(maxFollowDistance));
    if(delta_Vec3.length() > maxSpeed):
      delta_Vec3 = delta_Vec3.normalized().mults(maxSpeed);

    delta_Vec3 = delta_Vec3.mult(distortion_Vec3);

    delta_Vec3 = self.Stand.Transform.WorldRotation.rotate(delta_Vec3);
    return delta_Vec3;

  def ApplyBanking(self, Dt):
    forward_Quat = self.Ship.Transform.WorldRotation;

    bankFactor = math.fabs(self.Steerer.MouseVector.length())

    #Up toward camera
    towardCamera = self.Stand.Transform.WorldPosition.subtr(self.Ship.Transform.WorldPosition)
    bank_Quat = self.Ship.Orientation.GetLookAtDirectionWithUpRotation(self.Ship.Orientation.WorldForward, towardCamera);
    bank_Quat_down = self.Ship.Orientation.GetLookAtDirectionWithUpRotation(self.Ship.Orientation.WorldForward, towardCamera.mults(-1));

    #Up as camera up
    flat_Quat = self.Ship.Orientation.GetLookAtDirectionWithUpRotation(self.Ship.Orientation.WorldForward, self.Stand.Orientation.WorldUp);

    # This works in Zero, not here.
    up =    bank_Quat.between(self.Ship.Transform.WorldRotation).angle
    down =  bank_Quat_down.between(self.Ship.Transform.WorldRotation).angle
    diff = (abs(up) - abs(down)) / math.pi
    #print("Up  ", up, "\nDown", down, "\ndiff", diff)
    #print((up - down) / math.pi)
    if(diff > (.7)):
      bank_Quat = bank_Quat_down
    else:
      pass

    rot_Quat = forward_Quat.slerp(flat_Quat, Dt * (1-bankFactor) * self.RotationFixRate);
    rot_Quat = rot_Quat.slerp(bank_Quat, Dt * bankFactor * self.BankRate);
    rot_Quat = self.Ship.Transform.WorldRotation.slerp(rot_Quat, 0.8)
    self.Ship.Transform.WorldRotation = rot_Quat;
    pass

  def ApplyCameraFudge(self, Dt, target_Vec3, targetStandBias):
    if(self.StunTime > 0):
      targetStandBias = 1;

    if(targetStandBias <= 0):
      self.lastRotation = None;
      return 0;

    nextRotation = self.Stand.Transform.WorldRotation;
    if(self.lastRotation == None):
      self.lastRotation = nextRotation;
      return targetStandBias;

    rotateBy = nextRotation.mult(self.lastRotation.inverted());
    rotateBy = Quat().slerp(rotateBy, targetStandBias)

    targetPosition = self.Rest.Transform.WorldPosition
    offsetPosition = self.Ship.Transform.WorldPosition;
    self.Constraints.TranslateSystem(offsetPosition.subtr(targetPosition).mults(0.1));

    self.Constraints.RotateSystemPositionsAroundPoint(self.Ship.Transform.WorldPosition, rotateBy);

    self.lastRotation = nextRotation;
    return targetStandBias;

  def GetBias(self, actualSpeed):
    targetStandBias = max(self.FudgeThreshold - actualSpeed, 0) / self.FudgeThreshold;
    if(self.StunTime > 0):
      targetStandBias = 1;
    return targetStandBias;

  def GetDriverThrottle(self):
    return self.Driver.Throttle
  def GetDriverSpeed(self):
    return self.Driver.Speed

  def DetermineForwardPoint(self, targetStandBias):
    self.FollowDistance = self.Constraints.MoveTargetDistance
    #self.OverrideForwardPoint(targetStandBias);
    #self.Constraints.ConstrainTargetToStand(999, 1.0 - targetStandBias);
    #self.Constraints.ConstrainStandToTarget(999, 1.0);

    stretchFactor = self.GetDriverThrottle() * (1-targetStandBias);
    stretchFactor = stretchFactor*(self.StretchBy) + (1-stretchFactor);
    self.Constraints.TargetStretch = stretchFactor;
    self.Constraints.ApplyConstraints();
    self.AheadPos  = self.Constraints.Target.Transform.WorldPosition;
    #self.AheadPos = self.AheadPos.mults(stretchFactor);
    #self.AheadPos = self.AheadPos.add(self.Constraints.Owner.Transform.WorldPosition)
    #if(self.StunTime <= 0):
    #    self.AheadPos += self.Stand.Transform.WorldRotation.rotate(Vec3(0,0,-1) * (stretchFactor-1));

KeplerComposite.RegisterComponent(PlayerShipControllerComponent, "PlayerShipController")
