#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#Serialized Properties:
# "Health": 10,
# "DamageOnCollide": 0 or 1

class ExplodeOnDeath:
    ExplosionArchetype = "Explosion1"

    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "DeathEvent", self.OnDeath)
        pass
    

    def OnDeath(self, destroyer):
        explosion = KeplerComposite.CreateComposite(self.ExplosionArchetype, self.Space)
        explosion.Transform.Position = self.Owner.Transform.Position
        self.Owner.Audio.PlayShortSound("Explosion_Big_01")
        self.Owner.Audio.PlayShortSound("Explosion_Big_01")
        self.Owner.Audio.PlayShortSound("Explosion_Big_01")
        pass



KeplerComposite.RegisterComponent(ExplodeOnDeath, 'ExplodeOnDeath')