#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys

from KeplerMath import *

#Serialized
#   self.MaxSpeed = Property.Float(75);

class VelocityController:
  MaxSpeed = 75
  DriftFactor = 0

  def Initialize(self):
    KeplerComposite.Connect(self.Owner, 'LogicUpdate', self.OnLogicUpdate)
    self.Ship = self.Owner;
    self.Maneuvering = self.Ship.Maneuvering;
    #self.Cockpit = KeplerComposite.FindEntity(self.Owner, "..");
    self.AngularVelocity = Vec3(0, 0, 0)

  def OnLogicUpdate(self, Dt):
    if(not self.Ship.Stunnable.IsStunned()):
      self.ApplyThrust(Dt)
      self.FaceAhead(Dt)
    pass

  def ApplyThrust(self, Dt):
    thrust_Vec3 = self.Ship.Maneuvering.DesiredThrust;
    currentVeloc_Vec3 = self.Ship.RigidBody.Velocity;
    newVeloc_Vec3 = thrust_Vec3.mults(60);
    
    if(newVeloc_Vec3.length() > self.MaxSpeed):
        newVeloc_Vec3 = newVeloc_Vec3.normalized().mults(self.MaxSpeed);
        
    newVeloc_Vec3 = currentVeloc_Vec3.lerp(newVeloc_Vec3, 1 - self.DriftFactor);
    self.Ship.RigidBody.Velocity = newVeloc_Vec3
    self.AngularVelocity = Vec3(0,0,0);
    pass
  
  def UpdateParticleProperties(self, Dt):
      pass
  
  def FaceAhead(self, Dt):
    orient = self.Ship.Orientation;
    facingOffset = self.Maneuvering.FacingOffset;
    worldUp = self.Ship.Orientation.WorldUp;
    target_Quat = self.Ship.Orientation.GetLookAtDirectionWithUpRotation(self.Ship.Maneuvering.FacingOffset, self.Ship.Orientation.WorldUp);
    target_Quat = self.Ship.Transform.Rotation.slerp(target_Quat, 0.1);
    #self.Ship.Transform.Rotation = target_Quat;

KeplerComposite.RegisterComponent(VelocityController, "VelocityController")