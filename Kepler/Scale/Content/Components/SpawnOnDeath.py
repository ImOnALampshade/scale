#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
from KeplerComposite import FindEntity

class SpawnOnDeathComponent:
  def Initialize(self):
    self.MoveSpeed = 1
    self.HasSpawned = False

    KeplerComposite.Connect(self.Owner, 'DeathEvent', self.OnDeath)

  def OnDeath(self, destroyer):
    if not self.HasSpawned:
        player = FindEntity(self.Owner, "~/PlayerShip")
        spawned = KeplerComposite.CreateComposite(self.WhatToSpawn, self.Space)
        spawned.RigidBody.Position = self.Owner.Transform.WorldPosition
        direction = player.Transform.WorldPosition.subtr(spawned.Transform.Position)
        direction = direction.normalized()
        spawned.RigidBody.Velocity = direction.mults(self.MoveSpeed)
        self.HasSpawned = True
        self.Owner.Audio.PlayShortSound("Explosion_Big_01")
        self.Owner.Audio.PlayShortSound("Explosion_Big_01")
        self.Owner.Audio.PlayShortSound("Explosion_Big_01")



KeplerComposite.RegisterComponent(SpawnOnDeathComponent, "SpawnOnDeath")