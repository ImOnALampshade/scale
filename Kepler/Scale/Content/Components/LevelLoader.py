import KeplerComposite, time, Keys

from KeplerMath import *

class LevelLoader:
  Outro = False
  counter = 0
  
  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.LogicUpdate)
    pass

  def LogicUpdate(self, dt):
    if(self.counter < 30): #wait a bit so that screen shows 
      self.counter = self.counter + 1
      return
    space = KeplerComposite.CreateComposite('space', KeplerComposite.Game())
    #KeplerComposite.CreateComposite('PlayerRoot', space)
    
    space.PhysicsSystem.LoadCollisionLayerTable('CollisionLayerTables/TestTable.txt')
    KeplerComposite.CreateInstance('Instances/GAM300Main.json', space)
    HUDspace = KeplerComposite.CreateComposite('HUDSpace', KeplerComposite.Game())
    #KeplerComposite.CreateInstance('Instances/ParticleHackTest.json', space)
    #KeplerComposite.CreateComposite('CometEnemy', space)
    #KeplerComposite.CreateComposite('KitingEnemy', space)
    #KeplerComposite.CreateComposite('HomingEnemy', space)
    
    self.Space.Destroy()
    space.DispatchDown("Start")
    print ("Switch to game")
    pass

KeplerComposite.RegisterComponent(LevelLoader, 'LevelLoader')
