#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class DestroyOnCollide:
    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollisionStarted);
        pass

    def OnCollisionStarted(self, Other):
        if(self.Owner.Health == None):
          self.Owner.Destroy();
        else:
          self.Owner.Dispatch("TakeDamageEvent", 1)
        pass


KeplerComposite.RegisterComponent(DestroyOnCollide, 'DestroyOnCollide')