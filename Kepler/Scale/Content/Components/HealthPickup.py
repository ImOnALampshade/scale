#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite

#Serializable members
    #RecoverHealth
    #PickupSound

class HealthPickup:
    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollision)
        pass
        
    def OnCollision(self, Composite):
        #Since pickups can only collide with the player, we don't
        #have to worry about checking collisions
        Composite.Dispatch("Recover",self.RecoverHealth)
        self.Owner.Audio.PlayShortSound(self.PickupSound)
        self.Owner.Destroy()
        pass
        
KeplerComposite.RegisterComponent(HealthPickup,"HealthPickup")