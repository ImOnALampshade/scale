#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys

#Serialized members
    #OutroTime

class LevelOutroComponent:
    CurrTime = 0
    Enabled = False
    
    def Initialize(self):
        KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)
        KeplerComposite.Connect(self.Space, "AllWavesDefeated", self.OnWavesDefeated)
        
        #Check code!
        KeplerComposite.Connect(KeplerComposite.Game(), 'KeyDown', self.OnKeyDown)
        pass
        
    def OnWavesDefeated(self):
        self.Enabled = True;
        pass
        
    def SwitchToCredits(self):
        #space = KeplerComposite.CreateComposite('space', KeplerComposite.Game())       
        #KeplerComposite.CreateInstance('Instances/Credits.json', space)
        NewSpace = KeplerComposite.CreateComposite('CreditsScreen', self.Game)
        self.Space.Destroy()
        self.Game.FindEntity("/HUD").Destroy();
        NewSpace.DispatchDown("Start")
        print ("Switch to Credits")
        pass
        
    def OnLogicUpdate(self, Dt):
        if(self.Enabled == True):
            self.CurrTime += Dt
            if(self.CurrTime >= self.OutroTime):
                self.SwitchToCredits()
                self.Enabled = False
        pass
        
    def OnKeyDown(self, key, mods):
        if(key == Keys.KEY_P):
             self.OnWavesDefeated()
        #    print ("p was hit. Going to credits")
        #    self.SwitchToCredits()
            
        pass
            
KeplerComposite.RegisterComponent(LevelOutroComponent, 'LevelOutro')            