#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class OrientationComponent:
  TwoDimensional = True

  def Initialize(self):
    self.Up = Vec3(0, 1, 0)
    self.Forward = Vec3(0, 0, -1)
    self.Right = Vec3(1, 0, 0)

  def GetRotation(self):
    return self.Owner.Transform.Rotation;

  def GetWorldRotaiton(self):
    return self.Owner.Transform.WorldRotation;

  def GetLookAtDirectionWithUpRotation(self, direction, up):
    target = QuatFromBasis(direction.normalized(), up.normalized(), self.WorldRight)
    return target;
    #parentRot = self.GetWorldRotaiton().mult(self.GetRotation().inverted())
    #return parentRot.mult(target.inverted())

  def GetLookAtPointWithUpRotation(self, point, up):
    return self.GetLookAtDirectionWithUpRotation(point.subtr(self.Owner.Transform.WorldPosition), up)

  def GetLookAtDirectionRotation(self, direction):
    return self.GetLookAtDirectionWithUpRotation(direction, self.WorldUp)

  def GetLookAtPointWithUpRotation(self, point, up):
    direction = point.subtr(self.Owner.Transform.WorldPosition).normalized()
    return self.GetLookAtDirectionWithUpRotation(direction, up)

  def GetLookAtPointRotation(self, point):
    direction = point.subtr(self.Owner.Transform.WorldPosition).normalized()
    return self.GetLookAtDirectionRotation(direction)

  def LookAtDirection(self, direction):
    self.Owner.Transform.WorldRotation = self.GetLookAtDirectionRotation(direction)

  def LookAtPoint(self, point):
    self.Owner.Transform.WorldRotation = self.GetLookAtPointRotation(point)

  def LookAtPointWithUp(self, point, up):
    self.Owner.Transform.WorldRotation = self.GetLookAtPointWithUpRotation(point, up)

  @property
  def WorldUp(self):
    if(self.TwoDimensional):
      return self.Up
    else:
      return self.Owner.Transform.WorldRotation.rotate(self.Up)

  @property
  def WorldRight(self):
    right = self.Owner.Transform.WorldRotation.rotate(self.Right)
    if(self.TwoDimensional):
      right.z = 0;
      right = right.normalized()
    return right

  @property
  def WorldForward(self):
    forward = self.Owner.Transform.WorldRotation.rotate(self.Forward)
    if(self.TwoDimensional):
      forward.z = 0;
      forward = forward.normalized()
    return forward


KeplerComposite.RegisterComponent(OrientationComponent, 'Orientation')
