#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite

class ShipSubsystemComponent:

    def Initialize(self):
        print("Initializing shipsubsystem");
        self.StunTime = 0
        self.MaxHealth = 0
        self.Enabled = True
        if(self.Owner.ShipOverride != None):
            self.Ship = self.Owner.ShipOverride.Ship
        else:
            self.Ship = self.Owner.Parent()
        
        if(self.Ship.ShipSubsystem != None):
            self.Ship = self.Ship.ShipSubsystem.Ship
        else:
            pass
        
        KeplerComposite.Connect(self.Owner, "Awake", self.OnAwake)
        pass

    def OnAwake(self):
        pass

KeplerComposite.RegisterComponent(ShipSubsystemComponent, "ShipSubsystem")
