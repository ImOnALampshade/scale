#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys

from KeplerMath import *

class StunnableComponent:
  def Initialize(self):
    self.StunTimer = 0
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.LogicUpdate)
    KeplerComposite.Connect(self.Owner, "Stun", self.OnStun)
    pass

  def LogicUpdate(self, Dt):
    if(self.IsStunned()):
      self.StunTimer -= Dt
      self.Owner.Dispatch("Unstun")

  def IsStunned(self):
    return self.StunTimer > 0

  def OnStun(self, Time):
    if self.StunTimer < Time:
      self.StunTimer = Time

KeplerComposite.RegisterComponent(StunnableComponent, 'Stunnable')