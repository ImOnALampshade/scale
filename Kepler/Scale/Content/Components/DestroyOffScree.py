#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite

class DestroyOffScreenComponent:
  height = 15
  width  = 15*(16/12)

  def Initialize(self):
    KeplerComposite.Connect(self.Owner, "LogicUpdate", self.OnLogicUpdate)


  def OnLogicUpdate(self, Dt):
    pos = self.Owner.Transform.Position
    if(pos.x > self.width):
      self.Owner.Destroy()
    elif(pos.x < -self.width):
      self.Owner.Destroy()

    if(pos.y > self.height):
      self.Owner.Destroy()
    elif(pos.y < -self.height):
      self.Owner.Destroy()

KeplerComposite.RegisterComponent(DestroyOffScreenComponent, "DestroyOffScreen")