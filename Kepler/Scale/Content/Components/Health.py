#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

#Serialized Properties:
# "Health": 10,
# "DamageOnCollide": 0 or 1

class HealthComponent:
    Health = 1
    DamageOnCollide = 0
    def Initialize(self):
        #Zero.Connect(self.Space, Events.LogicUpdate, self.OnLogicUpdate)
        KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollide)
        KeplerComposite.Connect(self.Owner, "TakeDamageEvent", self.OnTakeDamage)
        KeplerComposite.Connect(self.Owner, "Recover", self.Heal)
        pass
    
    def OnCollide(self, Other):
        if (self.DamageOnCollide == 1):
            self.Owner.Dispatch("TakeDamageEvent", 1)
        pass

    def Heal(self, deltaHealth = 1):
        self.Health += deltaHealth;
        print("Health: " + repr(self.Health))
        pass
    
    def TakeDamage(self, sourceOfDamage):
        self.Health -= 1;
        print("Taking damage", self.Owner.Name(), self.Health)

        self.Owner.Dispatch("DamageTaken")
        if (self.Health <= 0):
            destroyer = sourceOfDamage;
            self.Owner.Dispatch("DeathEvent", destroyer)
            self.Owner.Destroy()
            pass
        pass

    def OnTakeDamage(self, Damage):
        self.Health -= Damage
        if(self.Health <= 0):
            self.Owner.Dispatch("DeathEvent", None)
            if(self.PlayUponDeath != ""):
                self.Owner.Audio.PlayShortSound(self.PlayUponDeath)
            self.Owner.Destroy()

KeplerComposite.RegisterComponent(HealthComponent, 'Health')