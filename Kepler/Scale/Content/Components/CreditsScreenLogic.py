#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite

class CreditsScreenLogicComponent:
  def Initialize(self):
    self.PausedSpace = 0
    KeplerComposite.Connect(self.Owner, 'GoToMainMenu', self.OnGoToMainMenu)


  def OnGoToMainMenu(self):
    KeplerComposite.CreateComposite('MainMenu', KeplerComposite.Game())
    self.Space.Destroy()


KeplerComposite.RegisterComponent(CreditsScreenLogicComponent, "CreditsScreenLogic")