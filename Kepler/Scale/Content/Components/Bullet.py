#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite

class BulletComponent:
  TimeOut = 2
  Damage = 1

  def Initialize(self):
    self.Timer = 0

    KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollision)
    KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
    pass

  def OnCollision(self, Composite):
    #create particle explosion
    #print("Just Hit: " + Composite.Name())
    Composite.Dispatch("TakeDamageEvent", self.Damage)
    self.Owner.Destroy()
    pass

  def OnLogicUpdate(self, Dt):
    if(self.Timer > self.TimeOut):
      self.Owner.Destroy()
    else:
      self.Timer += Dt
    pass


KeplerComposite.RegisterComponent(BulletComponent, "Bullet")