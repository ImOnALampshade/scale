#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, Keys
import os
from KeplerMath import Vec3

class MainMenuComponent:
  def Initialize(self):
    KeplerComposite.Connect(self.Owner, 'SwitchToGame', self.SwitchToGame)
    KeplerComposite.Connect(self.Owner, 'SwitchToOptions', self.SwitchToOptions)
    KeplerComposite.Connect(self.Owner, 'Quit', self.Quit)
    KeplerComposite.Connect(self.Game,  'KeyDown', self.OnKeyDown)
    KeplerComposite.Connect(self.Owner, 'Awake', self.OnStart)
    KeplerComposite.Connect(self.Owner, 'ToggleFullscreen', self.ToggleFullscreen)
    KeplerComposite.Connect(self.Owner, 'SwitchToCredits', self.SwitchToCredits)

  def ToggleFullscreen(self):
    window = self.Game.Window

    if window.Fullscreen == 1:
      window.Fullscreen = 0
    else:
      window.Fullscreen = 1

  def OnStart(self):
    #Inform Whatever is listening that we are in the main menu
    self.Owner.Dispatch("SwitchToMainMenu");
    pass

  def SwitchToGame(self):
    space = KeplerComposite.CreateComposite('LoadingScreenSpace', self.Game)

    self.Space.Destroy()
    space.Dispatch("Start")

  def SwitchToCredits(self):
      NewSpace = KeplerComposite.CreateComposite('CreditsScreen', self.Game)
      self.Space.Destroy()
      NewSpace.DispatchDown("Start")
      print ("Switch to Credits")

  def SwitchToOptions(self):
    print ("Switch to options")

  def Quit(self):
    KeplerComposite.Game().Game.ExitGame()

  def OnKeyDown(self, key, mods):
    if(key == Keys.KEY_F2):
      # import * is only allowed at the module level. It should be legal to create components
      # in the sandbox, but you'll have to import them manually.
      #from Sandbox import *
      KeplerComposite.LoadRecipeFile('Sandbox/sandboxspace.json')
      #KeplerComposite.CreateComposite('PlayerRoot', space)

      user = os.path.split(os.path.expanduser('~'))[-1]
      print ('Loading sandbox for user:', user, '\nExpecting instance file sandbox/'+user+'.json')
      KeplerComposite.CreateInstance('sandbox/' + user + 'spaces.json', self.Game)
      space = self.Game.FindEntity("/SandboxSpace")
      KeplerComposite.CreateInstance('sandbox/' + user + '.json', space)

      self.Space.Destroy()
      space.DispatchDown("Start")
      print ("Switch to sandbox")


KeplerComposite.RegisterComponent(MainMenuComponent, 'MainMenu')
