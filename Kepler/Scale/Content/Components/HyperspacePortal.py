#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite, time, Keys
from KeplerMath import *

class HyperspacePortalComponent:
  def Initialize(self):
    KeplerComposite.Connect(self.Space, "Awake", self.OnLevelStarted);
    KeplerComposite.Connect(self.Owner, "OnCollide", self.OnCollide)
    pass  

  def OnCollide(self, otherEntity):
    otherEntity.DispatchEvent("HyperspaceContact", self.Owner)
    pass

  def OnLevelStarted(self):
    #emitSize = self.Owner.SphericalParticleEmitter.EmitterSize
    #Action.Property(self.Owner.Actions, self.Owner.SphericalParticleEmitter, "EmitterSize", emitSize, self.AppearTime, Zero.Ease.QuadInOut);
    #self.Owner.SphericalParticleEmitter.EmitterSize = VectorMath.Vec3(0,0,0);
    pass

KeplerComposite.RegisterComponent(HyperspacePortalComponent, "HyperspacePortal")