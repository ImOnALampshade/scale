#Copyright (C) 2014 DigiPen Institute of Technology.
#Reproduction or disclosure of this file or its contents without
#the prior written consent of DigiPen Institute of Technology is
#prohibited.

import KeplerComposite
from KeplerMath import *

class TwoDObjectComponent:
  Enabled = True

  def Initialize(self):
    KeplerComposite.Connect(self.Space, 'LogicUpdate', self.OnLogicUpdate)
    pass

  #set move speed to a constant
  def OnLogicUpdate(self, Dt):
    if self.Enabled:
      pos = self.Owner.Transform.Position
      self.Owner.Transform.Position = Vec3(pos.x, pos.y, 0)

KeplerComposite.RegisterComponent(TwoDObjectComponent, "Object2D")