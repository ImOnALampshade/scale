function goToGame() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('SwitchToGame');
}

function goToOptions() {
  kepler.dispatchOnSpace('MouseSelect');
}

function quitGame() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('Quit');
}

function mouseOver(){
  kepler.dispatchOnSpace('MouseOver');
}

function goToCredits() {
    kepler.dispatchOnSpace('MouseSelect');
    kepler.dispatchOnSpace('SwitchToCredits');
}