function goToGame() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('SwitchToGame');
}

function goToOptions() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('SwitchToOptions');
  window.alert("Hello");
}

function quitGame() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('Quit');
}

function mouseOver(){
  kepler.dispatchOnSpace('MouseOver');
}

