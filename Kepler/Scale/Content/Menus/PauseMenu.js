function resumeGame() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('Resume');
}

function goToMainMenu() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('GoToMainMenu');
}

function quitGame() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('Quit');
}

function mouseOver(){
  kepler.dispatchOnSpace('MouseOver');
}

function restartGame() {
  kepler.dispatchOnSpace('MouseSelect');
  kepler.dispatchOnSpace('RestartGame');
}