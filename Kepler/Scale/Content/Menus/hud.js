
var recursive = 0.0;

function FadeOut() {
    if (recursive < 0) {
        return;
    }
    
    recursive -= 1;
    var percent = recursive / 25.0;
    //do opacity
    document.getElementById('NewWave').style.opacity = percent;
    setTimeout(FadeOut, 10);
}

function FadeIn() {
    if (recursive > 25) {
        // do the fade out here
        setTimeout(FadeOut, 1000);
        return;
    }
    
    recursive += 1;
    var percent = recursive / 25.0;
    //do opacity
    document.getElementById('NewWave').style.opacity = percent;
    setTimeout(FadeIn, 10);
}

function NewWave(WavesLeft) {
    document.getElementById('NewWave').innerHTML = WavesLeft + " Waves Left";
    recursive = 0;
    FadeIn();
}

function UpdateWeaponType(NewWeaponType) {
    document.getElementById("WeaponTypeValue").innerHTML = NewWeaponType;
}

function UpdateHealth(NewHealth) {
    document.getElementById("HealthValue").innerHTML = NewHealth;
}

function UpdateAmmo(NewAmmo) {
    document.getElementById("AmmoValue").innerHTML = NewAmmo;
}

function UpdateTheHud(NewHealth, NewWeaponType, NewAmmo) {
    document.getElementById("WeaponTypeValue").innerHTML = NewWeaponType;
    document.getElementById("HealthValue").innerHTML = NewHealth;
    document.getElementById("AmmoValue").innerHTML = NewAmmo;
    //UpdateHealth(NewHealth);
    //UpdateWeaponType(NewWeaponType);
    //UpdateAmmo(NewAmmo);
}

function MakeHUDRed() {
    document.getElementById('all').style.color = 'red';
}

function MakeHUDWhite() {
    document.getElementById('all').style.color = 'white';
}

kepler.registerForMessage("WeaponTypeUpdate", 'UpdateWeaponType');
kepler.registerForMessage("HealthUpdate", 'UpdateHealth');
kepler.registerForMessage("AmmoUpdate", 'UpdateAmmo');
kepler.registerForMessage("UpdateValues", 'UpdateTheHud');
kepler.registerForMessage("UpdateNewWave", 'NewWave');
kepler.registerForMessage("MakeTheHUDRed", 'MakeHUDRed');
kepler.registerForMessage("MakeTheHUDWhite", 'MakeHUDWhite');