#version 440

uniform vec3 pl_position;
uniform vec3 pl_diffuse;
uniform vec3 pl_specular;
uniform vec3 pl_eyepos;
uniform vec3 pl_ambient;
uniform float pl_specularPower;

in VertexData
{
  vec3 worldpos;
  vec3 normal;
  vec2 texcoord;
} v_in;

out vec4 end_color;

vec3 light = vec3(1);

void Light()
{
  vec3 N = v_in.normal;
  vec3 L = normalize(pl_position - v_in.worldpos);
  vec3 E = normalize(pl_eyepos - v_in.worldpos);
  vec3 R = normalize(-reflect(L, N));

  vec3 diffuse  = pl_diffuse * dot(N, L);
  vec3 specular = pl_specular * pow(max(dot(R, E), 0.0), pl_specularPower);
  vec3 ambient  = pl_ambient;

  diffuse = clamp(diffuse, 0, 1);
  specular = clamp(specular, 0, 1);

  light = clamp(diffuse + specular + ambient, 0.0, 1.0);
}

void main()
{
  Light();
  end_color = vec4(light, 1);
}
