#version 440

uniform mat4 projection_view_matrix;

buffer matrix_buffer
{
  struct
  {
    mat4 world_matrix;
    mat3 normal_matrix;
    vec3 tint;
  } matrix_list[];
};

in vec3 v_coord;
in vec2 v_texcoord;
in vec3 v_normal;

out VertexData
{
  vec2 texcoord;
  vec3 normal;
  vec3 world;
  flat vec3 tint;
} VertexOut;

void main()
{
  // Get the world and normal matrix from the matrix buffer
  mat4 world_matrix  = matrix_list[gl_InstanceID].world_matrix;
  mat3 normal_matrix = matrix_list[gl_InstanceID].normal_matrix;

  // Transform the vertex coordinate to world space
  vec4 world = world_matrix * vec4(v_coord, 1);

  // Fill in our output vertex
  VertexOut.texcoord = v_texcoord;
  VertexOut.normal   = normalize(normal_matrix * v_normal);
  VertexOut.world    = world.xyz;
  VertexOut.tint     = matrix_list[gl_InstanceID].tint;

  // The position is the projection * view * world position
  gl_Position = projection_view_matrix * world;
}
