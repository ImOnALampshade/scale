#version 440

in vec2 v_coord;

void main()
{
  gl_Position = vec4(v_coord, 0, 1);
}
