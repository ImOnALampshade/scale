#version 440

uniform mat4 projection_view_matrix;
uniform mat4 world_matrix;
uniform mat3 normal_matrix;

in vec3 v_coord;
in vec3 v_normal;
in vec2 v_texcoord;

out VertexData
{
  vec3 worldpos;
  vec3 normal;
  vec2 texcoord;
} v_out;

void main()
{
  vec4 w = world_matrix * vec4(v_coord, 1);

  v_out.worldpos = w.xyz;
  v_out.normal   = normalize(normal_matrix * v_normal);
  v_out.texcoord = v_texcoord;
  gl_Position    = projection_view_matrix * w;
}
