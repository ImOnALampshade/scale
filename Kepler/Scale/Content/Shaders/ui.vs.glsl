#version 440

in vec2 v_coord;

out VertexData
{
  vec2 screencoord;
} VertexOut;

void main()
{
  VertexOut.screencoord = (v_coord + 1.0) / 2.0 * vec2(1.0, -1.0);
  gl_Position = vec4(v_coord, 0, 1);
}
