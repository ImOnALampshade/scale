#version 440

uniform sampler2DArray diffuse_maps;
uniform sampler2DArray specular_maps;

uniform uint diffuse_index;
uniform uint specular_index;

in VertexData
{
  vec2 texcoord;
  vec3 normal;
  vec3 world;
  vec3 barycentric;
} VertexIn;

out vec3 out_diffuse;

void main()
{
  vec3 t = textureLod(diffuse_maps, vec3(VertexIn.texcoord, diffuse_index), 0).rgb;

  out_diffuse = t;
}
