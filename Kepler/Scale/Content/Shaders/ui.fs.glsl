#version 440

uniform sampler2D ui;
uniform ivec2 size;

in VertexData
{
  vec2 screencoord;
} VertexIn;

out vec4 end_color;

void main()
{
  end_color = texture(ui, VertexIn.screencoord);
}
