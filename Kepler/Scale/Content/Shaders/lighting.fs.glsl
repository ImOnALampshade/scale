#version 440

uniform sampler2D diffuse;
uniform sampler2D normal;
uniform sampler2D position;
uniform sampler2D depth;
uniform ivec2 viewsize;
uniform float gamma = 1.0;

uniform vec3 light_pos;
uniform vec3 light_color;
uniform vec3 light_attenuation;
uniform vec3 light_ambient;

uniform vec3 fog_color;
uniform float fog_dist;
uniform float fog_ramp;

out vec4 end_color;

float sum(vec3 c) { return c.x + c.y + c.z; }

vec3 Light(vec3 n, vec3 p, vec3 diff)
{ 
  vec3 l      = light_pos - p;
  float l_len = length(l);
  float ndotl = dot(n, l / l_len);
  float power = ndotl / sum(light_attenuation * vec3(l_len * l_len, l_len, 1));

  return diff * power * light_color + light_ambient;
}

void main()
{
  vec2 coord = gl_FragCoord.xy / viewsize;

  vec3 d = texture(diffuse, coord).rgb;
  vec3 n = texture(normal, coord).xyz;
  vec3 p = texture(normal, coord).xyz;
  float z = texture(depth, coord).x;

  //vec3 light = Light(n, p, d);
  
  //vec3 c = mix(light, fog_color, pow(z / fog_dist, fog_ramp));

  //end_color = vec4(c + d, 1);

  end_color = vec4(d, 1);
}
