// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Scale entry poin
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../AllocatorManager/allocatormanager.h"
#include "../KeplerCore/kepmain.h"
#include "../Logger/logger.h"
#include "../PyConsole/PyConsole.h"
#include "componentinit.h"
#include "createmeta.h"
#include <python/Python.h>

#include <Windows.h>

#ifdef _MSC_VER
extern "C" {  _declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001; }
#endif

using Utilities::MemoryManagement::AllocatorManager;

int main (int argc, char **argv)
{

#ifndef _DEBUG
  ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif

  AllocatorManager::Config config;

  config.ChooseMethod = AllocatorManager::PowerOfTwo;
  config.SizeAccuracy = 0;

  #ifdef _DEBUG
  config.DebugOn = true;
  config.PadBytes = 128;
  #else
  config.DebugOn = false;
  config.PadBytes = 0;
  #endif

  AllocatorManager::Initialize (config);

  KeplerUseAppDataDirectory();
  Logger::Init();
  KeplerUseGameDirectory();

  CreateMeta();
  CreateModules();

  Py_InitializeEx (0);
  InitPython();
  //PyConsole::Init();

  KeplerInit();

  InitComponents();

  FILE *init = fopen ("init.py", "rt");

  if (init)
    PyRun_AnyFileExFlags (init, "init.py", 1, nullptr);

  KeplerRun();

  //PyConsole::Free();

  Py_Finalize();

  Logger::Free();

  return 0;
}
