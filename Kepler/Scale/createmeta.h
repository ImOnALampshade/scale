// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Meta binding entry point
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef BIND_META_H
#define BIND_META_H

void CreateMeta();
void CreateModules();
void InitPython();

#endif
