// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Meta binding entry point
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../KeplerPythonBindings/pycomposite.h"
#include "../KeplerPythonBindings/PyMetaBindings.h"
#include "../KeplerPythonBindings/PyCallable.h"
#include "../PhysicsComponents/PhysicsSystem.h"
#include "../PhysicsComponents/RigidBodyComponent.h"

char Kepler[]     = "Kepler";
char KeplerMath[] = "KeplerMath";
char Actions[]    = "Actions";

void CreateModules()
{
  META_ADD_MODULE (Kepler);
  META_ADD_MODULE (KeplerMath);
  META_ADD_MODULE (Actions);

  BindComposite();
}

#include "../KeplerPythonBindings/glmbinding.h"
#include "../KeplerCore/load_factories.h"
#include "../GraphicsComponents/load_factories.h"
#include "../ComponentSandbox/load_factories.h"
#include "../AudioEngine/load_factories.h"
#include "../ActionListComponent/load_factories.h"

void CreateMeta()
{
  GlmBinding::CreateMeta();

  KeplerCore::LoadMeta();
  GraphicsComponents::LoadMeta();
  ComponentSandbox::LoadMeta();
  AudioInit::CreateMeta();
  PhysicsSystemComponent::CreateMeta();
  RigidBodyComponent::CreateMeta();
  ActionList::CreateMeta();
}

void InitPython()
{
  PyType_Ready (&PyCallable::Type);

  // Make sure Kepler, KeplerMath, and KeplerComposite modules are imported
  PyObject *k_ = PyImport_ImportModule("Kepler");
  PyObject *km = PyImport_ImportModule("KeplerMath");
  PyObject *kac = PyImport_ImportModule("Actions");
  PyObject *kc = PyImport_ImportModule("KeplerComposite");

  Py_XDECREF(k_);
  Py_XDECREF(km);
  Py_XDECREF(kac);
  Py_XDECREF(kc);
}