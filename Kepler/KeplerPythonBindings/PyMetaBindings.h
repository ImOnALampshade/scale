// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Functions for generating python bindings from the meta system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PY_META_BINDINGS_H_
#define PY_META_BINDINGS_H_

#include "../MetaTyping/metatype.h"
#include "../pyinclude.h"

PyObject *CreateModule (PyModuleDef &moduleDef);

template<const char *NAME, void (*CustomFunction) (PyObject *module), PyMethodDef *methods>
PyObject *_CreateModuleInitClosure()
{
  PyModuleDef &def = * (new PyModuleDef);
  memset (&def, 0, sizeof def);

  def = { PyModuleDef_HEAD_INIT };

  def.m_name = NAME;
  def.m_doc = NAME;
  def.m_size = -1;
  def.m_methods = methods;

  PyObject *module = CreateModule (def);

  if (module && CustomFunction)
    CustomFunction (module);

  return module;
}

template<const char *NAME, void (*CustomFunction) (PyObject *module) = nullptr, PyMethodDef *methods = nullptr>
void CreateModule()
{
  PyImport_AppendInittab (NAME, _CreateModuleInitClosure<NAME, CustomFunction, methods>);
}

#define META_ADD_MODULE(name) CreateModule<name, nullptr, nullptr>()
#define META_ADD_MODULE_CB(name, cb, methods) CreateModule<name, cb, methods>()

#endif