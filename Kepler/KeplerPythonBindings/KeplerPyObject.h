// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Generic PyObject for Kepler bindings.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_PY_OBJ_H_
#define KEPLER_PY_OBJ_H_

#include "../pyinclude.h"
#include "../MetaTyping/metatype.h"
#include "../KeplerCore/handle.h"

struct KeplerPyObjectBase
{
  PyObject_HEAD;
  MetaType *GetMeta();
  const MetaType *GetMeta() const;
};

PyTypeObject *GetTypeObject (MetaType &meta);

struct KeplerPyTypeObject
{
  PyTypeObject pyType;
  MetaType &meta;

  KeplerPyTypeObject (MetaType &meta_) : meta (meta_) { }
};

template<typename T>
struct KeplerPyObject : KeplerPyObjectBase
{
  T Value;
};

template<typename T>
struct KeplerPyHandleT : KeplerPyObjectBase
{
  KeplerPyHandleT (Handle<T> val) : Value (val) { }

  Handle<T> Value;

  void *operator new (size_t size, PyTypeObject *type) { return PyObject_New (void, type); }
  void operator delete (void *mem, PyTypeObject *) { Py_DECREF (reinterpret_cast<PyObject *> (mem)); }
};


typedef KeplerPyHandleT<char> KeplerPyHandle;

PyObject *KeplerPyObj_GetAttr (PyObject *self, char *name);
int       KeplerPyObj_SetAttr (PyObject *self, char *name, PyObject *value);

PyObject *KeplerPyObj_GetAttrObj (PyObject *self, PyObject *name);
int       KeplerPyObj_SetAttrObj (PyObject *self, PyObject *name, PyObject *value);

int       KeplerPyObj_Print (PyObject *self, FILE *out, int flags);
PyObject *KeplerPyObj_ToStr (PyObject *self);

PyObject *KeplerPyHdl_GetAttr (PyObject *self, char *name);
int       KeplerPyHdl_SetAttr (PyObject *self, char *name, PyObject *value);
int         KeplerPyHdl_Print (PyObject *self, FILE *out, int flags);
PyObject   *KeplerPyHdl_ToStr (PyObject *self);

PyObject *KeplerPyObj_New(PyTypeObject *myType, PyObject *args, PyObject *kwds);
PyObject *KeplerPyHdl_New(PyTypeObject *myType_, PyObject *args, PyObject *kwds);
int      KeplerPyHdl_Init (PyObject *self, PyObject *args, PyObject *kwds);
void KeplerPyObj_Dealloc (PyObject *self);
void KeplerPyHdl_Dealloc (PyObject *self);

#endif