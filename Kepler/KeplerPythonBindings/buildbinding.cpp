// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Build information for Python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "buildbinding.h"
#include "../KeplerCore/versioninfo.h"

namespace BuildInfoBinding
{
  PyKit::ModuleDef module ("KeplerBuild", "");

  // -------------------------------------------------------------------------

  void SetMethods (PyKit::ModuleDef &module);
  void SetGlobals (PyKitModule module);

  // -------------------------------------------------------------------------

  void SetMethods (PyKit::ModuleDef &module)
  {
  }

  void SetGlobals (PyKitModule module)
  {
    module.AddIntConst ("cpu_arch", sizeof (void *) * 8);
    module.AddStrConst ("version", KEPLER_VERSION_NAME);
    module.AddIntConst ("version_num", KEPLER_VERSION_NUM);
  }

  // -------------------------------------------------------------------------

  void BindFunctions()
  {
    PyImport_AppendInittab ("KeplerBuild",
                            PyKit::ModuleInitFunction<module, SetMethods, SetGlobals>);
  }
}
