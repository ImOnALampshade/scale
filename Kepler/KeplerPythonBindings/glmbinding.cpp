// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  GLM bindings for Python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../MetaTyping/metatype.h"
#include "glmbinding.h"
#include "../MetaTyping/bindhelpers.h"
#include "../KeplerMath/KeplerMath.h"
#include "../KeplerMath/WorldRay.h"

using namespace glm;

void GlmBinding::CreateMeta()
{
  Vec4::CreateMeta();
  Vec3::CreateMeta();
  Vec2::CreateMeta();
  Quat::CreateMeta();
  WorldRay::CreateMeta();
}
