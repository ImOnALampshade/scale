// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Functions for generating python bindings from the meta system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../TypePuns/delayedconstructor.h"
#include "../HashFunctions/hashfunctions.h"
#include "PyMetaBindings.h"
#include "PyCallable.h"
#include "KeplerPyObject.h"
#include "convertpytuple.h"

#include <iostream>

// -----------------------------------------------------------------------------

PyObject *CreateModule (PyModuleDef &moduleDef)
{
  PyObject *module = PyModule_Create (&moduleDef);

  MetaType::Table->ForEach ([module, &moduleDef] (HashTable<String, MetaType>::Payload &data) {
    MetaType &meta = data.value;

    if (meta.module != moduleDef.m_name)
      return;

    meta.py_type = GetTypeObject (meta);

    PyModule_AddObject (module, meta.name.CStr(), reinterpret_cast<PyObject *> (meta.py_type));

    meta.globalMethods.ForEach ([&] (HashTable<String, std::shared_ptr<MetaFunctionGlobal>>::Payload pl)
    {
      MetaFunctionGlobal &fn = *pl.value.get();

      PyCallable *pyCall = PyObject_New (PyCallable, &PyCallable::Type);
      new (pyCall) PyCallable (PyCallableClosure ([&fn] (PyObject *pyArgs, PyObject *pyKwdArgs) {
        Tuple args = ConvertFromPyTuple (pyArgs);
        DynamicVariant retVal = fn (args);
        return ConvertToPyObject (retVal);
      }));

      PyObject_SetAttrString (reinterpret_cast<PyObject *> (module), fn.name.CStr(), reinterpret_cast<PyObject *> (pyCall));
    });
  });

  return module;
}

// -----------------------------------------------------------------------------
