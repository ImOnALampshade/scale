// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Composite bindings for Python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "pymessaging.h"
#include "pycomposite.h"
#include "convertpytuple.h"
#include "KeplerPyObject.h"
#include "pyscriptcomponent.h"
#include "../KeplerCore/component.h"
#include "../KeplerCore/compositefactory.h"
#include "../KeplerCore/EntityFinder.h"

// -----------------------------------------------------------------------------

PyComposite::PyComposite (Handle<Composite> h) : composite (h) { }

// -----------------------------------------------------------------------------

static PyObject *PyComposite_GetAttrInner (PyObject *self_, char *name, bool tryDefault)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (self->composite.IsValid())
  {
    Handle<Component> component = self->composite->GetComponent (name);

    if (component.IsValid())
    {
      MetaType *componentMeta = component->Meta();

      if (!componentMeta)
      {
        PyScriptComponent *scriptComponent = dynamic_cast<PyScriptComponent *> (component.operator->());

        if (scriptComponent)
          return scriptComponent->GetPyObject();

        else
          Py_RETURN_NONE;
      }

      KeplerPyHandle *pyHandle = new ( (PyTypeObject *) componentMeta->py_type) KeplerPyHandle (component.as<char>());
      return reinterpret_cast<PyObject *> (pyHandle);
    }
  }

  if (tryDefault)
  {
    PyObject *pyname = PyUnicode_FromString(name);
    return PyObject_GenericGetAttr(self_, pyname);
  }

  else if (CompositeManagement::Factory->ComponentArchetype(name) != nullptr)
    Py_RETURN_NONE;
  else
    return nullptr;
}

static void SetNullHandleException()
{
  PyErr_Format (PyExc_RuntimeError, "Tried to dereference null composite");
}

// -----------------------------------------------------------------------------

PyObject *PyComposite::GetAttr (PyObject *self, char *name)
{
  return PyComposite_GetAttrInner (self, name, true);
}

PyObject *PyComposite::GetAttrObj (PyObject *self, PyObject *name)
{
  PyObject *attr = PyComposite_GetAttrInner (self, PyUnicode_AsUTF8 (name), false);
  return attr ? attr : PyObject_GenericGetAttr (self, name);
}

// -----------------------------------------------------------------------------

PyObject *PyComposite::Destroy (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (self->composite.IsValid())
  {
    self->composite->Destroy();
    Py_RETURN_NONE;
  }

  else
  {
    SetNullHandleException();
    return nullptr;
  }
}

PyObject *PyComposite::Parent (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (self->composite.IsValid())
  {
    Handle<Composite> parent = self->composite->Parent();

    if (parent.IsValid())
      return (PyObject *) new PyComposite (parent);

    else
      Py_RETURN_NONE;
  }

  else
  {
    SetNullHandleException();
    return nullptr;
  }
}

PyObject *PyComposite::Space (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (self->composite.IsValid())
  {
    Handle<Composite> space = self->composite->Space();

    if (space.IsValid())
      return (PyObject *) new PyComposite (space);

    else
      Py_RETURN_NONE;
  }

  else
  {
    SetNullHandleException();
    return nullptr;
  }
}

PyObject *PyComposite::GetGuid (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (self->composite.IsValid())
    return PyLong_FromLongLong (self->composite->GetGuid());

  else
  {
    SetNullHandleException();
    return nullptr;
  }
}

PyObject *PyComposite::IsValid (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (self->composite.IsValid())
    Py_RETURN_TRUE;

  else
    Py_RETURN_FALSE;
}

PyObject * PyComposite::FindEntityFromThis(PyObject *self_, PyObject *args_)
{
  PyComposite *self;
  char *path;

  if (!PyArg_ParseTuple(args_, "s", &path))
    return nullptr;

  self = reinterpret_cast<PyComposite*>(self_);

  Handle<Composite> origin = self->composite;
  Handle<Composite> retHandle = FindComposite(origin, path);

  if (retHandle.IsValid())
    return reinterpret_cast<PyObject *> (new PyComposite(retHandle));

  else
    Py_RETURN_NONE;
}

PyObject * PyComposite::Name(PyObject *self_, PyObject *args_)
{
  PyComposite* self = reinterpret_cast<PyComposite*>(self_);
  String Name = self->composite->InstanceName();
  return ConvertToPyObject(DynamicVariant(Name));
}

PyObject * PyComposite::Compare(PyObject *self, PyObject *other, int op)
{
  Handle<Composite> left;
  Handle<Composite> right;

  if (self == Py_None)
    left = nullptr;
  else if (self->ob_type == &PyComposite::Type)
    left = reinterpret_cast<PyComposite*>(self)->composite;
  else
  {
    PyErr_SetString(PyExc_TypeError, "Invalid comparison of composites.");
    return nullptr;
  }

  if (!left.IsValid())
    left = nullptr;

  if (other == Py_None)
    right = nullptr;
  else if (other->ob_type == &PyComposite::Type)
    right = reinterpret_cast<PyComposite*>(other)->composite;
  else
  {
    PyErr_SetString(PyExc_TypeError, "Invalid comparison of composites.");
    return nullptr;
  }

  if (!right.IsValid())
    right = nullptr;
  
  switch (op)
  {
  case Py_EQ:
    if (left == right)
      Py_RETURN_TRUE;
    else
      Py_RETURN_FALSE;
  case Py_NE:
    if (left != right)
      Py_RETURN_TRUE;
    else
      Py_RETURN_FALSE;
  default:
    PyErr_SetString(PyExc_TypeError, "Invalid comparison of composites.");
    return nullptr;
  }
}

// -----------------------------------------------------------------------------

void *PyComposite::operator new (size_t size)
{
  return PyObject_New (void, &PyComposite::Type);
}

// -----------------------------------------------------------------------------

PyTypeObject PyComposite::Type;

PyMethodDef PyComposite::Methods[] =
{
  { "Dispatch",     PyDispatchMessage,     METH_VARARGS, "" },
  { "DispatchDown", PyDispatchDownMessage, METH_VARARGS, "" },
  { "DispatchUp",   PyDispatchUpMessage,   METH_VARARGS, "" },
  { "Destroy",      PyComposite::Destroy,  METH_VARARGS, "" },
  { "Space",        PyComposite::Space,    METH_VARARGS, "" },
  { "Parent",       PyComposite::Parent,   METH_VARARGS, "" },
  { "GetGuid",      PyComposite::GetGuid,  METH_VARARGS, "" },
  { "IsValid",      PyComposite::IsValid,  METH_VARARGS, "" },
  { "FindEntity",   PyComposite::FindEntityFromThis,  METH_VARARGS, "" },
  { "Name",         PyComposite::Name,  METH_VARARGS, "" },
  { NULL }
};


// -----------------------------------------------------------------------------

void BindComposite (PyObject *module)
{
  PyComposite::Type.tp_getattr      = PyComposite::GetAttr;
  PyComposite::Type.tp_getattro     = PyComposite::GetAttrObj;
  PyComposite::Type.tp_methods      = PyComposite::Methods;
  PyComposite::Type.tp_name         = "KeplerComposite.Composite";
  PyComposite::Type.tp_basicsize    = sizeof PyComposite;
  PyComposite::Type.tp_flags        = Py_TPFLAGS_DEFAULT;
  PyComposite::Type.tp_richcompare  = PyComposite::Compare;

  PyType_Ready (&PyComposite::Type);

  PyModule_AddObject (module, "Composite", reinterpret_cast<PyObject *> (&PyComposite::Type));
}

// -----------------------------------------------------------------------------

char CompositeModule[] = "KeplerComposite";

PyMethodDef methods[] =
{
  { "Game",              PyGetGame,           METH_NOARGS,  "" },
  { "LoadRecipeFile",    PyLoadRecipe,        METH_VARARGS, "" },
  { "CreateGame",        PyCreateGame,        METH_VARARGS, "" },
  { "CreateSpace",       PyCreateSpace,       METH_VARARGS, "" },
  { "CreateComposite",   PyCreateComposite,   METH_VARARGS, "" },
  { "RegisterComponent", PyRegisterComponent, METH_VARARGS, "" },
  { "Connect",           PyConnect,           METH_VARARGS, "" },
  { "FindEntity",        PyFindEntity,        METH_VARARGS, "" },
  {"CreateInstance",     PyCreateInstance,    METH_VARARGS, "" }, 
  { nullptr }
};

// -----------------------------------------------------------------------------

PyObject *PyGetGame (PyObject *self, PyObject *args)
{
  return (PyObject *) new PyComposite (CoreEngine::Game);
}

PyObject *PyLoadRecipe (PyObject *self, PyObject *args)
{
  char *filename;

  if (!PyArg_ParseTuple (args, "s", &filename))
    return nullptr;

  CompositeManagement::Factory->LoadRecipeFile (filename);

  Py_RETURN_NONE;
}

PyObject *PyCreateGame (PyObject *self, PyObject *args)
{
  CompositeManagement::Factory->CreateGame();
  return reinterpret_cast<PyObject *> (new PyComposite (CoreEngine::Game));
}

PyObject *PyCreateSpace (PyObject *self, PyObject *args)
{
  char *name;

  if (!PyArg_ParseTuple (args, "s", &name))
    return nullptr;

  Handle<Composite> space = CompositeManagement::Factory->CreateSpace (name);

  return reinterpret_cast<PyObject *> (new PyComposite (space));
}

PyObject *PyCreateComposite (PyObject *self, PyObject *args)
{
  PyComposite *owner;
  char *name;

  if (!PyArg_ParseTuple (args, "sO!", &name, &PyComposite::Type, &owner))
    return nullptr;

  Handle<Composite> obj = CompositeManagement::Factory->CreateComposite (name, owner->composite);

  return reinterpret_cast<PyObject *> (new PyComposite (obj));
}

PyObject *PyRegisterComponent (PyObject *self, PyObject *args)
{
  PyObject *classType;
  char *name;

  if (!PyArg_ParseTuple (args, "Os", &classType, &name))
    return nullptr;

  Py_INCREF (classType);

  Component::Archetype arch;
  arch.ComponentSize = sizeof PyScriptComponent;
  arch.Constructor = [classType] (void *mem) {
    return new (mem) PyScriptComponent (classType);
  };
  arch.Meta = nullptr;
  arch.TypeName = name;

  CompositeManagement::Factory->ComponentArchetype (arch);

  Py_RETURN_NONE;
}

struct ConnectionClosure
{
  PyObject *function;

  ConnectionClosure (PyObject *fn) : function (fn)
  {
    Py_XINCREF (fn);
  }

  ConnectionClosure (const ConnectionClosure &c) : function (c.function)
  {
    Py_XINCREF (function);
  }

  ConnectionClosure (ConnectionClosure &&c) : function (c.function)
  {
    c.function = nullptr;
  }

  ConnectionClosure &operator= (const ConnectionClosure &c)
  {
    if (this != &c)
    {
      Py_XDECREF (function);
      function = c.function;
      Py_XINCREF (function);
    }

    return *this;
  }

  ConnectionClosure &operator= (ConnectionClosure &&c)
  {
    if (this != &c)
    {
      function = c.function;
      c.function = nullptr;
    }

    return *this;
  }

  ~ConnectionClosure()
  {
    Py_XDECREF (function);
  }

  void operator() (const Tuple &args)
  {
    if (!function)
      return;

    if (PyErr_Occurred())
    {
      PyErr_Print();
      PyErr_Clear();
    }

    PyObject *self = PyMethod_Self (function);
    PyObject *pyargs = ConvertToPyTuple (args);
    PyObject *result = PyObject_CallObject (function, pyargs);
    Py_DECREF (pyargs);

    if (!result || PyErr_Occurred())
    {
      PyErr_Print();
      PyErr_Clear();
    }

    else
      Py_DECREF (result);
  }
};

PyObject *PyConnect (PyObject *, PyObject *args)
{
  PyComposite *composite;
  char *name;
  PyObject *function;

  if (!PyArg_ParseTuple (args, "O!sO", &PyComposite::Type, &composite, &name, &function))
    return nullptr;

  if (!composite->composite.IsValid())
  {
    SetNullHandleException();
    return nullptr;
  }

  Handle<Composite> context;

  if (PyMethod_Check (function))
  {
    PyObject *self = PyMethod_Self (function);
    PyObject *owner = PyObject_GetAttrString (self, "Owner");

    if (owner)
    {
      if (owner->ob_type == &PyComposite::Type)
        context = reinterpret_cast<PyComposite *> (owner)->composite;

      Py_DECREF (owner);
    }
  }

  std::function<void (const Tuple &) > closure = ConnectionClosure (function);

  composite->composite->RegisterObserver (name, closure, context, Composite::OBSERVER_PASS_TUPLE);

  Py_RETURN_NONE;
}

PyObject *PyFindEntity (PyObject *self_, PyObject *args_)
{
  PyComposite *self;
  char *path;

  if (!PyArg_ParseTuple (args_, "O!s", &PyComposite::Type, &self, &path))
    return nullptr;

  Handle<Composite> origin = self->composite;
  Handle<Composite> retHandle = FindComposite (origin, path);

  if (retHandle.IsValid())
    return reinterpret_cast<PyObject *> (new PyComposite (retHandle));

  else
    Py_RETURN_NONE;
}


PyObject * PyCreateInstance(PyObject * self, PyObject * args)
{
  PyComposite *owner;
  char *name;

  if (!PyArg_ParseTuple(args, "sO!", &name, &PyComposite::Type, &owner))
    return nullptr;

  Handle<Composite> obj = CompositeManagement::Factory->CreateInstance(name, owner->composite);

  return reinterpret_cast<PyObject *> (new PyComposite(obj));
}

// -----------------------------------------------------------------------------

void BindComposite()
{
  META_ADD_MODULE_CB (CompositeModule, BindComposite, methods);
}

// -----------------------------------------------------------------------------

