// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Build information for Python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef BUILD_BINDING_H
#define BUILD_BINDING_H

#include "../PyKit/pymetaclass.h"

namespace BuildInfoBinding
{
  void BindFunctions();
}

#endif
