// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Callable function object within Python. We use this in place of
//  PyMethodDef because we don't want to do codegen to make C-style
//  functions for everything.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "PyCallable.h"

static void PyCallable_Dealloc (PyObject *self)
{
  reinterpret_cast<PyCallable *> (self)->~PyCallable();
  self->ob_type->tp_free (self);
}

static PyObject *PyCallable_Invoke (PyObject *self, PyObject *args, PyObject *kwdArgs)
{
  PyCallable *callable = reinterpret_cast<PyCallable *> (self);
  return (*callable) (args, kwdArgs);
}


PyCallable::PyCallable (PyCallableClosure closure)
  : CallingClosure (closure)
{
}

PyTypeObject PyCallable::Type = {
  PyVarObject_HEAD_INIT (NULL, 0)
  "Kepler.Callable",
  sizeof (PyCallable),
  0,                          /* tp_itemsize */
  PyCallable_Dealloc,         /* tp_dealloc */
  0,                          /* tp_print */
  0,                          /* tp_getattr */
  0,                          /* tp_setattr */
  0,                          /* tp_reserved */
  0,                          /* tp_repr */
  0,                          /* tp_as_number */
  0,                          /* tp_as_sequence */
  0,                          /* tp_as_mapping */
  0,                          /* tp_hash  */
  PyCallable_Invoke,          /* tp_call */
  0,                          /* tp_str */
  0,                          /* tp_getattro */
  0,                          /* tp_setattro */
  0,                          /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT,        /* tp_flags */
  "Wrapper around std::function to make python callables.",           /* tp_doc */
};

PyObject *PyCallable::operator() (PyObject *args, PyObject *kwdArgs)
{
  return CallingClosure (args, kwdArgs);
}