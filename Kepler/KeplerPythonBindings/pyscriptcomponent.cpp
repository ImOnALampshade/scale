// -----------------------------------------------------------------------------
// Author: Howard Hughes
//
// Components for Python scripts to use
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "pyscriptcomponent.h"
#include "pycomposite.h"
#include "../KeplerCore/game.h"

// -----------------------------------------------------------------------------

PyScriptComponent::PyScriptComponent (PyObject *pyClass)
{
  m_instance = PyObject_CallObject (pyClass, nullptr);
}

PyScriptComponent::~PyScriptComponent()
{
  Py_DECREF (m_instance);
}

// -----------------------------------------------------------------------------

static PyObject *ConvertPyObjectFromJson (const Serializer::Value &val)
{
  if (val.IsType<Json::Number>())
    return PyFloat_FromDouble (val.as<Json::Number>());

  if (val.IsType<Json::String>())
    return PyUnicode_FromString (val.as<Json::String>().c_str());

  if (val.IsType<Json::Null>())
    Py_RETURN_NONE;

  if (val.IsType<Json::Boolean>())
  {
    if (val.as<Json::Boolean>())
      Py_RETURN_TRUE;

    else
      Py_RETURN_FALSE;
  }

  if (val.IsType<Json::List>())
  {
    const Json::List &list = val;
    PyObject *pylist = PyList_New (list.size());

    for (unsigned i = 0; i < list.size(); ++i)
    {
      const Serializer::Value &item = list[i];
      PyList_SetItem (pylist, i, ConvertPyObjectFromJson (item));
    }

    return pylist;
  }

  if (val.IsType<Json::Object>())
  {
    const Json::Object &table = val;
    PyObject *dict = PyDict_New();

    table.ForEach ([dict] (const Json::Object::Payload &data) {
      PyObject *key = PyUnicode_FromString (data.key.c_str());
      PyObject *val = ConvertPyObjectFromJson (data.value);
      PyDict_SetItem (dict, key, val);

      Py_DECREF (key);
      Py_DECREF (val);
    });

    return dict;
  }

  Py_RETURN_NONE;
}

void PyScriptComponent::Initialize (const Json::Object &jsonData)
{
  PyObject *owner = reinterpret_cast<PyObject *> (new PyComposite (Owner()));
  PyObject *space = reinterpret_cast<PyObject *> (new PyComposite (Space()));
  PyObject *game  = reinterpret_cast<PyObject*>  (new PyComposite(CoreEngine::Game));
  PyObject_SetAttrString (m_instance, "Owner", owner);
  PyObject_SetAttrString (m_instance, "Space", space);
  PyObject_SetAttrString(m_instance, "Game", game);
  Py_DECREF (owner);
  Py_DECREF (space);
  Py_DECREF (game);

  jsonData.ForEach ([this] (const Json::Object::Payload &data) {
    PyObject_SetAttrString (m_instance, data.key.CStr(), ConvertPyObjectFromJson (data.value));
  });

  PyObject *initialize = PyObject_GetAttrString (m_instance, "Initialize");

  if (initialize)
  {
    PyObject *result = PyObject_CallObject (initialize, nullptr);
    Py_DECREF (initialize);

    if (!result)
    {
      PyErr_Print();
      PyErr_Clear();
    }

    else
      Py_DECREF (result);
  }

  else if (PyErr_Occurred())
    PyErr_Clear();
}

void PyScriptComponent::Free()
{
  PyObject *free = PyObject_GetAttrString (m_instance, "Free");

  if (free)
  {
    PyObject *result = PyObject_CallObject (free, nullptr);
    Py_DECREF (free);

    if (!result)
    {
      PyErr_Print();
      PyErr_Clear();
    }

    else
      Py_DECREF (result);
  }

  else if (PyErr_Occurred())
    PyErr_Clear();

}

PyObject *PyScriptComponent::GetPyObject()
{
  Py_INCREF (m_instance);
  return m_instance;
}

// -----------------------------------------------------------------------------
