// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Functions for convert to/from to and from PyTuples and Tuples
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "convertpytuple.h"
#include "KeplerPyObject.h"
#include "../String/string.h"
#include "../HashFunctions/hashfunctions.h"
#include "pycomposite.h"
#include "../MetaTyping/MetaObject.h"

PyObject *ConvertToPyTuple (const Tuple &tuple)
{
  PyObject *pytuple = PyTuple_New (tuple.size());

  for (unsigned i = 0; i < tuple.size(); ++i)
  {
    PyObject *obj = ConvertToPyObject (tuple[i]);
    PyTuple_SetItem (pytuple, i, obj);
  }

  return pytuple;
}

Tuple ConvertFromPyTuple (PyObject *pytuple)
{
  Tuple tuple;

  PyObject *iterator = PyObject_GetIter (pytuple);
  PyObject *item;

  while (item = PyIter_Next (iterator))
  {
    DynamicVariant var = ConvertFromPyObject (item);
    tuple.emplace_back (std::move (var));

    Py_DECREF (item);
  }

  Py_DECREF (iterator);

  return tuple;
}

PyObject *ConvertToPyObject (const Json::Value &var)
{
  if (var.IsType<Json::Number>())
    return PyFloat_FromDouble (var.as<Json::Number>());

  if (var.IsType<Json::Boolean>())
  {
    if (var.as<Json::Boolean>())
      Py_RETURN_TRUE;

    else
      Py_RETURN_FALSE;
  }

  if (var.IsType<Json::String>())
    return PyUnicode_FromString (var.as<Json::String>().c_str());

  if (var.IsType<Json::Null>())
    Py_RETURN_NONE;

  if (var.IsType<Json::List>())
  {
    const Json::List &jsonList = var;
    PyObject *list = PyList_New (var.as<Json::List>().size());

    for (unsigned i = 0; i < jsonList.size(); ++i)
    {
      PyObject *item = ConvertToPyObject (jsonList[i]);
      PyList_SetItem (list, i, item);
      Py_DECREF (item);
    }

    return list;
  }

  if (var.IsType<Json::Object>())
  {
    const Json::Object &jsonObj = var;
    PyObject *dict = PyDict_New();

    jsonObj.ForEach ([dict] (const Json::Object::Payload &data) {
      PyObject *key = PyUnicode_FromString (data.key.c_str());
      PyObject *value = ConvertToPyObject (data.value);

      PyDict_SetItem (dict, key, value);

      Py_DECREF (key);
      Py_DECREF (value);
    });

    return dict;
  }

  Py_RETURN_NOTIMPLEMENTED;
}

struct PyObjectHolder
{
  PyObject *val;

  PyObjectHolder (PyObject *v = nullptr) : val (v) { Py_XINCREF (val); }

  PyObjectHolder (const PyObjectHolder &that) : val (that.val) { Py_XINCREF (val); }

  PyObjectHolder &operator= (const PyObjectHolder &that)
  {
    if (this != &that)
    {
      Py_XDECREF (val);
      val = that.val;
      Py_XINCREF (val);
    }

    return *this;
  }

  ~PyObjectHolder()
  {
    Py_XDECREF (val);
  }
};

PyObject *ConvertToPyObject (const DynamicVariant &var)
{
  // If it is an empty variant, return none
  if (var.Addr() == nullptr)
    Py_RETURN_NONE;

  // If the variant holds a PyObject, return that object
  else if (var.IsType<PyObjectHolder>())
    return var.as<PyObjectHolder>().val;

  else if (var.IsType<Handle<Composite>>())
  {
    PyComposite *ret_;
    ret_ = new PyComposite (var.as<Handle<Composite>>());
    return reinterpret_cast<PyObject *> (ret_);
  }

  else if (MetaType::NameTable.IsCreated() && (*MetaType::NameTable).Contains (var.Type()))
  {
    PyObject *ret_;
    MetaType &meta = *MetaFromTypeInfo (var.Type());

    if (meta.bindAsHandle)
    {
      KeplerPyHandle *ret = PyObject_New (KeplerPyHandle, (PyTypeObject *) meta.py_type);
      memset (&ret->Value, 0, sizeof (ret->Value));
      ret->Value = var.as<Handle<char>>();
      ret_ = reinterpret_cast<PyObject *> (ret);
    }

    else
    {
      KeplerPyObject<char> *ret = PyObject_New (KeplerPyObject<char>, (PyTypeObject *) meta.py_type);
      meta.copyconstructor (var.Addr(), &ret->Value);
      ret_ = reinterpret_cast<PyObject *> (ret);
    }

    return ret_;
  }

  else if (var.IsType<bool>())
  {
    if (var.as<bool>())
      Py_RETURN_TRUE;

    else
      Py_RETURN_FALSE;
  }

  else if (var.IsType<int>() || var.IsType<unsigned int>())
    return PyLong_FromLong (var.as<int>());

  else if (var.IsType<float>() || var.IsType<double>())
    return PyFloat_FromDouble (var.as<float>());

  else if (var.IsType<String>())
    return PyUnicode_FromString (var.as<String>().CStr());

  else if (var.IsType<Json::Value>())
    return ConvertToPyObject (var.as<Json::Value>());

  return nullptr;
}

DynamicVariant ConvertFromPyObject (PyObject *obj)
{
  if (obj->ob_type->tp_getattr == KeplerPyObj_GetAttr)
  {
    KeplerPyTypeObject *kType = reinterpret_cast<KeplerPyTypeObject *> (obj->ob_type);
    DynamicVariant ret;
    ret.AllocateSpace (kType->meta.size, kType->meta.type, kType->meta.CopyConstructor_Variant, kType->meta.Destructor_Variant);
    auto *kObj = reinterpret_cast<KeplerPyObject<char>*> (obj);
    memcpy (ret.Addr(), &kObj->Value, kType->meta.size);
    return ret;
  }

  if (obj->ob_type->tp_getattr == KeplerPyHdl_GetAttr)
  {
    KeplerPyHandle *pyHdl = reinterpret_cast<KeplerPyHandle *> (obj);
    return MetaObject (pyHdl->Value, pyHdl->GetMeta());
  }

  if (obj->ob_type == &PyComposite::Type)
    return reinterpret_cast<PyComposite *> (obj)->composite;

  if (PyLong_Check (obj))
    return (int) PyLong_AsLong (obj);

  if (PyFloat_Check (obj))
    return (float) PyFloat_AsDouble (obj);

  if (PyUnicode_Check (obj))
    return String (PyUnicode_AsUTF8 (obj));

  if (PyDict_Check (obj))
  {
    PyObject *pyKey, *pyValue;
    Py_ssize_t pos = 0;
    HashTable<String, DynamicVariant> table (Utilities::Hash::SuperFast);

    while (PyDict_Next (obj, &pos, &pyKey, &pyValue))
    {
      DynamicVariant key = ConvertFromPyObject (pyKey);
      DynamicVariant val = ConvertFromPyObject (pyValue);

      table.Insert (key, val);
    }

    return table;
  }

  if (PyIter_Check (obj))
  {
    PyObject *iterator = PyObject_GetIter (obj);
    PyObject *item;
    std::vector<DynamicVariant> list;

    while (item = PyIter_Next (iterator))
    {
      DynamicVariant i = ConvertFromPyObject (item);
      list.emplace_back (std::move (i));
      Py_DECREF (item);
    }

    Py_DECREF (iterator);

    return list;
  }

  // Return an variant that holds the py object
  return DynamicVariant (PyObjectHolder (obj));
}
