// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Generic PyObject for Kepler bindings.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "KeplerPyObject.h"
#include "PyCallable.h"
#include "../MetaTyping/function.h"
#include "../MetaTyping/tuple.h"
#include "../String/string.h"
#include "../Logger/logger.h"
#include "convertpytuple.h"

#include <Python/Python.h>
#include <cstdio>
#include <memory>
#include <map>

static PyObject *getAttrInner (MetaType *meta, PyObject *pySelf, void *cppSelf, char *name)
{
  PyObject *call = nullptr;
  // Find the value in the method table
  meta->methods.CallOnValue (name,
  [&] (const MetaFunction &func) {
    PyCallable *pyCall = PyObject_New (PyCallable, &PyCallable::Type);

    new (pyCall) PyCallable (PyCallableClosure (
    [&func, meta, pySelf] (PyObject *pyArgs, PyObject *pyKwdArgs) {
      Tuple args = ConvertFromPyTuple (pyArgs);

      DynamicVariant ret;

      void *self = meta->bindAsHandle ?
                   reinterpret_cast<KeplerPyHandle *> (pySelf)->Value.operator->() :
                   &reinterpret_cast<KeplerPyObject<char>*> (pySelf)->Value;

      ret = func (self, args);

      if (ret.IsConstructed())
        return ConvertToPyObject (ret);

      else
        Py_RETURN_NONE;
    }));

    Py_INCREF (pySelf);

    call = reinterpret_cast<PyObject *> (pyCall);
  });

  if (call)
    return call;

  if (meta->propGetters.Contains (name))
  {
    DynamicVariant value = meta->Get (name, cppSelf);
    return ConvertToPyObject (value);
  }

  PyErr_Format (PyExc_NameError, "No property or method \"%s\" in built in type: %s", name, meta->name.CStr());
  return nullptr;
}

static int setAttrInner (MetaType *meta, PyObject *pySelf, void *cppSelf, char *name, PyObject *py_value)
{
  if (meta->propSetters.Contains (name))
  {
    meta->propSetters.CallOnValue (name, [&] (MetaType::Setter &setter) {
      DynamicVariant value;
      //setter.type->py_convertFrom (py_value, value);
      value = ConvertFromPyObject (py_value);
      meta->Set (name, cppSelf, value.Addr());
    });
    return 0;
  }

  return -1;
}

// -----------------------------------------------------------------------------

PyObject *KeplerPyObj_GetAttr (PyObject *self_, char *name)
{
  KeplerPyObject<char> *self = reinterpret_cast<KeplerPyObject<char> *> (self_);
  MetaType *meta = self->GetMeta();
  return getAttrInner (meta, self_, &self->Value, name);
}

int KeplerPyObj_SetAttr (PyObject *self_, char *name, PyObject *value)
{
  KeplerPyObject<char> *self = reinterpret_cast<KeplerPyObject<char> *> (self_);
  MetaType *meta = self->GetMeta();
  return setAttrInner (meta, self_, &self->Value, name, value);
}

PyObject *KeplerPyObj_GetAttrObj (PyObject *self, PyObject *name)
{
  return KeplerPyObj_GetAttr (self, PyUnicode_AsUTF8 (name));
}

int KeplerPyObj_SetAttrObj (PyObject *self, PyObject *name, PyObject *value)
{
  return KeplerPyObj_SetAttr (self, PyUnicode_AsUTF8 (name), value);
}

// -----------------------------------------------------------------------------

int KeplerPyObj_Print (PyObject *self_, FILE *out, int flags)
{
  KeplerPyObject<void *> *self = reinterpret_cast<KeplerPyObject<void *>*> (self_);
  DynamicVariant str;

  if (self->GetMeta()->Call ("ToString", self->Value, Tuple(), str))
  {
    if (str.IsType<String>())
      fputs (str.as<String>().CStr(), out);

    else if (str.IsType<String>())
      std::fputs (str.as<String>().CStr(), out);
  }

  else fputs (self->GetMeta()->name.CStr(), out);

  return 0;
}

PyObject *KeplerPyObj_ToStr (PyObject *self_)
{
  KeplerPyObject<char> *self = reinterpret_cast<KeplerPyObject<char>*> (self_);
  DynamicVariant str;

  if (self->GetMeta()->Call ("ToString", &self->Value, Tuple(), str))
  {
    if (str.IsType<String>())
      return PyUnicode_FromString (str.as<String>().CStr());

    else
      return PyUnicode_FromString ("Unknown string type returned from ToString");
  }

  else return PyUnicode_FromString (self->GetMeta()->name.CStr());
}

PyObject *KeplerPyHdl_GetAttr (PyObject *self_, char *name)
{
  KeplerPyHandle *self = reinterpret_cast<KeplerPyHandle *> (self_);
  MetaType *meta = self->GetMeta();

  return getAttrInner (meta, self_, self->Value.operator->(), name);
}

int KeplerPyHdl_SetAttr (PyObject *self_, char *name, PyObject *value)
{
  KeplerPyHandle *self = reinterpret_cast<KeplerPyHandle *> (self_);
  MetaType *meta = self->GetMeta();
  return setAttrInner (meta, self_, self->Value.operator->(), name, value);
}

int KeplerPyHdl_Print (PyObject *self_, FILE *out, int flags)
{

  KeplerPyHandle *self = reinterpret_cast<KeplerPyHandle *> (self_);

  DynamicVariant str;

  if (self->GetMeta()->Call ("ToString", self->Value.operator->(), Tuple(), str))
  {
    if (str.IsType<String>())
      fputs (str.as<String>().CStr(), out);

    else if (str.IsType<String>())
      std::fputs (str.as<String>().CStr(), out);
  }

  else fputs (self->GetMeta()->name.CStr(), out);

  return 0;
}

PyObject *KeplerPyHdl_ToStr (PyObject *self_)
{

  KeplerPyHandle *self = reinterpret_cast<KeplerPyHandle *> (self_);

  DynamicVariant str;

  if (self->GetMeta()->Call ("ToString", self->Value.operator->(), Tuple(), str))
  {
    if (str.IsType<String>())
      return PyUnicode_FromString (str.as<String>().CStr());

    else
      return PyUnicode_FromString ("Unknown string type returned from ToString");
  }

  else return PyUnicode_FromString (self->GetMeta()->name.CStr());
}

void KeplerPyObj_Dealloc (PyObject *self_)
{
  KeplerPyObject<void *> *self = reinterpret_cast<KeplerPyObject<void *>*> (self_);
  self->GetMeta()->Destroy (&self->Value);
}

void KeplerPyHdl_Dealloc (PyObject *self_)
{
  KeplerPyHandle *self = reinterpret_cast<KeplerPyHandle *> (self_);

  if (self->Value.GetInner()->refcount == 1)
    self->GetMeta()->Destroy (self->Value.GetInner()->ptr);

  self->~KeplerPyHandle();
  self->ob_base.ob_type->tp_free (self);
}

static std::map<String, std::shared_ptr<KeplerPyTypeObject>> s_pyObjTypes;

static PyTypeObject *CreateTypeObject (MetaType &meta);

PyTypeObject *GetTypeObject (MetaType &meta)
{
  if (s_pyObjTypes.find (meta.name) != s_pyObjTypes.end())
  {
    return &s_pyObjTypes[meta.name]->pyType;
  }

  else
  {
    return CreateTypeObject (meta);
  }
}


static PyTypeObject *CreateTypeObject (MetaType &meta)
{
  std::shared_ptr<KeplerPyTypeObject> ret = std::make_shared<KeplerPyTypeObject> (meta);
  //memset (ret.get(), 0, sizeof PyTypeObject);

  PyObject *objName = PyUnicode_FromFormat ("%s.%s", meta.module.CStr(), meta.name.CStr());

  ret->pyType = PyTypeObject {
    PyVarObject_HEAD_INIT (NULL, 0)
  };

  ret->pyType.tp_name = PyUnicode_AsUTF8 (objName);

  if (meta.bindAsHandle)
  {
    ret->pyType.tp_basicsize = sizeof KeplerPyHandle;
    ret->pyType.tp_dealloc = KeplerPyHdl_Dealloc;
    ret->pyType.tp_new = KeplerPyHdl_New;
    ret->pyType.tp_print = KeplerPyHdl_Print;
    ret->pyType.tp_str = KeplerPyHdl_ToStr;
    ret->pyType.tp_getattr = KeplerPyHdl_GetAttr;
    ret->pyType.tp_setattr = KeplerPyHdl_SetAttr;
  }

  else
  {
    ret->pyType.tp_basicsize = meta.size + sizeof KeplerPyObjectBase;
    ret->pyType.tp_dealloc = KeplerPyObj_Dealloc;
    ret->pyType.tp_new = KeplerPyObj_New;
    ret->pyType.tp_print = KeplerPyObj_Print;
    ret->pyType.tp_str = KeplerPyObj_ToStr;
    ret->pyType.tp_getattr = KeplerPyObj_GetAttr;
    ret->pyType.tp_setattr = KeplerPyObj_SetAttr;
  }

  int fail = PyType_Ready (&ret->pyType);
  BREAK_IF (fail != 0, "Could not create python binding.");

  s_pyObjTypes.emplace (std::make_pair (meta.name, std::shared_ptr<KeplerPyTypeObject> (ret)));
  return &ret->pyType;
}

PyObject *KeplerPyObj_New (PyTypeObject *myType_, PyObject *args, PyObject *kwds)
{
  KeplerPyTypeObject *myType = reinterpret_cast<KeplerPyTypeObject *> (myType_);
  PyObject *ret = myType->pyType.tp_alloc (&myType->pyType, 0);

  myType->meta.constructor (ret + 1, ConvertFromPyTuple (args));
  return ret;
}

PyObject *KeplerPyHdl_New(PyTypeObject *myType_, PyObject *args, PyObject *kwds)
{
  KeplerPyTypeObject *myType = reinterpret_cast<KeplerPyTypeObject *> (myType_);
  PyObject *ret = myType->pyType.tp_alloc(&myType->pyType, 0);

  KeplerPyHandle *ret_ =  reinterpret_cast<KeplerPyHandle*>(ret);
  memset(&ret_->Value, 0, sizeof(ret_->Value));
  return ret;
}

MetaType *KeplerPyObjectBase::GetMeta()
{
  return &reinterpret_cast<KeplerPyTypeObject *> (this->ob_base.ob_type)->meta;
}
const MetaType *KeplerPyObjectBase::GetMeta() const
{
  return &reinterpret_cast<KeplerPyTypeObject *> (this->ob_base.ob_type)->meta;
}
