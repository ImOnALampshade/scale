// -----------------------------------------------------------------------------
// Author: Howard Hughes
//
// Components for Python scripts to use
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PY_SCRIPT_COMPONENT_H
#define PY_SCRIPT_COMPONENT_H

#include "../KeplerCore/component.h"
#include "../pyinclude.h"

class PyScriptComponent : public Component
{
public:
  PyScriptComponent (PyObject *pyClass);
  ~PyScriptComponent();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  PyObject *GetPyObject();

private:
  PyObject *m_instance;
};


#endif
