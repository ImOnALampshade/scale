// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Wraps a python method within C++ for our event system.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "PyDelegate.h"
#include "convertpytuple.h"



PyObject* PyDelegate::Invoke(PyObject *args, PyObject *kwds /*= nullptr*/)
{
  PyObject * pMethod = PyObject_GetAttr(pySelf,pyMethodName);
  PyObject * retVal;

  // Check to make sure we got a thing, and that the thing is callable.
  if (pMethod && PyCallable_Check(pMethod))
  {
    retVal= PyObject_CallObject(pMethod, args);
  }

  Py_XDECREF(args);
  Py_XDECREF(kwds);

  return retVal;
}

PyObject * PyDelegate::Invoke(Tuple * args)
{
  PyObject * pyArgs = ConvertToPyTuple(*args);
  PyObject * retVal = Invoke(pyArgs, nullptr);
  Py_XDECREF(pyArgs);
  return retVal;
}

PyObject * PyDelegate::operator()(Tuple *args)
{
  return Invoke(args);
}