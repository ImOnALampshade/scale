// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Messaging system for python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PY_MESSAGING_H
#define PY_MESSAGING_H

#include "../pyinclude.h"

PyObject     *PyDispatchMessage (PyObject *self, PyObject *args);
PyObject *PyDispatchDownMessage (PyObject *self, PyObject *args);
PyObject   *PyDispatchUpMessage (PyObject *self, PyObject *args);

#endif
