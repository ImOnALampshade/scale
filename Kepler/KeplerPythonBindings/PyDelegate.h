// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Wraps a python method within C++ for our event system.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once
#ifndef PYDELEGATE_H_
#define PYDELEGATE_H_

#include "Python/Python.h"
#include "../MetaTyping/tuple.h"

struct PyDelegate
{
  PyDelegate(PyObject *pySelf_, PyObject *pyMethodName_)
    : pySelf(pySelf_), pyMethodName(pyMethodName_)
  {
  }

  PyObject *pySelf; // The PyObject that the script will be invoked on
  PyObject *pyMethodName; // The method string

  PyObject * operator()(Tuple *args);

  PyObject * Invoke(Tuple * args);
  PyObject * Invoke(PyObject *args, PyObject *kwds = nullptr);
};

#endif