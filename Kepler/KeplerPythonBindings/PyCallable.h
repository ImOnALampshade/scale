// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Callable function object within Python. We use this in place of
//  PyMethodDef because we don't want to do codegen to make C-style
//  functions for everything.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PY_CALLABLE_H_
#define PY_CALLABLE_H

#include <functional>

#include "../MetaTyping/function.h"
#include "../pyinclude.h"

typedef std::function<PyObject* (PyObject *, PyObject *) > PyCallableClosure;

struct PyCallable
{
  PyObject_HEAD

  PyCallable (PyCallableClosure closure = [] (PyObject *, PyObject *, PyObject *) {return (PyObject *) nullptr; });

  PyCallableClosure CallingClosure;

  PyObject *operator() (PyObject *args = nullptr, PyObject *kwdArgs = nullptr);

  static PyTypeObject Type;
};


#endif