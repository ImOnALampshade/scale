// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Messaging system for python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "pymessaging.h"
#include "pycomposite.h"
#include "convertpytuple.h"
#include "../KeplerCore/messagemeta.h"

// -----------------------------------------------------------------------------

static void SendMessage (Handle<Composite> self, String name, PyObject *args,
                         void (MessageMeta::*MetaSender) (Handle<Composite>, const Tuple &) const,
                         void (Composite::*CompositeSender) (String, const Tuple &))
{
  Tuple msgArgs;

  for (Py_ssize_t i = 1; i < PyTuple_Size (args); ++i)
  {
    PyObject *item = PyTuple_GetItem (args, i);
    msgArgs.emplace_back (ConvertFromPyObject (item));
  }

  MessageMeta *meta = MessageMeta::Get (name);

  if (meta)
    (meta->*MetaSender) (self, msgArgs);

  else
    (self.GetInner()->ptr->*CompositeSender) (name, msgArgs);
}

// -----------------------------------------------------------------------------

PyObject *PyDispatchMessage (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);
  
  if (PyTuple_Size (args) < 1)
  {
    PyErr_Format (PyExc_TypeError, "Dispatch takes at least 1 argument, none given");
    return nullptr;
  }

  String name = PyUnicode_AsUTF8 (PyTuple_GetItem (args, 0));
  

  SendMessage (self->composite, name, args,
               &MessageMeta::Dispatch,
               &Composite::DispatchTuple);

  Py_RETURN_NONE;
}

PyObject *PyDispatchDownMessage (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (PyTuple_Size(args) < 1)
  {
    PyErr_Format(PyExc_TypeError, "Dispatch takes at least 1 argument, none given");
    return nullptr;
  }

  String name = PyUnicode_AsUTF8(PyTuple_GetItem(args, 0));

  SendMessage (self->composite, name, args,
               &MessageMeta::DispatchDown,
               &Composite::DispatchDownTuple);

  Py_RETURN_NONE;
}

PyObject *PyDispatchUpMessage (PyObject *self_, PyObject *args)
{
  PyComposite *self = reinterpret_cast<PyComposite *> (self_);

  if (PyTuple_Size(args) < 1)
  {
    PyErr_Format(PyExc_TypeError, "Dispatch takes at least 1 argument, none given");
    return nullptr;
  }

  String name = PyUnicode_AsUTF8(PyTuple_GetItem(args, 0));

  SendMessage (self->composite, name, args,
               &MessageMeta::DispatchUp,
               &Composite::DispatchUpTuple);

  Py_RETURN_NONE;
}

// -----------------------------------------------------------------------------
