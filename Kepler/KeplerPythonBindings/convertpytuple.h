// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Functions for convert to/from to and from PyTuples and Tuples
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef CONVERT_PY_TUPLE_H
#define CONVERT_PY_TUPLE_H

#include "../MetaTyping/tuple.h"
#include "../Serializer/jsonparser.h"
#include "../pyinclude.h"

PyObject *ConvertToPyTuple (const Tuple &tuple);
Tuple   ConvertFromPyTuple (PyObject *pytuple);

PyObject      *ConvertToPyObject (const Json::Value &var);
PyObject      *ConvertToPyObject (const DynamicVariant &var);
DynamicVariant ConvertFromPyObject (PyObject *obj);

#endif
