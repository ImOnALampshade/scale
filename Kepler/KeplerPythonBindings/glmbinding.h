// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  GLM bindings for Python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef GLM_BINDING_H
#define GLM_BINDING_H

#include "../glminclude.h"

class GlmBinding
{
public:
  static void CreateMeta();
};

#endif
