// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Composite bindings for Python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef COMPOSITE_BINDING_H
#define COMPOSITE_BINDING_H

#include "PyMetaBindings.h"
#include "../KeplerCore/composite.h"

struct PyComposite
{
  PyObject_HEAD;
  Handle<Composite> composite;

  PyComposite (Handle<Composite> h);

  static PyTypeObject Type;
  static PyMethodDef Methods[];

  static PyObject *GetAttr (PyObject *self, char *name);
  static PyObject *GetAttrObj (PyObject *self, PyObject *name);

  static PyObject *Destroy (PyObject *self, PyObject *args);
  static PyObject *Parent (PyObject *self, PyObject *args);
  static PyObject *Space (PyObject *self, PyObject *args);
  static PyObject *GetGuid (PyObject *self, PyObject *args);
  static PyObject *IsValid (PyObject *self, PyObject *args);
  static PyObject *FindEntityFromThis(PyObject *self, PyObject *args);
  static PyObject *Name(PyObject *self, PyObject *args);
  static PyObject *Compare(PyObject *self, PyObject *other, int op);

  void *operator new (size_t size);
};

PyObject *PyGetGame (PyObject *self, PyObject *args);
PyObject *PyLoadRecipe (PyObject *self, PyObject *args);
PyObject *PyCreateGame (PyObject *self, PyObject *args);
PyObject *PyCreateSpace (PyObject *self, PyObject *args);
PyObject *PyCreateComposite (PyObject *self, PyObject *args);
PyObject *PyRegisterComponent (PyObject *self, PyObject *args);
PyObject *PyConnect (PyObject *self, PyObject *args);
PyObject *PyFindEntity (PyObject *self, PyObject *args);

PyObject * PyCreateInstance(PyObject * self, PyObject * args);

void BindComposite();

#endif
