// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph node for debug printing on creating/destruction/drawing
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "debugprintnode.h"

// -----------------------------------------------------------------------------

DebugPrintNode::DebugPrintNode()
{
  printf ("DebugPrintNode::DebugPrintNode\n");
}

DebugPrintNode::~DebugPrintNode()
{
  printf ("DebugPrintNode::~DebugPrintNode\n");
}

void DebugPrintNode::Draw (BatchGraph *graph)
{
  printf ("DebugPrintNode::Draw\n");
}

// -----------------------------------------------------------------------------
