// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for managing the rendering of an entire space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef BASIC_RENDERED_SPACE_COMPONENT_H
#define BASIC_RENDERED_SPACE_COMPONENT_H

#include "../KeplerCore/component.h"

// -----------------------------------------------------------------------------

class BasicRenderedSpaceComponent : public Component
{
private:
  unsigned int m_layer;
  bool         m_skip;

public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();
  virtual void Render () = 0;

  void SetSkipped (bool skip);

  bool         IsSkipped() const;
  unsigned int Layer() const;
};

// -----------------------------------------------------------------------------

#endif
