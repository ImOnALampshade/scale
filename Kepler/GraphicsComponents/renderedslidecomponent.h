// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Rendered space for displaying a full screen slide
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef RENDERED_SLIDE_COMPONENT_H
#define RENDERED_SLIDE_COMPONENT_H

#include "basicrenderedspacecomponent.h"

class RenderedSlideComponent : public BasicRenderedSpaceComponent
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  virtual void Render();

private:
  GLuint m_texture;
};

#endif
