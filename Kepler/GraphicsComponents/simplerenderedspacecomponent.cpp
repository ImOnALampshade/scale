// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for rendering a simple space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "../KeplerCore/game.h"
#include "simplerenderedspacecomponent.h"
#include "simplerenderablecomponent.h"

using namespace glm;

// -----------------------------------------------------------------------------

void SimpleRenderedSpaceComponent::Initialize (const Json::Object &jsonData)
{
  BasicRenderedSpaceComponent::Initialize (jsonData);

  m_near = jsonData.Locate ("near", 0.25f);
  m_far = jsonData.Locate ("far", 100.0f);
  m_fov = jsonData.Locate ("fov", 60.0f);

  bool found;

  found = jsonData.CallOnValue ("eye", [this] (const Json::List &l) {
    for (unsigned i = 0; i < l.size() && i < 3; ++i)
      m_eye[i] = float (l[i].as<Json::Number>());
  });

  if (!found)
    m_eye = vec3 (0);

  found = jsonData.CallOnValue ("direction", [this] (const Json::List &l) {
    for (unsigned i = 0; i < l.size() && i < 3; ++i)
      m_dir[i] = float (l[i].as<Json::Number>());
  });

  if (!found)
    m_dir = vec3 (0, 0, -1);

  found = jsonData.CallOnValue ("up", [this] (const Json::List &l) {
    for (unsigned i = 0; i < l.size() && i < 3; ++i)
      m_up[i] = float (l[i].as<Json::Number>());
  });

  if (!found)
    m_up = vec3 (0, 1, 0);

  GLuint prog = glCreateProgram();
  GLuint vert = glCreateShader (GL_VERTEX_SHADER);
  GLuint frag = glCreateShader (GL_FRAGMENT_SHADER);

  String vertFile = jsonData.Locate ("vertex");
  String fragFile = jsonData.Locate ("fragment");

  OpenGL::CompileShader (vert, vertFile);
  OpenGL::CompileShader (frag, fragFile);

  glAttachShader (prog, vert);
  glAttachShader (prog, frag);

  glBindAttribLocation (prog, 0, "v_coord");
  glBindAttribLocation (prog, 1, "v_texcoord");
  glBindAttribLocation (prog, 2, "v_normal");

  OpenGL::LinkProgram (prog);

  int l = glGetUniformLocation (prog, "pl_ambient");
  printf ("%i\n", l);

  m_shader = prog;

  glDeleteShader (vert);
  glDeleteShader (frag);

  m_plDiffuse = vec3 (1);
  m_plSpecular = vec3 (0.25f);
  m_plAmbient = vec3 (5 / 255.f);
  m_plSpecularPower = 1;
}

void SimpleRenderedSpaceComponent::Free()
{
  glDeleteProgram (m_shader.GetProgram());

  BasicRenderedSpaceComponent::Free();
}

// -----------------------------------------------------------------------------

void SimpleRenderedSpaceComponent::Render()
{
  glClear (GL_DEPTH_BUFFER_BIT);

  glEnable (GL_DEPTH_TEST);
  glEnable (GL_CULL_FACE);

  Owner()->Dispatch ("BindBuffers");

  glUseProgram (m_shader.GetProgram());

  int w, h;
  CoreEngine::Game->Dispatch ("GetWindowSize", &w, &h);

  mat4 proj = perspective (m_fov, float (w) / h, m_near, m_far);
  mat4 view = lookAt (m_eye, m_eye + m_dir, m_up);

  m_shader.SetUniform ("projection_view_matrix", proj * view);
  m_shader.SetUniform ("pl_position", m_eye);
  m_shader.SetUniform ("pl_diffuse", m_plDiffuse);
  m_shader.SetUniform ("pl_specular", m_plSpecular);
  m_shader.SetUniform ("pl_eyepos", m_eye);
  m_shader.SetUniform ("pl_ambient", m_plAmbient);
  m_shader.SetUniform ("pl_specularPower", m_plSpecularPower);

  for (::Handle<SimpleRenderableComponent> renderable : m_renderable)
    renderable->Render (m_shader);
}

// -----------------------------------------------------------------------------

glm::vec3 SimpleRenderedSpaceComponent::Eye() const
{
  return m_eye;
}

glm::vec3 SimpleRenderedSpaceComponent::Direction() const
{
  return m_dir;
}

glm::vec3 SimpleRenderedSpaceComponent::Up() const
{
  return m_up;
}

void SimpleRenderedSpaceComponent::Eye (const glm::vec3 &eye)
{
  m_eye = eye;
}

void SimpleRenderedSpaceComponent::Direction (const glm::vec3 &dir)
{
  m_dir = dir;
}

void SimpleRenderedSpaceComponent::Up (const glm::vec3 &up)
{
  m_up = up;
}

// -----------------------------------------------------------------------------

void SimpleRenderedSpaceComponent::AddRendered (::Handle<Component> renderable)
{
  m_renderable.push_back (renderable.as<SimpleRenderableComponent>());
}

// -----------------------------------------------------------------------------