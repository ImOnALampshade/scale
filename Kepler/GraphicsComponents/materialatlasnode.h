// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph material node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef MATERIAL_ATLAS_NODE_H
#define MATERIAL_ATLAS_NODE_H

#include "../BatchingGraph/batchinggraph.h"

// -----------------------------------------------------------------------------

struct Material
{
  GLuint m_diffuseIndex;
  GLuint m_specularIndex;
  String m_name;
};

// -----------------------------------------------------------------------------

class MaterialNode : public BatchNodeWithChild
{
public:
  MaterialNode (std::vector<GLuint> textures);

  virtual void Draw (BatchGraph *graph);

  void AddMaterial (const Material &m);

private:
  std::vector<GLuint>   m_textures;
  std::vector<Material> m_materials;
};

// -----------------------------------------------------------------------------

#endif
