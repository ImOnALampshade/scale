// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph scene owner node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "scenenode.h"

// -----------------------------------------------------------------------------

SceneNode::SceneNode()
{
}

// -----------------------------------------------------------------------------

void SceneNode::Draw (BatchGraph *graph)
{
  if (m_geometry) m_geometry->Draw (graph);

  if (m_particles) m_particles->Draw (graph);
}

// -----------------------------------------------------------------------------

std::shared_ptr<BatchNode> SceneNode::Geometry()  { return m_geometry; }
std::shared_ptr<BatchNode> SceneNode::Particles() { return m_particles; }

// -----------------------------------------------------------------------------

void SceneNode::Geometry (std::shared_ptr<BatchNode> geometry)   { m_geometry = geometry; }
void SceneNode::Particles (std::shared_ptr<BatchNode> particles) { m_particles = particles; }

// -----------------------------------------------------------------------------
