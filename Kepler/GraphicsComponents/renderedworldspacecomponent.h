// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for an entire rendered space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef RENDERED_WORLD_SPACE_COMPONENT_H
#define RENDERED_WORLD_SPACE_COMPONENT_H

#include "../OpenGLHelpers/shader.h"
#include "../BatchingGraph/batchinggraph.h"
#include "basicrenderedspacecomponent.h"

#include "batchnode.h"
#include "jointnode.h"
#include "scenenode.h"

// -----------------------------------------------------------------------------

class CameraComponent;
class RenderableComponent;

class RenderedWorldSpaceComponent : public BasicRenderedSpaceComponent
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  virtual void Render ();

  void AddCamera (String name, ::Handle<CameraComponent> camera);
  void AddObject (::Handle<RenderableComponent> object);

  BatchGraph &Graph();

  std::shared_ptr<JointNode> CameraParent();
  std::shared_ptr<JointNode> ShaderParent();
  std::shared_ptr<JointNode> MaterialParent();
  std::shared_ptr<JointNode> BatchParent();

  std::vector<ObjectMatrix> &MatrixScratchBuffer();
  GLuint MatrixBuffer();

  COMPONENT_META

private:

  BatchGraph m_graph;
  std::shared_ptr<JointNode> m_cameraParent;
  std::shared_ptr<JointNode> m_shaderParent;
  std::shared_ptr<JointNode> m_materialParent;
  std::shared_ptr<JointNode> m_batchParent;

  std::shared_ptr<SceneNode> m_sceneParent;

  std::vector <ObjectMatrix> m_matrixScratchBuffer;
  GLuint m_matrixBuffer;
};

// -----------------------------------------------------------------------------

#endif