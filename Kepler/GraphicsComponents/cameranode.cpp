// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph instanced rendering
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "cameranode.h"
#include "../KeplerCore/game.h"
#include "shadercontextmanagercomponent.h"
#include "batchproperties.h"

using namespace glm;

// -----------------------------------------------------------------------------

CameraNode::CameraNode (::Handle<CameraComponent> camera) :
  m_camera (camera)
{
}

void CameraNode::Draw (BatchGraph *graph)
{
  if (!m_camera.IsValid())
    return;

  mat4 m = m_camera->GetMatrix();
  graph->Property (BatchProperty::PROJECTION_VIEW, value_ptr (m));

  DrawChild (graph);
}

// -----------------------------------------------------------------------------
