// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for loading shader programs
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SHADER_LOADER_H
#define SHADER_LOADER_H

#include "../KeplerCore/component.h"

class ShaderLoaderComponent : public Component
{
private:
  HashTable<String, unsigned> m_shaders;
  HashTable<String, OpenGL::Shader> m_programs;

public:
  ShaderLoaderComponent();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  void LinkNewShader (const Json::Object &jsonProgram);
  OpenGL::Shader *GetProgram (String name);

  COMPONENT_META
};

#endif
