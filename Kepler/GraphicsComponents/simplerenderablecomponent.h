// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for rendering a simple object
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SIMPLE_RENDERED_COMPONENT_H
#define SIMPLE_RENDERED_COMPONENT_H

#include "../KeplerCore/component.h"

// -----------------------------------------------------------------------------

class SimpleRenderableComponent : public Component
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  virtual void Render (OpenGL::Shader &shader);

  COMPONENT_META
private:
  unsigned                     m_modelIndex;
  ::Handle<TransformComponent> m_transform;

};

// -----------------------------------------------------------------------------

#endif
