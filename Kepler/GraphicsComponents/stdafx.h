// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Precompiled header for Graphics
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../glminclude.h"
#include "../OpenGLHelpers/indirect.h"
#include "../OpenGLHelpers/glinclude.h"
#include "../OpenGLHelpers/shader.h"
#include "../OpenGLHelpers/texture.h"
#include "../HashTable/hashtable.h"
#include "../String/string.h"
#include "../KeplerCore/transformcomponent.h"

#include <mutex>
#include <queue>
#include <algorithm>
#include <memory>
#include <vector>

