// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for placing the camera in the world
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "../KeplerCore/game.h"
#include "cameracomponent.h"

using namespace glm;

// -----------------------------------------------------------------------------

void CameraComponent::Initialize (const Json::Object &jsonData)
{
  m_transform = Owner()->GetComponent<TransformComponent>();

  // Tell the space to add a camera
  Space()->Dispatch ("AddCamera", Handle());

  m_near = float (jsonData.Locate ("Near", 1.0).as<Json::Number>());
  m_far  = float (jsonData.Locate ("Far", 10000.0).as<Json::Number>());
  m_fov  = float (jsonData.Locate ("FieldOfView", 20.0).as<Json::Number>());
}

Mat4 CameraComponent::GetMatrix()
{
  float aspect = getAspectRatio();

  return perspective (m_fov * DEGREE, aspect, m_near, m_far) * inverse (m_transform->GetMatrix());
}

// -----------------------------------------------------------------------------

float CameraComponent::Far() const
{
  return m_far;
}

void CameraComponent::Far (float far)
{
  m_far = far;
}

// -----------------------------------------------------------------------------

float CameraComponent::Near() const
{
  return m_near;
}

void CameraComponent::Near (float near)
{
  m_near = near;
}

// -----------------------------------------------------------------------------

float CameraComponent::FieldOfView() const
{
  return m_fov;
}

void CameraComponent::FieldOfView (float fov)
{
  m_fov = fov;
}

// -----------------------------------------------------------------------------

Vec2 CameraComponent::WorldToScreen (Vec3 worldCoordinates)
{

  Vec3 direction = worldCoordinates - m_transform->WorldPosition();
  direction = m_transform->WorldRotation().inverted().rotate (direction);
  direction = direction.normalized() * distanceToScreenPlane();
  return Vec2 (direction.x / getAspectRatio(), direction.y);
}

WorldRay CameraComponent::ScreenToWorld (Vec2 screenCoordinates)
{
  Vec3 origin = m_transform->WorldPosition();
  Vec3 direction = Vec3 (screenCoordinates.x * getAspectRatio(), -screenCoordinates.y, -distanceToScreenPlane());
  direction = m_transform->WorldRotation().rotate (direction);
  return WorldRay (origin, direction);
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (Camera)
{
  META_INHERIT (CameraComponent, "Kepler", "Component");
  META_HANDLE (CameraComponent);

  META_ADD_PROP (CameraComponent, Near);
  META_ADD_PROP (CameraComponent, Far);
  META_ADD_PROP (CameraComponent, FieldOfView);

  META_ADD_METHOD (CameraComponent, WorldToScreen);
  META_ADD_METHOD (CameraComponent, ScreenToWorld);

  META_FINALIZE_PTR (CameraComponent, MetaPtr);
}

float CameraComponent::distanceToScreenPlane()
{
  return 1 / tan ( (m_fov / 2) *DEGREE);
}

float CameraComponent::getAspectRatio()
{
  int iw, ih;
  CoreEngine::Game->Dispatch("GetWindowSize", &iw, &ih);
  return float(iw) / float(ih);
}

