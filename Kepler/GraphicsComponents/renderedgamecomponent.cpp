// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for rendering the actual game into the window
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "renderedgamecomponent.h"

using namespace std;
using namespace glm;

// -----------------------------------------------------------------------------



void RenderedGameComponent::Initialize (const Json::Object &jsonData)
{
  RegisterMessageProc ("AddRenderedSpace", function<void (::Handle<BasicRenderedSpaceComponent>) > (
                         bind (&RenderedGameComponent::AddRenderedSpace, this, placeholders::_1)));


  vec2 vbuffer[] = {
    vec2 (-1, -1),
    vec2 (+1, -1),
    vec2 (+1, +1),
    vec2 (-1, +1)
  };
  unsigned char ibuffer[] = {
    0, 1, 2,
    0, 2, 3
  };

  unsigned buffers[2];
  glGenBuffers (2, buffers);
  glGenVertexArrays (1, &m_quadArray);
  m_quadVBuffer = buffers[0];
  m_quadIBuffer = buffers[1];

  glBindVertexArray (m_quadArray);

  glBindBuffer (GL_ARRAY_BUFFER,         m_quadVBuffer);
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, m_quadIBuffer);

  // Creating the storage - and the data itself is immutable!
  glBufferStorage (GL_ARRAY_BUFFER, sizeof vbuffer, vbuffer, 0);
  glBufferStorage (GL_ELEMENT_ARRAY_BUFFER, sizeof ibuffer, ibuffer, 0);

  glEnableVertexAttribArray (0);

  glBindVertexBuffer (0, m_quadVBuffer, 0, sizeof vec2);
  glVertexAttribFormat (0, 2, GL_FLOAT, GL_FALSE, 0);

  GL_CHECK_ERRORS();
}

void RenderedGameComponent::Free()
{
  glDeleteBuffers (1, &m_quadVBuffer);
  glDeleteVertexArrays (1, &m_quadArray);
}

void RenderedGameComponent::Render()
{
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  for (::Handle<BasicRenderedSpaceComponent> space : m_renderedSpaces)
    if (space.IsValid())
      space->Render();
}

// -----------------------------------------------------------------------------

void RenderedGameComponent::BindQuad()
{
  glBindVertexArray (m_quadArray);
}

void RenderedGameComponent::DrawQuad()
{
  glDrawElements (GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, nullptr);
}

// -----------------------------------------------------------------------------

bool SortSpaces (Handle<BasicRenderedSpaceComponent> lhs,
                 Handle<BasicRenderedSpaceComponent> rhs)
{
  return lhs->Layer() < rhs->Layer();
}

void RenderedGameComponent::AddRenderedSpace (::Handle<BasicRenderedSpaceComponent> space)
{
  m_renderedSpaces.erase (
  remove_if (m_renderedSpaces.begin(), m_renderedSpaces.end(), [] (::Handle<BasicRenderedSpaceComponent> space) {
    return !space.IsValid();
  }),
  m_renderedSpaces.end()
  );

  m_renderedSpaces.insert
  (
    upper_bound (m_renderedSpaces.begin(), m_renderedSpaces.end(), space, SortSpaces),
    space
  );
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (RenderedGame)
{
}
