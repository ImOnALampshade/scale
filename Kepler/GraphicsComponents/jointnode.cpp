// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Scene geometry node for
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "jointnode.h"
#include "../HashFunctions/hashfunctions.h"

// -----------------------------------------------------------------------------

JointNode::JointNode() :
  m_namedIndex (Utilities::Hash::SuperFast)
{
}

void JointNode::Draw (BatchGraph *graph)
{
  DrawChildren (graph);
}

void JointNode::AddNamedChild (String name, std::shared_ptr<BatchNode> node)
{
  // Add the child to the named list
  m_namedIndex.Insert (name, Children().size());

  // Add the actual child
  AddChild (node);
}

std::shared_ptr<BatchNode> JointNode::GetChild (String name)
{
  auto found = m_namedIndex.Find (name);

  if (found == m_namedIndex.end())
    return std::shared_ptr<BatchNode>();

  else
    return Children() [found->value];
}

// -----------------------------------------------------------------------------
