// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for rendering a simple space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SIMPLE_RENDERED_SPACE_COMPONENT_H
#define SIMPLE_RENDERED_SPACE_COMPONENT_H

#include "basicrenderedspacecomponent.h"
#include <vector>

// -----------------------------------------------------------------------------

class SimpleRenderableComponent;

class SimpleRenderedSpaceComponent : public BasicRenderedSpaceComponent
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  virtual void Render();

  glm::vec3       Eye() const;
  glm::vec3 Direction() const;
  glm::vec3        Up() const;

  void       Eye (const glm::vec3 &);
  void Direction (const glm::vec3 &);
  void        Up (const glm::vec3 &);

  void AddRendered (::Handle<Component> renderable);

  COMPONENT_META
private:
  std::vector<::Handle<SimpleRenderableComponent>> m_renderable;
  OpenGL::Shader m_shader;

  float m_near;
  float m_far;
  float m_fov;
  glm::vec3 m_eye;
  glm::vec3 m_dir;
  glm::vec3 m_up;

  glm::vec3 m_plDiffuse;
  glm::vec3 m_plSpecular;
  glm::vec3 m_plAmbient;
  float     m_plSpecularPower;
};

// -----------------------------------------------------------------------------

#endif
