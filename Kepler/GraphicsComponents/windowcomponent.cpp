// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for bringing up and controlling a window
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "../KeplerCore/compositefactory.h"
#include "../KeplerCore/versioninfo.h"
#include "../Logger/logger.h"
#include "windowcomponent.h"
#include <cassert>
#include <iostream>

using namespace std;


Handle<Composite> GetOwnerFromGLFWwindow (GLFWwindow *w)
{
  void *ptr = glfwGetWindowUserPointer (w);
  WindowComponent *wcomp = reinterpret_cast<WindowComponent *> (ptr);
  return wcomp->Owner();
}

WindowComponent *GetComponentFromGLFWwindow (GLFWwindow *w)
{
  void *ptr = glfwGetWindowUserPointer (w);
  WindowComponent *wcomp = reinterpret_cast<WindowComponent *> (ptr);
  return wcomp;
}

// -----------------------------------------------------------------------------

WindowComponent::WindowComponent()
{
  glfwInit();

  glfwSetErrorCallback ([] (int error, const char *description) {
    printf ("GLFW Error %i: %s", error, description);
  });

  glfwWindowHint (GLFW_REFRESH_RATE, 60);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 4);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint (GLFW_RESIZABLE, GL_FALSE);

  m_glWindow = glfwCreateWindow (800, 600, "", nullptr, nullptr);
  m_isFullscreen = false;

  ERROR_IF (!m_glWindow, "Failed to create a window\n");

  glfwSetWindowUserPointer (m_glWindow, this);
  glfwMakeContextCurrent (m_glWindow);

  // Some functions aren't loaded by GLEW that we need, so make sure to turn on load "experimenta" functions
  glewExperimental = GL_TRUE;
  glewInit();
  // glewExpiremental may generate an invalid enum, so just pop that off OpenGL's error stack
  glGetError();
}

WindowComponent::~WindowComponent()
{
  glfwDestroyWindow (m_glWindow);
  glfwTerminate();
}

void WindowComponent::Initialize (const Json::Object &jsonData)
{
  unsigned width;
  unsigned height;

  jsonData.CallOn ("width", [&width] (const Json::Object::Payload &data) {
    width = unsigned (data.value.as<Json::Number>());
  });
  jsonData.CallOn ("height", [&height] (const Json::Object::Payload &data) {
    height = unsigned (data.value.as<Json::Number>());
  });

  jsonData.CallOn ("name", [this] (const Json::Object::Payload &data) {
    String name = String::Format ("%s - %s (%u bit)", data.value.as<Json::String>().c_str(), KEPLER_NAME_OFFICIAL, sizeof (void *) * 8);
    glfwSetWindowTitle (m_glWindow, name.CStr());
  });

  glfwSetWindowCloseCallback (m_glWindow, [] (GLFWwindow *) {
    CoreEngine::Running = false;
  });

  glfwSetCharCallback (m_glWindow, [] (GLFWwindow *w, unsigned c) {
    GetOwnerFromGLFWwindow (w)->Dispatch ("KeyTyped", c);
  });

  glfwSetKeyCallback (m_glWindow, [] (GLFWwindow *w, int key, int, int action, int mods) {
    switch (action)
    {
    case GLFW_PRESS:
      GetOwnerFromGLFWwindow (w)->Dispatch ("KeyDown", key, mods);
      break;

    case GLFW_RELEASE:
      GetOwnerFromGLFWwindow (w)->Dispatch ("KeyUp", key, mods);
      break;

    case GLFW_REPEAT:
      GetOwnerFromGLFWwindow (w)->Dispatch ("KeyHeld", key, mods);
      break;
    }

    GetOwnerFromGLFWwindow (w)->Dispatch ("KeyEvent", key, action, mods);
  });

  glfwSetWindowFocusCallback (m_glWindow, [] (GLFWwindow *w, int focused) {
    if (focused)
      GetOwnerFromGLFWwindow (w)->Dispatch ("WindowFocus");

    else
    {
      // Minimize the window
      glfwIconifyWindow (w);
      GetOwnerFromGLFWwindow (w)->Dispatch ("WindowUnfocus");
    }
  });

  glfwSetMouseButtonCallback (m_glWindow, [] (GLFWwindow *w, int button, int action, int mods) {
    switch (action)
    {
    case GLFW_PRESS:
      GetOwnerFromGLFWwindow (w)->Dispatch ("MouseDown", button, mods);
      break;

    case GLFW_RELEASE:
      GetOwnerFromGLFWwindow (w)->Dispatch ("MouseUp", button, mods);
      break;
    }
  });

  glfwSetScrollCallback (m_glWindow, [] (GLFWwindow *w, double x, double y) {
    GetOwnerFromGLFWwindow (w)->Dispatch ("MouseScroll", float (x), float (y));
  });

  glfwSetCursorPosCallback (m_glWindow, [] (GLFWwindow *w, double x, double y) {
    WindowComponent *me = GetComponentFromGLFWwindow (w);

    GetOwnerFromGLFWwindow (w)->Dispatch ("MouseMove", float (x), float (y));
  });

  glfwSetCursorEnterCallback (m_glWindow, [] (GLFWwindow *w, int action) {

    if (action)
      GetOwnerFromGLFWwindow (w)->Dispatch ("MouseEnter");

    else
      GetOwnerFromGLFWwindow (w)->Dispatch ("MouseExit");

  });

  glfwSetWindowSizeCallback (m_glWindow, [] (GLFWwindow *win, int w, int h) {
    glViewport (0, 0, w, h);

    WindowComponent *me = GetComponentFromGLFWwindow (win);

    GetOwnerFromGLFWwindow (win)->Dispatch ("WindowResize", unsigned (w), unsigned (h));
  });

  RegisterMessageProc ("GraphicsUpdate", std::function<void() > ([this]() {Update(); }));

  m_glVersion = (const char *) glGetString (GL_VERSION);
  sscanf (m_glVersion.CStr(), "%i.%i", &m_major, &m_minor);

  // Log some information about the OpenGL context
  LOG (String ("OpenGL Version:        ") + m_glVersion);
  LOG (String ("OpenGL Renderer:       ") + (const char *) glGetString (GL_RENDERER));
  LOG (String ("OpenGL Vendor:         ") + (const char *) glGetString (GL_VENDOR));
  LOG (String ("OpenGL Shader Version: ") + (const char *) glGetString (GL_SHADING_LANGUAGE_VERSION));

  RegisterMessageProc ("GetWindowSize", function<void (int *, int *) > (
  [this] (int *w, int *h) {
    glfwGetWindowSize (m_glWindow, w, h);
  }));

  RegisterMessageProc ("GetWindowPos", function<void (int *, int *) > (
  [this] (int *x, int *y) {
    glfwGetWindowPos (m_glWindow, x, y);
  }));

  m_renderedGame = Owner()->GetComponent<RenderedGameComponent>();
  GL_CHECK_ERRORS();

  //glClear (GL_COLOR_BUFFER_BIT);
  //glfwSwapBuffers (m_glWindow);

  auto fullscreen = [this] (bool full) { Fullscreen (full); };

  #ifdef _DEBUG

  jsonData.CallOnValue ("fullscreen", [this, width, height] (bool full) {
    if (full)
      Fullscreen (1);

    else
      glfwSetWindowSize (m_glWindow, width, height);
  });

  #else

  Fullscreen (1);

  #endif
}

// -----------------------------------------------------------------------------

void WindowComponent::Update ()
{
  // Tell the rendered game to go render the world, and then swap the buffers
  m_renderedGame->Render();

  glfwSwapBuffers (m_glWindow);
  glfwPollEvents();
}

// -----------------------------------------------------------------------------

GLFWwindow *WindowComponent::GetWindow()
{
  return m_glWindow;
}

String WindowComponent::OpenGLVersion()
{
  return m_glVersion;
}

unsigned WindowComponent::OpenGLVersionMajor()
{
  return m_minor;
}

unsigned WindowComponent::OpenGLVersionMinor()
{
  return m_minor;
}

// -----------------------------------------------------------------------------

bool WindowComponent::IsKeyDown (int keycode)
{
  return glfwGetKey (m_glWindow, keycode) == GL_TRUE;
}

bool WindowComponent::IsMouseDown (int mouseCode)
{
  return glfwGetMouseButton (m_glWindow, mouseCode) == GL_TRUE;
}

Vec2 WindowComponent::GetMousePos()
{
  double x, y;
  int w, h;

  glfwGetCursorPos (m_glWindow, &x, &y);
  glfwGetWindowSize (m_glWindow, &w, &h);

  return Vec2 (float (x / w) * 2 - 1, float (y / h) * 2 - 1);
}

// -----------------------------------------------------------------------------

glm::ivec2 WindowComponent::Size()
{
  glm::ivec2 size;
  glfwGetWindowSize (m_glWindow, &size.x, &size.y);
  return size;
}

float WindowComponent::AspectRatio()
{
  int w, h;
  glfwGetWindowSize (m_glWindow, &w, &h);

  return float (w) / float (h);
}

// -----------------------------------------------------------------------------

#ifdef _WIN32

// Don't include ALL of windows.h because that's really bad
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

// Expose the win32 and wgl GLFW functions
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

#include <Windows.h>
#include <GLFW/glfw3native.h>

void WindowComponent::Fullscreen (int fullscreen_)
{
  bool fullscreen = (fullscreen_ == 1);

  // If trying to set to the same value it already is, don't do anything.
  if (m_isFullscreen == fullscreen) return;

  m_isFullscreen = fullscreen;

  HWND win = glfwGetWin32Window (m_glWindow);

  if (fullscreen)
  {
    // Get the current styles for the window
    DWORD style = GetWindowLong (win, GWL_STYLE);
    DWORD exStyle = GetWindowLong (win, GWL_EXSTYLE);

    // Select the monitor the window is currenlty on, or the primary if none is selected
    MONITORINFO monitorInfo = { sizeof MONITORINFO };
    HMONITOR monitor = MonitorFromWindow (win, MONITOR_DEFAULTTOPRIMARY);
    GetMonitorInfo (monitor, &monitorInfo);

    // Make the window take up the entire monitor we have selected
    unsigned x = monitorInfo.rcMonitor.left;
    unsigned y = monitorInfo.rcMonitor.top;
    unsigned w = monitorInfo.rcMonitor.right - monitorInfo.rcMonitor.left;
    unsigned h = monitorInfo.rcMonitor.bottom - monitorInfo.rcMonitor.top;

    WINDOWPLACEMENT current_placement;

    if (GetWindowPlacement (win, &current_placement))
    {
      m_saved.x = current_placement.rcNormalPosition.left;
      m_saved.y = current_placement.rcNormalPosition.top;
      m_saved.width = current_placement.rcNormalPosition.right - current_placement.rcNormalPosition.left;
      m_saved.height = current_placement.rcNormalPosition.bottom - current_placement.rcNormalPosition.top;

      m_saved.isMaximized = !!IsZoomed (win);

      m_saved.style = style;
      m_saved.exStyle = exStyle;
    }

    // Restore the window before changing things
    if (m_saved.isMaximized)
      SendMessage (win, WM_SYSCOMMAND, SC_RESTORE, 0);

    // Make the window have no frame (Completely undecorated)
    SetWindowLong (win, GWL_STYLE, style & ~ (WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU));
    SetWindowLong (win, GWL_EXSTYLE, exStyle & ~ (WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_WINDOWEDGE | WS_EX_STATICEDGE));

    SetWindowPos (win, NULL,
                  x, y,
                  w, h,
                  SWP_NOZORDER | SWP_NOACTIVATE | SWP_FRAMECHANGED);
  }

  else
  {
    // Set window to default styling, with a frame (Decorated)
    SetWindowLong (win, GWL_STYLE,   m_saved.style);
    SetWindowLong (win, GWL_EXSTYLE, m_saved.exStyle);

    // Set the window to its old position. If it was maximized, send the message to do that again
    SetWindowPos (win, NULL,
                  m_saved.x, m_saved.y,
                  m_saved.width, m_saved.height,
                  SWP_NOZORDER | SWP_NOACTIVATE | SWP_FRAMECHANGED);

    if (m_saved.isMaximized)
      SendMessage (win, WM_SYSCOMMAND, SC_MAXIMIZE, 0);
  }

  UpdateWindow (win);
}

#else

void WindowComponent::SetFullscreen (int)
{
  // Ignore calls to set fullscreen on operating systems we can't do it with
}

#endif

int WindowComponent::Fullscreen()
{
  return m_isFullscreen ? 1 : 0;
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (Window)
{
  META_INHERIT (WindowComponent, "Kepler", "Component");

  META_HANDLE (WindowComponent);

  META_ADD_PROP_READONLY (WindowComponent, AspectRatio);

  META_ADD_METHOD (WindowComponent, IsKeyDown);
  META_ADD_METHOD (WindowComponent, IsMouseDown);
  META_ADD_METHOD (WindowComponent, GetMousePos);

  META_ADD_PROP (WindowComponent, Fullscreen);

  META_FINALIZE_PTR (WindowComponent, MetaPtr);
}

