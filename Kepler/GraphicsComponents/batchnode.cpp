// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph instanced rendering
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "batchnode.h"
#include "batchproperties.h"

using namespace glm;
using namespace std;

// -----------------------------------------------------------------------------

MeshNode::MeshNode (GLuint indirectIndex, GLuint matrixBuffer, vector<ObjectMatrix> &scratch) :
  m_indirectIndex (indirectIndex *sizeof IndirectDraw),
  m_matrixBuffer (matrixBuffer),
  m_scratch (scratch),
  m_render (Utilities::Hash::SuperFast)
{
}

// -----------------------------------------------------------------------------

void MeshNode::AddRenderable (Handle<RenderableComponent> renderable)
{
  // Add the renderable at "{effect}~{material}"
  String name = renderable->Effect() + '~' + renderable->Material();
  m_render[name].push_back (renderable);
}

void MeshNode::RemoveRenderable (Handle<RenderableComponent> renderable)
{
  String name = renderable->Effect() + '~' + renderable->Material();

  // Find the object in the table
  m_render.CallOnValue (name, [this, &renderable] (vector < Handle<RenderableComponent>> &list) {
    // Erase it from the list
    list.erase (remove (list.begin(), list.end(), renderable), list.end());
  });
}

// -----------------------------------------------------------------------------

void MeshNode::Draw (BatchGraph *graph)
{
  // Get the current effect and material being drawn
  void *effectNamePtr   = graph->Property (BatchProperty::EFFECT_NAME);
  void *materialNamePtr = graph->Property (BatchProperty::MATERIAL_NAME);

  if (effectNamePtr && materialNamePtr)
  {
    String &effectName = *reinterpret_cast<String *> (effectNamePtr);
    String &materialName = *reinterpret_cast<String *> (materialNamePtr);

    // Loop up "{effect}~{material}" in the table, and draw the elements using that. If there aren't any,
    // we can safely ignore this (Because this mesh isn't drawn with this material/effect combination)
    m_render.CallOnValue (effectName + '~' + materialName,
    [this] (std::vector<Handle<RenderableComponent>> &list) {
      RenderBatch (list);
    });
  }

  else
  {
    m_render.ForEach ([this] (HashTable<String, std::vector<Handle<RenderableComponent>>>::Payload &data) {
      RenderBatch (data.value);
    });
  }
}

// -----------------------------------------------------------------------------

void MeshNode::RenderBatch (std::vector<Handle<RenderableComponent>> &batch)
{

  // For every draw command our buffer size allows us to issue
  for (size_t start = 0; start < batch.size(); start += m_scratch.size())
  {
    GLuint leftToRender = batch.size() - start;
    GLuint canRender = GLuint (m_scratch.size());
    GLuint thisIteration;

    // Being rendered this iteration:
    // If there are more elements left to render than we can render, render all that we can.
    // Otherwise, we can render everything that is left, so render everything left to render.
    if (leftToRender > canRender)
      thisIteration = canRender;

    else
      thisIteration = leftToRender;

    // Fill in the scratch buffer
    for (size_t i = 0; i < thisIteration; ++i)
    {
      // Get the object currently rendering
      ::Handle<RenderableComponent> &renderable = batch[start + i];

      // Get the world matrix, calculate the normal matrix from that
      mat4 w = renderable->GetWorldMatrix();
      mat3 n = transpose (inverse (mat3 (w)));

      // Set the scratch buffer at the current index
      m_scratch[i].ModelToWorld = w;
      m_scratch[i].ModelToWorldNormal1 = n[0];
      m_scratch[i].ModelToWorldNormal2 = n[1];
      m_scratch[i].ModelToWorldNormal3 = n[2];
      m_scratch[i].Tint = renderable->Tint();
    }

    // Send the matrix data in the scratch buffer over to the GPU
    glBufferSubData (GL_SHADER_STORAGE_BUFFER, 0, thisIteration * sizeof ObjectMatrix, m_scratch.data());

    // Send the number of instances to draw over to the GPU
    glBufferSubData (GL_DRAW_INDIRECT_BUFFER, m_indirectIndex + offsetof (IndirectDraw, primCount), sizeof (GLuint), &thisIteration);

    // Issue the draw call on all objects
    glDrawElementsIndirect (GL_TRIANGLES, GL_UNSIGNED_INT, reinterpret_cast<GLvoid *> (m_indirectIndex));
  }
}

// -----------------------------------------------------------------------------
