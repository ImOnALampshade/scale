// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SHADER_CONTEXT_MANAGER_COMPONENT_H
#define SHADER_CONTEXT_MANAGER_COMPONENT_H

#include "../KeplerCore/component.h"

class ShaderContextManagerComponent : public Component
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  void Shader (OpenGL::Shader *shader);
  OpenGL::Shader *Shader() const;

  COMPONENT_META

private:
  OpenGL::Shader *m_boundShader;
};

#endif
