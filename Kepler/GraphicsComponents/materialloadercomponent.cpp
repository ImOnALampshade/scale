// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for loading materials
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "materialloadercomponent.h"
#include "../HashFunctions/hashfunctions.h"
#include "renderedworldspacecomponent.h"

#include "materialatlasnode.h"

using namespace std;

// -----------------------------------------------------------------------------

MaterialLoaderComponent::MaterialLoaderComponent() :
  m_atlasses(),
  m_textures (Utilities::Hash::SuperFast),
  m_materials (Utilities::Hash::SuperFast)
{
}

// -----------------------------------------------------------------------------

void MaterialLoaderComponent::Initialize (const Json::Object &jsonData)
{
  //////////////////////////
  // Load atlases

  jsonData.CallOnValue ("Atlases", [this] (const Json::List &atlasList) {
    // Generate the texture atlasses that are going to be used
    m_atlasses.resize (atlasList.size());
    glGenTextures (m_atlasses.size(), m_atlasses.data());

    // Loop through and load every atlas
    for (size_t i = 0; i < atlasList.size(); ++i)
    {
      GLuint atlas = m_atlasses[i];

      // Get the filenames into a vector of strings so we can call LoadTextureArray
      vector<String> filenames (atlasList[i].as<Json::List>().begin(),
                                atlasList[i].as<Json::List>().end());

      // Bind the texture, load it
      glBindTexture (GL_TEXTURE_2D_ARRAY, atlas);
      OpenGL::LoadTextureArray (filenames);

      // Add every texture to the hash table of texture infos
      for (size_t j = 0; j < filenames.size(); ++j)
      {
        String filename = filenames[j];
        m_textures.Insert (filename, TextureInfo (atlas, j));
      }
    }
  });

  //////////////////////////
  // Load materials

  jsonData.CallOnValue ("Materials", [this] (const Json::List &materialList) {
    auto RenderedWorld = Owner()->GetComponent<RenderedWorldSpaceComponent>();

    for (const Json::Object &material : materialList)
    {
      // Get the name of the material, diffuse, and specular textures from the json
      String name     = material.Locate ("name", Json::String (""));
      String diffuse  = material.Locate ("diffuse", Json::String (""));
      String specular = material.Locate ("specular", Json::String (""));

      // Find the info for the diffuse and specular
      TextureInfo diffuseInfo  = m_textures.Locate (diffuse);
      TextureInfo specularInfo = m_textures.Locate (specular);

      // The "name" of the material atlas node is just "<atlas # diffuse> ~ <atlas # specular>"
      String nodeName = String::Format ("%u %u", diffuseInfo.Atlas, specularInfo.Atlas);

      // Get the child node of the name
      auto node = RenderedWorld->MaterialParent()->GetChild (nodeName);

      // If the child did not exist, create it
      if (!node)
      {
        vector<GLuint> textures { diffuseInfo.Atlas, specularInfo.Atlas };
        node = make_shared<MaterialNode> (textures);

        RenderedWorld->MaterialParent()->AddNamedChild (nodeName, node);
        static_pointer_cast<MaterialNode> (node)->Child (RenderedWorld->BatchParent());
      }

      // Create the material indexing data
      Material materialNodeData;
      materialNodeData.m_name = name;
      materialNodeData.m_diffuseIndex = diffuseInfo.Index;
      materialNodeData.m_specularIndex = specularInfo.Index;

      // Add the material data to the atlas node
      static_pointer_cast<MaterialNode> (node)->AddMaterial (materialNodeData);

      // Add it to the hash table
      m_materials.Insert (name, MaterialInfo (diffuseInfo, specularInfo));
    }
  });

  GL_CHECK_ERRORS();
}

void MaterialLoaderComponent::Free()
{
  glDeleteTextures (m_atlasses.size(), m_atlasses.data());
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (MaterialLoader)
{
  META_INHERIT (MaterialLoaderComponent, "Kepler", "Component");
  META_HANDLE (MaterialLoaderComponent);

  META_FINALIZE_PTR (MaterialLoaderComponent, MetaPtr);
}
