// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for loading models
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#define GLM_SWIZZLE

#include "stdafx.h"
#include "../kmo/scene.h"
#include "../Logger/logger.h"
#include "batchnode.h"
#include "modelloadercomponent.h"
#include "renderedworldspacecomponent.h"

using namespace std;
using namespace glm;

// -----------------------------------------------------------------------------

ModelLoaderComponent::ModelLoaderComponent() :
  m_offsets (Utilities::Hash::SuperFast)
{
}

void ModelLoaderComponent::Initialize (const Json::Object &jsonData)
{
  jsonData.CallOn ("models", [this] (const Json::Object::Payload &data) {
    const Json::List &sceneNames = data.value.as<Json::List>();

    vector<Scene> scenes;
    vector<String> names;

    size_t pbufferSize = 0;

    for (const Json::Object &model : sceneNames)
    {
      String file = model.Locate ("file", Json::String());
      String name = model.Locate ("name", Json::String());

      names.emplace_back (name);

      // Add the scene to the list of scenes
      scenes.emplace_back ();
      scenes.back().LoadFbx (file);

      printf ("Bounding radius of %s is %g\n", file.c_str(), scenes.back().BoundingRadius);
    }

    // The size of the indirect buffer is just the number of models times the size of an indirect command
    pbufferSize = scenes.size() * sizeof IndirectDraw;

    // Create out buffers and VAO
    unsigned buffers[3];
    glGenBuffers (3, buffers);
    glGenVertexArrays (1, &m_array);

    m_vbuffer = buffers[0];
    m_ibuffer = buffers[1];
    m_pbuffer = buffers[2];

    // Bind VAO and buffers to appropriate targets
    glBindVertexArray (m_array);

    glBindBuffer (GL_ARRAY_BUFFER, m_vbuffer);
    glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, m_ibuffer);
    glBindBuffer (GL_DRAW_INDIRECT_BUFFER, m_pbuffer);

    std::vector<Scene::Vertex>   VertexBuffer;
    std::vector<Scene::Triangle> IndexBuffer;

    // Allocate storage for the indirect buffer
    glBufferStorage (GL_DRAW_INDIRECT_BUFFER, pbufferSize, nullptr, GL_DYNAMIC_STORAGE_BIT);

    // Current offset into each buffer
    GLsizei pbufferOffset = 0;

    // Base vertex to place into indirect draw command
    GLuint baseVert = 0;

    for (size_t i = 0; i < scenes.size(); ++i)
    {
      Scene &s = scenes[i];
      GLsizei size;

      // Initialize the indirect command
      IndirectDraw indirect;
      indirect.count = GLuint (s.Triangles.size() * 3);
      indirect.primCount = 1;
      indirect.firstIndex = baseVert;
      indirect.baseVertex = baseVert;
      indirect.baseInstance = 0;

      // Add the vertices to the vertex buffer
      VertexBuffer.insert (VertexBuffer.end(), scenes[i].Vertices.begin(), scenes[i].Vertices.end());

      // Add the indicies to the index buffer
      IndexBuffer.insert (IndexBuffer.end(), scenes[i].Triangles.begin(), scenes[i].Triangles.end());

      // Add the draw command to the indirect buffer
      size = GLsizei (sizeof IndirectDraw);
      glBufferSubData (GL_DRAW_INDIRECT_BUFFER, pbufferOffset, sizeof indirect, &indirect);
      pbufferOffset += size;

      // Increment our base vertex
      baseVert += s.Triangles.size() * 3;

      // Add the offset to the map of offsets
      m_offsets[names[i]] = unsigned (i);
    }

    // Create the vertex buffer and element array bufer, immutable contents
    glBufferStorage (GL_ARRAY_BUFFER,         VertexBuffer.size() * sizeof Scene::Vertex,  VertexBuffer.data(), 0);
    glBufferStorage (GL_ELEMENT_ARRAY_BUFFER, IndexBuffer.size() * sizeof Scene::Triangle, IndexBuffer.data(),  0);

    // Enable vertex attributes 0, 1, and 2
    glEnableVertexAttribArray (0);
    glEnableVertexAttribArray (1);
    glEnableVertexAttribArray (2);

    // Bind our vbuffer for each attribute, with stride set to the size of a vertex
    glBindVertexBuffer (0, m_vbuffer, 0, sizeof Scene::Vertex);
    glBindVertexBuffer (1, m_vbuffer, 0, sizeof Scene::Vertex);
    glBindVertexBuffer (2, m_vbuffer, 0, sizeof Scene::Vertex);

    // Format our vertices with the offest of each vertex attribute (Coordinate, texture coordinate, and normal)
    glVertexAttribFormat (0, 3, GL_FLOAT, GL_FALSE, offsetof (Scene::Vertex, Coordinate));
    glVertexAttribFormat (1, 2, GL_FLOAT, GL_FALSE, offsetof (Scene::Vertex, Texcoord));
    glVertexAttribFormat (2, 3, GL_FLOAT, GL_FALSE, offsetof (Scene::Vertex, Normal));
  });
}

// -----------------------------------------------------------------------------

void ModelLoaderComponent::Free()
{
  unsigned buffers[] = { m_vbuffer, m_ibuffer, m_pbuffer };
  glDeleteBuffers (3, buffers);
  glDeleteVertexArrays (1, &m_array);
}

// -----------------------------------------------------------------------------

unsigned ModelLoaderComponent::GetModelIndex (String model)
{
  return m_offsets.Locate (model, -1);
}

unsigned ModelLoaderComponent::IndirectBuffer() { return m_pbuffer; }
unsigned ModelLoaderComponent::VertexArray()    { return m_array;   }

const HashTable<String, unsigned> &ModelLoaderComponent::Offsets() { return m_offsets; }

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (ModelLoader)
{
}
