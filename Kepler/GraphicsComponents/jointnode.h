// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Scene geometry node for
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef JOINT_NODE_H
#define JOINT_NODE_H

#include "../BatchingGraph/batchinggraph.h"

class JointNode : public BatchNodeWithChildren
{
public:
  JointNode();

  virtual void Draw (BatchGraph *graph);
  void AddNamedChild (String name, std::shared_ptr<BatchNode> node);
  std::shared_ptr<BatchNode> GetChild (String name);

private:
  HashTable<String, unsigned> m_namedIndex;
};

#endif
