// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph scene owner node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SHADER_STORAGE_BUFFER_NODE_H
#define SHADER_STORAGE_BUFFER_NODE_H

#include "../BatchingGraph/batchinggraph.h"
#include "../OpenGLHelpers/glinclude.h"

class ShaderStorageBufferNode : public BatchNodeWithChild
{
public:
  ShaderStorageBufferNode (GLuint buffer, GLuint binding);

  virtual void Draw (BatchGraph *graph);
private:
  GLuint m_buffer;
  GLuint m_binding;
};

#endif
