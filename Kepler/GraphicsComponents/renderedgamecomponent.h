// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for rendering the actual game into the window
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef RENDERED_GAME_COMPONENT_H
#define RENDERED_GAME_COMPONENT_H

#include "../KeplerCore/component.h"
#include "basicrenderedspacecomponent.h"

// -----------------------------------------------------------------------------

class RenderedGameComponent : public Component
{
private:
  std::vector<::Handle<BasicRenderedSpaceComponent>> m_renderedSpaces;

  unsigned m_quadVBuffer;
  unsigned m_quadIBuffer;
  unsigned m_quadArray;

public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  void Render();

  void BindQuad();
  void DrawQuad();

  void AddRenderedSpace (::Handle<BasicRenderedSpaceComponent> space);

  COMPONENT_META
};

// -----------------------------------------------------------------------------

#endif
