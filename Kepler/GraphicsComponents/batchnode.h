// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph instanced rendering
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef BATCH_INSTANCING_NODE_H
#define BATCH_INSTANCING_NODE_H

#include "../BatchingGraph/batchinggraph.h"
#include "renderablecomponent.h"

#pragma pack(push, 1)
struct ObjectMatrix
{
  glm::mat4 ModelToWorld;

  glm::vec3 ModelToWorldNormal1;
  float _pad1;
  glm::vec3 ModelToWorldNormal2;
  float _pad2;
  glm::vec3 ModelToWorldNormal3;
  float _pad3;

  glm::vec3 Tint;
  float _pad4;
};
#pragma pack(pop)

class MeshNode : public BatchNode
{
public:
  MeshNode (GLuint indirectIndex, GLuint matrixBuffer, std::vector<ObjectMatrix> &scratch);

  void    AddRenderable (Handle<RenderableComponent> renderable);
  void RemoveRenderable (Handle<RenderableComponent> renderable);
  virtual void Draw (BatchGraph *graph);

private:
  void RenderBatch (std::vector<Handle<RenderableComponent>> &batch);

  GLuint m_indirectIndex;
  GLuint m_matrixBuffer;
  std::vector<ObjectMatrix> &m_scratch;

  HashTable<String, std::vector<Handle<RenderableComponent>>> m_render;
};

#endif
