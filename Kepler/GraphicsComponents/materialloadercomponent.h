// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for loading materials
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef MATERIAL_LOADER_H
#define MATERIAL_LOADER_H

#include "../KeplerCore/component.h"

// -----------------------------------------------------------------------------

class MaterialLoaderComponent : public Component
{
public:
  struct TextureInfo
  {
    GLuint Atlas;
    GLuint Index;

    TextureInfo() : Atlas (0), Index (0) { }
    TextureInfo (GLuint A, GLuint I) : Atlas (A), Index (I) { }
  };

  struct MaterialInfo
  {
    TextureInfo Diffuse;
    TextureInfo Specular;

    MaterialInfo() : Diffuse(), Specular() { }

    MaterialInfo (TextureInfo d, TextureInfo s) : Diffuse (d), Specular (s) { }
  };

  MaterialLoaderComponent();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  COMPONENT_META

private:
  std::vector<GLuint>             m_atlasses;
  HashTable<String, TextureInfo>  m_textures;
  HashTable<String, MaterialInfo> m_materials;
};

// -----------------------------------------------------------------------------

#endif
