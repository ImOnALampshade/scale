// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for an entire rendered space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "../Logger/logger.h"
#include "../KeplerCore/game.h"
#include "renderedworldspacecomponent.h"
#include "renderablecomponent.h"
#include "shaderloadercomponent.h"
#include "modelloadercomponent.h"

#include "batchnode.h"
#include "cameranode.h"
#include "vertexarraynode.h"
#include "shaderstoragebuffernode.h"

#include "batchproperties.h"

using namespace std;
using namespace glm;

// -----------------------------------------------------------------------------

void RenderedWorldSpaceComponent::Initialize (const Json::Object &jsonData)
{
  BasicRenderedSpaceComponent::Initialize (jsonData);

  {
    size_t size = (size_t) jsonData.Locate ("MatrixBufferSize", 10.0).as<Json::Number>();

    glGenBuffers (1, &m_matrixBuffer);
    glBindBuffer (GL_SHADER_STORAGE_BUFFER, m_matrixBuffer);

    glBufferStorage (GL_SHADER_STORAGE_BUFFER, size * sizeof ObjectMatrix, nullptr, GL_DYNAMIC_STORAGE_BIT);

    m_matrixScratchBuffer.resize (size);
  }

  auto ModelLoader = Owner()->GetComponent<ModelLoaderComponent>();

  m_sceneParent = make_shared<SceneNode>();
  m_cameraParent   = make_shared<JointNode>();
  m_shaderParent   = make_shared<JointNode>();
  m_materialParent = make_shared<JointNode>();
  m_batchParent    = make_shared<JointNode>();

  m_graph.Root (static_pointer_cast<BatchNode> (m_cameraParent));
  m_graph.ResizePropBag (BatchProperty::COUNT);

  auto VAOnode = make_shared<VertexArrayNode> (ModelLoader->VertexArray(), ModelLoader->IndirectBuffer());
  auto SSBOnode = make_shared<ShaderStorageBufferNode> (m_matrixBuffer, 0);

  VAOnode->Child (SSBOnode);
  SSBOnode->Child (m_shaderParent);

  m_sceneParent->Geometry (VAOnode);

  ModelLoader->Offsets().ForEach ([this] (const HashTable<String, unsigned>::Payload &data) {
    auto node = make_shared<MeshNode> (data.value, m_matrixBuffer, m_matrixScratchBuffer);
    m_batchParent->AddNamedChild (data.key, node);
  });

  RegisterMessageProc ("AddCamera", function<void (::Handle<Component>) > (
  [this] (::Handle<Component> handle) {
    shared_ptr<CameraNode> cameraNode = make_shared<CameraNode> (handle.as<CameraComponent>());

    m_cameraParent->AddChild (cameraNode);
    cameraNode->Child (m_sceneParent);
  }));

  RegisterMessageProc ("AddRenderedObject", function<void (::Handle<Component>) > (
  [this] (::Handle<Component> handle_) {
    ::Handle<RenderableComponent> handle = handle_.as<RenderableComponent>();
    shared_ptr<BatchNode> node = m_batchParent->GetChild (handle->Mesh());

    if (node)
      static_pointer_cast<MeshNode> (node)->AddRenderable (handle);

    else
      BREAK (String::Format ("Invalid node name: %s", handle->Mesh().c_str()));
  }));

  RegisterMessageProc ("RemoveRenderedObject", function<void (::Handle<Component>) > (
  [this] (::Handle<Component> handle_) {
    ::Handle<RenderableComponent> handle = handle_.as<RenderableComponent>();
    shared_ptr<BatchNode> node = m_batchParent->GetChild (handle->Mesh());

    if (node)
      static_pointer_cast<MeshNode> (node)->RemoveRenderable (handle);

    else
      BREAK (String::Format ("Invalid node name: %s", handle->Mesh().c_str()));
  }));
}

void RenderedWorldSpaceComponent::Free()
{
  BasicRenderedSpaceComponent::Free();
  glDeleteBuffers (1, &m_matrixBuffer);
}

// -----------------------------------------------------------------------------

#include "shaderloadercomponent.h"

void RenderedWorldSpaceComponent::Render()
{
  // Enable backface culling and depth testing
  glEnable (GL_DEPTH_TEST);
  glEnable (GL_CULL_FACE);
  glCullFace (GL_BACK);
  glDepthFunc (GL_LESS);

  // Draw everything in the graph
  m_graph.Draw();

  glDisable (GL_DEPTH_TEST);
  glDisable (GL_CULL_FACE);
}

// -----------------------------------------------------------------------------

void RenderedWorldSpaceComponent::AddCamera (String name, ::Handle<CameraComponent> camera)
{
  shared_ptr<CameraNode> cameraNode = make_shared<CameraNode> (camera);
  m_cameraParent->AddNamedChild (name, static_pointer_cast<BatchNode> (cameraNode));
}

void RenderedWorldSpaceComponent::AddObject (::Handle<RenderableComponent> object)
{
  shared_ptr<MeshNode> node = static_pointer_cast<MeshNode> (m_batchParent->GetChild (object->Mesh()));
  node->AddRenderable (object);
}

// -----------------------------------------------------------------------------

BatchGraph &RenderedWorldSpaceComponent::Graph()
{
  return m_graph;
}

// -----------------------------------------------------------------------------

shared_ptr<JointNode> RenderedWorldSpaceComponent::CameraParent()
{
  return m_cameraParent;
}

shared_ptr<JointNode> RenderedWorldSpaceComponent::ShaderParent()
{
  return m_shaderParent;
}

shared_ptr<JointNode> RenderedWorldSpaceComponent::MaterialParent()
{
  return m_materialParent;
}

shared_ptr<JointNode> RenderedWorldSpaceComponent::BatchParent()
{
  return m_batchParent;
}

std::vector<ObjectMatrix> &RenderedWorldSpaceComponent::MatrixScratchBuffer()
{
  return m_matrixScratchBuffer;
}

GLuint RenderedWorldSpaceComponent::MatrixBuffer()
{
  return m_matrixBuffer;
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (RenderedWorldSpace)
{
  META_INHERIT (RenderedWorldSpaceComponent, "Kepler", "Component");
  META_HANDLE (RenderedWorldSpaceComponent);

  META_FINALIZE_PTR (RenderedWorldSpaceComponent, MetaPtr);
}
