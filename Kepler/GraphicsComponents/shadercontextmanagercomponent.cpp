// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "shadercontextmanagercomponent.h"

using namespace glm;

// -----------------------------------------------------------------------------

void ShaderContextManagerComponent::Initialize (const Json::Object &jsonData)
{
  m_boundShader = nullptr;
}

void ShaderContextManagerComponent::Free()
{
}

// -----------------------------------------------------------------------------

void ShaderContextManagerComponent::Shader (OpenGL::Shader *shader)
{
  m_boundShader = shader;
  glUseProgram (m_boundShader->GetProgram());
}

OpenGL::Shader *ShaderContextManagerComponent::Shader() const
{
  return m_boundShader;
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (ShaderContextManager)
{
}
