// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph frame buffer node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "framebuffernode.h"

FrameBufferNode::FrameBufferNode (GLuint buffer) :
  m_buffer (buffer)
{
}

void FrameBufferNode::Draw (BatchGraph *graph)
{
  glBindFramebuffer (GL_DRAW_FRAMEBUFFER, m_buffer);
  DrawChild (graph);
}
