// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for loading models
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef MODEL_LOADER_COMPONENT_H
#define MODEL_LOADER_COMPONENT_H

#include "../KeplerCore/component.h"

class ModelLoaderComponent : public Component
{
private:
  unsigned                    m_vbuffer; // The vertex buffer
  unsigned                    m_ibuffer; // The element array buffer
  unsigned                    m_pbuffer; // The indirect buffer
  unsigned                    m_array;   // The vertex array
  HashTable<String, unsigned> m_offsets;

public:
  ModelLoaderComponent();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  void BindBuffers();

  unsigned GetModelIndex (String model);

  unsigned IndirectBuffer();
  unsigned VertexArray();

  const HashTable<String, unsigned> &Offsets();

  COMPONENT_META
};

#endif
