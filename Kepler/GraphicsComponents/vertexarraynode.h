// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph scene owner node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef VERTEX_ARRAY_NODE_H
#define VERTEX_ARRAY_NODE_H

#include "../BatchingGraph/batchinggraph.h"
#include "../OpenGLHelpers/glinclude.h"

class VertexArrayNode : public BatchNodeWithChild
{
public:
  VertexArrayNode (GLuint vertexArray, GLuint indirectBuffer);

  virtual void Draw (BatchGraph *graph);
private:
  GLuint m_vertexArray;
  GLuint m_indirectBuffer;
};

#endif
