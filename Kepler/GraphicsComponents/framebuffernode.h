// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph frame buffer node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef FRAME_BUFFER_NODE_H
#define FRAME_BUFFER_NODE_H

#include "../BatchingGraph/batchinggraph.h"
#include "../OpenGLHelpers/glinclude.h"

class FrameBufferNode : public BatchNodeWithChild
{
public:
  FrameBufferNode (GLuint buffer);

  virtual void Draw (BatchGraph *graph);
private:
  GLuint m_buffer;
};

#endif
