// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph scene owner node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "vertexarraynode.h"

VertexArrayNode::VertexArrayNode (GLuint vertexArray, GLuint indirectBuffer) :
  m_vertexArray (vertexArray),
  m_indirectBuffer (indirectBuffer)
{
}

void VertexArrayNode::Draw (BatchGraph *graph)
{
  glBindBuffer (GL_DRAW_INDIRECT_BUFFER, m_indirectBuffer);
  glBindVertexArray (m_vertexArray);

  DrawChild (graph);
}
