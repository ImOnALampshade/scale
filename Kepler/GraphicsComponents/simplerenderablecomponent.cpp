// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for rendering a simple object
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "modelloadercomponent.h"
#include "simplerenderablecomponent.h"
#include "simplerenderedspacecomponent.h"

using namespace glm;

// -----------------------------------------------------------------------------

void SimpleRenderableComponent::Initialize (const Json::Object &jsonData)
{
  m_modelIndex = 0;
  m_transform = Owner()->GetComponent (TransformComponent::Name).as<TransformComponent>();

  jsonData.CallOnValue ("model", [this] (Json::String name) {
    auto modelLoader = Owner()->Space()->GetComponent (ModelLoaderComponent::Name).as<ModelLoaderComponent>();
    m_modelIndex = modelLoader->GetModelIndex (name);
  });

  Owner()->Space()->GetComponent (SimpleRenderedSpaceComponent::Name).as<SimpleRenderedSpaceComponent>()->AddRendered (Handle());
}

void SimpleRenderableComponent::Free()
{
}

void SimpleRenderableComponent::Render (OpenGL::Shader &shader)
{
  mat4 world_matrix  = m_transform->GetMatrix();
  mat3 normal_matrix = transpose (inverse (mat3 (world_matrix)));

  shader.SetUniform ("world_matrix", world_matrix);
  shader.SetUniform ("normal_matrix", normal_matrix);

  glDrawElementsIndirect (GL_TRIANGLES, GL_UNSIGNED_INT, reinterpret_cast<GLvoid *> (m_modelIndex * sizeof IndirectDraw));
}

// -----------------------------------------------------------------------------