// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph shader node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SHADER_NODE_H
#define SHADER_NODE_H

#include "../BatchingGraph/batchinggraph.h"

class ShaderNode : public BatchNodeWithChild
{
public:
  ShaderNode (OpenGL::Shader *program, String name);

  virtual void Draw (BatchGraph *graph);

private:
  OpenGL::Shader *m_program;
  String          m_effectName;
  GLint           m_projectionViewMatrixLocation;
};

#endif
