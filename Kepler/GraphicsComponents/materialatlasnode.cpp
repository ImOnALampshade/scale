// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph instanced rendering
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "materialatlasnode.h"
#include "batchproperties.h"

using namespace std;
using namespace glm;

// -----------------------------------------------------------------------------

MaterialNode::MaterialNode (std::vector<GLuint> textures) :
  m_textures (textures),
  m_materials()
{
}

void MaterialNode::Draw (BatchGraph *graph)
{
  glBindTextures (0, m_textures.size(), m_textures.data());

  void *shaderPtr = graph->Property (BatchProperty::SHADER_PTR);
  OpenGL::Shader *shader = reinterpret_cast<OpenGL::Shader *> (shaderPtr);

  // Bind all the materials, and draw the children for those materials
  for (Material &material : m_materials)
  {
    static const String diffuse_index ("diffuse_index");
    static const String specular_index ("specular_index");

    shader->SetUniform (diffuse_index, material.m_diffuseIndex);
    shader->SetUniform (specular_index, material.m_specularIndex);

    graph->Property (BatchProperty::MATERIAL_NAME, &material.m_name);
    DrawChild (graph);
  }
}

void MaterialNode::AddMaterial (const Material &m)
{
  m_materials.push_back (m);
}

// -----------------------------------------------------------------------------
