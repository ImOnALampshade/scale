// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph scene owner node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "shaderstoragebuffernode.h"

// -----------------------------------------------------------------------------

ShaderStorageBufferNode::ShaderStorageBufferNode (GLuint buffer, GLuint binding) :
  m_buffer (buffer),
  m_binding (binding)
{
}

void ShaderStorageBufferNode::Draw (BatchGraph *graph)
{
  glBindBuffer (GL_SHADER_STORAGE_BUFFER, m_buffer);
  glBindBufferBase (GL_SHADER_STORAGE_BUFFER, m_binding, m_buffer);

  DrawChild (graph);
}

// -----------------------------------------------------------------------------
