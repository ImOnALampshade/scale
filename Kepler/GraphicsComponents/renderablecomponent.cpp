// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Base class for a rendering component for an object in a world
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "renderablecomponent.h"
#include "renderedworldspacecomponent.h"
#include "shaderloadercomponent.h"
#include "modelloadercomponent.h"

// -----------------------------------------------------------------------------

void RenderableComponent::Initialize (const Json::Object &jsonData)
{
  m_transform = Owner()->GetComponent<TransformComponent>();

  m_effect   = jsonData.Locate ("Effect",   String ("Default")).as<String>();
  m_material = jsonData.Locate ("Material", String ("None")).as<String>();
  m_model    = jsonData.Locate ("Mesh",     String ("")).as<String>();

  jsonData.CallOnValue ("Tint",
  [this] (const Json::List &arr) {
    for (unsigned i = 0; i < arr.size() && i < 3; ++i)
      m_tint[i] = float (arr[i].as<Json::Number>());
  },
  [this]() {
    m_tint = glm::vec3 (1.0f);
  });

  Space()->Dispatch ("AddRenderedObject", Handle());
}

void RenderableComponent::Free()
{
  Space()->Dispatch ("RemoveRenderedObject", Handle());
}

// -----------------------------------------------------------------------------

glm::mat4 RenderableComponent::GetWorldMatrix()
{
  return m_transform->GetMatrix();
}

String RenderableComponent::Effect()
{
  return m_effect;
}

String RenderableComponent::Material()
{
  return m_material;
}

String RenderableComponent::Mesh()
{
  return m_model;
}

Vec3 RenderableComponent::Tint()
{
  return m_tint;
}

void RenderableComponent::Tint (Vec3 tint)
{
  m_tint = tint;
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (Renderable)
{
  META_INHERIT (RenderableComponent, "Kepler", "Component");

  META_HANDLE (RenderableComponent);

  META_ADD_PROP_READONLY (RenderableComponent, Effect);
  META_ADD_PROP_READONLY (RenderableComponent, Material);
  META_ADD_PROP_READONLY (RenderableComponent, Mesh);

  META_ADD_PROP (RenderableComponent, Tint);

  META_FINALIZE_PTR (RenderableComponent, MetaPtr);
}
