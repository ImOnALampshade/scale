// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph node for debug printing on creating/destruction/drawing
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef DEBUG_PRINT_NODE_H
#define DEBUG_PRINT_NODE_H

#include "../BatchingGraph/batchinggraph.h"

// -----------------------------------------------------------------------------

class DebugPrintNode : public BatchNode
{
public:
  DebugPrintNode();
  ~DebugPrintNode();

  virtual void Draw (BatchGraph *graph);
};

// -----------------------------------------------------------------------------

#endif
