// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph property index list
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef BATCH_PROPERTIES_H
#define BATCH_PROPERTIES_H

struct BatchProperty
{
  enum
  {
    MATERIAL_NAME,
    EFFECT_NAME,
    SHADER_PTR,
    PROJECTION_VIEW,


    COUNT
  };
};

#endif
