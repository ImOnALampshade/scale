// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Rendered space for displaying a full screen slide
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "renderedslidecomponent.h"

using namespace std;

// -----------------------------------------------------------------------------

void RenderedSlideComponent::Initialize (const Json::Object &jsonData)
{
  BasicRenderedSpaceComponent::Initialize (jsonData);

  glGenTextures (1, &m_texture);
  glBindTexture (GL_TEXTURE_2D_ARRAY, m_texture);

  vector<String> files (1);
  files[0] = jsonData.Locate ("File", Json::String()).as<Json::String>();

  OpenGL::LoadTextureArray (files);
}

void RenderedSlideComponent::Free()
{
  BasicRenderedSpaceComponent::Free();

  glDeleteTextures (1, &m_texture);
}

// -----------------------------------------------------------------------------

void RenderedSlideComponent::Render()
{
}

// -----------------------------------------------------------------------------
