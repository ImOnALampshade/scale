// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for managing the rendering of an entire space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "basicrenderedspacecomponent.h"
#include "renderedgamecomponent.h"
#include "../KeplerCore/game.h"

// -----------------------------------------------------------------------------

void BasicRenderedSpaceComponent::Initialize (const Json::Object &jsonData)
{
  m_layer = 0;
  jsonData.CallOn ("layer", [this] (const Json::Object::Payload &data) {
    m_layer = unsigned (data.value.as<Json::Number>());
  });

  m_skip = false;

  CoreEngine::Game->Dispatch ("AddRenderedSpace", Handle().as<BasicRenderedSpaceComponent>());
}

void BasicRenderedSpaceComponent::Free()
{
}

void BasicRenderedSpaceComponent::SetSkipped (bool skip)
{
  m_skip = skip;
}

bool BasicRenderedSpaceComponent::IsSkipped() const
{
  return m_skip;
}

unsigned int BasicRenderedSpaceComponent::Layer() const
{
  return m_layer;
}

// -----------------------------------------------------------------------------
