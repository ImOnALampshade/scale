// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for placing the camera in the world
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef CAMERA_COMPONENT_H
#define CAMERA_COMPONENT_H

#include "../KeplerCore/component.h"
#include "../KeplerMath/WorldRay.h"

class TransformComponent;

class CameraComponent : public Component
{
public:
  virtual void Initialize (const Json::Object &jsonData);

  Mat4 GetMatrix();

  float Far() const;
  void Far (float);

  float Near() const;
  void Near (float);

  float FieldOfView() const;
  void FieldOfView (float);

  Vec2 WorldToScreen(Vec3 worldCoordinates);
  WorldRay ScreenToWorld(Vec2 screenCoordinates);

  COMPONENT_META

private:
  ::Handle<TransformComponent> m_transform;

  float distanceToScreenPlane();
  float getAspectRatio();

  float m_near;
  float m_far;
  float m_fov;
};

#endif
