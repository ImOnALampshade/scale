// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph shader node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "shadernode.h"
#include "batchproperties.h"

using namespace glm;

ShaderNode::ShaderNode (OpenGL::Shader *program, String name) :
  m_program (program),
  m_effectName (name)
{
  m_projectionViewMatrixLocation = m_program->Uniform ("projection_view_matrix").location;
}

void ShaderNode::Draw (BatchGraph *graph)
{
  glUseProgram (m_program->GetProgram());

  // Get a pointer to the view projection matrix from the property bag
  void *cameraPtr = graph->Property (BatchProperty::PROJECTION_VIEW);
  glUniformMatrix4fv (m_projectionViewMatrixLocation, 1, GL_FALSE, reinterpret_cast<float *> (cameraPtr));

  graph->Property (BatchProperty::SHADER_PTR, m_program);
  graph->Property (BatchProperty::EFFECT_NAME, &m_effectName);

  DrawChild (graph);
}

