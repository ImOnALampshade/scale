// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "load_factories.h"
#include "../KeplerCore/compositefactory.h"

#include "windowcomponent.h"
#include "renderedgamecomponent.h"
#include "renderedworldspacecomponent.h"
#include "renderablecomponent.h"
#include "modelloadercomponent.h"
#include "shaderloadercomponent.h"
#include "cameracomponent.h"
#include "shadercontextmanagercomponent.h"
#include "materialloadercomponent.h"

void GraphicsComponents::LoadMeta()
{
  WindowComponent::CreateMeta();
  RenderedGameComponent::CreateMeta();
  RenderedWorldSpaceComponent::CreateMeta();
  RenderableComponent::CreateMeta();
  ModelLoaderComponent::CreateMeta();
  ShaderLoaderComponent::CreateMeta();
  CameraComponent::CreateMeta();
  ShaderContextManagerComponent::CreateMeta();
  MaterialLoaderComponent::CreateMeta();
}

void GraphicsComponents::LoadFactories()
{
  CREATE_FACTORY (Window);
  CREATE_FACTORY (RenderedGame);
  CREATE_FACTORY (RenderedWorldSpace);
  CREATE_FACTORY (Renderable);
  CREATE_FACTORY (ModelLoader);
  CREATE_FACTORY (ShaderLoader);
  CREATE_FACTORY (Camera);
  CREATE_FACTORY (ShaderContextManager);
  CREATE_FACTORY (MaterialLoader);
}
