// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Base class for a rendering component for an object in a world
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef RENDERABLE_COMPONENT_H
#define RENDERABLE_COMPONENT_H

#include "../KeplerCore/component.h"

// -----------------------------------------------------------------------------

class TransformComponent;

class RenderableComponent : public Component
{
private:
  String m_effect;
  String m_material;
  String m_model;
  glm::vec3 m_tint;
  ::Handle<TransformComponent> m_transform;

public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  glm::mat4 GetWorldMatrix();

  String   Effect();
  String Material();
  String     Mesh();
  Vec3       Tint();

  void Tint (Vec3);

  COMPONENT_META
};

// -----------------------------------------------------------------------------

#endif
