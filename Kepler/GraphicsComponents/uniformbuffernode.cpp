// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph node for uniform buffers
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include  "uniformbuffernode.h"

// -----------------------------------------------------------------------------

UniformBufferNode::UniformBufferNode (GLuint bindingIndex, GLuint buffer, GLintptr offset, GLintptr size) :
  m_bindingIndex (bindingIndex),
  m_buffer (buffer),
  m_offset (offset),
  m_size (size)
{
}

void UniformBufferNode::Draw()
{
  glBindBufferRange (GL_UNIFORM_BUFFER, m_bindingIndex, m_buffer, m_offset, m_size);

  DrawChildren();
}

// -----------------------------------------------------------------------------
