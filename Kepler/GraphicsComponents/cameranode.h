// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph instanced rendering
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef CAMERA_NODE_H
#define CAMERA_NODE_H

#include "../BatchingGraph/batchinggraph.h"
#include "jointnode.h"
#include "cameracomponent.h"

class CameraNode : public BatchNodeWithChild
{
public:
  CameraNode (::Handle<CameraComponent> camera);

  virtual void Draw (BatchGraph *graph);

private:
  ::Handle<CameraComponent> m_camera;
};

#endif
