// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for loading shader programs
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "../KeplerCore/game.h"
#include "../Logger/logger.h"
#include "shaderloadercomponent.h"
#include "renderedworldspacecomponent.h"
#include "shadernode.h"

using namespace glm;
using namespace std;

// -----------------------------------------------------------------------------

namespace
{
  unsigned ShaderFromTypeString (String type)
  {
    // Creat the correct type of shader
    if (type == "vertex")
      return glCreateShader (GL_VERTEX_SHADER);

    else if (type == "fragment")
      return glCreateShader (GL_FRAGMENT_SHADER);

    else if (type == "geometry")
      return glCreateShader (GL_GEOMETRY_SHADER);

    else if (type == "tesselation_control")
      return glCreateShader (GL_TESS_CONTROL_SHADER);

    else if (type == "tesselation_evaluation")
      return glCreateShader (GL_TESS_EVALUATION_SHADER);

    else if (type == "compute")
      return glCreateShader (GL_COMPUTE_SHADER);

    else
      BREAK (String::Format ("Invalid shader type %s\n", type.CStr()));

    return 0;
  }
}

// -----------------------------------------------------------------------------

ShaderLoaderComponent::ShaderLoaderComponent() :
  m_shaders (Utilities::Hash::SuperFast),
  m_programs (Utilities::Hash::SuperFast)
{
}

// -----------------------------------------------------------------------------

void ShaderLoaderComponent::Initialize (const Json::Object &jsonData)
{
  GL_CHECK_ERRORS();
  jsonData.CallOn ("shaders", [this] (const Json::Object::Payload &data) {
    for (const Json::Object &jsonShader : data.value.as<Json::List>())
    {
      unsigned shader;

      String type = jsonShader.Find ("type")->value;
      String file = jsonShader.Find ("file")->value;

      shader = ShaderFromTypeString (type);

      if (OpenGL::CompileShader (shader, file))
        m_shaders[file] = shader;

      else
        glDeleteShader (shader);
    }
  });

  jsonData.CallOn ("effects", [this] (const Json::Object::Payload &data) {
    for (const Json::Object &jsonProgram : data.value.as<Json::List>())
      LinkNewShader (jsonProgram);
  });
}

void ShaderLoaderComponent::Free()
{
  m_shaders.ForEach ([] (const HashTable<String, unsigned>::Payload &data) {
    glDeleteShader (data.value);
  });

  m_programs.ForEach ([] (const HashTable<String, OpenGL::Shader>::Payload &data) {
    glDeleteProgram (data.value.GetProgram());
  });
}

// -----------------------------------------------------------------------------

void ShaderLoaderComponent::LinkNewShader (const Json::Object &jsonProgram)
{
  static String shader_types[] = {
    "vertex_shader",
    "fragment_shader",
    "geometry_shader",
    "tesselation_control_shader",
    "tesselation_evaluation_shader"
  };

  unsigned program = glCreateProgram();

  String name = jsonProgram.Locate ("name", String (""));
  String type = jsonProgram.Locate ("type", String ("forward"));

  // For each of our shader types (Array listed above)
  for (String type : shader_types)
  {
    bool missing = false;

    jsonProgram.CallOn (type, [this, program, &missing] (const Json::Object::Payload &data) {
      String shader_name = data.value;

      auto found = m_shaders.Find (shader_name);

      // If not found, log the error
      if (found == m_shaders.end())
      {
        LOG ("Invalid shader name: " + shader_name);
        missing = true;
      }

      // Attatch the shader we got on to our program
      else
        glAttachShader (program, found->value);

    });

    // If there was a missing shader, abort the linkage
    if (missing)
    {
      glDeleteProgram (program);
      return;
    }
  }

  if (type == "deferred")
  {
    glBindAttribLocation (program, 0, "v_coord");

    // We also need to load outputs from the JSON file
  }

  else if (type == "screen")
  {
    glBindAttribLocation (program, 0, "v_coord");
  }

  else if (type == "forward")
  {
    glBindAttribLocation (program, 0, "v_coord");
    glBindAttribLocation (program, 1, "v_texcoord");
    glBindAttribLocation (program, 2, "v_normal");

    glBindFragDataLocation (program, 0, "out_diffuse");
    glBindFragDataLocation (program, 1, "out_specular");
    glBindFragDataLocation (program, 2, "out_normal");
    glBindFragDataLocation (program, 3, "out_position");
  }

  else if (type == "particle")
  {
    glBindAttribLocation (program, 0, "v_coord");
    glBindAttribLocation (program, 1, "v_texcoord");

    glBindFragDataLocation (program, 0, "out_diffuse");
    glBindFragDataLocation (program, 1, "out_specular");
    glBindFragDataLocation (program, 2, "out_normal");
    glBindFragDataLocation (program, 3, "out_position");
  }

  bool linked = OpenGL::LinkProgram (program);

  // If linking the program works, add it to the program list. Otherwise, delete it.
  if (!linked)
    glDeleteProgram (program);

  OpenGL::Shader shader (program);
  auto insertResult = m_programs.Insert (name, shader);

  if (type == "forward")
  {
    // Set the diffuse, specular, and geometry maps to tex units 0, 1, and 2, respectively.
    // This is always going to be the case.
    glProgramUniform1i (program, shader.Uniform ("diffuse_maps").location,  0);
    glProgramUniform1i (program, shader.Uniform ("specular_maps").location, 1);
    glProgramUniform1i (program, shader.Uniform ("geometry_maps").location, 2);

    // Get the index for the matrix_buffer block, and bind it
    shader.SetBufferBinding ("matrix_buffer", 0);

    // Add a ShaderNode the the BatchGraph of the rendered world
    auto renderedWorld = Owner()->GetComponent<RenderedWorldSpaceComponent>();

    shared_ptr<ShaderNode> shaderNode = make_shared<ShaderNode> (&insertResult.second->value, name);

    renderedWorld->ShaderParent()->AddNamedChild (name, shaderNode);
    shaderNode->Child (renderedWorld->MaterialParent());
  }
}

// -----------------------------------------------------------------------------

OpenGL::Shader *ShaderLoaderComponent::GetProgram (String name)
{
  auto found = m_programs.Find (name);

  if (found == m_programs.end())
    return nullptr;

  else
    return &found->value;
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (ShaderLoader)
{
}
