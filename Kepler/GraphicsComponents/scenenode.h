// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Batch graph scene owner node
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SCENE_NODE_H
#define SCENE_NODE_H

#include "../BatchingGraph/batchinggraph.h"
#include "jointnode.h"

class SceneNode : public BatchNode
{
public:
  SceneNode ();

  virtual void Draw (BatchGraph *graph);

  std::shared_ptr<BatchNode> Geometry();
  std::shared_ptr<BatchNode> Particles();

  void Geometry (std::shared_ptr<BatchNode>);
  void Particles (std::shared_ptr<BatchNode>);

private:
  std::shared_ptr<BatchNode> m_geometry;
  std::shared_ptr<BatchNode> m_particles;
};

#endif
