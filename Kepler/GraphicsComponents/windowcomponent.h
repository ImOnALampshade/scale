// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for bringing up and controlling a window
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef WINDOW_COMPONENT_H
#define WINDOW_COMPONENT_H

#include "../KeplerCore/component.h"
#include "renderedgamecomponent.h"

class WindowComponent : public Component
{
private:
  #ifdef _WIN32
  struct SavedWindow
  {
    unsigned x, y, width, height;
    bool isMaximized;
    unsigned long style, exStyle;
  } m_saved;
  #endif

  GLFWwindow *m_glWindow;
  unsigned    m_major, m_minor;
  String      m_glVersion;
  bool        m_isFullscreen;
  ::Handle<RenderedGameComponent> m_renderedGame;

public:
  WindowComponent();
  ~WindowComponent();

  virtual void Initialize (const Json::Object &jsonData);

  void Update();

  GLFWwindow *GetWindow();

  String OpenGLVersion();
  unsigned OpenGLVersionMajor();
  unsigned OpenGLVersionMinor();
  bool IsKeyDown (int keycode);
  bool IsMouseDown (int mouseCode);
  Vec2 GetMousePos();

  glm::ivec2 Size();

  float AspectRatio();

  void Fullscreen (int fullscreen);
  int Fullscreen();

  COMPONENT_META
};

#endif
