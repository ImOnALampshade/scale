// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  MetaType class for C++
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../HashFunctions/hashfunctions.h"
#include "metatype.h"

using namespace Utilities::Hash;

DelayedConstructor<HashTable<String, MetaType> > MetaType::Table;
DelayedConstructor<HashTable<const type_info *, String> > MetaType::NameTable;

// -----------------------------------------------------------------------------

MetaType::MetaType (String name, MetaType *base) :
  name (name),
  propGetters (SuperFast),
  propSetters (SuperFast),
  methods (SuperFast),
  py_callables (SuperFast),
  py_statics (SuperFast),
  globalMethods (SuperFast),
  bindAsHandle (false)
{
  if (base)
  {
    // Copy getters, setters, and methods over from the base class
    propGetters   = base->propGetters;
    propSetters   = base->propSetters;
    methods       = base->methods;
    bindAsHandle  = base->bindAsHandle;
    globalMethods = base->globalMethods;
  }
}

MetaType::MetaType (String name, const char *base)
  : MetaType (name, & (*MetaType::Table).Find (base)->value)
{

}

// -----------------------------------------------------------------------------

void MetaType::Create (void *obj, const Tuple &args)
{
  constructor (obj, args);
}

void MetaType::Destroy (void *obj)
{
  destructor (obj, Tuple());
}

// -----------------------------------------------------------------------------

bool MetaType::Get (String name, void *obj, void *val) const
{
  return propGetters.CallOnValue (name, [&] (const Getter &get) {
    get.implementation (obj, val);
  });
}

bool MetaType::Set (String name, void *obj, void *val) const
{
  return propSetters.CallOnValue (name, [&] (const Setter &set) {
    set.implementation (obj, val);
  });
}

bool MetaType::Call (String name, void *obj, const Tuple &args, DynamicVariant &result) const
{
  return methods.CallOnValue (name, [&] (const MetaFunction &func) {
    result = func (obj, args);
  });
}

DynamicVariant MetaType::Get (String name, void *obj)
{
  DynamicVariant var;
  propGetters.CallOnValue (name, [&] (const Getter &get) {
    MetaType *meta = MetaFromTypeInfo (get.type);

    var.m_type = get.type;

    if (meta)
    {
      var.m_type = meta->type;
      var.m_obj = malloc (meta->size);
      var.m_size = meta->size;
      var.m_copy = meta->CopyConstructor_Variant;
      var.m_dtor = meta->Destructor_Variant;

      get.implementation (obj, var.Addr());
    }

    else if (*get.type == typeid (float))
    {
      var.Construct<float>();
      get.implementation (obj, var.Addr());
    }

    else if (*get.type == typeid (int))
    {
      var.Construct<int>();
      get.implementation (obj, var.Addr());
    }

    else if (*get.type == typeid (String))
    {
      var.Construct<String>();
      get.implementation (obj, var.Addr());
    }

    else if (*get.type == typeid (String))
    {
      var.Construct<String>();
      get.implementation (obj, var.Addr());
    }

  });

  return var;
}

// -----------------------------------------------------------------------------

void MetaType::AddMethod (String name, MetaFunction function)
{
  methods.Insert (name, function);
}

void MetaType::AddMethodGlobal (String name, MetaFunctionGlobal function)
{
  globalMethods.Insert (name, std::make_shared<MetaFunctionGlobal> (function));
}

// -----------------------------------------------------------------------------

MetaType *MetaType::AddType (const MetaType &val)
{
  if (!Table.IsCreated())
  {
    Table.Construct (SuperFast);
    NameTable.Construct (TypeInfo);
  }

  auto result = Table->Insert (val.name, val);
  if(!val.bindAsHandle)
    NameTable->Insert (val.type, val.name);
  else
    NameTable->Insert(val.handleType, val.name);

  if (result.first)
    return &result.second->value;

  else
    return nullptr;
}

MetaType * MetaType::GetMeta(String name)
{
  if (!Table.IsCreated())
  {
    Table.Construct(SuperFast);
    NameTable.Construct(TypeInfo);
  }

  if (Table->Contains(name))
  {
    MetaType * meta = &Table->Locate(name, "");
    return meta;
  }
  return nullptr;
}

MetaType * MetaType::GetMeta(const std::type_info *typeInfo)
{
  if (!Table.IsCreated())
  {
    Table.Construct(SuperFast);
    NameTable.Construct(TypeInfo);
  }

  String name = MetaType::NameTable->Locate(typeInfo, "");
  if (name != "")
    return GetMeta(name);
  else
    return nullptr;
}

// -----------------------------------------------------------------------------

MetaType *MetaFromTypeInfo (const type_info *type)
{
  String name = MetaType::NameTable->Locate (type);

  if (name.Empty()) return nullptr;

  MetaType *meta = nullptr;

  MetaType::Table->CallOnValue (name, [&meta] (MetaType &val) {
    meta = &val;
  });

  return meta;
}

MetaType *MetaFromVariant (const DynamicVariant &var)
{
  return MetaFromTypeInfo (var.Type());
}

// -----------------------------------------------------------------------------
