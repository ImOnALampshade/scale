// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function object for the metatype system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef META_FUNCTION_H
#define META_FUNCTION_H

// -----------------------------------------------------------------------------

#include "tuple.h"
#include <functional>
#include <vector>

struct MetaType;

struct MetaFunction
{
  const type_info *returnTypeID;
  std::vector<const type_info *> argTypeIDs;

  MetaType *returnType;
  std::vector<MetaType *> argTypes;
  std::function<DynamicVariant (void *, const Tuple &) > implementation;

  void PostInit();

  DynamicVariant operator() (void *, const Tuple &) const;

  template<typename ClassType, typename ReturnType, typename... ArgTypes>
  void SetFunction (ReturnType (ClassType::*fn) (ArgTypes...));

  template<typename ClassType, typename... ArgTypes>
  void SetFunction (void (ClassType::*fn) (ArgTypes...));

  template<typename...>
  struct TypeAdder;

  template<typename T, typename... Args>
  struct TypeAdder<T, Args...>
  {
    static void AddTypes (std::vector<const type_info *> &types)
    {
      types.push_back (&typeid (T));
      TypeAdder<Args...>::AddTypes (types);
    }
  };

  template<>
  struct TypeAdder<>
  {
    static void AddTypes (std::vector<const type_info *> &types) { }
  };
};

template<typename ClassType, typename ReturnType, typename... ArgTypes>
void MetaFunction::SetFunction (ReturnType (ClassType::*fn) (ArgTypes...))
{
  TypeAdder<ArgTypes...>::AddTypes (argTypeIDs);
  returnTypeID = &typeid (ReturnType);

  implementation = [fn] (void *obj_, const Tuple &args_) -> DynamicVariant {
    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    std::tuple<ArgTypes...> args = ToStlTuple<ArgTypes...> (args_);
    ReturnType ret = CallFunctionFromTuple::CallFunction (obj, fn, args);

    return DynamicVariant (ret);
  };
}

template<typename ClassType, typename... ArgTypes>
void MetaFunction::SetFunction (void (ClassType::*fn) (ArgTypes...))
{
  TypeAdder<ArgTypes...>::AddTypes (argTypeIDs);
  returnTypeID = &typeid (void);

  implementation = [fn] (void *obj_, const Tuple &args_) ->DynamicVariant {
    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    std::tuple<ArgTypes...> args = ToStlTuple<ArgTypes...> (args_);
    CallFunctionFromTuple::CallFunction (obj, fn, args);
    return DynamicVariant();
  };
}

// -----------------------------------------------------------------------------


struct MetaFunctionGlobal
{
  String name;
  std::vector<String> argNames;

  String returnTypeName;
  std::vector<String> argTypeNames;

  MetaType *returnType;
  std::vector<MetaType *> argTypes;
  std::function<DynamicVariant (const Tuple &) > implementation;

  void PostInit();

  DynamicVariant operator() (const Tuple &) const;

  template<typename ReturnType, typename... ArgTypes>
  void SetFunction (ReturnType (*fn) (ArgTypes...));

  template<typename... ArgTypes>
  void SetFunction (void (*fn) (ArgTypes...));
};


template<typename ReturnType, typename... ArgTypes>
void MetaFunctionGlobal::SetFunction (ReturnType (*fn) (ArgTypes...))
{
  implementation = [fn] (const Tuple &args_) -> DynamicVariant {
    std::tuple<ArgTypes...> args = ToStlTuple<ArgTypes...> (args_);
    ReturnType ret = CallFunctionFromTupleGlobal<ArgTypes...>::CallFunction (fn, args);

    return DynamicVariant (ret);
  };
}

template<typename... ArgTypes>
void MetaFunctionGlobal::SetFunction (void (*fn) (ArgTypes...))
{
  implementation = [fn] (const Tuple &args_) ->DynamicVariant {
    std::tuple<ArgTypes...> args = ToStlTuple<ArgTypes...> (args_);
    CallFunctionFromTupleGlobal<ArgTypes...>::CallFunction (fn, args);
    return DynamicVariant();
  };
}

#endif
