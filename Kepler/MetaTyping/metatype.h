// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  MetaType class for C++
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef METATYPE_H
#define METATYPE_H

#include "../HashTable/hashtable.h"
#include "../String/string.h"
#include "../TypePuns/variant.h"
#include "../TypePuns/delayedconstructor.h"
#include "../KeplerCore/handle.h"
#include "function.h"
#include <functional>
#include <typeinfo>
#include <type_traits>
#include <vector>
#include <memory>

// -----------------------------------------------------------------------------

struct MetaFunction;

struct MetaType
{
  static DelayedConstructor<HashTable<String, MetaType> > Table;
  static DelayedConstructor<HashTable<const type_info *, String>> NameTable;

  MetaType (String name, const char *base);
  MetaType (String name, MetaType *base = nullptr);

  void Create (void *obj, const Tuple &args);
  void Destroy (void *obj);

  bool Get (String name, void *obj, void *val) const;
  bool Set (String name, void *obj, void *val) const;
  bool Call (String name, void *obj, const Tuple &args, DynamicVariant &result) const;

  DynamicVariant Get (String name, void *obj);

  template<typename ClassType, typename ArgType>
  void AddGetter (String name, ArgType (ClassType::*getter) (void));

  template<typename ClassType, typename ArgType>
  void AddGetter (String name, ArgType (ClassType::*getter) (void) const);

  template<typename ClassType, typename ArgType>
  void AddSetter (String name, void (ClassType::*setter) (ArgType));

  template<typename ClassType, typename Type>
  void AddMember (String name, Type ClassType::*member);

  template<typename ClassType, typename... Args>
  void CreateConstructor();

  template<typename ClassType>
  void Initialize();

  void AddMethod (String name, MetaFunction function);
  void AddMethodGlobal (String name, MetaFunctionGlobal function);

  static MetaType *AddType (const MetaType &val);

  typedef Variant < sizeof std::function<void() >> FunctionVariant;

  struct Getter
  {
    const std::type_info *type;
    std::function<void (void *, void *) > implementation;
  };

  struct Setter
  {
    const std::type_info *type;
    std::function<void (void *, void *) > implementation;
  };

  String name;
  String module;
  const type_info *type;
  const type_info *handleType;
  HashTable<String, Getter> propGetters;
  HashTable<String, Setter> propSetters;
  HashTable<String, MetaFunction> methods;
  HashTable<String, std::shared_ptr<MetaFunctionGlobal>> globalMethods;
  MetaFunction constructor;
  void (*copyconstructor) (void *obj, void *dest);
  MetaFunction destructor;
  size_t       size;
  bool         bindAsHandle;

  void * (*CopyConstructor_Variant) (const void *);
  void (*Destructor_Variant) (void *);

  HashTable<String, void *>  py_callables;
  HashTable<String, void *>  py_statics;

  static MetaType *GetMeta (String name);
  static MetaType *GetMeta (const std::type_info *typeInfo);

  void *py_type;

  // convert from a value (first argument, input) to a PyObject (return value)
  std::function<void * (void *) > py_convertTo;
  // convert from a PyObject (first argument, input) to a value (second argument, output)
  std::function<void (void *, void *) > py_convertFrom;
};

MetaType *MetaFromTypeInfo (const type_info *type);
MetaType *MetaFromVariant (const DynamicVariant &var);

template<size_t SIZE>
MetaType *MetaFromVariant (const Variant<SIZE> &var)
{
  return MetaFromTypeInfo (var.Type());
}

// -----------------------------------------------------------------------------

template<typename ClassType, typename ArgType>
void MetaType::AddGetter (String name, ArgType (ClassType::*getter) (void))
{
  Getter get;
  get.type = &typeid (ArgType);
  get.implementation = [getter] (void *obj_, void *arg_) {
    typedef std::remove_reference<ArgType>::type ArgTypeNoRef;
    typedef std::remove_const<ArgTypeNoRef>::type ArgTypeNonConst;
    typedef std::add_pointer<ArgTypeNonConst>::type ArgTypePtr;

    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    ArgTypePtr arg = reinterpret_cast<ArgTypePtr> (arg_);

    *arg = (obj->*getter) ();
  };

  propGetters.Insert (name, get);
}

template<typename ClassType, typename ArgType>
void MetaType::AddGetter (String name, ArgType (ClassType::*getter) (void) const)
{
  Getter get;
  get.type = &typeid (ArgType);
  get.implementation = [getter] (void *obj_, void *arg_) {
    typedef std::remove_reference<ArgType>::type ArgTypeNoRef;
    typedef std::remove_const<ArgTypeNoRef>::type ArgTypeNonConst;
    typedef std::add_pointer<ArgTypeNonConst>::type ArgTypePtr;

    const ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    ArgTypePtr arg = reinterpret_cast<ArgTypePtr> (arg_);

    *arg = (obj->*getter) ();
  };

  propGetters.Insert (name, get);
}

// -----------------------------------------------------------------------------

template<typename ClassType, typename ArgType>
void MetaType::AddSetter (String name, void (ClassType::*setter) (ArgType))
{
  Setter set;
  set.type = &typeid (ArgType);
  set.implementation = [setter] (void *obj_, void *arg_) {
    typedef std::remove_reference<ArgType>::type ArgTypeNoRef;
    typedef std::remove_const<ArgTypeNoRef>::type ArgTypeNonConst;
    typedef std::add_pointer<ArgTypeNonConst>::type ArgTypePtr;

    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    ArgTypePtr arg = reinterpret_cast<ArgTypePtr> (arg_);

    (obj->*setter) (*arg);
  };

  propSetters.Insert (name, set);
}

// -----------------------------------------------------------------------------

template<typename ClassType, typename Type>
void MetaType::AddMember (String name, Type ClassType::*member)
{
  typedef std::remove_reference<Type>::type TypeNoRef;
  typedef std::remove_const<TypeNoRef>::type TypeNonConst;
  typedef std::add_pointer<TypeNonConst>::type TypePtr;

  Setter set;
  set.type = &typeid (Type);
  set.implementation = [member] (void *obj_, void *arg_) {
    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    TypePtr arg = reinterpret_cast<TypePtr> (arg_);

    obj->*member = *arg;
  };

  propSetters.Insert (name, set);

  Getter get;
  get.type = &typeid (Type);
  get.implementation = [member] (void *obj_, void *arg_) {
    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    TypePtr arg = reinterpret_cast<TypePtr> (arg_);

    *arg = obj->*member;
  };

  propGetters.Insert (name, get);
}

// -----------------------------------------------------------------------------

template<typename ClassType, typename... Args>
void MetaType::CreateConstructor()
{
  constructor.implementation = [] (void *obj_, const Tuple &args_) {
    std::tuple<Args...> args = ToStlTuple<Args...> (args_);
    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    CallConstructorFromTuple::CallConstructor<ClassType> (obj, args);

    return DynamicVariant();
  };

  copyconstructor = [] (void *obj_, void *dest) {
    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    new (dest) ClassType (*obj);
  };

  destructor.implementation = [] (void *obj_, const Tuple &args_) {
    ClassType *obj = reinterpret_cast<ClassType *> (obj_);
    obj->~ClassType();

    return DynamicVariant();
  };

  CopyConstructor_Variant = DynamicVariant::Copy < ClassType > ;

  Destructor_Variant = DynamicVariant::Destroy < ClassType > ;
}

// -----------------------------------------------------------------------------

template<typename ClassType>
void MetaType::Initialize()
{
  size = sizeof ClassType;
  type = &typeid (ClassType);
  handleType = &typeid(::Handle<ClassType>);
}

// -----------------------------------------------------------------------------

#endif
