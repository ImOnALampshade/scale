// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  The super complex tuple implementation we're using
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef TUPLE_H
#define TUPLE_H

#include "../String/string.h"
#include "../TypePuns/dynamicvariant.h"
#include <tuple>
#include <vector>

typedef std::vector<DynamicVariant> Tuple;

class TupleTypeError
{
public:
  TupleTypeError (unsigned index, String errorString);

  unsigned Index();
  String   ErrorString();

public:
  const unsigned m_index;
  const String   m_errorStr;
};

template<unsigned Index>
struct _ConvertFromTupleHelper
{
  template<typename... Args>
  static void ConvertTuple (Tuple &out, const std::tuple<Args...> &in)
  {
    out[Index - 1] = std::get<Index - 1> (in);
    _ConvertFromTupleHelper<Index - 1>::ConvertTuple (out, in);
  }
};

template<>
struct _ConvertFromTupleHelper < 0 >
{
  template<typename... Args>
  static void ConvertTuple (Tuple &out, const std::tuple<Args...> &in)
  {
  }
};

template<typename... Args> Tuple FromStlTuple (const std::tuple<Args...> &in)
{
  Tuple out (sizeof... (Args));
  _ConvertFromTupleHelper<sizeof... (Args) >::ConvertTuple (out, in);
  return out;
}
template<unsigned Index>
struct _ConvertTupleHelper
{
  template<typename T>
  static void GrabValue (T &val, const Tuple &in)
  {
    val = in[Index - 1].as<T>();
  }

  template<>
  static void GrabValue<float> (float &val, const Tuple &in)
  {
    if (in[Index - 1].IsType<float>())
      val = in[Index - 1];

    else if (in[Index - 1].IsType<int>())
      val = float (in[Index - 1].as<int>());
  }

  template<typename... Args>
  static void ConvertTuple (std::tuple<Args...> &out, const Tuple &in)
  {
    typedef std::remove_reference <decltype (std::get<Index - 1> (out)) > ::type CurrentType;
    GrabValue<CurrentType> (std::get<Index - 1> (out), in);
    _ConvertTupleHelper<Index - 1>::ConvertTuple (out, in);
  }
};

template<>
struct _ConvertTupleHelper<0>
{
  template<typename... Args>
  static void ConvertTuple (std::tuple<Args...> &out, const Tuple &in)
  {
  }
};

template<typename... Args> std::tuple<Args...> ToStlTuple (const Tuple &in)
{
  std::tuple<Args...> out;
  _ConvertTupleHelper<sizeof... (Args) >::ConvertTuple (out, in);
  return out;
}

struct CallConstructorFromTuple
{
  template <unsigned N>
  struct ApplyFunc
  {
    template <typename Class, typename... ArgsT, typename... Args>
    static void ApplyTuple (void *c, const std::tuple<ArgsT...> &t, Args... args)
    {
      ApplyFunc<N - 1>::ApplyTuple<Class> (c, t, std::get<N - 1> (t), args...);
    }
  };

  template<>
  struct ApplyFunc < 0 >
  {
    template <typename Class, typename... ArgsT, typename... Args>
    static void ApplyTuple (void *c, const std::tuple<ArgsT...> &t, Args... args)
    {
      new (c) Class (args...);
    }
  };

  template<typename Class, typename... Args>
  static void CallConstructor (void *c, const std::tuple<Args...> &args)
  {
    ApplyFunc<sizeof... (Args) >::ApplyTuple<Class> (c, args);
  }
};

// -----------------------------------------------------------------------------

struct CallFunctionFromTuple
{
  template <unsigned N>
  struct ApplyFunc
  {
    template <typename Class, typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
    static Return ApplyTuple (Class *c, Return (Class::*fn) (ArgsF...), const std::tuple<ArgsT...> &t, Args... args)
    {
      return ApplyFunc<N - 1>::ApplyTuple (c, fn, t, std::get<N - 1> (t), args...);
    }
  };

  template<>
  struct ApplyFunc < 0 >
  {
    template <typename Class, typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
    static Return ApplyTuple (Class *c, Return (Class::*fn) (ArgsF...), const std::tuple<ArgsT...> &t, Args... args)
    {
      return (c->*fn) (args...);
    }
  };

  template<typename Class, typename Return, typename... Args>
  static Return CallFunction (Class *c, Return (Class::*fn) (Args...), const std::tuple<Args...> &args)
  {
    return ApplyFunc<sizeof... (Args) >::ApplyTuple (c, fn, args);
  }
};

template<typename... Args>
struct CallFunctionFromTupleGlobal
{
  template <unsigned N>
  struct ApplyFunc
  {
    template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
    static Return ApplyTuple (Return (*fn) (ArgsF...), const std::tuple<ArgsT...> &t, Args... args)
    {
      return ApplyFunc<N - 1>::ApplyTuple (fn, t, std::get<N - 1> (t), args...);
    }
  };

  template<>
  struct ApplyFunc < 0 >
  {
    template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
    static Return ApplyTuple (Return (*fn) (ArgsF...), const std::tuple<ArgsT...> &t, Args... args)
    {
      return (*fn) (args...);
    }
  };

  template<typename Return, typename... Args>
  static Return CallFunction (Return (*fn) (Args...), const std::tuple<Args...> &args)
  {
    return ApplyFunc<sizeof... (Args) >::ApplyTuple (fn, args);
  }
};

#endif