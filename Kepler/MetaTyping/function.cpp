// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function object for the metatype system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "function.h"
#include "metatype.h"

// -----------------------------------------------------------------------------
// Post Init is run after all metadata has been added to the meta tabel.
// It finds the the metatype object that corresponds with the type names
// and stores a pointer to that metatype object for later use.
// -----------------------------------------------------------------------------
void MetaFunction::PostInit()
{
  for (const type_info *type : argTypeIDs)
  {
    MetaType *meta = MetaFromTypeInfo (type);
    argTypes.push_back (meta);
  }

  returnType = MetaFromTypeInfo (returnTypeID);
}

// -----------------------------------------------------------------------------

DynamicVariant MetaFunction::operator() (void *obj, const Tuple &args) const
{
  return implementation (obj, args);
}

// -----------------------------------------------------------------------------
// Post Init is run after all metadata has been added to the meta tabel.
// It finds the the metatype object that corresponds with the type names
// and stores a pointer to that metatype object for later use.
// -----------------------------------------------------------------------------
void MetaFunctionGlobal::PostInit()
{
  //run through argTypesNames and populate
  //vector of metatype pointers representing the argtypes
  for (String argName : argTypeNames)
  {
    MetaType::Table->CallOnValue (argName, [this] (MetaType &meta) {
      argTypes.push_back (&meta);
    });
  }

  //should have a name present.
  assert (!returnTypeName.Empty());

  //grab metatype pointer to returnType
  MetaType::Table->CallOnValue (returnTypeName, [this] (MetaType &meta) { returnType = &meta; });
}

// -----------------------------------------------------------------------------

DynamicVariant MetaFunctionGlobal::operator() (const Tuple &args) const
{
  return implementation (args);
}
