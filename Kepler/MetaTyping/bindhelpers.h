// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  MetaType class for C++
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef META_MACROS_H
#define META_MACROS_H

#include "../MetaTyping/metatype.h"

#define META_CREATE(type, modname) MetaType meta##type(#type); \
  do { meta##type.module = modname; meta##type.Initialize<type>(); } while(0)
#define META_INHERIT(type, modname, parent) MetaType meta##type(#type, parent); \
  do { meta##type.module = modname; meta##type.Initialize<type>(); } while(0)
#define META_HANDLE(type) meta##type.bindAsHandle = true;
#define META_CTOR(type, ...) meta##type.CreateConstructor<type, __VA_ARGS__>()
#define META_ADD_MEMBER(type, member) meta##type.AddMember(#member, &type::member)
#define META_ADD_PROP_GET(type, prop) meta##type.AddGetter(#prop, &type::prop);
#define META_ADD_PROP_SET(type, prop) meta##type.AddSetter(#prop, &type::prop);
#define META_ADD_METHOD(type, method) do \
  { \
    MetaFunction fnc; \
    fnc.SetFunction(&type::method); \
    meta##type.AddMethod(#method, fnc); \
  } while (false)
#define META_ADD_FUNCTION(type, fn_name, function) do \
  { \
    MetaFunctionGlobal fnc; \
    fnc.SetFunction(&function); \
    fnc.name = fn_name; \
    meta##type.AddMethodGlobal(fn_name, fnc); \
  } while (false)
#define META_FINALIZE(type) MetaType::AddType(meta##type)
#define META_FINALIZE_PTR(type, ptr) ptr = MetaType::AddType(meta##type);

#define META_ADD_PROP(type, prop) \
  META_ADD_PROP_GET(type, prop); \
  META_ADD_PROP_SET(type, prop)

#define META_ADD_PROP_READONLY(type, prop) \
  META_ADD_PROP_GET(type, prop)

#define META_ADD_PROP_WRITEONLY(type, prop) \
  META_ADD_PROP_SET(type, prop)

#endif
