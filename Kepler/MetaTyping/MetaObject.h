// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  A special variant which contains a handle and a meta type.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../KeplerCore/handle.h"
#include "metatype.h"

struct MetaObject
{
  MetaObject()
    : Contents(nullptr), Meta(nullptr)
  {
  }
  template<typename T>
  MetaObject(Handle<T> content, MetaType *meta)
    : Contents(nullptr), Meta(meta)
  {
    Contents = *reinterpret_cast<Handle<char>*>(&content);
  }
  Handle<char> Contents;
  MetaType *Meta;
};