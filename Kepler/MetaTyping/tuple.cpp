// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  The super complex tuple implementation we're using
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "tuple.h"

// -----------------------------------------------------------------------------

TupleTypeError::TupleTypeError (unsigned index, String errorString) :
  m_index (index),
  m_errorStr (errorString)
{
}

unsigned TupleTypeError::Index()
{
  return m_index;
}

String TupleTypeError::ErrorString()
{
  return m_errorStr;
}

// -----------------------------------------------------------------------------
