// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Combines multiple mip-mapped images into a single image file
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../kto/header.h"
#include "../String/string.h"
#include <IL/il.h>
#include <vector>
#include <cassert>

using namespace std;

FILE *outfile;
vector<String> inputs;

namespace
{
  int WritePNGs();
}


int main (int argc, const char **argv)
{
  if (argc < 3)
  {
    printf ("usage: kto-maker <output> <compression mode> <inputs...>\n");
    return -1;
  }

  // We need OpenIL to load the input files
  ilInit();

  String output      = argv[1];
  String compression = argv[2];

  for (int i = 3; i < argc; ++i)
    inputs.push_back (argv[i]);

  // Open the file we want to write the output to
  outfile = fopen (output.c_str(), "wb");

  // No compression is used
  if (compression == "none")
    return WritePNGs();

  else
    return 0;
}

namespace
{
  // ---------------------------------------------------------------------------

  int WritePNGs()
  {
    for (size_t i = 0; i < inputs.size(); ++i)
    {
      String infile = inputs[i];


      ILint ilImg;
      ILboolean ilLoaded;
      void *inputData;

      ilImg = ilGenImage();
      ilBindImage (ilImg);
      ilLoaded = ilLoadImage (infile.c_str());

      if (!ilLoaded)
      {
        printf ("Failed to load image %s\n", infile.c_str());
        return -1;
      }

      ilLoaded = ilConvertImage (IL_RGBA, IL_UNSIGNED_BYTE);

      if (!ilLoaded)
      {
        printf ("Failed to convert image %s to RGBA8\n", infile.c_str());
        return -1;
      }

      ILint w = ilGetInteger (IL_IMAGE_WIDTH);
      ILint h = ilGetInteger (IL_IMAGE_HEIGHT);

      uint32_t size[] = { w, h };

      printf ("Writing %s to KTO file\n", infile.c_str());

      if (i == 0)
      {
        // Write the header to the file
        Kto::Header header;
        header.mipmaps = uint8_t (inputs.size());
        header.width = w;
        header.height = h;

        fwrite (&header, sizeof header, 1, outfile);
      }


      fwrite (size, sizeof uint32_t, 2, outfile);
      fwrite (ilGetData(), 4, w * h, outfile);

      ilDeleteImage (ilImg);
    }

    fclose (outfile);

    return 0;
  }

  // ---------------------------------------------------------------------------
}

