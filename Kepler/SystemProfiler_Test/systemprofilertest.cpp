//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <catch.hpp>
#include <iostream>
#include "../SystemProfiler/systemprofiler.h"
#include <random>
#include <math.h>
#include <chrono>

TEST_CASE ("SystemProfilerTest1")
{
  SECTION ("TestProcessTimeLogging")
  {
    using Utilities::SystemProfiler;
    using namespace std::chrono;

    SystemProfiler::Configuration config;
    config.LogFilePathDestination = String (".\\");
    config.RecordInterval = std::chrono::milliseconds (100);
    SystemProfiler profiler (config);

    std::minstd_rand randomGen;


    for (int i = 0; i < 400; i += 1)
    {
      std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();


      //loop 500 sqrts
      int loops = randomGen() % 500000;

      for (int k = 0; k < loops; k += 1)
      {
        float l = sqrtf (34);
        l = 1.0f;
      }

      std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
      milliseconds deltaTime = duration_cast<milliseconds> (endTime - startTime);
      profiler.LogProcessTime ("SquareCompute", deltaTime);


      startTime = std::chrono::high_resolution_clock::now();

      //loop 500 float additions.
      //loops = randomGen() % 50000000;
      for (int k = 0; k < loops; k += 1)
      {
        float l;
        l = 0;
        l += 15;
      }

      endTime = std::chrono::high_resolution_clock::now();
      deltaTime = duration_cast<milliseconds> (endTime - startTime);
      profiler.LogProcessTime ("FloatAddition", deltaTime);

      /*startTime = std::chrono::high_resolution_clock::now();
      profiler.SaveData( profiler.GetLogName() );
      profiler.LoadPreviousLogFile();
      endTime = std::chrono::high_resolution_clock::now();
      deltaTime = duration_cast<milliseconds>(endTime - startTime);
      profiler.LogProcessTime(L"SaveAndLoad", deltaTime);*/



    }


  }
}