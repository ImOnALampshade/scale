//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <catch.hpp>
#include "../String/string.h"

TEST_CASE ("UNICODE - Constructors")
{
  SECTION ("Default construction")
  {
    String s;

    CHECK (s.Length() == 0);
    CHECK (s.Empty());
  }

  SECTION ("String construction")
  {
    String s ("Hello, world!");

    CHECK (s == "Hello, world!");
  }

  SECTION ("Formated construction")
  {
    String s = String::Format ("Hello, %s!", "world");

    CHECK (s == "Hello, world!");
  }

  SECTION ("Copy construction")
  {
    String s1 ("Hello, world!");

    {
      String s2 (s1);
      CHECK (s1 == s2);
    }

    CHECK (s1 == "Hello, world!");
  }
}

TEST_CASE ("UNICODE - Assignment")
{
  const char *cString = "Hello, world! (C string)";
  String string = "Hello, world! (String object)";
  String test ("Something");

  SECTION ("string = litera")
  {
    test.Assign (cString, strlen (cString));

    CHECK (test == cString);
  }

  SECTION ("string = string")
  {
    test = string;

    CHECK (test == string);
  }
}

TEST_CASE ("UNICODE - Appending")
{
  String s ("Hello");

  SECTION ("Appending litera")
  {
    s += ", world!";
    CHECK (s == "Hello, world!");
  }

  SECTION ("Appending string object")
  {
    s += String (", world!");
    CHECK (s == "Hello, world!");
  }
}

TEST_CASE ("UNICODE - Lazy Copy")
{
  String s1 ("This is a string");
  String s2 (s1);

  CHECK (s1 == s2);

  SECTION ("Modify copy")
  {
    s2 += " something else";

    CHECK (s1 != s2);
  }

  SECTION ("Modify origina")
  {
    s1 += " something else";

    CHECK (s1 != s2);
  }
}