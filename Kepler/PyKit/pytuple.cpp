// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  An object that can hold any type of object that fits within a fixed size
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "pytuple.h"

// -----------------------------------------------------------------------------

PyKitTuple::PyKitTuple (PyObject *tuple) : m_tuple (tuple)
{
}

PyKitTuple::PyKitTuple (Py_ssize_t size) : m_tuple (PyTuple_New (size))
{
}

PyKitTuple::PyKitTuple (const PyKitTuple &that) : m_tuple (that.m_tuple)
{
  Py_XINCREF (m_tuple);
}

PyKitTuple::~PyKitTuple()
{
  Py_XDECREF (m_tuple);
}

// -----------------------------------------------------------------------------

PyKitTuple &PyKitTuple::operator = (const PyKitTuple &that)
{
  if (this != &that)
  {
    Py_XDECREF (m_tuple);
    m_tuple = that.m_tuple;
    Py_XINCREF (m_tuple);
  }

  return *this;
}

// -----------------------------------------------------------------------------

bool PyKitTuple::IsValid()
{
  return (m_tuple == nullptr);
}

// -----------------------------------------------------------------------------

PyKitTuple::operator PyObject *()
{
  return m_tuple;
}

// -----------------------------------------------------------------------------

Py_ssize_t PyKitTuple::Size()
{
  if (!m_tuple)
    return 0;

  else
    return PyTuple_Size (m_tuple);
}

// -----------------------------------------------------------------------------

PyKitObject PyKitTuple::operator[] (Py_ssize_t i)
{
  PyObject *obj = PyTuple_GetItem (m_tuple, i);
  return obj;
}

PyKitTuple PyKitTuple::Slice (Py_ssize_t low, Py_ssize_t high)
{
  return PyTuple_GetSlice (m_tuple, low, high);
}

// -----------------------------------------------------------------------------

bool PyKitTuple::SetItem (Py_ssize_t i, PyKitObject o)
{
  return PyTuple_SetItem (m_tuple, i, o) == 0;
}

// -----------------------------------------------------------------------------

bool PyKitTuple::Check (PyKitObject o)
{
  return PyTuple_Check (o);
}

bool PyKitTuple::CheckExact (PyKitObject o)
{
  return PyTuple_CheckExact (o);
}

// -----------------------------------------------------------------------------
