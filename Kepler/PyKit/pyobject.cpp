// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for bringing up and controlling a window
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "pyobject.h"

// -----------------------------------------------------------------------------

PyKitObject::PyKitObject (PyObject *obj) :
  m_obj (obj)
{
  Py_INCREF (obj);
}

PyKitObject::PyKitObject (const PyKitObject &that) :
  m_obj (that.m_obj)
{
  Py_XINCREF (m_obj);
}

PyKitObject::~PyKitObject()
{
  Py_XDECREF (m_obj);
}

// -----------------------------------------------------------------------------

PyKitObject &PyKitObject::operator= (const PyKitObject &that)
{
  if (this != &that)
  {
    Py_XDECREF (m_obj);
    m_obj = that.m_obj;
    Py_XINCREF (m_obj);
  }

  return *this;
}

// -----------------------------------------------------------------------------

PyKitObject::operator PyObject *() const
{
  return m_obj;
}

PyObject *PyKitObject::operator->()
{
  return m_obj;
}

PyObject &PyKitObject::operator*()
{
  return *m_obj;
}

// -----------------------------------------------------------------------------

bool PyKitObject::IsValid() const
{
  return m_obj != nullptr;
}

// -----------------------------------------------------------------------------

bool PyKitObject::HasAttr (const char *attr)
{
  return PyObject_HasAttrString (m_obj, attr) == 1;
}

bool PyKitObject::DelAttr (const char *attr)
{
  return PyObject_DelAttrString (m_obj, attr) != -1;
}

bool PyKitObject::SetAttr (PyKitObject obj, const char *attr)
{
  return PyObject_SetAttrString (m_obj, attr, obj) != -1;
}

PyKitObject PyKitObject::GetAttr (const char *attr)
{
  return PyObject_GetAttrString (m_obj, attr);
}

// -----------------------------------------------------------------------------

bool PyKitObject::HasAttrObj (PyKitObject attr)
{
  return PyObject_HasAttr (m_obj, attr) == 1;
}

bool PyKitObject::DelAttrObj (PyKitObject attr)
{
  return PyObject_DelAttr (m_obj, attr) != -1;
}

bool PyKitObject::SetAttrObj (PyKitObject obj, PyKitObject attr)
{
  return PyObject_SetAttr (m_obj, attr, obj) != -1;
}

PyKitObject PyKitObject::GetAttrObj (PyKitObject attr)
{
  return PyObject_GetAttr (m_obj, attr);
}

// -----------------------------------------------------------------------------

PyKitObject PyKitObject::ToString()
{
  return PyObject_Str (m_obj);
}

// -----------------------------------------------------------------------------

PyKitObject PyKitObject::Call (const char *format, ...)
{
  va_list v;
  va_start (v, format);

  PyKitObject tuple = Py_VaBuildValue (format, v);

  return PyObject_CallFunction (m_obj, "O", tuple);
}

PyKitObject PyKitObject::CallMethod (const char *name, const char *format, ...)
{
  va_list v;
  va_start (v, format);

  PyKitObject tuple = Py_VaBuildValue (format, v);

  return PyObject_CallMethod (m_obj, name, "O", tuple);
}

// -----------------------------------------------------------------------------

Py_hash_t PyKitObject::Hash()
{
  return PyObject_Hash (m_obj);
}

// -----------------------------------------------------------------------------

bool PyKitObject::TypeCheck (PyTypeObject *type)
{
  return PyObject_TypeCheck (m_obj, type);
}

// -----------------------------------------------------------------------------

Py_ssize_t PyKitObject::Length()
{
  return PyObject_Length (m_obj);
}

Py_ssize_t PyKitObject::Size()
{
  return PyObject_Size (m_obj);
}

// -----------------------------------------------------------------------------

PyKitObject PyKitObject::Dir()
{
  return PyObject_Dir (m_obj);
}

PyKitObject PyKitObject::Iter()
{
  return PyObject_GetIter (m_obj);
}

// -----------------------------------------------------------------------------
