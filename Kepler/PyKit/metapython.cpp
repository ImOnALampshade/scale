// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Meta-Programming stuff for Python binding
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "metapython.h"
#include <cmath>

// -----------------------------------------------------------------------------

namespace PyKit
{
  // ---------------------------------------------------------------------------

  PyKitObject ConvertToPyObject (void)
  {
    Py_RETURN_NONE;
  }

  PyKitObject ConvertToPyObject (bool val)
  {
    if (val)
      Py_RETURN_TRUE;

    else
      Py_RETURN_FALSE;
  }

  // ---------------------------------------------------------------------------

  PyKitObject ConvertToPyObject (const char *str)
  {
    Py_ssize_t len = strlen (str);
    return PyUnicode_FromStringAndSize (str, len);
  }

  PyKitObject ConvertToPyObject (const wchar_t *str)
  {
    Py_ssize_t len = wcslen (str);
    return PyUnicode_FromWideChar (str, len);
  }

  PyKitObject ConvertToPyObject (const String &str)
  {
    return PyUnicode_FromStringAndSize (str.CStr(), str.Length());
  }

  PyKitObject ConvertToPyObject (const std::string &str)
  {
    return PyUnicode_FromStringAndSize (str.c_str(), str.size());
  }

  PyKitObject ConvertToPyObject (const std::wstring &str)
  {
    return PyUnicode_FromWideChar (str.c_str(), str.size());
  }

  // ---------------------------------------------------------------------------

  PyKitObject ConvertToPyObject (char i)
  {
    return PyLong_FromLong (i);
  }

  PyKitObject ConvertToPyObject (short i)
  {
    return PyLong_FromLong (i);
  }

  PyKitObject ConvertToPyObject (int i)
  {
    return PyLong_FromLong (i);
  }

  PyKitObject ConvertToPyObject (long i)
  {
    return PyLong_FromLong (i);
  }

  PyKitObject ConvertToPyObject (long long i)
  {
    return PyLong_FromLongLong (i);
  }

  PyKitObject ConvertToPyObject (void *p)
  {
    return PyLong_FromVoidPtr (p);
  }

  // ---------------------------------------------------------------------------

  PyKitObject ConvertToPyObject (unsigned char i)
  {
    return PyLong_FromUnsignedLong (i);
  }

  PyKitObject ConvertToPyObject (unsigned short i)
  {
    return PyLong_FromUnsignedLong (i);
  }

  PyKitObject ConvertToPyObject (unsigned int i)
  {
    return PyLong_FromUnsignedLong (i);
  }

  PyKitObject ConvertToPyObject (unsigned long i)
  {
    return PyLong_FromUnsignedLong (i);
  }

  PyKitObject ConvertToPyObject (unsigned long long i)
  {
    return PyLong_FromUnsignedLongLong (i);
  }

  // ---------------------------------------------------------------------------

  PyKitObject ConvertToPyObject (float f)
  {
    return PyFloat_FromDouble (f);
  }

  PyKitObject ConvertToPyObject (double f)
  {
    return PyFloat_FromDouble (f);
  }

  // ---------------------------------------------------------------------------

  PyKitObject ConvertToPyObject (PyObject *o)
  {
    return o;
  }

  // ---------------------------------------------------------------------------

  bool ConvertFromPyObject (PyKitObject obj, bool &val)
  {
    int isTrue = PyObject_IsTrue (obj);

    if (isTrue == 1)
    {
      val = true;
      return true;
    }

    else if (isTrue == 0)
    {
      val = false;
      return true;
    }

    return false;
  }

  // ---------------------------------------------------------------------------

  bool ConvertFromPyObject (PyKitObject obj, String &str)
  {
    if (!PyUnicode_Check (obj))
      return false;

    char *cstr = PyUnicode_AsUTF8 (obj);
    str = String::Format ("%s", cstr);

    return true;
  }

  bool ConvertFromPyObject (PyKitObject obj, std::string &str)
  {
    if (!PyUnicode_Check (obj))
      return false;

    str = PyUnicode_AsUTF8 (obj);
    return true;
  }

  bool ConvertFromPyObject (PyKitObject obj, std::wstring &str)
  {
    if (!PyUnicode_Check (obj))
      return false;

    wchar_t *cstr = PyUnicode_AsWideCharString (obj, nullptr);
    str = cstr;
    PyMem_Free (cstr);
    return true;
  }

  // ---------------------------------------------------------------------------

  bool ConvertFromPyObject (PyKitObject obj, char &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = char (PyLong_AsLong (obj));
    return true;
  }

  bool ConvertFromPyObject (PyKitObject obj, short &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = short (PyLong_AsLong (obj));
    return true;
  }

  bool ConvertFromPyObject (PyKitObject obj, int &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = _PyLong_AsInt (obj);
    return true;
  }

  bool ConvertFromPyObject (PyKitObject obj, long &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = PyLong_AsLong (obj);
    return true;
  }

  bool ConvertFromPyObject (PyKitObject obj, long long &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = PyLong_AsLongLong (obj);
    return true;
  }

  bool ConvertFromPyObject (PyKitObject obj, void *&p)
  {
    if (!PyLong_Check (obj))
      return false;

    p = PyLong_AsVoidPtr (obj);
    return true;
  }

  // ---------------------------------------------------------------------------

  bool ConvertFromPyObject (PyKitObject obj, unsigned char &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = unsigned char (PyLong_AsUnsignedLong (obj));
    return false;
  }

  bool ConvertFromPyObject (PyKitObject obj, unsigned short &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = unsigned short (PyLong_AsUnsignedLong (obj));
    return false;
  }

  bool ConvertFromPyObject (PyKitObject obj, unsigned int &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = unsigned int (PyLong_AsUnsignedLong (obj));
    return false;
  }

  bool ConvertFromPyObject (PyKitObject obj, unsigned long &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = PyLong_AsUnsignedLong (obj);
    return false;
  }

  bool ConvertFromPyObject (PyKitObject obj, unsigned long long &i)
  {
    if (!PyLong_Check (obj))
      return false;

    i = PyLong_AsUnsignedLongLong (obj);
    return false;
  }

  // ---------------------------------------------------------------------------

  bool ConvertFromPyObject (PyKitObject obj, float &f)
  {
    if (PyFloat_Check (obj))
    {
      f = float (PyFloat_AsDouble (obj));
      return true;
    }

    if (PyLong_Check (obj))
    {
      f = float (PyLong_AsDouble (obj));
      return true;
    }

    return false;
  }

  bool ConvertFromPyObject (PyKitObject obj, double &f)
  {
    if (PyFloat_Check (obj))
    {
      f = PyFloat_AsDouble (obj);
      return true;
    }

    if (PyLong_Check (obj))
    {
      f = PyLong_AsDouble (obj);
      return true;
    }

    return false;
  }

  // ---------------------------------------------------------------------------

  void ConverToTuple_Helper (PyKitTuple &t, Py_ssize_t i)
  {
    // This function intentionally left blank
  }

  // ---------------------------------------------------------------------------

  ModuleDef::ModuleDef (const char *name, const char *doc)
  {
    static PyModuleDef BaseModule =
    {
      PyModuleDef_HEAD_INIT,
      nullptr,
      nullptr,
      -1,
      nullptr,
      nullptr,
      nullptr,
      nullptr,
      nullptr
    };

    m_module = BaseModule;
    m_module.m_name = name;
    m_module.m_doc  = doc;
  }

  void ModuleDef::AddMethod (PyMethodDef method)
  {
    m_methods.emplace_back (method);
  }

  // ---------------------------------------------------------------------------
}

// -----------------------------------------------------------------------------
