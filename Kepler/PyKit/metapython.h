// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Meta-Programming stuff for Python binding
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef META_PYTHON_H
#define META_PYTHON_H

#include "../String/string.h"
#include "../pyinclude.h"
#include "pymodule.h"
#include "pyobject.h"
#include "pytuple.h"
#include <string>
#include <vector>
#include <tuple>
#include <type_traits>

// -----------------------------------------------------------------------------

namespace PyKit
{
  // ---------------------------------------------------------------------------

  PyKitObject ConvertToPyObject (void);
  PyKitObject ConvertToPyObject (bool val);

  PyKitObject ConvertToPyObject (const char *str);
  PyKitObject ConvertToPyObject (const wchar_t *str);
  PyKitObject ConvertToPyObject (const String &str);
  PyKitObject ConvertToPyObject (const std::string &str);
  PyKitObject ConvertToPyObject (const std::wstring &str);

  PyKitObject ConvertToPyObject (char i);
  PyKitObject ConvertToPyObject (short i);
  PyKitObject ConvertToPyObject (int i);
  PyKitObject ConvertToPyObject (long i);
  PyKitObject ConvertToPyObject (long long i);
  PyKitObject ConvertToPyObject (void *p);

  PyKitObject ConvertToPyObject (unsigned char i);
  PyKitObject ConvertToPyObject (unsigned short i);
  PyKitObject ConvertToPyObject (unsigned int i);
  PyKitObject ConvertToPyObject (unsigned long i);
  PyKitObject ConvertToPyObject (unsigned long long i);

  PyKitObject ConvertToPyObject (float f);
  PyKitObject ConvertToPyObject (double f);

  PyKitObject ConvertToPyObject (PyObject *);

  // ---------------------------------------------------------------------------

  bool ConvertFromPyObject (PyKitObject obj, bool &val);

  bool ConvertFromPyObject (PyKitObject obj, String &str);
  bool ConvertFromPyObject (PyKitObject obj, std::string &str);
  bool ConvertFromPyObject (PyKitObject obj, std::wstring &str);

  bool ConvertFromPyObject (PyKitObject obj, char &i);
  bool ConvertFromPyObject (PyKitObject obj, short &i);
  bool ConvertFromPyObject (PyKitObject obj, int &i);
  bool ConvertFromPyObject (PyKitObject obj, long &i);
  bool ConvertFromPyObject (PyKitObject obj, long long &i);
  bool ConvertFromPyObject (PyKitObject obj, void *&p);

  bool ConvertFromPyObject (PyKitObject obj, unsigned char &i);
  bool ConvertFromPyObject (PyKitObject obj, unsigned short &i);
  bool ConvertFromPyObject (PyKitObject obj, unsigned int &i);
  bool ConvertFromPyObject (PyKitObject obj, unsigned long &i);
  bool ConvertFromPyObject (PyKitObject obj, unsigned long long &i);

  bool ConvertFromPyObject (PyKitObject obj, float &f);
  bool ConvertFromPyObject (PyKitObject obj, double &f);

  // ---------------------------------------------------------------------------

  void ConverToTuple_Helper (PyKitTuple &t, Py_ssize_t i);

  template<typename T, typename... Args>
  void ConverToTuple_Helper (PyKitTuple &t, Py_ssize_t i, const T &first, const Args &... args)
  {
    PyKitObject created = ConvertToPyObject (first);
    Py_INCREF (created);
    t.SetItem (i, created);
    ConverToTuple_Helper (t, i + 1, args...);
  }

  template<typename... Args>
  PyKitTuple ConverToTuple (const Args &... args)
  {
    PyKitTuple t (sizeof... (args));
    ConverToTuple_Helper (t, 0, args...);

    return t;
  }

  // ---------------------------------------------------------------------------

  template<typename... Args>
  PyKitObject CallFunction (PyKitObject obj, const Args &... args)
  {
    PyKitTuple param = ConverToTuple (args...);
    return PyObject_CallFunction (obj, "O", PyObject * (param));
  }

  template<typename... Args>
  PyKitObject CallMethod (PyKitObject obj, const char *method, const Args &... args)
  {
    PyKitTuple param = ConverToTuple (args...);
    return PyObject_CallMethod (obj, method, "O", (PyObject *) param);
  }

  // ---------------------------------------------------------------------------

  struct TupleToTuple_Continue
  {
    template<unsigned N, typename... TupleTypes>
    static bool Call (PyKitTuple pytuple, std::tuple<TupleTypes...> &tuple)
    {
      return TupleToTuple_Helper<N, TupleTypes...> (pytuple, tuple);
    }
  };

  struct TupleToTuple_Stop
  {
    template<unsigned N, typename... TupleTypes>
    static bool Call (PyKitTuple pytuple, std::tuple<TupleTypes...> &tuple)
    {
      return true;
    }
  };

  template<unsigned N, typename... TupleTypes>
  bool TupleToTuple_Helper (PyKitTuple pytuple, std::tuple<TupleTypes...> &tuple)
  {
    if (ConvertFromPyObject (pytuple[N], std::get<N> (tuple)) == false)
    {
      PyErr_Format (PyExc_TypeError, "Invalid argument type: %s", pytuple[N]->ob_type->tp_name);
      return false;
    }

    return std::conditional<N + 1 == sizeof... (TupleTypes), TupleToTuple_Stop,
           TupleToTuple_Continue>::type::
           Call<N + 1> (pytuple, tuple);
  }

  template<typename... TupleTypes>
  bool TupleToTuple (const char *name, PyKitTuple pytuple, std::tuple<TupleTypes...> &tuple)
  {
    if (sizeof... (TupleTypes) != pytuple.Size())
    {
      PyErr_Format (PyExc_TypeError, "function takes %i positional arguments but %i were given",
                    sizeof... (TupleTypes), pytuple.Size());
      return false;
    }

    return std::conditional<sizeof... (TupleTypes) == 0,
           TupleToTuple_Stop, TupleToTuple_Continue>::type::Call<0> (pytuple, tuple);
  }

  template <unsigned N>
  struct ApplyFunc
  {
    template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
    static Return ApplyTuple (Return (*fn) (ArgsF...), const std::tuple<ArgsT...> &t, Args... args)
    {
      return ApplyFunc<N - 1>::ApplyTuple (fn, t, std::get<N - 1> (t), args...);
    }
  };

  template<>
  struct ApplyFunc < 0 >
  {
    template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
    static Return ApplyTuple (Return (*fn) (ArgsF...), const std::tuple<ArgsT...> &t, Args... args)
    {
      return fn (args...);
    }
  };

  template<typename Return>
  struct FunctionCaller
  {
    template<typename... Args>
    static PyObject *CallFunction (Return (*fn) (Args...), const std::tuple<Args...> &t)
    {
      PyKitObject o = ConvertToPyObject (ApplyFunc<sizeof... (Args) >::ApplyTuple (fn, t));
      Py_INCREF (o);
      return o;
    }
  };

  template<>
  struct FunctionCaller<void>
  {
    template<typename... Args>
    static PyObject *CallFunction (void (*fn) (Args...), const std::tuple<Args...> &t)
    {
      ApplyFunc<sizeof... (Args) >::ApplyTuple (fn, t);
      Py_RETURN_NONE;
    }
  };

  template<typename Return, typename... Args>
  struct FunctionDef
  {
    template<Return (*fn) (Args...) >
    static PyObject *Definition (PyObject *self, PyObject *args)
    {
      assert (PyTuple_Check (args));

      Py_XINCREF (args);
      std::tuple<Args...> tuple;

      if (!TupleToTuple<Args...> ("", args, tuple))
        return nullptr;

      return FunctionCaller<Return>::CallFunction<Args...> (fn, tuple);
    }

    template<Return (*fn) (Args...) >
    static PyMethodDef Define (const char *name, const char *doc)
    {
      PyMethodDef def;
      def.ml_doc   = doc;
      def.ml_name  = name;
      def.ml_flags = METH_VARARGS;
      def.ml_meth  = &Definition < fn > ;
      return def;
    }
  };

  // ---------------------------------------------------------------------------

  struct ModuleDef
  {
    PyModuleDef               m_module;
    std::vector<PyMethodDef>  m_methods;

    ModuleDef (const char *name, const char *doc);

    void AddMethod (PyMethodDef method);
  };

  template<ModuleDef &module, void (*SetMethods) (ModuleDef &), void (*SetGlobals) (PyKitModule) >
  PyObject *ModuleInitFunction()
  {
    SetMethods (module);
    module.m_methods.emplace_back (PyMethodDef());
    module.m_module.m_methods = module.m_methods.data();

    PyObject *m = PyModule_Create (&module.m_module);
    assert (m);

    Py_INCREF (m);
    SetGlobals (m);

    return m;
  }

  // ---------------------------------------------------------------------------
}

// -----------------------------------------------------------------------------

#endif
