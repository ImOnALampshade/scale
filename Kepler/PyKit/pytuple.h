// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  An object that can hold any type of object that fits within a fixed size
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PY_TUPLE_H
#define PY_TUPLE_H

#include "pyobject.h"
#include "../pyinclude.h"

// -----------------------------------------------------------------------------

class PyKitTuple
{
private:
  PyObject *m_tuple;
public:
  PyKitTuple (PyObject *tuple = nullptr);
  PyKitTuple (Py_ssize_t size);
  PyKitTuple (const PyKitTuple &);
  ~PyKitTuple();

  PyKitTuple &operator= (const PyKitTuple &);

  bool IsValid();

  operator PyObject *();

  Py_ssize_t Size();

  PyKitObject operator[] (Py_ssize_t i);
  PyKitTuple Slice (Py_ssize_t low, Py_ssize_t high);

  bool SetItem (Py_ssize_t i, PyKitObject o);

  static bool Check (PyKitObject o);
  static bool CheckExact (PyKitObject o);
};

// -----------------------------------------------------------------------------

#endif
