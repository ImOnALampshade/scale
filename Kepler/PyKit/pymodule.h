// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  PyKit module object
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PYKIT_MODULE_H
#define PYKIT_MODULE_H

#include "pyobject.h"
#include "../pyinclude.h"

// -----------------------------------------------------------------------------

class PyKitModule
{
private:
  PyObject *m_module;
public:
  PyKitModule (PyObject *module = nullptr);
  PyKitModule (const PyKitModule &);
  ~PyKitModule();

  PyKitModule &operator= (const PyKitModule &);

  bool AddObject (const char *name, PyKitObject obj);
  bool AddIntConst (const char *name, long val);
  bool AddStrConst (const char *name, const char *val);

  operator PyObject *();
};

// -----------------------------------------------------------------------------

#endif
