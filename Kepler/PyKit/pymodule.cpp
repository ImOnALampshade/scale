// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  PyKit module object
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "pymodule.h"

// -----------------------------------------------------------------------------

PyKitModule::PyKitModule (PyObject *module) :
  m_module (module)
{
}

PyKitModule::PyKitModule (const PyKitModule &that) :
  m_module (that.m_module)
{
  Py_XINCREF (m_module);
}

PyKitModule::~PyKitModule()
{
  Py_XDECREF (m_module);
}

// -----------------------------------------------------------------------------

PyKitModule &PyKitModule::operator = (const PyKitModule &that)
{
  if (this != &that)
  {
    Py_XDECREF (m_module);
    m_module = that.m_module;
    Py_XINCREF (m_module);
  }

  return *this;
}

// -----------------------------------------------------------------------------

bool PyKitModule::AddObject (const char *name, PyKitObject obj)
{
  return PyModule_AddObject (m_module, name, obj) == 0;
}

bool PyKitModule::AddIntConst (const char *name, long val)
{
  return PyModule_AddIntConstant (m_module, name, val) == 0;
}

bool PyKitModule::AddStrConst (const char *name, const char *val)
{
  return PyModule_AddStringConstant (m_module, name, val) == 0;
}

// -----------------------------------------------------------------------------

PyKitModule::operator PyObject *()
{
  return m_module;
}

// -----------------------------------------------------------------------------
