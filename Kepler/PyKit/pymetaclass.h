// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Meta-Programming stuff for Python binding (now with class!)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PYKIT_META_CLASS_H
#define PYKIT_META_CLASS_H

#include "../HashTable/hashtable.h"
#include "../HashFunctions/hashfunctions.h"
#include "../pyinclude.h"
#include "metapython.h"
#include <vector>

// -----------------------------------------------------------------------------

namespace PyKit
{
  // ---------------------------------------------------------------------------

  template<typename T>
  struct PyStruct
  {
    PyObject_HEAD;
    T object;

    PyStruct() : object() {}
    PyStruct (const T &obj) : object (obj) {}

    struct Property
    {
      PyObject * (*GetProperty) (PyStruct<T> *);
      bool (*SetProperty) (PyStruct<T> *, PyObject *);
    };

    template<typename U>
    struct PropertyDef
    {
      template<const U & (T::*getter) () const >
      static PyObject *GetProperty (PyStruct<T> *self)
      {
        const U &val = (self->object.*getter) ();
        PyKitObject obj = ConvertToPyObject (val);
        Py_INCREF (obj);
        return obj;
      }

      template<U (T::*getter) () const >
      static PyObject *GetProperty (PyStruct<T> *self)
      {
        U val = (self->object.*getter) ();
        PyKitObject obj = ConvertToPyObject (val);
        Py_INCREF (obj);
        return obj;
      }

      template<void (T::*setter) (const U &) >
      static bool SetProperty (PyStruct<T> *self, PyObject *value)
      {
        U val;

        Py_INCREF (value);

        if (!ConvertFromPyObject (value, val))
          return false;

        (self->object.*setter) (val);
        return true;
      }

      template<void (T::*setter) (U) >
      static bool SetProperty (PyStruct<T> *self, PyObject *value)
      {
        U val;

        Py_INCREF (value);

        if (!ConvertFromPyObject (value, val))
          return false;

        (self->object.*setter) (val);
        return true;
      }

      template<const U & (T::*getter) () const, void (T::*setter) (const U &) >
      static void CreateProperty (String name)
      {
        Property p;
        p.GetProperty = GetProperty < getter > ;
        p.SetProperty = SetProperty < setter > ;
        Properties().Insert (name, p);
      }

      template<U (T::*getter) () const, void (T::*setter) (U) >
      static void CreateProperty (String name)
      {
        Property p;
        p.GetProperty = GetProperty < getter >;
        p.SetProperty = SetProperty < setter >;
        Properties().Insert (name, p);
      }
    };

    static PyTypeObject             Type;
    static std::vector<PyMethodDef> Methods;
    static std::vector<PyMemberDef> Members;
    static PyNumberMethods          NumberMethods;
    static PySequenceMethods        SequenceMethods;
    static const char              *TypeName;
    static const char              *FullTypeName;
    static const char              *DocString;
    static PyCFunction              Constructor;
    static bool                     TypeReady;

    static HashTable<String, Property> &Properties()
    {
      using Utilities::Hash::SuperFast;
      static HashTable<String, Property> table (SuperFast);
      return table;
    }

    static PyObject *New (PyTypeObject *subtype, PyObject *args, PyObject *kwds)
    {
      return Constructor (nullptr, args);
    }

    static void Delete (PyObject *self_)
    {
      PyStruct<T> *self = reinterpret_cast<PyStruct<T> *> (self_);

      // Call the destructor
      self->~PyStruct<T>();
      self->ob_base.ob_type->tp_free (self);
    }

    static PyObject *GetAttr (PyObject *self_, char *name)
    {
      PyStruct<T> *self = reinterpret_cast<PyStruct<T> *> (self_);

      auto found = Properties().Find (name);

      if (found == Properties().end())
        return nullptr;

      return found->value.GetProperty (self);
    }

    static int SetAttr (PyObject *self_, char *name, PyObject *property)
    {
      PyStruct<T> *self = reinterpret_cast<PyStruct<T> *> (self_);

      auto found = Properties().Find (name);

      if (found == Properties().end())
        return -1;

      found->value.SetProperty (self, property);
      return 0;
    }

    static PyObject *GetAttrO (PyObject *self, PyObject *name)
    {
      char *c_name = PyUnicode_AsUTF8 (name);

      if (PyObject *obj = GetAttr (self, c_name))
        return obj;

      else
        return PyObject_GenericGetAttr (self, name);
    }

    static int SetAttrO (PyObject *self, PyObject *name, PyObject *val)
    {
      char *c_name = PyUnicode_AsUTF8 (name);
      int result = SetAttr (self, c_name, val);

      if (result == -1)
        return PyObject_GenericSetAttr (self, name, val);

      else
        return 0;
    }

    template <unsigned N>
    struct ApplyFunc
    {
      template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
      static Return ApplyTuple (T *ptr, Return (T::*fn) (ArgsF...), const std::tuple<ArgsT...> &t,
                                Args... args)
      {
        return ApplyFunc<N - 1>::ApplyTuple (ptr, fn, t, std::get<N - 1> (t), args...);
      }
    };

    template<>
    struct ApplyFunc < 0 >
    {
      template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
      static Return ApplyTuple (T *ptr, Return (T::*fn) (ArgsF...), const std::tuple<ArgsT...> &t,
                                Args... args)
      {
        T &ref = *ptr;
        return (ref.*fn) (args...);
      }
    };

    template<typename Return>
    struct FunctionCaller
    {
      template<typename... Args>
      static PyObject *CallFunction (T *ptr, Return (T::*fn) (Args...), const std::tuple<Args...> &t)
      {
        PyKitObject o = ConvertToPyObject (ApplyFunc<sizeof... (Args) >::ApplyTuple (ptr, fn, t));
        Py_INCREF (o);
        return o;
      }
    };

    template<>
    struct FunctionCaller < void >
    {
      template<typename... Args>
      static PyObject *CallFunction (T *ptr, void (T::*fn) (Args...), const std::tuple<Args...> &t)
      {
        ApplyFunc<sizeof... (Args) >::ApplyTuple (ptr, fn, t);
        Py_RETURN_NONE;
      }
    };

    template<typename Return, typename... Args>
    struct FunctionDef
    {
      template<Return (T::*fn) (Args...) >
      static PyObject *Definition (PyObject *self_, PyObject *args)
      {
        PyStruct<T> *self = reinterpret_cast<PyStruct<T> *> (self_);
        assert (PyTuple_Check (args));

        Py_XINCREF (args);
        std::tuple<Args...> tuple;

        if (!TupleToTuple<Args...> ("", args, tuple))
          return nullptr;

        return FunctionCaller<Return>::CallFunction (&self->object, fn, tuple);
      }

      template<Return (T::*fn) (Args...) >
      static PyMethodDef Define (const char *name, const char *doc)
      {
        PyMethodDef def;
        def.ml_name = name;
        def.ml_meth = Definition < fn > ;
        def.ml_flags = METH_VARARGS;
        def.ml_doc = doc;
        return def;
      }

    };

    static PyObject *CreateType ()
    {
      Methods.push_back (PyMethodDef());
      Members.push_back (PyMemberDef());

      Type.ob_base.ob_base.ob_refcnt = 1;
      Type.tp_methods     = Methods.data();
      Type.tp_members     = Members.data();
      Type.tp_as_number   = &NumberMethods;
      Type.tp_as_sequence = &SequenceMethods;
      Type.tp_name        = FullTypeName;
      Type.tp_doc         = DocString;

      if (!Type.tp_new)
        Type.tp_new         = New;

      if (!Type.tp_dealloc)
        Type.tp_dealloc     = Delete;

      Type.tp_basicsize   = sizeof PyStruct<T>;

      Type.tp_getattr = GetAttr;
      Type.tp_setattr = SetAttr;
      Type.tp_getattro = GetAttrO;
      Type.tp_setattro = SetAttrO;

      int result = PyType_Ready (&Type);
      assert (result == 0);

      TypeReady = true;
      return reinterpret_cast<PyObject *> (&Type);
    }

    static PyObject *Create (const T &val)
    {
      PyStruct *obj = PyObject_New (PyStruct, &Type);
      new (obj) PyStruct (val);
      return reinterpret_cast<PyObject *> (obj);
    }

    static bool TypeCheck (PyObject *obj)
    {
      return PyObject_TypeCheck (obj, &Type);
    }

    static bool FromPyObject (PyObject *obj, T &val)
    {
      if (!TypeCheck (obj))
        return false;

      val = reinterpret_cast<PyStruct *> (obj)->object;
      return true;
    }

    template<typename MemberType, MemberType T::*member>
    static void AddMember (int type, int flags, char *name, char *doc)
    {
      PyMemberDef def;
      def.name   = name;
      def.type   = type;
      def.flags  = flags;
      def.offset = offsetof (PyStruct, object) + offsetof (T, *member);
      def.doc    = doc;
      Members.push_back (def);
    }

    template<typename Op>
    static PyObject *BinaryOperator (PyObject *lhs_, PyObject *rhs_)
    {
      T &lhs = reinterpret_cast<PyStruct *> (lhs_)->object,
         &rhs = reinterpret_cast<PyStruct *> (rhs_)->object;
      Op o;
      return Create (o (lhs, rhs));
    }

    template<typename Op>
    static PyObject *UnaryOperator (PyObject *self)
    {
      T &obj = reinterpret_cast<PyStruct *> (self)->object;

      Op o;
      return Create (o (obj));
    }
  };

  template<typename T> PyTypeObject             PyStruct<T>::Type;
  template<typename T> std::vector<PyMethodDef> PyStruct<T>::Methods;
  template<typename T> std::vector<PyMemberDef> PyStruct<T>::Members;
  template<typename T> PyNumberMethods          PyStruct<T>::NumberMethods;
  template<typename T> PySequenceMethods        PyStruct<T>::SequenceMethods;
  template<typename T> const char              *PyStruct<T>::TypeName;
  template<typename T> const char              *PyStruct<T>::FullTypeName;
  template<typename T> const char              *PyStruct<T>::DocString;
  template<typename T> PyCFunction              PyStruct<T>::Constructor;
  template<typename T> bool                     PyStruct<T>::TypeReady = false;

// ---------------------------------------------------------------------------

  template<typename T>
  PyObject *ConvertToPyObject (const T &val)
  {
    assert (PyStruct<T>::TypeReady);

    PyStruct<T> *obj = PyObject_New (PyStruct<T>, &PyStruct<T>::Type);
    new (obj) PyStruct<T> (val);

    return reinterpret_cast<PyObject *> (obj);
  }

// ---------------------------------------------------------------------------

  template<typename T>
  bool ConvertFromPyObject (PyKitObject obj, T &val)
  {
    assert (PyStruct<T>::TypeReady);

    if (!PyObject_TypeCheck (obj, &PyStruct<T>::Type))
      return false;

    PyObject *self_ = obj;
    PyStruct<T> *self = reinterpret_cast<PyStruct<T> *> (self_);

    val = self->object;

    return true;
  }

// ---------------------------------------------------------------------------

  template<typename T>
  struct PyPointer
  {
    PyObject_HEAD;
    T *object;

    PyPointer() : object (nullptr) {}
    PyPointer (T *obj) : object (obj) {}

    struct Property
    {
      PyObject * (*GetProperty) (PyPointer<T> *);
      bool (*SetProperty) (PyPointer<T> *, PyObject *);
    };

    template<typename U>
    struct PropertyDef
    {
      template<const U & (T::*getter) () const >
      static PyObject *GetProperty (PyPointer<T> *self)
      {
        const U &val = (self->object->*getter) ();
        PyKitObject obj = ConvertToPyObject (val);
        Py_INCREF (obj);
        return obj;
      }

      template<U (T::*getter) () const >
      static PyObject *GetProperty (PyPointer<T> *self)
      {
        U val = (self->object->*getter) ();
        PyKitObject obj = ConvertToPyObject (val);
        Py_INCREF (obj);
        return obj;
      }

      template<void (T::*setter) (const U &) >
      static bool SetProperty (PyPointer<T> *self, PyObject *value)
      {
        U val;

        Py_INCREF (value);

        if (!ConvertFromPyObject (value, val))
          return false;

        (self->object->*setter) (val);
        return true;
      }

      template<void (T::*setter) (U) >
      static bool SetProperty (PyPointer<T> *self, PyObject *value)
      {
        U val;

        Py_INCREF (value);

        if (!ConvertFromPyObject (value, val))
          return false;

        (self->object->*setter) (val);
        return true;
      }

      template<const U & (T::*getter) () const, void (T::*setter) (const U &) >
      static void CreateProperty (String name)
      {
        Property p;
        p.GetProperty = GetProperty < getter >;
        p.SetProperty = SetProperty < setter >;
        Properties().Insert (name, p);
      }

      template<U (T::*getter) () const, void (T::*setter) (U) >
      static void CreateProperty (String name)
      {
        Property p;
        p.GetProperty = GetProperty < getter >;
        p.SetProperty = SetProperty < setter >;
        Properties().Insert (name, p);
      }
    };

    static PyTypeObject             Type;
    static std::vector<PyMethodDef> Methods;
    static std::vector<PyMemberDef> Members;
    static PyNumberMethods          NumberMethods;
    static PySequenceMethods        SequenceMethods;
    static const char              *TypeName;
    static const char              *FullTypeName;
    static const char              *DocString;
    static bool                     TypeReady;

    static HashTable<String, Property> &Properties()
    {
      using Utilities::Hash::SuperFast;
      static HashTable<String, Property> table (SuperFast);
      return table;
    }

    static void Delete (PyObject *self_)
    {
      PyPointer<T> *self = reinterpret_cast<PyPointer<T> *> (self_);

      // Call the destructor
      self->~PyPointer<T>();
      self->ob_base.ob_type->tp_free (self);
    }

    static PyObject *GetAttr (PyObject *self_, char *name)
    {
      PyPointer<T> *self = reinterpret_cast<PyPointer<T> *> (self_);

      auto found = Properties().Find (name);

      if (found == Properties().end())
        return nullptr;

      return found->value.GetProperty (self);
    }

    static int SetAttr (PyObject *self_, char *name, PyObject *property)
    {
      PyPointer<T> *self = reinterpret_cast<PyPointer<T> *> (self_);

      auto found = Properties().Find (name);

      if (found == Properties().end())
        return -1;

      found->value.SetProperty (self, property);
      return 0;
    }

    static PyObject *GetAttrO (PyObject *self, PyObject *name)
    {
      char *c_name = PyUnicode_AsUTF8 (name);

      if (PyObject *obj = GetAttr (self, c_name))
        return obj;

      else
        return PyObject_GenericGetAttr (self, name);
    }

    static int SetAttrO (PyObject *self, PyObject *name, PyObject *val)
    {
      char *c_name = PyUnicode_AsUTF8 (name);

      int result = SetAttr (self, c_name, val);

      if (result == -1)
        return PyObject_GenericSetAttr (self, name, val);

      else
        return result;
    }

    template <unsigned N>
    struct ApplyFunc
    {
      template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
      static Return ApplyTuple (T *ptr, Return (T::*fn) (ArgsF...), const std::tuple<ArgsT...> &t,
                                Args... args)
      {
        return ApplyFunc<N - 1>::ApplyTuple (ptr, fn, t, std::get<N - 1> (t), args...);
      }
    };

    template<>
    struct ApplyFunc < 0 >
    {
      template <typename Return, typename... ArgsF, typename... ArgsT, typename... Args>
      static Return ApplyTuple (T *ptr, Return (T::*fn) (ArgsF...), const std::tuple<ArgsT...> &t,
                                Args... args)
      {
        return (ptr->*fn) (args...);
      }
    };

    template<typename Return>
    struct FunctionCaller
    {
      template<typename... Args>
      static PyObject *CallFunction (T *ptr, Return (T::*fn) (Args...), const std::tuple<Args...> &t)
      {
        PyKitObject o = ConvertToPyObject (ApplyFunc<sizeof... (Args) >::ApplyTuple (ptr, fn, t));
        Py_INCREF (o);
        return o;
      }
    };

    template<>
    struct FunctionCaller < void >
    {
      template<typename... Args>
      static PyObject *CallFunction (T *ptr, void (T::*fn) (Args...), const std::tuple<Args...> &t)
      {
        ApplyFunc<sizeof... (Args) >::ApplyTuple (ptr, fn, t);
        Py_RETURN_NONE;
      }
    };

    template<typename Return, typename... Args>
    struct FunctionDef
    {
      template<Return (T::*fn) (Args...) >
      static PyObject *Definition (PyObject *self_, PyObject *args)
      {
        PyPointer<T> *self = reinterpret_cast<PyPointer<T> *> (self_);
        assert (PyTuple_Check (args));

        if (!self->object)
        {
          PyErr_Format (PyExc_RuntimeError, "NULL target for object of type %s", FullTypeName);
          return nullptr;
        }

        Py_XINCREF (args);
        std::tuple<Args...> tuple;

        if (!TupleToTuple<Args...> ("", args, tuple))
          return nullptr;

        return FunctionCaller<Return>::CallFunction (self->object, fn, tuple);
      }

      template<Return (T::*fn) (Args...) >
      static PyMethodDef Define (const char *name, const char *doc)
      {
        PyMethodDef def;
        def.ml_name = name;
        def.ml_meth = Definition < fn >;
        def.ml_flags = METH_VARARGS;
        def.ml_doc = doc;
        return def;
      }
    };

    static PyObject *CreateType ()
    {
      PyMethodDef IsValid = {
        "is_valid",
        [] (PyObject *self_, PyObject *args) {
          PyPointer<T> *self = reinterpret_cast<PyPointer<T> *> (self_);

          if (self->object != nullptr)
            Py_RETURN_TRUE;

          else
            Py_RETURN_FALSE;
        },
        METH_NOARGS,
        "Checks if this object is valid"
      };

      Methods.push_back (IsValid);
      Methods.push_back (PyMethodDef());
      Members.push_back (PyMemberDef());

      Type.ob_base.ob_base.ob_refcnt = 1;
      Type.tp_methods = Methods.data();
      Type.tp_members = Members.data();
      Type.tp_as_number = &NumberMethods;
      Type.tp_as_sequence = &SequenceMethods;
      Type.tp_name = FullTypeName;
      Type.tp_doc = DocString;
      Type.tp_new = nullptr; // Cannot instantiate new pointers
      Type.tp_dealloc = Delete;
      Type.tp_basicsize = sizeof PyPointer<T>;
      Type.tp_itemsize = 0;

      Type.tp_getattr = GetAttr;
      Type.tp_setattr = SetAttr;
      Type.tp_getattro = GetAttrO;
      Type.tp_setattro = SetAttrO;

      int result = PyType_Ready (&Type);
      assert (result == 0);

      TypeReady = true;
      return reinterpret_cast<PyObject *> (&Type);
    }

    static PyObject *Create (T *val)
    {
      PyPointer *obj = PyObject_New (PyPointer, &Type);
      new (obj) PyPointer (val);
      return reinterpret_cast<PyObject *> (obj);
    }

    static bool TypeCheck (PyObject *obj)
    {
      return PyObject_TypeCheck (obj, &Type);
    }
  };

  template<typename T> PyTypeObject             PyPointer<T>::Type;
  template<typename T> std::vector<PyMethodDef> PyPointer<T>::Methods;
  template<typename T> std::vector<PyMemberDef> PyPointer<T>::Members;
  template<typename T> PyNumberMethods          PyPointer<T>::NumberMethods;
  template<typename T> PySequenceMethods        PyPointer<T>::SequenceMethods;
  template<typename T> const char              *PyPointer<T>::TypeName;
  template<typename T> const char              *PyPointer<T>::FullTypeName;
  template<typename T> const char              *PyPointer<T>::DocString;
  template<typename T> bool                     PyPointer<T>::TypeReady = false;


// ---------------------------------------------------------------------------
}

// -----------------------------------------------------------------------------

#endif
