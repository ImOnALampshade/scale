// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Functions for storing Python module definitions before PyInit
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "PyModuleDef.h"

PyModuleDef PyKitModuleDef::CreatePythonModuleDefinition()
{
  PyModuleDef ret = { PyModuleDef_HEAD_INIT };
  ret.m_size = -1;
  ret.m_name = ModuleName.CStr();
  ret.m_doc = DocumentString.CStr();
  ret.m_methods = Methods.data();
  
  ret.m_reload = NULL;
  ret.m_traverse = NULL;
  ret.m_clear = NULL;
  ret.m_free = NULL;

  return ret;
}

void PyKitModuleDef::AddMethod(const PyMethodDef &methodDef)
{
  Methods.insert(Methods.end() - 2, methodDef);
}

const std::vector<PyMethodDef> & PyKitModuleDef::GetMethods()
{
  return Methods;
}

PyKitModuleDef::PyKitModuleDef()
{
  PyMethodDef blankDef;
  Methods.push_back(blankDef);
}

