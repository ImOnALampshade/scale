// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for bringing up and controlling a window
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PYKIT_PY_OBJECT_H
#define PYKIT_PY_OBJECT_H

#include "../pyinclude.h"

// -----------------------------------------------------------------------------

class PyKitObject
{
private:
  PyObject *m_obj;

public:
  PyKitObject (PyObject *obj = nullptr);
  PyKitObject (const PyKitObject &that);
  ~PyKitObject();

  PyKitObject &operator= (const PyKitObject &that);

  operator PyObject *() const;
  PyObject *operator->();
  PyObject &operator*();

  bool IsValid() const;

  bool HasAttr (const char *attr);
  bool DelAttr (const char *attr);
  bool SetAttr (PyKitObject obj,const char *attr);
  PyKitObject GetAttr (const char *attr);

  bool HasAttrObj (PyKitObject attr);
  bool DelAttrObj (PyKitObject attr);
  bool SetAttrObj (PyKitObject obj, PyKitObject attr);
  PyKitObject GetAttrObj (PyKitObject attr);

  PyKitObject ToString();

  PyKitObject Call (const char *format, ...);
  PyKitObject CallMethod (const char *name, const char *format, ...);

  Py_hash_t Hash();

  bool TypeCheck (PyTypeObject *type);

  Py_ssize_t Length();
  Py_ssize_t Size();

  PyKitObject Dir();
  PyKitObject Iter();
};

// -----------------------------------------------------------------------------

#endif
