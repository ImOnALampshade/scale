// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Functions for storing Python module definitions before PyInit
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PYKIT_MODULE_DEF_H_
#define PYKIT_MODULE_DEF_H

#include "../pyinclude.h"
#include <vector>
#include "../String/string.h"

class PyKitModuleDef
{
private:
  std::vector<PyMethodDef> Methods;
public:
  PyKitModuleDef();

  PyModuleDef CreatePythonModuleDefinition();
  String ModuleName;
  String DocumentString;

  void AddMethod (const PyMethodDef &methodDef);
  const std::vector<PyMethodDef> &GetMethods();
};

#endif