// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  Object Allocator Test Cases
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include <catch.hpp>
#include <cstdlib>
#include <cstdio>
#include <ctime>



#include "../ObjectAllocator/objectallocator.h"

TEST_CASE("Object Allocator: Allocation", "[Allocate]")
{
  
  srand(static_cast<unsigned int>(time(nullptr)));

  SECTION("Allocate Object")
  {

    ObjectAllocator::Config config;
    config.ObjectSize = 61;
    config.ObjectsPerPage = 12;
    config.DebugOn = true;
    config.PadBytes = 3;
    config.Alignment = 32;

    ObjectAllocator objAllocator(config);

    void* allocatedObject = nullptr;
    void* noCharacter = nullptr;

    std::vector<void*> objectsToFree;
    objectsToFree.reserve( 32 );

    for (int i = 0; i < 25; i += 1)
    {
      allocatedObject = objAllocator.OA_ALLOCATE();
      CHECK( allocatedObject != noCharacter );
      objectsToFree.push_back( allocatedObject );

    }

    for(void* it : objectsToFree ) // c++ 11 loop.
    {
      objAllocator.OA_DEALLOCATE(it); //this makes loop easy.
    }

    ObjectAllocator::Stats const& stats = objAllocator.Statistics();

    printf("Config:\n");
    printf("Alignment: %d\n", config.Alignment);
    printf("ObjectSize: %d\n", config.ObjectSize);
    printf("ObjectsPerPage: %d\n\n", config.ObjectsPerPage);


    printf("Stats:\n");
    printf("Allocations: %d\n", stats.Allocations);
    printf("Deallocations: %d\n", stats.Deallocations);
    printf("FreeObjects: %d\n", stats.FreeObjects);
    printf("MostObjects: %d\n", stats.MostObjects);
    printf("ObjectsInUse: %d\n", stats.ObjectsInUse);
    printf("ObjectTotalSize: %d\n", stats.ObjectTotalSize);
    printf("PagesInUse: %d\n", stats.PagesInUse);
    printf("PageSize: %d\n", stats.PageSize);
    printf("Kilobytes: %f\n", (stats.PagesInUse * stats.PageSize) / 1000.0f);
  }

  SECTION("Test Pad-Byte Corruption")
  {
    bool caughtPadByteWrite = false;

    ObjectAllocator::Config config;
    config.ObjectSize = 61;
    config.ObjectsPerPage = 12;
    config.DebugOn = true;
    config.PadBytes = 8; // pad bytes need to be at least four or more for this test.
    config.Alignment = 5;
    ObjectAllocator objAllocator(config);
    void* allocatedObject = nullptr;
    allocatedObject = objAllocator.OA_ALLOCATE();
    

    //write into pad bytes!
    int* object = reinterpret_cast<int*>(allocatedObject);
    object -= 1; //Offset pointer to write out of bounds.
    *object = 4; //Write some stuff into the pad bytes.

    try
    {
      objAllocator.OA_DEALLOCATE(allocatedObject);
    }
    catch (std::exception const & e)
    {
      std::printf("Exception:\n%s\n", e.what());
      caughtPadByteWrite = true;
    }

    CHECK(caughtPadByteWrite);

    //this will throw an exception because the object is still technically not freed...
    //but catch catches it and does the next test.
  }

  SECTION("Test Double Free")
  {
    bool caughtDoubleFree = false;

    ObjectAllocator::Config config;
    config.ObjectSize = 61;
    config.ObjectsPerPage = 12;
    config.DebugOn = true;
    config.PadBytes = 8;
    config.Alignment = 5;
    ObjectAllocator objAllocator(config);
    void* allocatedObject = nullptr;

    allocatedObject = objAllocator.OA_ALLOCATE();

    try
    {
      std::printf("First Deallocate.\n");
      objAllocator.OA_DEALLOCATE(allocatedObject);
    }
    catch (std::exception const & e)
    {
      caughtDoubleFree = true;
      std::printf("Exception:\n%s\n", e.what());
    }

    CHECK(!caughtDoubleFree);

    try
    {
      std::printf("Second Deallocate.\n");
      objAllocator.OA_DEALLOCATE(allocatedObject);
    }
    catch (std::exception const & e)
    {
      caughtDoubleFree = true;
      std::printf("Exception:\n%s\n", e.what());
    }

    CHECK(caughtDoubleFree);
  }

}