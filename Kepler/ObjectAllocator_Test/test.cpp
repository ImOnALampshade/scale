//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#define CATCH_CONFIG_RUNNER
#include <catch.hpp>

int main (int argc, char **const argv)
{
  return Catch::Session().run (argc, argv);
}
