// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Kepler Texture Object header definition
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KTO_HEADER_H
#define KTO_HEADER_H

#include <cstdint>
#include <memory>

// -----------------------------------------------------------------------------

namespace Kto
{
#pragma pack(push,1)
  struct Header
  {
    uint8_t  mipmaps; // The number of mipmaps in the image
    uint32_t width;   // The width of the full image
    uint32_t height;  // The height of the full image
  };
#pragma pack(pop)
}

// -----------------------------------------------------------------------------

#endif
