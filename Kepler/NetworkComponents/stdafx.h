//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

// SDL networking hooks
#include "sdl_net.h"

// STL
#include <vector>
#include <map>
#include <set>
#include <algorithm> // std::sort
#include <list>

// Types
typedef Uint16 port_t;
typedef Uint16 flags_t;
typedef Uint32 ip4_t;
typedef Uint64 ip6_t;
typedef Uint32 echoid_t;
typedef Uint64 milisecond;
typedef Uint32 ConnectionID;

// Forward declarations
class EchoComponent;
class NetworkSystemComponent;

// Component.
#include "../KeplerCore/component.h"

// Protocol
#include "Packet.h"
#include "Envelope.h"
#include "EchoPacket.h"
#include "NetMessage.h"

// Networking architecture
#include "Socket.h"
#include "Endpoint.h"
#include "Link.h"
#include "Listener.h"

// Networking components
#include "NetworkSystem.h"
#include "Echo.h"

// Assert
#include <assert.h>

// Logging
#include "../Logger/logger.h"
#define LOGGING 1
#if LOGGING
#define NetLog(...) wprintf(__VA_ARGS__)
#else
#define NetLog(...)
#endif