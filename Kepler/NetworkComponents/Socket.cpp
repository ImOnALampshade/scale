// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  A wrapper object around a UDP Socket.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"

const int MAX_PACKET_SIZE = 65535;

namespace Network
{

  void SocketObject::Update()
  {
    int hasPacket;
    do {
      hasPacket = SDLNet_UDP_Recv(m_osSocket, m_recvBuffer);
      if (hasPacket)
      {
        Endpoint checkEndpoint(m_recvBuffer);
        Envelope *packet = Envelope::ReadFromBuffer(m_recvBuffer);
        dump_packet(packet->GetBuffer()->data, packet->GetBufferSize());
        if (m_knownEndpoints.find(checkEndpoint.GetStreamID()) != m_knownEndpoints.end())
        {
          m_knownEndpoints[checkEndpoint.GetStreamID()]->push_back(packet);
        }
        else if (packet->GetHeader()->flags & Envelope::Header::FLAG_SYN)
        {
          m_syns.push_back(std::make_pair(checkEndpoint, packet));
        }
        else
        {
          NetLog(L"Threw out a packet.\n");
          delete(packet);
        }
        NetLog(L"Got a packet\n");
      }
    } while (hasPacket);
  }

  SocketObject::SocketObject(uint16_t port /*= 0*/)
  {
    m_osSocket = SDLNet_UDP_Open(port);
    m_recvBuffer = SDLNet_AllocPacket(MAX_PACKET_SIZE);
  }

  SocketObject::~SocketObject()
  {
    SDLNet_UDP_Close(m_osSocket);
    SDLNet_FreePacket(m_recvBuffer);
    for (auto pair : m_knownEndpoints)
    {
      delete(pair.second);
    }
    m_knownEndpoints.clear();
  }

  void SocketObject::SendTo(const Endpoint &endpoint, Envelope *data)
  {
    data->GetBuffer()->address = endpoint.GetAddress();
    data->GetBuffer()->channel = -1;
    data->GetBuffer()->len = data->GetBufferSize();
    data->GetHeader()->ID = endpoint.GetStreamID();
    
    NetLog(L"Sent:\n");
    dump_packet(data->GetBuffer()->data, data->GetBuffer()->len);
    SDLNet_UDP_Send(m_osSocket, -1, data->GetBuffer());
  }

  void SocketObject::AddEndpoint(ConnectionID id, const Endpoint *endpoint)
  {
    if (!HasConnection(id))
    {
      m_knownEndpoints[id] = new std::vector < Envelope* >();
      NetLog(L"New connection established.");
    }
  }

  bool SocketObject::HasConnection(ConnectionID id)
  {
    return (m_knownEndpoints.find(id) != m_knownEndpoints.end());
  }

  void SocketObject::ForgetConnection(ConnectionID id)
  {
    auto *vec = m_knownEndpoints[id];
    if (vec)
    {
      while (vec->size() > 0)
      {
        Envelope *env = vec->back();
        delete(env);
        vec->pop_back();
      }
      delete(vec);
      NetLog(L"Connection lost.");
    }
    m_knownEndpoints.erase(id);
  }

  Envelope* SocketObject::PeekSyn(/* out */ Endpoint *sender)
  {
    if (m_syns.size() > 0)
    {
      if (sender)
        *sender = m_syns.back().first;
      return m_syns.back().second;
    }
    else
      return nullptr;
  }

  Envelope* SocketObject::RecvSyn(/* out */ Endpoint *sender)
  {
    Envelope *ret = PeekSyn(sender);
    if (ret)
      m_syns.pop_back();
    return ret;
  }

  Envelope* SocketObject::RecvFrom(ConnectionID id)
  {
    if (HasConnection(id) && !m_knownEndpoints[id]->empty())
    {
      Envelope *ret = m_knownEndpoints[id]->back();
      m_knownEndpoints[id]->pop_back();
      return ret;
    }
    else return nullptr;
  }

  void SocketObject::SetPort(port_t port)
  {
    SDLNet_UDP_Close(m_osSocket);
    m_osSocket = SDLNet_UDP_Open(port);
  }

}