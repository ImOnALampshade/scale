// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  A structure for storing information about how to reach a remote system.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETENDPOINT_H
#define NETENDPOINT_H

namespace Network
{
  class Endpoint
  {
  private:
    IPaddress m_addr;
    ConnectionID m_stream;

  public:
    Endpoint();
    Endpoint(IPaddress addr, port_t port, ConnectionID stream = 0);
    Endpoint(String addr, port_t port, ConnectionID stream);
    Endpoint(const UDPpacket * fromPacket);

    IPaddress GetAddress() const;
    port_t GetPort() const;
    ConnectionID GetStreamID() const;
  };
}
#endif
