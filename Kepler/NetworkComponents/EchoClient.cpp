//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "stdafx.h"
#include "EchoClient.h"
#include <GLFW/glfw3.h>
#include "..\KeplerCore\composite.h"
#include "..\KeplerCore\compositefactory.h"

EchoClient::EchoClient()
{

}

void EchoClient::Initialize (const Json::Object &jsonData)
{
  if (jsonData.Contains ("ip"))
  {
    m_ipAddr = (String) jsonData.Find ("ip")->value;
  }

  if (jsonData.Contains ("targetport"))
  {
    m_targetPort = int ( (double) (jsonData.Find ("targetport")->value));
  }

  if (jsonData.Contains ("hostport"))
  {
    m_targetPort = int ( (double) (jsonData.Find ("hostport")->value));
  }

  Component::RegisterMessageProc ("NetRcvd",
  [this] (void *data) {
    onNetworkMessage (reinterpret_cast<const Network::NetMessage *> (data));
  });
  //m_eventIDa = CoreEngine::Game->RegisterObserver("KeyEvent",
  //  std::function<void(KeyEvent*) >(
  //  [this](KeyEvent *data) {
  //  onKeyEvent(data);
  //}));
  m_eventIDb = CoreEngine::Game->RegisterObserver ("LogicUpdate",
                                                   std::function<void() > (
  [this]() {
    this->onUpdate (nullptr);
  }));
}

void EchoClient::Free()
{
  CoreEngine::Game->UnregisterObserver ("KeyEvent", m_eventIDa);
  CoreEngine::Game->UnregisterObserver ("LogicUpdate", m_eventIDb);
}

void EchoClient::onNetworkMessage (const Network::NetMessage *data)
{
  char buffer[255];
  memcpy (buffer, data->GetPayload(), data->GetHeader()->PayloadSize);
  printf (data->GetPayload());
}

void EchoClient::onUpdate (void *data)
{
  //::Handle<Component> echoc = Component::Owner()->GetComponent("network_echo");
  //EchoComponent *echo = reinterpret_cast<EchoComponent*>((echoc.GetInner()->ptr));
  //echo->Send("HelloWorld", 10, false);
  //printf("I sent a thing.");
}

//void EchoClient::onKeyEvent (KeyEvent *data)
//{
//  if (data->Action == GLFW_PRESS)
//  {
//    switch (data->Key)
//    {
//    case GLFW_KEY_Z:
//      // Server
//      NetworkSystemComponent::GetInstance()->SetPort (m_hostPort);
//      NetworkSystemComponent::GetInstance()->BeginListening();
//      break;
//
//    case GLFW_KEY_X:
//      // Client connect
//      NetworkSystemComponent::GetInstance()->ConnectToPlayer (m_ipAddr, m_hostPort);
//      break;
//
//    case GLFW_KEY_C:
//      // Send message
//      ::Handle<Component> echoc = Component::Owner()->GetComponent ("network_echo");
//      EchoComponent *echo = reinterpret_cast<EchoComponent *> ( (echoc.GetInner()->ptr));
//      echo->Send ("HelloWorld\0", 11, false);
//      printf ("I sent a thing.");
//      break;
//    }
//  }
//}
