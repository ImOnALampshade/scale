// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  An object which sends and receives data to a single endpoint
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"


namespace Network
{
  void Link::Update()
  {
    if (m_isOpen && !m_isFinished)
    {
      milisecond timeDiff = 0; // NetworkSystemComponent::GetInstance()->GetCurrentTime() - m_lastMessageTime;
    }
    Envelope * rcvd = m_socket->RecvFrom(m_partner.GetStreamID());
    ProcessEnvelope(rcvd);
  }

  Link::Link(const Endpoint *partner, SocketObject *socket)
    : m_socket(socket), m_partner(*partner), m_lastSeqSent(0), m_lastAckRcvd(0),
    m_lastMessageTime(0), m_isOpen(true), m_isFinished(false)
  {

  }

  void Link::ProcessEnvelope(Envelope *envelope)
  {
    if (!envelope) return;

    NetLog(L"Got a packet.\n");

    Uint32 prevAck = m_lastAckRcvd;
    Uint32 prevSeq = m_lastSeqRcvd;
    const Envelope::Header * header = envelope->GetHeader();

    // If we've already received a packet after this one, stop.
    if (prevSeq >= header->SequenceNumber)
      return;

    // Check for FIN and RST
    if (header->flags & Envelope::Header::FLAG_FIN)
    {
      if (m_isOpen)
        SendFinPacket();
      m_isOpen = false;
    }
    if (header->flags & Envelope::Header::FLAG_RST)
    {
      // TODO: Log connection reset
      m_isFinished = true;
      m_isOpen = false;
    }

    // Update ack received
    if (header->flags & Envelope::Header::FLAG_ACK)
    {
      m_lastAckRcvd = header->AckNumber;
    }
    else // We don't ack acks.
    {
      SendAckPacket((header->flags & Envelope::Header::FLAG_SYN) && !(header->flags & Envelope::Header::FLAG_ACK));
    }

    // Update sequence and process contents
    m_lastSeqRcvd = header->SequenceNumber;
    unsigned index = 0;
    while (index < header->PayloadSize)
    {
      EchoPacket *message;
      EchoPacket::ReadFromBuffer(&message, envelope->GetPayload() + index, header->PayloadSize - index);
      index += message->GetTotalSize();
      NetworkSystemComponent::GetInstance()->DispatchNetworkMessage(message);
      delete(message);
    }


  }

  void Link::SendFinPacket()
  {
    Envelope rstPacket("", 0);
    rstPacket.GetHeader()->flags |= rstPacket.GetHeader()->FLAG_FIN;
    SendPacket(&rstPacket);
    m_isFinished = true;
    NetLog(L"Sent a syn packet\n");
  }

  void Link::SendRstPacket()
  {
    Envelope rstPacket("", 0);
    rstPacket.GetHeader()->flags |= rstPacket.GetHeader()->FLAG_RST;
    SendPacket(&rstPacket);
    m_isOpen = false;
    m_isFinished = true;
    NetLog(L"Sent a rst packet\n");
  }

  void Link::SendSynPacket()
  {
    Envelope synPacket("", 0);
    synPacket.GetHeader()->flags = synPacket.GetHeader()->FLAG_SYN;
    SendPacket(&synPacket);
    NetLog(L"Sent a syn packet\n");
  }

  void Link::SendAckPacket(bool synAck)
  {
    Envelope ackPacket("", 0);
    ackPacket.GetHeader()->flags |= ackPacket.GetHeader()->FLAG_ACK;
    if(synAck)
      ackPacket.GetHeader()->flags |= ackPacket.GetHeader()->FLAG_SYN;
    ackPacket.GetHeader()->AckNumber = m_lastAckRcvd;
    ackPacket.GetHeader()->SequenceNumber = m_lastSeqSent;
    m_socket->SendTo(m_partner, &ackPacket);
    NetLog(L"Sent a ack packet (flags %x)\n", ackPacket.GetHeader()->flags);
  }

  void Link::SendPacket(Envelope *data)
  {
    data->GetHeader()->AckNumber = m_lastAckRcvd;
    data->GetHeader()->SequenceNumber = ++m_lastSeqSent;
    m_socket->SendTo(m_partner, data);
    NetLog(L"Sent a packet\n");
  }

  const Endpoint & Link::GetPartner()
  {
    return m_partner;
  }

  void Link::Initialize()
  {
    m_socket->AddEndpoint(m_partner.GetStreamID(), &m_partner);
    m_isFinalized = false;
  }

  void Link::Finalize()
  {
    m_socket->ForgetConnection(m_partner.GetStreamID());
    m_isFinalized = true;
  }

  Link::~Link()
  {
    if (!m_isFinalized)
      Finalize();
  }

  bool Link::IsOpen()
  {
    return m_isOpen;
  }

}