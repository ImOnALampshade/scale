// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Component for maintaining and initiating connections.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include <random>

static NetworkSystemComponent *singleton;

#define PACKET_BUFFER_SIZE 65536

void NetworkSystemComponent::update (milisecond dt)
{
  m_currentTick += dt;

  char buffer[PACKET_BUFFER_SIZE];
  char *writeHead = &buffer[0];

  // Collect and pending messages
  Uint32 payloadSize = 0;

  for (auto pair: m_echoes)
  {
    EchoComponent *echo = pair.second;
    Uint32 writeSize = echo->GenerateNextPacket(writeHead);

    if (writeSize > 0)
    {
      //assert(reinterpret_cast<Network::EchoPacket::Header*>(writeHead)->PayloadSize == writeSize);
      payloadSize += writeSize;
      writeHead += writeSize;
    }

    BREAK_IF (payloadSize > PACKET_BUFFER_SIZE, "Attempting to send too large of a packet in a single frame.");
  }

  // Send pending messages
  if (payloadSize > 0)
  {
    Network::Envelope sendFrame (buffer, payloadSize);
    SendToAll (&sendFrame);
  }

  // Collect inputs
  m_socket->Update();

  // Receive inputs
  for (Network::Link *conn : m_activeConnections)
  {
    conn->Update();
  }

  if (m_listener)
  {
    Network::Endpoint ask = m_listener->Peek();

    if (ask.GetStreamID() == 0)
    {
    }
    else if (m_socket->HasConnection (ask.GetStreamID()))
    {
      m_listener->Reject();
    }

    else
    {
      Network::Link *newConn = m_listener->Accept();
      NetLog (L"Got a connection!");
      m_activeConnections.push_back (newConn);
    }
  }
}

echoid_t NetworkSystemComponent::getNextEcho()
{
  // Echo anatomy:
  ///////////////////////
  //   4 bits | 28 bits
  // PlayerID | EchoID
  //     $$$$ | $$$$$$$$$$$$$$$$$$$$$$$$$$$$

  echoid_t playerMask = m_playerIndex << 28;
  return playerMask | (m_nextEcho++);
}


NetworkSystemComponent::NetworkSystemComponent()
  : m_matchServerLink (nullptr), m_listener (0), m_nextEcho (1024),
    m_isInCleanup (false), m_currentTurn (0), m_playerIndex (0), m_currentTick (0)
{
  singleton = this;
  SDLNet_Init();
}

NetworkSystemComponent::~NetworkSystemComponent()
{
  SDLNet_Quit();
}

NetworkSystemComponent *NetworkSystemComponent::GetInstance()
{
  return singleton;
}

void NetworkSystemComponent::Initialize (const Json::Object &jsonData)
{
  m_socket = new Network::SocketObject (0);
  RegisterMessageProc ("LogicUpdate", [this] (void *data)
  {
    this->update (0);
  });
}

void NetworkSystemComponent::Free()
{
  if (m_socket)
  {
    delete (m_socket);
    m_socket = 0;
  }

  if (m_matchServerLink)
  {
    delete (m_matchServerLink);
    m_matchServerLink = 0;
  }

  if (m_listener)
  {
    delete (m_listener);
    m_listener = 0;
  }

  for (size_t i = 0; i < m_activeConnections.size(); ++i)
  {
    delete (m_activeConnections[i]);
  }

  singleton = nullptr;
}

void NetworkSystemComponent::DispatchNetworkMessage (Network::EchoPacket *message)
{
  echoid_t id = message->GetHeader()->ID;
  m_echoes[id]->Receive (message);
}

void NetworkSystemComponent::RegisterEcho (EchoComponent *toReg)
{
  if (toReg->ID() == 0)
    toReg->ID (getNextEcho());

  else
    assert (toReg->ID() < 1024);

  m_echoes[toReg->ID()] = toReg;
}

void NetworkSystemComponent::DeRegisterEcho (EchoComponent *toReg)
{
  if (m_isInCleanup)
    assert (m_echoes.find (toReg->ID()) != m_echoes.end());

  m_echoes.erase (toReg->ID());
}

Uint32 NetworkSystemComponent::GetCurrentGameTurn()
{
  return m_currentTurn;
}

void NetworkSystemComponent::ConnectToMMS()
{
  //m_matchServerLink = new Network::Link(&Network::Endpoint(mmsaddr, mmsport, static_cast<Uint32>(rand())), m_socket);

}

void NetworkSystemComponent::SendMMSMessage (String message, int size)
{

}

void NetworkSystemComponent::ConnectToPlayer (String ipaddr, Uint16 port)
{
  Network::Endpoint target (ipaddr, port, GenerateConnectionID());
  Network::Link *newConnection = new Network::Link (&target, m_socket);
  newConnection->SendSynPacket();
  m_activeConnections.push_back (newConnection);
  newConnection->Initialize();
}

void NetworkSystemComponent::BeginListening()
{
  StopListening();
  m_listener = new Network::Listener();
  m_listener->Listen (m_socket);
}

void NetworkSystemComponent::StopListening()
{
  if (m_listener)
  {
    delete (m_listener);
    m_listener = 0;
  }
}

void NetworkSystemComponent::SetPort (port_t port)
{
  if (m_socket)
  {
    m_socket->SetPort (port);
  }

  else
  {
    m_socket = new Network::SocketObject (port);
  }
}

void NetworkSystemComponent::SendToAll (Network::Envelope *packet)
{
  for (Network::Link *link : m_activeConnections)
  {
    link->SendPacket (packet);
  }
}

ConnectionID NetworkSystemComponent::GenerateConnectionID()
{
  return std::rand();
}

bool NetworkSystemComponent::IsConnected()
{
  return this->m_activeConnections.size() > 0;
}

void NetworkSystemComponent::Disconnect()
{
  for (Network::Link *link : m_activeConnections)
  {
    if (link->IsOpen())
    {
      link->SendFinPacket();
      link->Finalize();
      delete (link);
    }
  }

  m_activeConnections.clear();
}

META_REGISTER_FUNCTION (NetworkSystem)
{
  META_INHERIT (NetworkSystemComponent, "Kepler", "Component");

  META_ADD_METHOD (NetworkSystemComponent, GetCurrentGameTurn);
  META_ADD_METHOD (NetworkSystemComponent, ConnectToMMS);
  META_ADD_METHOD (NetworkSystemComponent, SendMMSMessage);
  META_ADD_METHOD (NetworkSystemComponent, ConnectToPlayer);
  META_ADD_METHOD (NetworkSystemComponent, BeginListening);
  META_ADD_METHOD (NetworkSystemComponent, StopListening);
  META_ADD_METHOD (NetworkSystemComponent, Disconnect);

  META_ADD_PROP_READONLY (NetworkSystemComponent, IsConnected);

  META_FINALIZE_PTR (NetworkSystemComponent, MetaPtr);
}
