// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  A structure for storing information about how to reach a remote system.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"

namespace Network
{
  Endpoint::Endpoint()
    : m_stream (0)
  {
    m_addr.host = 0;
    m_addr.port = 0;
  }

  Endpoint::Endpoint (IPaddress addr, port_t port, ConnectionID stream /*= 0*/)
    : m_stream (stream)
  {
    m_addr = addr;
  }


  Endpoint::Endpoint (String addr, port_t port, ConnectionID stream)
  {
    SDLNet_ResolveHost (&m_addr, addr.CStr(), port);
  }

  Endpoint::Endpoint (const UDPpacket *fromPacket)
    : m_addr (fromPacket->address)
  {
    //const Envelope *read = reinterpret_cast<Envelope*>(fromPacket->data);
    Envelope *read = Envelope::ReadFromBuffer (fromPacket);
    m_stream = read->GetHeader()->ID;
  }

  IPaddress Endpoint::GetAddress() const
  {
    return m_addr;
  }

  port_t Endpoint::GetPort() const
  {
    return m_addr.port;
  }

  ConnectionID Endpoint::GetStreamID() const
  {
    return m_stream;
  }
}