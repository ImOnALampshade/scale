// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Component for network syncronization.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------


#include "stdafx.h"
#include "../KeplerCore/composite.h"

EchoComponent::EchoComponent()
  : m_id (0), m_syncFreq (1)
{

}

void EchoComponent::Initialize (const Json::Object &jsonData)
{
  NetworkSystemComponent::GetInstance()->RegisterEcho (this);
}

void EchoComponent::Free()
{
  NetworkSystemComponent::GetInstance()->DeRegisterEcho (this);
}

echoid_t EchoComponent::ID()
{
  return m_id;
}

void EchoComponent::ID (echoid_t id)
{
  assert (m_id == 0);
  m_id = id;
}

void EchoComponent::Send (const void *data, Uint32 size, bool critical)
{
  Network::NetMessage   *sendPacket = new Network::NetMessage (size);
  memcpy (sendPacket->GetPayload(), data, size);
  m_outgoing.push_back (sendPacket);
}

void EchoComponent::SetSyncFrequency (int value)
{
  assert (value > 0);
  m_syncFreq = value;
}

void EchoComponent::Receive (Network::EchoPacket *message)
{
  size_t size = message->GetHeader()->PayloadSize;
  size_t index = 0;

  while (index < size)
  {
    const Network::NetMessage *innerMessage = Network::NetMessage::CreateReadOnly (message->GetPayload() + index);
    index += innerMessage->GetSize();
    Owner()->Dispatch ("NetRcvd", const_cast<Network::NetMessage *> (innerMessage));
  }
}

void EchoComponent::update (int currentFrame)
{
  if (currentFrame % m_syncFreq == 0)
  {
    Owner()->Dispatch ("NetSync", nullptr);
  }
}

Uint32 EchoComponent::GenerateNextPacket(char *buffer)
{
  if (m_outgoing.empty())
    return 0;

  Network::EchoPacket packet = Network::EchoPacket::WriteToBuffer (buffer);
  packet.GetHeader()->PayloadSize = 0;

  buffer = packet.GetPayload();

  for (size_t i = 0; i < m_outgoing.size(); ++i)
  {
    Network::NetMessage *iter = m_outgoing[i];
    const char *origin = iter->GetBuffer();
    Uint32 size = iter->GetSize();
    memcpy (buffer, origin, size);
    buffer += iter->GetSize();
    packet.GetHeader()->PayloadSize += Uint32(iter->GetSize());
    delete (iter);
  }

  m_outgoing.clear();

  packet.GetHeader()->ID = ID();
  return packet.GetTotalSize();
}

META_REGISTER_FUNCTION (Echo)
{
  META_INHERIT (EchoComponent, "Kepler", "Component");

  META_ADD_PROP (EchoComponent, ID);

  META_ADD_METHOD (EchoComponent, Send);
  META_ADD_METHOD (EchoComponent, SetSyncFrequency);

  META_FINALIZE_PTR (EchoComponent, MetaPtr);
}
