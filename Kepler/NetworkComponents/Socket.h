// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  A wrapper object around a UDP Socket.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETSOCKET_H
#define NETSOCKET_H

#include "../KeplerCore/component.h"
#include <map>
#include <vector>

namespace Network 
{
  class Endpoint;

  class SocketObject
  {
  private:
    UDPsocket m_osSocket;
    UDPpacket *m_recvBuffer;
    std::map<ConnectionID, std::vector<Envelope*> *> m_knownEndpoints;
    std::vector<std::pair<Endpoint, Envelope*>> m_syns;
  public:
    SocketObject(uint16_t port = 0);
    ~SocketObject();

    void SendTo(const Endpoint &endpoint, Envelope *data);
    void Update();

    void AddEndpoint(ConnectionID id, const Endpoint *endpoint);
    bool HasConnection(ConnectionID id);
    void ForgetConnection(ConnectionID id);

    Envelope* PeekSyn(/* out */ Endpoint *sender);
    Envelope* RecvSyn(/* out */ Endpoint *sender);
    Envelope* RecvFrom(ConnectionID id);

    // Debug stuff
    void SetPort(port_t port);
  };
}
#endif
