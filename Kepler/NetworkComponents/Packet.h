// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  A template for a buffer with a header and a payload
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETPACKET_H
#define NETPACKET_H

#include "../KeplerCore/component.h"


static void dump_packet(const unsigned char * buffer, size_t length)
{
  printf("========================\n");
  for (size_t i = 0; i < length; ++i)
  {
    printf("%02x ", buffer[i]);
    if (i % 8 == 7)
      printf("\n");
  }
  printf("\n========================\n");
}

namespace Network
{
  template<class Header_Type>
  class Packet
  {
  private:
    char *m_buffer;
    bool  m_ownsBuffer;

    Packet()
    {
      m_buffer = 0;
      m_ownsBuffer = true;
    }
  public:
    Packet(Packet &moved)
    {
      m_buffer = moved.m_buffer;
      m_ownsBuffer = moved.m_ownsBuffer;

      moved.m_buffer = 0;
      moved.m_ownsBuffer = false;
    }

    //Packet(const Packet &copy) = delete;
    
    Packet(Uint32 payloadSize)
    {
      if (payloadSize > 0)
      {
        m_buffer = new char[payloadSize + sizeof(Header_Type)];
        memset(m_buffer, 0, payloadSize);
        GetHeader()->PayloadSize = payloadSize;
        m_ownsBuffer = true;
      }
      else
      {
        m_ownsBuffer = false;
      }
    }

    // Creates a packet with the specified MESSAGE.
    Packet(const char * message, Uint32 size)
    {
      const Header_Type *readHeader = reinterpret_cast<const Header_Type*>(message);
      m_buffer = new char[readHeader->PayloadSize + sizeof(Header_Type)];
      memcpy(GetPayload(), message, readHeader->PayloadSize + sizeof(Header_Type));
      GetHeader()->PayloadSize = size;
      m_ownsBuffer = true;
    }

    static void ReadFromBuffer(Packet *out, const char * buffer, Uint32 size)
    {
      const Header_Type *readHeader = reinterpret_cast<const Header_Type*>(buffer);
      size_t bufferSize = readHeader->PayloadSize + sizeof(Header_Type);
      assert(size >= bufferSize);
      assert(out->m_buffer == nullptr);
      out->m_buffer = new char[bufferSize];
      out->m_ownsBuffer = true;
      memcpy(out->m_buffer, buffer, bufferSize);
    }

    static void ReadFromReadOnlyBuffer(Packet *out, const char * buffer)
    {
      assert(out);
      out->m_buffer = const_cast<char*>(buffer);
      out->m_ownsBuffer = false;
    }

    ~Packet()
    {
      if(m_ownsBuffer)
        delete[](m_buffer);
    }

    char * GetBuffer()
    {
      return m_buffer;
    }
    Header_Type * GetHeader()
    {
      return reinterpret_cast<Header_Type*>(m_buffer);
    }
    const Header_Type * GetHeader() const
    {
      return reinterpret_cast<Header_Type*>(m_buffer);
    }
    char * GetPayload()
    {
      return reinterpret_cast<char*>(m_buffer)+sizeof(Header_Type);
    }
    const char * GetPayload() const
    {
      return reinterpret_cast<char*>(m_buffer)+sizeof(Header_Type);
    }

    void SetBuffer(char * value)
    {
      assert(m_buffer == nullptr);
      m_ownsBuffer = false;
      m_buffer = value;
    }
  };
}
#endif
