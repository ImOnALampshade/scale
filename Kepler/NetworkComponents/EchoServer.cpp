//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "stdafx.h"
#include "EchoServer.h"
#include "../KeplerCore/composite.h"

EchoServer::EchoServer()
{

}

void EchoServer::Initialize (const Json::Object &jsonData)
{
  Component::RegisterMessageProc ("net_rcvd",
  [this] (void *data) {
    onNetworkMessage (reinterpret_cast<const Network::EchoPacket *> (data));
  });
}

void EchoServer::Free()
{

}

void EchoServer::onNetworkMessage (const Network::EchoPacket *data)
{
  (Component::Owner()->GetComponent ("echo").as<EchoComponent>())->
  Send (data->GetPayload(), data->GetHeader()->PayloadSize, false);
}
