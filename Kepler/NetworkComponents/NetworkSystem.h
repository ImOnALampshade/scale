// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Component for maintaining and initiating connections.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETSYSTEM_COMPONENT_H
#define NETSYSTEM_COMPONENT_H

#include "../KeplerCore/component.h"

class NetworkSystemComponent : public Component
{
private:
  typedef std::pair <echoid_t, Network::NetMessage>  EchoMessagePair;

  Network::SocketObject *m_socket;
  Network::Link *m_matchServerLink;
  Network::Listener *m_listener;
  echoid_t m_nextEcho;
  std::vector<Network::Link *> m_activeConnections;
  std::map<echoid_t, EchoComponent *> m_echoes;
  std::vector < EchoMessagePair > m_outgoingMessages;
  bool m_isInCleanup;
  Uint32 m_currentTurn;
  Uint8 m_playerIndex;
  milisecond m_currentTick;

  echoid_t getNextEcho();

  // Events actions
  void update (milisecond dt);
  void onLevelEnd (void *);
public:
  NetworkSystemComponent();
  ~NetworkSystemComponent();

  // Singleton
  static NetworkSystemComponent *GetInstance();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  // Internal! Do not call.
  void DispatchNetworkMessage (Network::EchoPacket *message);
  // Internal! Do not call.
  void RegisterEcho (EchoComponent *toReg);
  // Internal! Do not call.
  void DeRegisterEcho(EchoComponent *toReg);
  // Internal! Do not call.
  ConnectionID GenerateConnectionID();
  // Internal! Do not call.
  // Enqueues a message for sending in the next outgoing envelope.
  void EnqueueMessage(Network::NetMessage *message, const EchoComponent &echo);
  
  // Gets the current networking step.
  Uint32 GetCurrentGameTurn();

  // Connects to the matchmaking server
  void ConnectToMMS();

  // Sends a message to the matchmaking server
  void SendMMSMessage (String message, int size);

  // Connects to a player
  void ConnectToPlayer (String ipaddr, Uint16 port);

  // Begins listening for incoming connections
  void BeginListening();

  // Stops accepting incoming connections
  void StopListening();

  // Disconnects from all active connections.
  void Disconnect();

  // Returns whether or not we have an active connection.
  bool IsConnected();

  void SendToAll (Network::Envelope *packet);

  // Debugging methods
  void SetPort (port_t port);

  COMPONENT_META
};

#endif
