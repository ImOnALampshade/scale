// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Debug module for networking
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NET_DEBUG_ECHOSERVER_H
#define NET_DEBUG_ECHOSERVER_H

#include "../KeplerCore/component.h"

class EchoServer : public Component
{
private:

public:
  EchoServer();

  virtual void Initialize(const Json::Object &jsonData);
  virtual void Free();

private:
  void onNetworkMessage(const Network::EchoPacket *data);
};

#endif
