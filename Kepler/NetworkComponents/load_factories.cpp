// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "load_factories.h"
#include "../KeplerCore/compositefactory.h"
#include "EchoClient.h"
#include "EchoServer.h"

void NetworkComponents::LoadFactories()
{
  CREATE_FACTORY (NetworkSystem);
  CREATE_FACTORY (Echo);
}

void NetworkComponents::LoadDebugFactories()
{
  //CREATE_FACTORY(EchoServer, test_echo_server);
  //CREATE_FACTORY(EchoClient, test_echo_client);
}
