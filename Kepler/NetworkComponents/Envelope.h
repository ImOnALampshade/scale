// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  The top level packet for sending messages over the network.
//  Several messages are placed inside of one EchoPacket, which
//  is placed inside of one Envelope.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETENVOLOPE_H
#define NETENVOLOPE_H

#include "../KeplerCore/component.h"
#include "Packet.h"

namespace Network
{
  class Endpoint;

  class Envelope
  {
  public:
    struct Header
    {
      ConnectionID ID;
      flags_t flags;
      Uint32 PayloadSize;
      Uint32 SequenceNumber;
      Uint32 AckNumber;

      static const flags_t FLAG_SYN = 0x0001;
      static const flags_t FLAG_ACK = 0x0002;
      static const flags_t FLAG_FIN = 0x0004;
      static const flags_t FLAG_RST = 0x0008;
    };

  private:
    UDPpacket *m_packet;

    Packet<Envelope::Header> &getInternalPacket()
    {
      return reinterpret_cast<Packet<Envelope::Header>&>((m_packet->data));
    }
    const Packet<Envelope::Header> &getInternalPacket() const
    {
      return reinterpret_cast<const Packet<Envelope::Header>&>((m_packet->data));
    }

    Envelope(const UDPpacket * buffer)
    {
      m_packet = SDLNet_AllocPacket(buffer->len);
      m_packet->address = buffer->address;
      m_packet->channel = buffer->channel;
      memcpy(m_packet->data, buffer->data, buffer->len);
    }

  public:
    Envelope(const char * message, Uint32 size, ConnectionID id = 0)
    {
      m_packet = nullptr;
      m_packet = SDLNet_AllocPacket(size + sizeof(Header));
      memset(m_packet->data, 0, size + sizeof(Header));
      m_packet->len = size + sizeof(Header);
      printf(SDLNet_GetError());
      memcpy(GetPayload(), message, size);
      GetHeader()->PayloadSize = size;
    }

    static Envelope * ReadFromBuffer(const UDPpacket * buffer)
    {
      return new Envelope(buffer);
    }
    ~Envelope()
    {
      SDLNet_FreePacket(m_packet);
    }

    UDPpacket * GetBuffer() { return m_packet; }
    Header * GetHeader() { return getInternalPacket().GetHeader(); }
    const Header * GetHeader() const { return getInternalPacket().GetHeader(); }
    char * GetPayload() { return getInternalPacket().GetPayload(); }

    Uint32 GetBufferSize() const
    {
      return GetHeader()->PayloadSize + sizeof(Header);
    }


  };
}
#endif