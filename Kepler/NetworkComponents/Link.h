// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  An object which sends and receives data to a single endpoint
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETCONNECTION_H
#define NETCONNECTION_H

#include "../KeplerCore/component.h"

namespace Network
{
  class Endpoint;

  class Link
  {
  private:
    SocketObject *m_socket;
    Endpoint m_partner;
    Uint32 m_lastSeqSent;
    Uint32 m_lastSeqRcvd;
    Uint32 m_lastAckRcvd;
    milisecond m_lastMessageTime;

    // Can receive
    bool m_isOpen;

    // Can send
    bool m_isFinished;

    // Has disposed
    bool m_isFinalized;
  public:
    Link(const Endpoint *partner, SocketObject *socket);
    ~Link();
    void Update();
    void Initialize();
    void Finalize();

    void ProcessEnvelope(Envelope *envelope);
    void SendAckPacket(bool synAck = false);
    void SendSynPacket();
    void SendFinPacket();
    void SendRstPacket();
    void SendPacket(Envelope *data);
    const Endpoint &GetPartner();

    bool IsOpen();
  };
}
#endif
