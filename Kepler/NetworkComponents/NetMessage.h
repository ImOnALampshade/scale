// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  The lower level packet for sending messages over the network.
//  Several messages are placed inside of one EchoPacket, which
//  is placed inside of one Envelope.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETMESSAGE_H
#define NETMESSAGE_H

#include "../KeplerCore/component.h"

namespace Network
{
  class NetMessage
  {
    struct Header
    {
      Uint32 PayloadSize;
    };

  private:
    Packet<Header> m_packet;

    NetMessage(Packet<Header> &createFrom)
      : m_packet(createFrom)
    { }
    NetMessage()
      : m_packet(0)
    { }
  public:
    NetMessage(const char * message, Uint32 size, ConnectionID id = 0)
      : m_packet(message, size)
    { }
    NetMessage(Uint32 size)
      : m_packet(size)
    { }
    static NetMessage * ReadFromBuffer(const char * buffer, Uint32 size)
    {
      return new NetMessage(Packet<Header>(buffer, size));
    }

    static NetMessage * CreateReadOnly(const char * buffer)
    {
      NetMessage *ret = new NetMessage();
      Packet<Header>::ReadFromReadOnlyBuffer(&ret->m_packet, buffer);
      return ret;
    }

    const char * GetBuffer() { return m_packet.GetBuffer(); }
    Header * GetHeader() { return m_packet.GetHeader(); }
    char * GetPayload() { return m_packet.GetPayload(); }
    const Header * GetHeader() const { return m_packet.GetHeader(); }
    const char * GetPayload()  const { return m_packet.GetPayload(); }
    Uint32  GetSize() const { return GetHeader()->PayloadSize + sizeof(Header); }
  };
}
#endif
