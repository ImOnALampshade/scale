// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Debug module for networking
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NET_DEBUG_ECHOCLIENT_H
#define NET_DEBUG_ECHOCLIENT_H

#include "../KeplerCore/component.h"

class EchoClient : public Component
{
private:
  int    m_targetPort;
  int    m_hostPort;
  String m_ipAddr;
public:
  EchoClient();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

private:
  void onNetworkMessage (const Network::NetMessage *data);
  void onUpdate (void *data);
  //void onKeyEvent (KeyEvent *data);

  uint32_t m_eventIDa;
  uint32_t m_eventIDb;
};

#endif
