// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  An object which listens for incoming syn packets
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETLISTENER_H
#define NETLISTENER_H

#include "../KeplerCore/component.h"

namespace Network
{
  class SocketObject;
  class Endpoint;

  class Listener
  {
  private:
    SocketObject *m_socket;
  public:
    Listener();

    void Listen(SocketObject *socket);
    void EndListen();
    Endpoint Peek();
    Link * Accept();
    void Reject(bool sendRst = true);
  };
}
#endif
