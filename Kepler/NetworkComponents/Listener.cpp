// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  An object which listens for incoming syn packets
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"

namespace Network
{
  Listener::Listener()
    : m_socket(nullptr)
  {

  }

  void Listener::Listen(SocketObject *socket)
  {
    m_socket = socket;
  }

  void Listener::EndListen()
  {
    m_socket = nullptr;
  }

  Endpoint Listener::Peek()
  {
    Endpoint ret;
    if (m_socket->PeekSyn(&ret))
      return ret;
    else
      return Endpoint();
  }

  Link *Listener::Accept()
  {
    if (!m_socket)
      return nullptr;

    Link * ret = nullptr;
    Endpoint otherEnd;
    Envelope * synEnvelope = m_socket->RecvSyn(&otherEnd);

    if (!synEnvelope)
      return nullptr;

    if (synEnvelope->GetHeader()->PayloadSize != 0)
    {
      // TODO: Deal with syn header stuff here.
    }
    if(m_socket->HasConnection(otherEnd.GetStreamID()))
    {
      // Already have a connection open to this other (possibly duplicate syn?)
      // Either way, ignore it.
      goto cleanup;
    }

    // Success case.
    assert(ret == nullptr); // Guard against ill-conceived code changes.
    ret = new Link(&otherEnd, m_socket);
    ret->Initialize();
    ret->ProcessEnvelope(synEnvelope);

  cleanup:
    if (synEnvelope)
      delete synEnvelope;
    return ret;
  }

  void Listener::Reject(bool sendRst)
  {
    if (!m_socket)
      return;
    Endpoint otherEnd;

    Envelope * synEnvelope = m_socket->RecvSyn(&otherEnd);

    // Send an RST packet
    if (synEnvelope && sendRst)
    {
      Envelope rstPacket("", 0);
      rstPacket.GetHeader()->flags |= rstPacket.GetHeader()->FLAG_RST;
      m_socket->SendTo(otherEnd, &rstPacket);
    }

    if (synEnvelope)
      delete synEnvelope;
  }
}