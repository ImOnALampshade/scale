// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  The mid-level packet for sending messages over the network.
//  Several messages are placed inside of one EchoPacket, which
//  is placed inside of one Envelope.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETECHOPACKET_H
#define NETECHOPACKET_H

#include "../KeplerCore/component.h"
#include "Packet.h"

namespace Network
{
  class EchoPacket
  {
    struct Header
    {
      echoid_t ID;
      Uint32 PayloadSize;
    };

  private:
    Packet<Header> m_packet;

    EchoPacket(Packet<Header> &createFrom)
     : m_packet(createFrom)
    { }
    
  public:
    EchoPacket()
      : m_packet(0)
    {
    }
    EchoPacket(EchoPacket &&other)
      : m_packet(other.m_packet)
    { }
    
    static void ReadFromBuffer(EchoPacket **out, const char * buffer, Uint32 size)
    {
      (*out) = new EchoPacket();
      Packet<Header>::ReadFromBuffer(&(*out)->m_packet, buffer, size);
    }
    static EchoPacket WriteToBuffer(char * buffer)
    {
      EchoPacket ret;
      ret.m_packet.SetBuffer(buffer);
      return ret;
    }

    const char * GetBuffer() { return m_packet.GetBuffer(); }
    Header * GetHeader() { return m_packet.GetHeader(); }
    const Header * GetHeader() const { return m_packet.GetHeader(); }
    char * GetPayload() { return m_packet.GetPayload(); }
    const char * GetPayload() const { return m_packet.GetPayload(); }
    unsigned GetTotalSize() { return m_packet.GetHeader()->PayloadSize + sizeof(Header); }
  };
}
#endif
