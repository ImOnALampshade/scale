// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Component for network syncronization.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef NETECHO_COMPONENT_H
#define NETECHO_COMPONENT_H

#include "../KeplerCore/component.h"

class EchoComponent : public Component
{
private:
  echoid_t m_id;
  size_t m_syncFreq;
  std::vector < Network::NetMessage * > m_outgoing;

public:
  EchoComponent();

  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  echoid_t ID();
  void ID (echoid_t id);

  // Sends a message immediately
  void Send (const void *data, Uint32 size, bool critical);

  // Sets how often this object sends syncs.
  void SetSyncFrequency (int value);

  // INTERNAL: Processes a message and dispatches it to components.
  void Receive (Network::EchoPacket *message);

  // INTERNAL: Retrieves the EchoPacket for sending
  Uint32  GenerateNextPacket(char *buffer);

  COMPONENT_META

private:
  void update (int currentFrame);
};

#endif
