// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Common things to all serializers
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "common.h"

namespace Serializer
{
  Result::Result() :
    WasSuccess (true),
    FileName(),
    ErrorStr(),
    ErrorLine()
  {
  }

  Result::Result (CommonErrorTypes error, unsigned line) :
    WasSuccess (false),
    FileName(),
    ErrorStr(),
    ErrorLine (line)
  {
    switch (error)
    {
    case Serializer::Result::END_OF_FILE:
      ErrorStr = "Unexpected end of file: ";
      break;

    case Serializer::Result::UNEXPECTED_CHAR:
      ErrorStr = "Unexpected character in stream";
      break;
    }
  }

  Result::Result (String error, unsigned line)
  {
  }
}

// -----------------------------------------------------------------------------
