// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Parser for JSON files
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../HashFunctions/hashfunctions.h"
#include "jsonparser.h"
#include <codecvt>
#include <cwchar>
#include <locale>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
using namespace Serializer;

// -----------------------------------------------------------------------------

static Value ReadNumber (const String &file, size_t &start, unsigned &line);
static String ReadString (const String &file, size_t &start, unsigned &line);
static Value ReadObject (const String &file, size_t &start, unsigned &line);
static Value ReadList (const String &file, size_t &start, unsigned &line);

static Value ReadValue (const String &file, size_t &start, unsigned &line);

// -----------------------------------------------------------------------------

static Value ReadNumber (const String &file, size_t &start, unsigned &line)
{
  char *endPtr;
  Json::Number total = strtod (file.c_str() + start, &endPtr);

  start = endPtr - file.c_str() - 1;

  return total;
}

static String ReadString (const String &file, size_t &start, unsigned &line)
{
  size_t endPoint = start;

  do
  {
    endPoint = file.Find ('"', endPoint + 1);

    if (endPoint == String::npos)
      throw Json::ParseError (String ("Unterminated string on line %i", line));
  }
  while (file[endPoint - 1] == '\\');

  String str;

  for (size_t i = start + 1; i < endPoint; ++i)
  {
    if (file[i] == '\\')
    {
      ++i;

      switch (file[i])
      {
      case '\\':
      case '"':
        str += (file[i]);
        break;

      case 'b':
        str += ('\b');
        break;

      case 'f':
        str += ('\f');
        break;

      case 'n':
        str += ('\n');
        break;

      case 'r':
        str += ('\r');
        break;

      case 't':
        str += ('\t');
        break;

      case 'v':
        str += ('\v');
        break;
      }
    }

    else
      str += (file[i]);
  }

  start = endPoint;
  return str;
}

static Value ReadObject (const String &file, size_t &start, unsigned &line)
{
  unsigned lineStart = line;

  Value val;
  Json::Object &object = val.Construct<Json::Object> (Utilities::Hash::SuperFast);

  ++start;

  for (size_t &i = start; i < file.Length(); ++i)
  {
    wchar_t letter = file[i];

    if (isspace (letter))
    {
      if (letter == '\n')
        ++line;

      continue;
    }

    else if (letter == '}')
    {
      ++i;
      return val;
    }

    else if (letter == ',')
      continue;

    else if (letter == '"')
    {
      String key = ReadString (file, start, line);

      while (file[start] != ':')
        ++start;

      ++start;

      object[key] = ReadValue (file, start, line);
    }

    else
      throw Json::ParseError (String::Format ("Invalid character %c on line %i", letter, line));
  }

  throw Json::ParseError (String::Format ("Unterminated object on line %i", lineStart));
}

static Value ReadList (const String &file, size_t &start, unsigned &line)
{
  unsigned lineStart = line;

  Value val;
  Json::List &list = val.Construct<Json::List>();

  ++start;

  for (size_t &i = start; i < file.Length(); ++i)
  {
    char letter = file[i];

    if (isspace (letter))
    {
      if (letter == '\n')
        ++line;

      continue;
    }

    else if (letter == ']')
      return val;

    else if (letter == ',')
      continue;

    else
      list.emplace_back (ReadValue (file, i, line));
  }

  throw Json::ParseError (String::Format ("Unterminated array on line %i", lineStart));
}

// -----------------------------------------------------------------------------

static Value ReadValue (const String &file, size_t &start, unsigned &line)
{
  for (size_t &i = start; i < file.Length(); ++i)
  {
    wchar_t letter = file[i];

    if (isspace (letter))
    {
      if (letter == '\n')
        ++line;

      continue;
    }

    else if (isdigit (letter) || letter == '.' || letter == '-')
      return ReadNumber (file, i, line);

    else
      switch (letter)
      {
      case 'n':
        if (file.SubString (i, 4) == "null")
        {
          start = i + 4;
          return Json::Null (nullptr);
        }

        else
          throw Json::ParseError (String::Format ("Invalid token starting with 'n' on line %i", line));

      case 't':
        if (file.SubString (i, 4) == "true")
        {
          start = i + 4;
          return Json::Boolean (true);
        }

        else
          throw Json::ParseError (String::Format ("Invalid token starting with 't' on line %i", line));

        break;

      case 'f':
        if (file.SubString (i, 5) == "false")
        {
          start = i + 5;
          return Json::Boolean (false);
        }

        else
          throw Json::ParseError (String::Format ("Invalid toekn starting with 'f' on line %i", line));

        break;

      case '"':
        return ReadString (file, i, line);

      case '{':
        return ReadObject (file, i, line);

      case '[':
        return ReadList (file, i, line);

      default:
        throw Json::ParseError (String::Format ("Invalid character %c on line %i", char (letter), line));
      }
  }

  throw Json::ParseError ("Unexpected end of file");
}

// -----------------------------------------------------------------------------

Value Json::ReadFile (const ::String &filename)
{
  ifstream in (filename.CStr());

  // Return an uninitialized variant if the file could not be opened
  if (!in)
    return Value();

  // Read the entire file into a String
  String file = String::LoadFile (filename);

  // Read the value in the file
  size_t start = 0;
  unsigned line = 1;
  return ReadValue (file, start, line);
}

// -----------------------------------------------------------------------------
