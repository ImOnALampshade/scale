// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  Parser for CSV files
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef CSV_PARSER_H
#define CSV_PARSER_H


#include "../HashTable/hashtable.h"
#include "../String/string.h"
#include "common.h"
#include <exception>
#include <vector>
#include <fstream>


// -----------------------------------------------------------------------------

namespace Csv
{
  class ParseError : std::exception
  {
  private:
    String m_errStr;
  public:
    ParseError (const String &errStr) : m_errStr (errStr) {}
    virtual const char *what() throw() { return m_errStr.CStr(); }
  };

  typedef std::vector<Serializer::Value>         Row;
  typedef ::String          String;
  typedef double            Number;
  typedef bool              Boolean;


  class CsvTable;
  std::wostream &operator<< (std::wostream &os, Csv::CsvTable const &tabel);

  class CsvTable
  {
  public:
    Serializer::Value GetValue (unsigned int Row, unsigned int Column) const;
    size_t GetColumnCount (unsigned int Row) const;
    size_t GetRowCount() const;

    void SetValue (unsigned int Row, unsigned int Column, Serializer::Value Value);

    void AppendValue (unsigned int Row, Serializer::Value Value);
    void AddRow();
    void AddRow (Row const &NewRow);

    void Save (String const &Filename) const;

    void SetSize (size_t Width, size_t Height);

    friend std::ostream &operator<< (std::ostream &os, const Csv::CsvTable &tabel);

  private:
    std::vector< Row > m_Values;
  };


  typedef CsvTable          Table;

  Serializer::Value ReadFile (const ::String &filename);
}

// -----------------------------------------------------------------------------

#endif
