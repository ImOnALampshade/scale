// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Common things to all serializers
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SERIALIZERS_COMMON_H
#define SERIALIZERS_COMMON_H

#include "../TypePuns/variant.h"
#include "../String/string.h"

// -----------------------------------------------------------------------------

namespace Serializer
{
  typedef Variant<64> Value;

  struct Result
  {
    enum CommonErrorTypes { END_OF_FILE, UNEXPECTED_CHAR };

    Result();
    Result (CommonErrorTypes error, unsigned line);
    Result (String error, unsigned line);

    bool     WasSuccess;
    String   FileName;
    String   ErrorStr;
    unsigned ErrorLine;
  };
}

// -----------------------------------------------------------------------------

#endif
