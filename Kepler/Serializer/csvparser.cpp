// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  Parser for CSV files
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../HashFunctions/hashfunctions.h"
#include "../Serializer/csvparser.h"
#include <codecvt>
#include <cwchar>
#include <locale>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
using namespace Serializer;

// -----------------------------------------------------------------------------

static Value ReadNumber (const string &file, size_t &start, unsigned &line);
static String ReadString (const string &file, size_t &start, unsigned &line);
static Value ReadValue (const string &file, size_t &start, unsigned &line);

// -----------------------------------------------------------------------------

namespace Csv
{
  std::ostream &operator<< (std::ostream &os, const Csv::CsvTable &tabel)
  {

    for (unsigned int i = 0; i < tabel.GetRowCount(); ++i)
    {
      for (unsigned int j = 0; j < tabel.GetColumnCount (i); ++j)
      {
        Serializer::Value cellValue = tabel.GetValue (i, j);


        if (cellValue.IsType<Csv::Boolean>())
          os << (cellValue.as<Csv::Boolean>() ? "true" : "false");

        if (cellValue.IsType<Csv::Number>())
          os << cellValue.as<Csv::Number>();

        if (cellValue.IsType<Csv::String>())
        {
          String const &str = cellValue.as<Csv::String>();
          string formatted;
          formatted.reserve (str.Length() + 32);

          for (char c : str)
          {
            if (c == '"')
            {
              formatted.append ("\"\"");
            }

            else
            {
              formatted.append (1, c);
            }
          }


          //put quotes around this string if there are commas.
          if (formatted.find (',') != string::npos || formatted.find ('"') != string::npos)
          {
            formatted.insert (0, 1, '"');
            formatted.push_back ('"');
          }


          os << formatted;
        }

        if (j + 1 < tabel.GetColumnCount (i))
        {
          os << ',';
        }

      }

      os << std::endl;
    }

    return os;
  }
}

static Value ReadNumber (const string &file, size_t &start, unsigned &line)
{
  char *endPtr;

  Csv::Number total = strtod (file.c_str() + start, &endPtr);


  //check case where someone types a minus at the begging of a sentence.
  if ( (file.c_str() + start) [0] == '-')
  {
    if (total == 0.0) //0.0 is returned on error. so -0 is going to be treated as a string...
    {
      return ReadString (file, start, line); //should have been interpreted as a string.
    }
  }

  //otherwise return as number like normal.
  //and move offset into file forward.
  start = endPtr - file.c_str();

  return total;
}


static String ReadString (const string &file, size_t &start, unsigned &line)
{
  size_t endPoint  = start;
  String str;
  //Read through string parsing out quotes.
  bool inQuotedString = false;

  if (file[start] =='"')
  {
    inQuotedString = true;
    endPoint += 1;
  }

  do
  {

    if (file[endPoint] == '"')
    {
      if (file[endPoint+1] =='"') //if two quotes are in a row that is paired down to 1 in the string.
      {
        str += file[endPoint]; // append quote
        endPoint+=2; //move over quotes
      }

      else if (inQuotedString)   // if its just 1 single quote that is the end of a string.
      {
        start = endPoint + 1; //skip endquote
        return str;
      }

      else
      {
        throw Csv::ParseError (String ("Unexpected quote in line %i", line));
      }
    }

    else if (file[endPoint] == ',')
    {
      if (inQuotedString) //read it if we are in a quoted string, else that is end of string.
      {
        str += file[endPoint];
        endPoint += 1;
      }

      else
      {
        start = endPoint;
        return str;
      }
    }

    else
    {
      str += file[endPoint];
      endPoint += 1;
    }

  }
  while (file[endPoint] != '\n' && endPoint < file.length());

  start = endPoint;
  return str;

}

// -----------------------------------------------------------------------------
static Value ReadValue (const string &file, size_t &start, unsigned &line)
{
  wchar_t letter = file[start];

  if (isdigit (letter) || letter == '.' || letter == '-')
  {
    return ReadNumber (file, start, line);
  }

  else
    switch (letter)
    {
    case 't':
      if (string (file, start, 4) == "true")
      {
        start = start + 4;
        return Csv::Boolean (true);
      }

      else
      {
        return ReadString (file, start, line);
      }

      break;

    case 'f':
      if (string (file, start, 5) == "false")
      {
        start = start + 5;
        return Csv::Boolean (false);
      }

      else
      {
        return ReadString (file, start, line);
      }

      break;

    default:
      return ReadString (file, start, line);
    }
}

// -----------------------------------------------------------------------------

static Value ReadTable (const string &file, size_t &start, unsigned &line)
{

  Value val;
  Csv::Table &table = val.Construct<Csv::Table>();

  for (size_t &i = start; i < file.size(); ++i)
  {
    Csv::Row newRow;

    while (file[i] != '\n' && i < file.length())
    {
      //skip initial comma if one is present.
      if (file[i] == ',')
      {
        i += 1;
      }

      //read value
      newRow.push_back (ReadValue (file, i, line));
    }

    table.AddRow (newRow);
    line += 1;
  }

  return val;
}

// -----------------------------------------------------------------------------

Value Csv::ReadFile (const ::String &filename)
{
  ifstream in (filename.CStr());

  // Return an uninitialized variant if the file could not be opened
  if (!in)
    return Value();

  auto utf8 = new codecvt_utf8<wchar_t, 0x10ffff, consume_header>();
  in.imbue (locale (in.getloc(), utf8));
  // This is not a memory leak, the locale will delete the codec when it is
  // destroyed.

  // Read the entire file into a wstring
  string file;

  in.seekg (0, std::ios::end);
  file.reserve (size_t (in.tellg()));
  in.seekg (0, std::ios::beg);

  //okay so default construction of these iterators has them pointing to the end of a stream.
  //that is why the last constructor has no arguments...
  file.assign ( (std::istreambuf_iterator<char> (in)) , std::istreambuf_iterator<char>());

  // Read the value in the file
  size_t start = 0;
  unsigned line = 1;
  return ReadTable (file, start, line);
}


Serializer::Value Csv::CsvTable::GetValue (unsigned int Row, unsigned int Column) const
{
  return m_Values[Row][Column];
}

size_t Csv::CsvTable::GetColumnCount (unsigned int Row) const
{
  return m_Values[Row].size();
}

size_t Csv::CsvTable::GetRowCount() const
{
  return m_Values.size();
}

void Csv::CsvTable::SetValue (unsigned int Row, unsigned int Column, Serializer::Value Value)
{
  m_Values[Row][Column] = Value;
}

void Csv::CsvTable::AppendValue (unsigned int Row, Serializer::Value Value)
{
  m_Values[Row].push_back (Value);
}

void Csv::CsvTable::AddRow()
{
  m_Values.push_back (std::vector<Serializer::Value>());
}

void Csv::CsvTable::AddRow (std::vector<Serializer::Value> const &NewRow)
{
  m_Values.push_back (NewRow);
}

void Csv::CsvTable::Save (String const &Filename) const
{
  ofstream outFile (Filename.CStr());


  if (outFile.is_open())
  {
    operator<< (outFile, (*this));
    outFile.close();
  }

}

void Csv::CsvTable::SetSize (size_t Width, size_t Height)
{
  m_Values.resize (Height);

  for (size_t i = 0; i < m_Values.size(); i += 1)
  {
    m_Values[i].resize (Width);
  }
}

// -----------------------------------------------------------------------------
