// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Parser for JSON files
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include "../HashTable/hashtable.h"
#include "../String/string.h"
#include "common.h"
#include <exception>
#include <vector>

// -----------------------------------------------------------------------------

namespace Json
{
  class ParseError : std::exception
  {
  private:
    String m_errStr;
  public:
    ParseError (const String &errStr) : m_errStr (errStr) {}
    virtual const char *what() throw() { return m_errStr.CStr(); }
  };

  typedef ::String                               String;
  typedef double                                 Number;
  typedef bool                                   Boolean;
  typedef void                                  *Null;
  typedef HashTable<String, Serializer::Value>   Object;
  typedef std::vector<Serializer::Value>         List;
  typedef Serializer::Value                      Value;

  Serializer::Value ReadFile (const ::String &filename);
}

// -----------------------------------------------------------------------------

#endif
