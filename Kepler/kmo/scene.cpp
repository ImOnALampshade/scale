// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Kepler Model Object
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../glminclude.h"
#include "../Logger/logger.h"
#include "scene.h"
#include <cstdio>
#include <cstdint>
#include <fbxsdk.h>

using namespace std;
using namespace glm;

// -----------------------------------------------------------------------------

static vec2 FbxToGlm (FbxVector2 v) { return vec2 (v[0], v[1]); }
static vec4 FbxToGlm (FbxVector4 v) { return vec4 (v[0], v[1], v[2], v[3]); }

static void AddNodeToScene (Scene *s, FbxNode *node);

// -----------------------------------------------------------------------------

bool Scene::LoadFbx (String filename)
{
  bool failed = false;
  FbxManager *manager = FbxManager::Create();

  FbxIOSettings *ioSettings = FbxIOSettings::Create (manager, IOSROOT);

  manager->SetIOSettings (ioSettings);

  FbxScene *scene = FbxScene::Create (manager, "");
  FbxImporter *importer = FbxImporter::Create (manager, "");

  if (!importer->Initialize (filename.CStr(), -1, ioSettings))
    failed = true;

  if (!importer->Import (scene))
    failed = true;

  // If the import failed, return false to the caller
  if (failed)
  {
    manager->Destroy();
    return false;
  }

  // Get the root node of the scene
  FbxNode *root = scene->GetRootNode();

  BoundingRadius = 0.0f;

  for (int i = 0; i < root->GetChildCount(); ++i)
    AddNodeToScene (this, root->GetChild (i));

  manager->Destroy();

  return true;
}

// -----------------------------------------------------------------------------

static void AddMeshToScene (Scene *scene, FbxMesh *mesh)
{
  for (int i = 0; i < mesh->GetPolygonCount(); ++i)
  {
    // Skip this mesh if it is not triangulated
    if (mesh->GetPolygonSize (i) != 3)
    {
      // TODO: Fix Nontriangulated model
      return;
    }
  }

  // Loop through all the polygons
  for (int i = 0; i < mesh->GetPolygonCount(); ++i)
  {
    Scene::Triangle tri;

    // For each vertex in the triangle
    for (int j = 0; j < 3; ++j)
    {
      FbxVector4 coord (0, 0, 0, 0);
      FbxVector4 normal (0, 0, 0, 0);
      FbxVector2 texcoord (0, 0);
      bool hasnouvs;

      // Get the coordinate, normal, and UV (named map1, maya's default) for the vertex
      coord = mesh->GetControlPointAt (mesh->GetPolygonVertex (i, j));
      mesh->GetPolygonVertexNormal (i, j, normal);
      mesh->GetPolygonVertexUV (i, j, "map1", texcoord, hasnouvs);

      // Create the vertex with the coordinate and normal loaded from the file
      Scene::Vertex vert;
      vert.Coordinate = FbxToGlm (coord).xyz;
      vert.Normal     = FbxToGlm (normal).xyz;
      vert.Texcoord   = FbxToGlm (texcoord).xy;

      // Add the index of this vertex in the vertex buffer to the triangle
      tri.Vertices[j] = scene->Vertices.size();

      // Add the vertex to the list of vertices
      scene->Vertices.emplace_back (vert);

      // If the distance from the current coordinate to the center is larger than
      // the bounding radius, update the bounding radius.
      scene->BoundingRadius = max (length (vert.Coordinate), scene->BoundingRadius);
    }

    // Add the triangle to the list of triangles
    scene->Triangles.emplace_back (tri);
  }
}

static void AddNodeToScene (Scene *scene, FbxNode *node)
{
  // If this node is a mesh, then do mesh things to it
  if (FbxMesh *mesh = node->GetMesh())
    AddMeshToScene (scene, mesh);

  // Add all child nodes to the scene
  for (int i = 0; i < node->GetChildCount(); ++i)
    AddNodeToScene (scene, node->GetChild (i));
}

// -----------------------------------------------------------------------------
