// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Kepler Model Object
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_MODEL_OBJECT_H
#define KEPLER_MODEL_OBJECT_H

#include "../glminclude.h"
#include "../String/string.h"
#include <initializer_list>
#include <vector>

// -----------------------------------------------------------------------------

#pragma pack(push, 1)
struct Scene
{
  struct Vertex
  {
    glm::vec3 Coordinate;
    glm::vec2 Texcoord;
    glm::vec3 Normal;

    Vertex() { }
    Vertex (glm::vec3 coord, glm::vec2 tcoord, glm::vec3 norm) :
      Coordinate (coord), Texcoord (tcoord), Normal (norm)
    {
    }
  };

  struct Triangle
  {
    unsigned Vertices[3];

    Triangle() { }
    Triangle (unsigned i1, unsigned i2, unsigned i3)
    {
      Vertices[0] = i1;
      Vertices[1] = i2;
      Vertices[2] = i3;
    }

    Triangle &operator= (std::initializer_list<unsigned> &l)
    {
      unsigned *v = Vertices;

      for (unsigned i : l)
        * (v++) = i;

      return *this;
    }
  };

  std::vector<Vertex>   Vertices;
  std::vector<Triangle> Triangles;
  float                 BoundingRadius;

  bool LoadFbx (String filename);
};
#pragma pack(pop)

// -----------------------------------------------------------------------------

#endif
