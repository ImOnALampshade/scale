// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  The allocator manager manages all of the different sized object allocators
//  that will be needed. They are referance counted so that one object allocator
//  can be used between different parts of a program that need the same size
//  allocator.
//
//  -Referance Counted
//  -Determine Appropriate MemoryAllocator to use
//  -Keep Track Of Exisiting Allocators
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "allocatormanager.h"
#include "..\ObjectAllocator\objectallocator.h"
#include "..\ObjectAllocator\memorymanagement.h"

#include <algorithm>
#include <cassert>

namespace Utilities
{

  namespace MemoryManagement
  {

// -----------------------------------------------------------------------------
// Intialize static member variables of allocator manager.
// -----------------------------------------------------------------------------
    bool   AllocatorManager::s_Initialized = false;
    ObjectAllocator AllocatorManager::s_AllocatorCreator;
    AllocatorManager::Config AllocatorManager::s_Config;
    AllocatorManager::AllocatorTable AllocatorManager::s_Allocators;

// -----------------------------------------------------------------------------
// public functions of the AllocatorManager
// -----------------------------------------------------------------------------
    ObjectAllocator* AllocatorManager::GiveAllocator(size_t ObjectSize, unsigned int Alignment)
    {

      size_t actualObjectSize = GetAllocatorObjectSize( ObjectSize );

      auto first = s_Allocators.begin();
      auto last  = s_Allocators.end();

      first = std::lower_bound(first, last, actualObjectSize, CompareAllocatorRow);

      size_t maxObjSize = static_cast<size_t>(ObjectSize + (ObjectSize * s_Config.SizeAccuracy));

      bool foundAllocator;

      if( s_Config.ChooseMethod == Threshold )
      {
        foundAllocator = (first != last) && (*first).first >= actualObjectSize && (*first).first <= maxObjSize;
      }
      else
      {
        foundAllocator = (first != last) && actualObjectSize == (*first).first;
      }

      //verify that lower_bound found a row with the correct object size.
      if (foundAllocator)
      {
        //we found an object allocator group that is the correct size
        // so now we need to find one with the correct alignment.

        auto firstInstance = first->second.begin();
        auto lastInstance  = first->second.end();
        //find the first instance which has the same alignment as what is desired.
        firstInstance = std::lower_bound(firstInstance, lastInstance, Alignment, CompareAllocatorInstance);
        if (firstInstance != lastInstance && Alignment == (*firstInstance).ObjectAllocator->Configuration().Alignment )
        {
          //we found an object allocator with a matching size and alignment! return it! increase refcount!
          firstInstance->ReferanceCount += 1;
          //verify that it has the correct alignment.
          assert( firstInstance->ObjectAllocator->Configuration().Alignment == Alignment );
          return firstInstance->ObjectAllocator;
        }
        else // we did not find an allocator with matching alignment
        {
          
          //create allocator with proper alignment and insert it.
          ObjectAllocator* newOA = CreateAllocator(actualObjectSize, Alignment);
          //insert the object allocator into the row. it has one referance to it.
          first->second.insert( firstInstance, AllocatorInstance( newOA, 1 ) );

          return newOA; // give the new allocator to the user.
        }

      }
      else // their is no object allocators with this size.
      {
        //put a new row into the list.
        first = s_Allocators.insert(first, AllocatorRow(actualObjectSize, AllocatorList()));
        //create a new allocator push it into the row.
        ObjectAllocator* newOA = CreateAllocator(actualObjectSize, Alignment);
        first->second.reserve(4); //reserve space for four allocator instances.
        first->second.push_back( AllocatorInstance(newOA, 1 ) ); //add in allocator instance.

        return newOA; // give the new allocator to the user.
      }
    }
    
    void AllocatorManager::ReleaseAllocator(ObjectAllocator* Allocator)
    {
      auto first = s_Allocators.begin();
      auto last = s_Allocators.end();
      size_t objSize = Allocator->Configuration().ObjectSize;
      first = std::lower_bound(first, last, objSize, CompareAllocatorRow);

      //assert that an allocator of this size should have been found.
      assert( first != last && objSize == (*first).first );
      auto fInstance = first->second.begin();
      auto lInstance = first->second.end();
      unsigned int alignment = Allocator->Configuration().Alignment;
      fInstance = std::lower_bound(fInstance, lInstance, alignment, CompareAllocatorInstance);
      //assert that this allocator was found.
      assert(fInstance != lInstance && Allocator == (*fInstance).ObjectAllocator);

      //one less referance to it now.
      fInstance->ReferanceCount -= 1;
      
      if( fInstance->ReferanceCount < 1 ) // if no more people are referancing it, time to free it.
      {
        //clean up the memory of the ObjectAllocator
        DestroyAllocator( fInstance->ObjectAllocator );
        //erase it from the allocator row.
        first->second.erase( fInstance );
      }

    }
    
    void AllocatorManager::Initialize(Config const& Configuration)
    {
      assert( s_Initialized == false );

      s_Config = Configuration;

      ObjectAllocator::Config config;
      config.Alignment = 0; //alignmnet doesnt matter for OAs.
      config.DebugOn = s_Config.DebugOn;
      config.ObjectSize = sizeof(ObjectAllocator); // this holds all OAs.
      config.ObjectsPerPage = 16; // how many object allocators we gonna have???
      config.PadBytes = s_Config.PadBytes;

      s_AllocatorCreator.Initialize( config );
      s_Allocators.reserve( 16 ); //Reserve space for 16 different sizes of object allocators.
      s_Initialized = true;
    }

// -----------------------------------------------------------------------------
// private functions of the allocator manager.
// -----------------------------------------------------------------------------
    size_t AllocatorManager::GetAllocatorObjectSize(size_t DesiredObjectSize)
    {
      switch( s_Config.ChooseMethod )
      {
        case Exact:
        {
          return DesiredObjectSize;
        }

        case PowerOfTwo:
        {
          return Utilities::MemoryManagement::NextPowerOf2( DesiredObjectSize );
        }

        case Threshold:
        {
          return DesiredObjectSize;
        }

        default:
        {
          return DesiredObjectSize;
        }
      }
    }

    ObjectAllocator* AllocatorManager::CreateAllocator(size_t ObjectSize, unsigned int Alignment)
    {
      assert( s_Initialized == true );

      ObjectAllocator::Config config;
      config.Alignment = Alignment;
      config.ObjectSize = ObjectSize;
      config.ObjectsPerPage = 128;
      config.DebugOn = s_Config.DebugOn;
      config.PadBytes =s_Config.PadBytes;

      //placement new the allocator.
      ObjectAllocator* newOA = new (s_AllocatorCreator.OA_ALLOCATE()) ObjectAllocator(config);
      assert( newOA != nullptr );
      return newOA;
    }

    void AllocatorManager::DestroyAllocator(ObjectAllocator* OA)
    {
      //call deconstructor of object allocator.
      OA->~ObjectAllocator();
      //return memory to the allocator creator.
      s_AllocatorCreator.OA_DEALLOCATE( OA );
    }

    bool AllocatorManager::CompareAllocatorRow(AllocatorRow const& Row, size_t const& objectSize)
    {
      return Row.first < objectSize;
    }

    bool AllocatorManager::CompareAllocatorInstance(AllocatorInstance const& Instance, unsigned int const& Alignment)
    {
      return Instance.ObjectAllocator->Configuration().Alignment < Alignment;
    }


  }// namespace MemoryManagement

}// namespace Utilities
