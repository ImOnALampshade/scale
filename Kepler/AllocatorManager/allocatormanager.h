// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  The allocator manager manages all of the different sized object allocators
//  that will be needed. They are referance counted so that one object allocator
//  can be used between different parts of a program that need the same size
//  allocator.
//
//  -Referance Counted
//  -Determine Appropriate MemoryAllocator to use
//  -Keep Track Of Exisiting Allocators
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ALLOCATORMANAGER_H
#define ALLOCATORMANAGER_H

#include <vector>

class ObjectAllocator;

namespace Utilities
{

namespace MemoryManagement
{

// -----------------------------------------------------------------------------
class AllocatorManager
{
public:

  // -----------------------------------------------------------------------------
  enum SizeMethod
  {
    Exact,
    PowerOfTwo,
    Threshold
  };

  // -----------------------------------------------------------------------------
  struct Config
  {
    Config():DebugOn(false), PadBytes(0), ChooseMethod( Exact ), SizeAccuracy(0.0f)
    {
    
    }

    bool         DebugOn;       //Create Allocators with debug on?
    unsigned int PadBytes;      //number of bytes to pad each object with when debug is on.
    SizeMethod   ChooseMethod;  //How the Allocator Manager will choose correct size
    float        SizeAccuracy;  //0.0 to N. GiveAllocator will match you with an allocator that is 0% to N% larger than ObjectSize (threshold mode)
  };

private:
  // -----------------------------------------------------------------------------
  struct AllocatorInstance
  {
    AllocatorInstance(): ObjectAllocator(nullptr), ReferanceCount(0){}

    AllocatorInstance(ObjectAllocator* OA, unsigned int RefCount)
      : ObjectAllocator(OA), ReferanceCount(RefCount){}

    ObjectAllocator* ObjectAllocator;
    unsigned int     ReferanceCount;
  };

// -----------------------------------------------------------------------------
  typedef std::vector<AllocatorInstance> AllocatorList;
  typedef std::pair<size_t, AllocatorList> AllocatorRow;
  typedef std::vector<AllocatorRow> AllocatorTable;

// -----------------------------------------------------------------------------
public:
  static ObjectAllocator* GiveAllocator( size_t ObjectSize, unsigned int Alignment );
  static void ReleaseAllocator( ObjectAllocator* Allocator );
  static void Initialize( Config const& Configuration );

// -----------------------------------------------------------------------------
private:
  static size_t GetAllocatorObjectSize( size_t DesiredObjectSize );
  static ObjectAllocator* CreateAllocator(size_t ObjectSize, unsigned int Alignment );
  static void DestroyAllocator( ObjectAllocator* OA );
  static bool CompareAllocatorRow(AllocatorRow const&, size_t const&);
  static bool CompareAllocatorInstance(AllocatorInstance const&, unsigned int const&);

// -----------------------------------------------------------------------------
private:
  static bool             s_Initialized;      //whether or not we intialized the allocator
  static Config           s_Config;           //configuration of the allocator manager
  static ObjectAllocator  s_AllocatorCreator; //The master Object Allocator.
  static AllocatorTable   s_Allocators;       //The master object

};

}// namespace MemoryManagement

}// namespace Utilities


#endif