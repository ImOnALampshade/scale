//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#define CATCH_CONFIG_RUNNER
#include <catch.hpp>
#include "../AllocatorManager/allocatormanager.h"

int main (int argc, char **argv)
{
  Utilities::MemoryManagement::AllocatorManager::Config config;

  config.ChooseMethod = Utilities::MemoryManagement::AllocatorManager::PowerOfTwo;
  config.DebugOn = false;

  Utilities::MemoryManagement::AllocatorManager::Initialize (config);
  return Catch::Session().run (argc, argv);
}
