//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <catch.hpp>
#include <iostream>
#include <functional>
#include "../Serializer/csvparser.h"

using namespace std;

void PrintCsv (const Serializer::Value &val)
{

  if (val.IsType<Csv::Table>())
  {
    Csv::Table const &tabel = val;

    for (unsigned int i = 0; i < tabel.GetRowCount(); ++i)
    {
      for (unsigned int j = 0; j < tabel.GetColumnCount (i); ++j)
      {
        Serializer::Value cellValue = tabel.GetValue (i, j);


        if (cellValue.IsType<Csv::Boolean>())
          wcout << (cellValue.as<Csv::Boolean>() ? "true" : "false");

        if (cellValue.IsType<Csv::Number>())
          wcout << cellValue.as<Csv::Number>();

        if (cellValue.IsType<Csv::String>())
          wcout << cellValue.as<Csv::String>();

        if (j + 1 < tabel.GetColumnCount (i))
        {
          wcout << ',';
        }

      }

      wcout << std::endl;
    }

  }

}

TEST_CASE ("CSV Tests 1")
{
  SECTION ("Basic parsing")
  {
    Serializer::Value result;

    try
    {
      result = Csv::ReadFile ("test1.csv");
    }

    catch (Csv::ParseError &e)
    {
      wcout << e.what();
    }

    REQUIRE (result.IsConstructed());
    PrintCsv (result);

    result.as<Csv::Table>().SetValue (2,2, Csv::String ("WhatsUpDock"));

    result.as<Csv::Table>().Save ("test1_copy.csv");

  }
}