//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <catch.hpp>
#include <iostream>
#include <functional>
#include "../Serializer/jsonparser.h"

using namespace std;

void PrintJson (const Serializer::Value &val)
{
  if (val.IsType<Json::Boolean>())
    wcout << val.as<Json::Boolean>() ? "true" : "false";

  if (val.IsType<Json::Number>())
    wcout << val.as<Json::Number>();

  if (val.IsType<Json::String>())
    wcout << val.as<Json::String>();

  if (val.IsType<Json::Null>())
    wcout << "(null)";

  if (val.IsType<Json::List>())
  {
    wcout << "[ ";

    for (const Serializer::Value &item : val.as<Json::List>())
    {
      PrintJson (item);
      wcout << ", ";
    }

    wcout << " ]";
  }

  if (val.IsType<Json::Object>())
  {
    wcout << "{ ";

    const Json::Object &obj = val;

    for (auto &it = obj.begin(); it != obj.end(); ++it)
    {
      auto &pair = *it;
      wcout << pair.key << ": ";
      PrintJson (pair.value);
      wcout << ", ";
    }

    wcout << " }";
  }
}

TEST_CASE ("JSON Tests 1")
{
  SECTION ("Basic parsing")
  {
    Serializer::Value result;

    try
    {
      result = Json::ReadFile ("test1.json");
    }

    catch (Json::ParseError &e)
    {
      wcout << e.what();
    }

    REQUIRE (result.IsConstructed());
    PrintJson (result);
  }
}