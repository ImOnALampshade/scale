//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <catch.hpp>
#include <string>
#include "../TypePuns/variant.h"

class TestClass
{
public:
  static unsigned total;
  int val;

  TestClass (int val_) :
    val (val_)
  {
    ++total;
  }

  TestClass (const TestClass &that) :
    val (that.val)
  {
    ++total;
  }

  ~TestClass()
  {
    --total;
  }
};

unsigned TestClass::total = 0;

TEST_CASE ("Variant construction")
{
  const int SIZE = 64;
  Variant<SIZE> var;

  SECTION ("Default")
  {
    CHECK (!var.IsConstructed());
  }

  SECTION ("Primitive")
  {
    Variant<SIZE> var ( (int) 4);

    CHECK (var.as<int>() == 4);
  }

  SECTION ("Object")
  {
    unsigned total_before = TestClass::total;

    {
      Variant<SIZE> var = TestClass (4);

      unsigned total_after = TestClass::total;

      CHECK (total_before != total_after);
      CHECK (var.as<TestClass>().val == 4);
    }

    CHECK (total_before == TestClass::total);
  }

  SECTION ("Construct method")
  {
    std::string str = "__Hello, world!";

    var.Construct<std::string> (str.begin() + 2, str.end());

    CHECK (var.as<std::string>() == "Hello, world!");
  }
}