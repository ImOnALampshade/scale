// -------------------------------------------------------------------------------------------------
// Collection of hashing function for strings
// Howard Hughes
// -------------------------------------------------------------------------------------------------

#ifndef HASHFUNCTIONS_H
#define HASHFUNCTIONS_H

#include <cstdint>
#include "../String/string.h"

typedef std::uint32_t (*StringHasher) (const String &);

// -------------------------------------------------------------------------------------------------

namespace Utilities
{
  namespace Hash
  {
    // ---------------------------------------------------------------------------------------------

    std::uint32_t FNV1 (const String &str);
    std::uint32_t FNV1A (const String &str);
    std::uint32_t SuperFast (const String &str);
    std::uint32_t PJW (const String &str);
    std::uint32_t Simple (const String &str);
    std::uint32_t Zero (const String &str);

    std::uint32_t TypeInfo (const type_info *const &);

    // ---------------------------------------------------------------------------------------------
  }
}

// -------------------------------------------------------------------------------------------------

#endif
