// -------------------------------------------------------------------------------------------------
// Collection of hashing function for strings
// Howard Hughes
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -------------------------------------------------------------------------------------------------

#include "hashfunctions.h"

namespace Utilities
{
  namespace Hash
  {
    // ---------------------------------------------------------------------------------------------

    namespace
    {
      const std::uint32_t FNV_PRIME = 16777619;
      const std::uint32_t FNV_BASE  = 2166136261;
    }

    std::uint32_t FNV1 (const String &str)
    {
      std::uint32_t hash = FNV_BASE;

      for (char c : str.GetUnderlying())
      {
        hash *= FNV_PRIME;
        hash ^= c;
      }

      return hash;
    }

    std::uint32_t FNV1A (const String &str)
    {
      std::uint32_t hash = FNV_BASE;

      for (char c : str.GetUnderlying())
      {
        hash ^= c;
        hash *= FNV_PRIME;
      }

      return hash;
    }

    // ---------------------------------------------------------------------------------------------

#define GET_16_BITS(d) (*((const std::uint16_t *) (d)))

    std::uint32_t SuperFast (const String &str)
    {
      size_t      len = str.Length();
      const char *data = str.CStr();

      std::uint32_t hash = std::uint32_t (len), tmp;
      int rem;

      if (len == 0) return 0;

      rem = len & 3;
      len >>= 2;

      /* Main loop */
      for (; len > 0; len--) {
        hash += GET_16_BITS (data);
        tmp = (GET_16_BITS (data + 2) << 11) ^ hash;
        hash = (hash << 16) ^ tmp;
        data += 2 * sizeof (uint16_t);
        hash += hash >> 11;
      }

      /* Handle end cases */
      switch (rem) {
      case 3:
        hash += GET_16_BITS (data);
        hash ^= hash << 16;
        hash ^= ( (signed char) data[sizeof (uint16_t)]) << 18;
        hash += hash >> 11;
        break;

      case 2:
        hash += GET_16_BITS (data);
        hash ^= hash << 11;
        hash += hash >> 17;
        break;

      case 1:
        hash += (signed char) *data;
        hash ^= hash << 10;
        hash += hash >> 1;
      }

      /* Force "avalanching" of final 127 bits */
      hash ^= hash << 3;
      hash += hash >> 5;
      hash ^= hash << 4;
      hash += hash >> 17;
      hash ^= hash << 25;
      hash += hash >> 6;

      return hash;
    }

    // ---------------------------------------------------------------------------------------------

    std::uint32_t PJW (const String &str)
    {
      std::uint32_t hash = 0;

      for (char c : str.GetUnderlying())
      {
        hash = (hash << 4);
        hash += c;

        int bits = hash & 0xF0000000;

        if (bits)
        {
          hash = hash ^ (bits >> 24);
          hash = hash ^ bits;
        }
      }

      return hash;
    }

    // ---------------------------------------------------------------------------------------------

    std::uint32_t Simple (const String &str)
    {
      unsigned hash = 0;

      for (char c : str.GetUnderlying())
        hash += c;

      return hash;
    }

    // ---------------------------------------------------------------------------------------------

    std::uint32_t Zero (const String &)
    {
      return 0;
    }

    std::uint32_t TypeInfo (const type_info *const &info)
    {
      return std::uint32_t (info->hash_code());
    }

    // ---------------------------------------------------------------------------------------------
  }
}

// -------------------------------------------------------------------------------------------------
