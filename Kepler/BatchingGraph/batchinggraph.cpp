// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Scene graph class
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "batchinggraph.h"

using namespace std;

// -----------------------------------------------------------------------------

void BatchNodeWithChild::Child (std::shared_ptr<BatchNode> node)
{
  m_child = node;
}

std::shared_ptr<BatchNode> BatchNodeWithChild::Child() const
{
  return m_child;
}

void BatchNodeWithChild::DrawChild (BatchGraph *graph)
{
  m_child->Draw (graph);
}

// -----------------------------------------------------------------------------

std::vector<std::shared_ptr<BatchNode>> &BatchNodeWithChildren::Children()
{
  return m_children;
}

const std::vector<std::shared_ptr<BatchNode>> &BatchNodeWithChildren::Children() const
{
  return m_children;
}

void BatchNodeWithChildren::AddChild (std::shared_ptr<BatchNode> node)
{
  m_children.push_back (node);
}

void BatchNodeWithChildren::DrawChildren (BatchGraph *graph)
{
  for (size_t i = 0; i < m_children.size(); ++i)
    m_children[i]->Draw (graph);
}

// -----------------------------------------------------------------------------

BatchGraph::BatchGraph (unsigned propBagSize) :
  m_propertyBag (propBagSize)
{
}

void BatchGraph::Draw()
{
  m_root->Draw (this);
}

void BatchGraph::ResizePropBag (unsigned size)
{
  m_propertyBag.resize (size);
}

shared_ptr<BatchNode> BatchGraph::Root()
{
  return m_root;
}

const shared_ptr<BatchNode> BatchGraph::Root() const
{
  return m_root;
}

void BatchGraph::Root (shared_ptr<BatchNode> node)
{
  m_root = node;
}

void *BatchGraph::Property (unsigned name)
{
  return m_propertyBag[name];
}

void BatchGraph::Property (unsigned name, void *value)
{
  m_propertyBag[name] = value;
}

// -----------------------------------------------------------------------------
