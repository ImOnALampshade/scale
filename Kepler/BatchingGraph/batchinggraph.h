// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Scene graph class
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef BATCH_GRAPH_H
#define BATCH_GRAPH_H

#include <memory>
#include <vector>

// -----------------------------------------------------------------------------

class BatchGraph;

class BatchNode
{
public:
  virtual ~BatchNode() { }
  virtual void Draw (BatchGraph *graph) = 0;

  friend class BatchGraph;
};

class BatchNodeWithChild : public BatchNode
{
public:
  void Child (std::shared_ptr<BatchNode> node);
  std::shared_ptr<BatchNode> Child() const;

  void DrawChild (BatchGraph *graph);

private:
  std::shared_ptr<BatchNode> m_child;
};

class BatchNodeWithChildren : public BatchNode
{
public:
  std::vector<std::shared_ptr<BatchNode>> &Children();
  const std::vector<std::shared_ptr<BatchNode>> &Children() const;

  void AddChild (std::shared_ptr<BatchNode> node);

  void DrawChildren (BatchGraph *graph);

private:
  std::vector<std::shared_ptr<BatchNode>> m_children;
};

// -----------------------------------------------------------------------------

class BatchGraph
{
public:
  BatchGraph (unsigned propBagSize = 0);

  void Draw();
  void ResizePropBag (unsigned size);

  std::shared_ptr<BatchNode> Root();
  const std::shared_ptr<BatchNode> Root() const;
  void Root (std::shared_ptr<BatchNode> node);

  void *Property (unsigned name);
  void Property (unsigned name, void *value);

private:
  std::shared_ptr<BatchNode> m_root;
  std::vector<void *> m_propertyBag;
};

// -----------------------------------------------------------------------------

#endif
