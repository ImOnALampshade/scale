// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Include file for Python
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_PY_INCLUDE_H
#define KEPLER_PY_INCLUDE_H

#pragma once

#ifndef HAVE_ROUND
#define HAVE_ROUND
#endif

#include <python/Python.h>
#include <python/structmember.h>

#endif
