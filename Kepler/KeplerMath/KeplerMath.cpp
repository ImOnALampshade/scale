// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  GLM helper methods to make scripting less sucky
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "KeplerMath.h"
#include "../MetaTyping/bindhelpers.h"
#include "../Logger/logger.h"

#define TOLERANCE 0.01f

#define AreEqual(a, b) (abs((a)-(b)) < TOLERANCE)

static Vec3 s_worldForward = Vec3 (glm::vec3 (0, 0, -1));
static Vec3 s_worldUp = Vec3 (glm::vec3 (0, 1, 0));
static Vec3 s_worldRight = Vec3(glm::vec3(1, 0, 0));

static Vec3 *s_worldAxes = nullptr;
static Quat *s_worldRotations = nullptr;

static void initWorldAxes();
static int  findNearestAxis(Vec3 &fromVector);

// -----------------------------------------------------------------------------
// Vec4
float Vec4::length() { return glm::length (*reinterpret_cast<glm::vec4 *> (this)); }
void Vec4::normalize() { glm::normalize (*reinterpret_cast<glm::vec4 *> (this)); }
Vec4 Vec4::normalized() { auto ret = (glm::vec4 (*this)); return glm::normalize (ret); }
Vec4 Vec4::lerp (Vec4 target, float factor) { return (*this) * (1 - factor) + (target) * (factor); }
float Vec4::dot (Vec4 other) { return (x*other.x + y*other.y + z*other.z + w*other.w); }

Vec4 Vec4::mults(float scalar) { return Vec4(x*scalar, y*scalar, z*scalar, w*scalar); }
Vec4 Vec4::mult(Vec4 other) { return (asGlm()) * other; }
Vec4 Vec4::add(Vec4 other) { return (asGlm()) + other; }
Vec4 Vec4::subtr(Vec4 other) { return (asGlm()) - other; }

glm::vec4 &Vec4::asGlm() { return *reinterpret_cast<glm::vec4 *> (this); }

String Vec4::ToString()
{
  return String::Format("Vec4 (%g, %g, %g, %g)", x, y, z, w);
}

void Vec4::CreateMeta()
{
  META_CREATE (Vec4, "KeplerMath");

  META_CTOR (Vec4, float, float, float, float);

  META_ADD_METHOD (Vec4, length);
  META_ADD_METHOD (Vec4, normalize);
  META_ADD_METHOD (Vec4, normalized);
  META_ADD_METHOD (Vec4, lerp);
  META_ADD_METHOD (Vec4, dot);
  META_ADD_METHOD (Vec4, ToString);

  META_ADD_METHOD (Vec4, mults);
  META_ADD_METHOD (Vec4, subtr);
  META_ADD_METHOD (Vec4, mult);
  META_ADD_METHOD (Vec4, add);

  META_ADD_MEMBER (Vec4, x);
  META_ADD_MEMBER (Vec4, y);
  META_ADD_MEMBER (Vec4, z);
  META_ADD_MEMBER (Vec4, w);

  META_FINALIZE (Vec4);
}


// -----------------------------------------------------------------------------
// Vec3
float Vec3::length() { return glm::length (asGlm()); }
void Vec3::normalize() { glm::normalize (*reinterpret_cast<glm::vec3 *> (this)); }
Vec3 Vec3::normalized()
{
  auto ret = (glm::vec3 (*this));
  ret = glm::normalize (ret);
  if (ret.x != ret.x)
  {
    ret.x = 0;
    ret.y = 0;
    ret.z = 0;
  }
  return ret;
}
Vec3 Vec3::lerp (Vec3 target, float factor) { return (*this) * (1 - factor) + (target) * (factor); }
float Vec3::dot (Vec3 other) { return (x*other.x + y*other.y + z*other.z); }
Vec3 Vec3::cross (Vec3 other) { return glm::cross ( (*this), other); }
glm::vec3 &Vec3::asGlm() { return *reinterpret_cast<glm::vec3 *> (this); }

Vec3 Vec3::mults(float scalar) { return Vec3(x*scalar, y*scalar, z*scalar); }
Vec3 Vec3::mult(Vec3 other) { return (asGlm()) * other; }
Vec3 Vec3::add(Vec3 other) { return (asGlm()) + other; }
Vec3 Vec3::subtr(Vec3 other) { return (asGlm()) - other; }

String Vec3::ToString()
{ 
  return String::Format ("Vec3 (%g, %g, %g)", x, y, z);
}

void Vec3::CreateMeta()
{
  META_CREATE (Vec3, "KeplerMath");

  META_CTOR (Vec3, float, float, float);

  META_ADD_METHOD (Vec3, length);
  META_ADD_METHOD (Vec3, normalize);
  META_ADD_METHOD (Vec3, normalized);
  META_ADD_METHOD (Vec3, lerp);
  META_ADD_METHOD (Vec3, dot);
  META_ADD_METHOD (Vec3, cross);
  META_ADD_METHOD (Vec3, ToString);

  META_ADD_METHOD (Vec3, mults);
  META_ADD_METHOD (Vec3, subtr);
  META_ADD_METHOD (Vec3, mult);
  META_ADD_METHOD (Vec3, add);

  META_ADD_MEMBER (Vec3, x);
  META_ADD_MEMBER (Vec3, y);
  META_ADD_MEMBER (Vec3, z);

  META_FINALIZE (Vec3);
}

// -----------------------------------------------------------------------------
// Vec2
float Vec2::length() { return glm::length (*reinterpret_cast<glm::vec2 *> (this)); }
void  Vec2::normalize() { glm::normalize (*reinterpret_cast<glm::vec2 *> (this)); }
Vec2  Vec2::normalized() { auto ret = (glm::vec2 (*this)); return glm::normalize (ret); }
Vec2  Vec2::lerp (Vec2 target, float factor) { return (*this) * (1 - factor) + (target) * (factor); }
float Vec2::dot (Vec2 other) { return (x*other.x + y*other.y); }
glm::vec2 &Vec2::asGlm() { return *reinterpret_cast<glm::vec2 *> (this); }

Vec2 Vec2::mults(float scalar) { return Vec2(x*scalar, y*scalar); }
Vec2 Vec2::mult(Vec2 other) { return (asGlm()) * other; }
Vec2 Vec2::add(Vec2 other) { return (asGlm()) + other; }
Vec2 Vec2::subtr(Vec2 other) { return (asGlm()) - other; }

String Vec2::ToString()
{
  return String::Format("Vec2 (%g, %g)", x, y);
}

void Vec2::CreateMeta()
{
  META_CREATE (Vec2, "KeplerMath");

  META_CTOR (Vec2, float, float);

  META_ADD_METHOD (Vec2, length);
  META_ADD_METHOD (Vec2, normalize);
  META_ADD_METHOD (Vec2, normalized);
  META_ADD_METHOD (Vec2, lerp);
  META_ADD_METHOD (Vec2, dot);
  META_ADD_METHOD (Vec2, ToString);

  META_ADD_METHOD (Vec2, mults);
  META_ADD_METHOD (Vec2, subtr);
  META_ADD_METHOD (Vec2, mult);
  META_ADD_METHOD (Vec2, add);

  META_ADD_MEMBER (Vec2, x);
  META_ADD_MEMBER (Vec2, y);

  META_FINALIZE (Vec2);
}

// -----------------------------------------------------------------------------
// Quat
float Quat::length() { return glm::length (*reinterpret_cast<glm::quat *> (this)); }
Quat  Quat::lerp (Quat target, float factor) { return (*this) * (1 - factor) + (target) * (factor); }
glm::quat &Quat::asGlm() { return *reinterpret_cast<glm::quat *> (this); }
Quat Quat::mult(Quat other) { return (*this) * other; }

void  Quat::normalize()
{
  BREAK_IF(length() == 0, "Quat of length 0 exists.");
  asGlm() = glm::normalize(asGlm());
}
Quat  Quat::normalized()
{
  auto ret = Quat(*this);
  ret.normalize();
  return ret;
}

Quat Quat::inverted()
{
  return glm::inverse (asGlm());
}

Vec3  Quat::rotate (Vec3 original)
{

  return asGlm() * original;

  //glm::quat retQ;
  //retQ.x = original.x;
  //retQ.y = original.y;
  //retQ.z = original.z;
  //retQ.w = 0;
  //
  //retQ = asGlm() * retQ * inverted();
  //return Vec3 (retQ.x, retQ.y, retQ.z);
}


Quat  Quat::slerp (Quat target, float factor)
{
  return Quat(glm::slerp(asGlm(), target, factor));
}

Vec3  Quat::axis()
{
  glm::vec3 axis = glm::axis(asGlm());
  BREAK_IF(axis.x != axis.x, "Quat has no axis");
  return axis;
}

void Quat::axis (Vec3 value)
{
  BREAK_IF(value.x != value.x, "Quat has no axis");
  (*this) = FromAxisAngle (value, angle());
}

float Quat::angle()
{
  float angle =glm::angle(normalized().asGlm());
  BREAK_IF(angle != angle, "Quaternion failed"); // NaN. You're not wrong, GLM, you're just an asshole.
  if (angle > PI)
    angle -= 2 * PI;
  return angle;
}

void Quat::angle (float value)
{
  (*this) = FromAxisAngle (axis(), value);
}

Vec3 Quat::eulerAngles()
{
  return glm::eulerAngles (asGlm());
}

Quat Quat::FromAxisAngle (Vec3 axis, float angle)
{
  BREAK_IF(axis.x != axis.x, "Invalid axis for AxisAngle quat");
  return glm::angleAxis (angle, axis);
}

Quat Quat::FromBasis (Vec3 rawForward, Vec3 rawUp, Vec3 rawRight)
{
  if (s_worldAxes == nullptr)
    initWorldAxes();

  // Zero'th, normalize our axes
  if (AreEqual(rawForward.length(), 0))
  {
    rawForward = rawUp.cross(rawRight);
    if (AreEqual(rawForward.length(), 0))
      return Quat();
  }
  if (AreEqual(rawUp.length(), 0))
  {
    rawUp = glm::cross(rawRight, rawForward);
    if (rawUp.x != rawUp.x)
      rawUp = s_worldRotations[2].rotate(rawForward);
  }
  BREAK_IF(rawForward.x != rawForward.x, "Cannot build a quaternion from a NaN vector."); // Check NaN's.
  BREAK_IF(rawUp.x != rawUp.x, "Cannot build a quaternion from a NaN vector."); // Check NaN's.

  Vec3 forward, up, right;
  
  forward = rawForward.normalized();

  // First: Make sure the basis is orthogonal
  right = Vec3(glm::cross (rawUp, -forward)).normalized();
  if (right.x != right.x) // NaN
  {
    // Up is invalid
    right = rawRight.normalized();
    up = glm::cross(-forward, right);
    up.normalize();
  }
  else
  {
    up = Vec3(glm::cross(-forward, right)).normalized();
  }
  

  // Use that to compute two quaternions: One to get the forward in the right place,
  //  and another to get Up in the right place.
  Quat q1, q2;

  //printf(String(forward.ToString() + "\n" + up.ToString() + "\n----\n").CStr());

  int index = findNearestAxis(rawForward);
  Quat &q0 = s_worldRotations[index];
  Vec3 currentForward = s_worldAxes[index];

  // First rotation
  float dprod = forward.dot(currentForward);
  if (abs(dprod) < 0.9999f)
  {
    Vec3 axis = Vec3(glm::cross(currentForward, forward)).normalized();
    float angle = acos(currentForward.dot(forward));
    q1 = Quat::FromAxisAngle(axis.normalized(), angle);
  }
  else if (forward.dot(currentForward) <= -1)
  {
    q1 = Quat::FromAxisAngle(up, 180*DEGREE);
  }

  // Second rotation
  Vec3 inverseUp = Quat(q0*q1).inverted().rotate (up);

  if (abs(inverseUp.dot(s_worldUp)) < 1)
  {
    float angle = atan2(-inverseUp.x, inverseUp.y);
    q2 = Quat::FromAxisAngle(s_worldForward, -angle);
  }
  else if (inverseUp.dot(s_worldUp) <= -1)
  {
    q2 = Quat::FromAxisAngle(s_worldForward, 180*DEGREE);
  }


  Quat ret = Quat(q0*q1*q2);
  assert(ret.w == ret.w); // Checking for NaN


  //// Add this check back in if quats ever start misbehaving again.
  //#ifdef _DEBUG
  //  {
  //    Vec3 rotated = ret.rotate(s_worldForward);
  //    Vec3 upRotated = ret.rotate(s_worldUp);
  //    if (!AreEqual(rotated.dot(forward), 1) || !AreEqual(upRotated.dot(up), 1))
  //    {
  //      printf("QUAT FROM BASIS FAILED\n");
  //      printf("q0:  %s\n", q0.ToString().CStr());
  //      printf("q1:  %s\n", q1.ToString().CStr());
  //      printf("q2:  %s\n", q2.ToString().CStr());
  //      printf("----------------------------------\n");
  //      printf("Ret: %s\n", ret.ToString().CStr());
  //      printf("f0: %s\nf1: %s\n", forward.ToString().CStr(), rotated.ToString().CStr());
  //      printf("u0: %s\nu1: %s\n", up.ToString().CStr(), Vec3(ret.rotate(s_worldUp)).ToString().CStr());
  //      printf("d : %s\n", Quat::FromAxisAngle(s_worldForward, 180 * DEGREE).rotate(upRotated).ToString().CStr());
  //      __debugbreak();
  //      return(Quat::FromBasis(rawForward, rawUp, rawRight));
  //      BREAK("Quaternion is wrong.");
  //    }
  //  }
  //#endif

  return ret;
}

Quat Quat::FromEulerAngles (Vec3 pitchYawRoll)
{
  Quat q1 = Quat::FromAxisAngle(Vec3(1, 0, 0), pitchYawRoll.x);
  Quat q2 = Quat::FromAxisAngle(Vec3(0, 1, 0), pitchYawRoll.y);
  Quat q3 = Quat::FromAxisAngle(Vec3(0, 0, 1), pitchYawRoll.z);
  Quat ret = Quat(q1*q2*q3);

  return ret;
}

Quat Quat::between (Quat other)
{
  return Quat(this->inverted() * other);
}

Quat Quat::toward (Quat other, float maxAngle)
{
  Quat ret = between (other);

  if (abs(ret.angle()) > maxAngle)
  {
    ret = Quat::FromAxisAngle(ret.axis(), maxAngle);
  }

  return Quat(asGlm() * ret);
}

String Quat::ToString()
{
  Vec3 myAxis = this->axis();
  return String::Format("Quat: (%g %g %g %g): Rotation by %g around (%g, %g, %g)", x, y, z, w, angle(),
    myAxis.x, myAxis.y, myAxis.z);
}

void initWorldAxes()
{
  s_worldAxes = new Vec3[6];
  s_worldRotations = new Quat[6];

  Quat forwardQuat = Quat();
  Quat upQuat = Quat::FromAxisAngle(s_worldRight, -90 * DEGREE);
  Quat rightQuat = Quat::FromAxisAngle(s_worldUp,  90 * DEGREE);

  s_worldRotations[0] = Quat();
  s_worldRotations[1] = Quat() * Quat::FromAxisAngle(s_worldUp, 180*DEGREE);
  s_worldRotations[2] = upQuat;
  s_worldRotations[3] = upQuat.inverted();
  s_worldRotations[4] = rightQuat;
  s_worldRotations[5] = rightQuat.inverted();

  for (int i = 0; i < 6; ++i)
  {
    s_worldRotations[i] = s_worldRotations[i].normalized();
    s_worldAxes[i] = s_worldRotations[i].rotate(s_worldForward).normalized();
  }
}

int findNearestAxis(Vec3 &vec)
{
  //for (int i = 0; i < 2; ++i)
  //  if (glm::dot(vec.asGlm(), s_worldAxes[i].asGlm()) >= 0.0f)
  //  {
  //    return i;
  //  }
  //BREAK_IF(vec.length() != 0, "");
  return 0;
}

void Quat::CreateMeta()
{
  META_CREATE (Quat, "KeplerMath");

  META_CTOR (Quat);

  META_ADD_METHOD(Quat, inverted);
  META_ADD_METHOD(Quat, length);
  META_ADD_METHOD(Quat, normalize);
  META_ADD_METHOD(Quat, normalized);
  META_ADD_METHOD(Quat, lerp);

  META_ADD_METHOD(Quat, rotate);
  META_ADD_METHOD(Quat, slerp);
  META_ADD_METHOD(Quat, eulerAngles);
  META_ADD_METHOD(Quat, between);
  META_ADD_METHOD(Quat, toward);

  META_ADD_METHOD(Quat, ToString);

  META_ADD_METHOD(Quat, mult);

  META_ADD_PROP(Quat, axis);
  META_ADD_PROP(Quat, angle);

  META_ADD_FUNCTION(Quat, "QuatFromAxisAngle", Quat::FromAxisAngle);
  META_ADD_FUNCTION(Quat, "QuatFromBasis", Quat::FromBasis);
  META_ADD_FUNCTION(Quat, "QuatFromEulerAngles", Quat::FromEulerAngles);

  META_ADD_MEMBER(Quat, x);
  META_ADD_MEMBER(Quat, y);
  META_ADD_MEMBER(Quat, z);
  META_ADD_MEMBER(Quat, w);

  META_FINALIZE (Quat);
}