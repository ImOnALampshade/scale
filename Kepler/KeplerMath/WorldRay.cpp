// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Binding structure for Raycasts
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "WorldRay.h"
#include "../MetaTyping/bindhelpers.h"

WorldRay::WorldRay(Vec3 origin, Vec3 direction)
  : Origin(origin), Direction(direction)
{
  Direction = Direction.normalized();
}

Vec3 WorldRay::SampleAtDistance(float distance)
{
  return Origin + Direction * distance;
}

String WorldRay::ToString()
{
  return String("WorldRay: (Origin: ") + Origin.ToString() + ", Direction: " + Direction.ToString();
}

void WorldRay::CreateMeta()
{
  META_CREATE(WorldRay, "KeplerMath");
  META_CTOR(WorldRay, Vec3, Vec3);

  META_ADD_MEMBER(WorldRay, Origin);
  META_ADD_MEMBER(WorldRay, Direction);
  META_ADD_METHOD(WorldRay, SampleAtDistance);
  META_ADD_METHOD(WorldRay, ToString);

  META_FINALIZE(WorldRay);
}


