// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  GLM helper methods to make scripting less sucky
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_VEC4_H_
#define KEPLER_VEC4_H_

#include "../glminclude.h"
#include "../String/string.h"

struct Vec4 : glm::vec4
{
  Vec4 (glm::vec4 fromGlm = glm::vec4 (0, 0, 0, 0))
    : glm::vec4 (fromGlm) {}
  Vec4 (float x, float y, float z, float w)
    : glm::vec4 (x, y, z, w) {}

  float length();
  void normalize();
  Vec4 normalized();
  Vec4 lerp (Vec4 target, float factor);
  float dot (Vec4 other);

  Vec4 mults(float scalar);
  Vec4 mult(Vec4 other);
  Vec4 add(Vec4 other);
  Vec4 subtr(Vec4 other);

  glm::vec4 &asGlm();
  operator glm::vec4 &() {
    return asGlm();
  }

  String ToString();

  static void CreateMeta();
};

struct Vec3 : glm::vec3
{
  Vec3 (glm::vec3 fromGlm = glm::vec3 (0, 0, 0))
    : glm::vec3 (fromGlm) {}
  Vec3 (float x, float y, float z)
    : glm::vec3 (x, y, z) {}

  float length();
  void normalize();
  Vec3 normalized();
  Vec3 lerp (Vec3 target, float factor);
  float dot (Vec3 other);
  Vec3 cross (Vec3 other);

  Vec3 mults(float scalar);
  Vec3 mult(Vec3 other);
  Vec3 add(Vec3 other);
  Vec3 subtr(Vec3 other);

  glm::vec3 &asGlm();
  operator glm::vec3 &() {
    return asGlm();
  }

  String ToString();

  static void CreateMeta();
};

struct Vec2 : glm::vec2
{
  Vec2 (glm::vec2 fromGlm = glm::vec2 (0, 0))
    : glm::vec2 (fromGlm) {}
  Vec2 (float x, float y)
    : glm::vec2 (x, y) {}

  float length();
  void normalize();
  Vec2 normalized();
  Vec2 lerp (Vec2 target, float factor);
  float dot (Vec2 other);

  Vec2 mults(float scalar);
  Vec2 mult(Vec2 other);
  Vec2 add(Vec2 other);
  Vec2 subtr(Vec2 other);

  glm::vec2 &asGlm();
  operator glm::vec2 &() {
    return asGlm();
  }

  String ToString();

  static void CreateMeta();
};

struct Quat : glm::quat
{
  Quat (glm::quat fromGlm = glm::quat (1, 0, 0, 0))
    : glm::quat(fromGlm) { normalize(); }

  float length();
  void  normalize();
  Quat  normalized();
  Quat  lerp (Quat target, float factor);

  Quat inverted();

  Vec3  rotate (Vec3 original);
  Quat  slerp (Quat target, float factor);
  Vec3  axis();
  Vec3  eulerAngles();
  void axis (Vec3 value);
  float angle();
  void angle(float value);

  Quat mult(Quat other);

  Quat between (Quat other);
  Quat toward (Quat other, float maxAngle);

  glm::quat &asGlm();
  operator glm::quat &() {
    return asGlm();
  }

  String ToString();

  static Quat FromAxisAngle (Vec3 axis, float angle);
  static Quat FromBasis (Vec3 forward, Vec3 up, Vec3 right);
  static Quat FromEulerAngles (Vec3 pitchYawRoll);

  static void CreateMeta();
};

typedef glm::mat4 Mat4;
typedef glm::mat3 Mat3;
typedef glm::mat2 Mat2;

#endif