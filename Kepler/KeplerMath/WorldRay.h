// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Binding structure for Raycasts
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_RAY_H_
#define KEPLER_RAY_H_

#include "../KeplerMath/KeplerMath.h"

struct WorldRay
{
  WorldRay(Vec3 origin = Vec3(0,0,0), Vec3 direction=Vec3(0,0,-1));

  Vec3 Origin;
  Vec3 Direction;

  Vec3 SampleAtDistance(float distance);

  String ToString();

  static void CreateMeta();
};

#endif