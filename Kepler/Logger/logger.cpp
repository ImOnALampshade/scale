// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Asynchronys file logging system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "logger.h"
#include <atomic>
#include <condition_variable>
#include <cstdio>
#include <queue>
#include <thread>



// -----------------------------------------------------------------------------

using namespace std;

#ifdef _WIN32
// Being on Win32 is different from compiling with microsofts compiler!
void winlog (String msg);
#endif

namespace
{
  thread             LoggerThread;
  FILE              *LogFile;
  queue<String>      LogStrings;
  mutex              LogLock;
  condition_variable LogGate;
  atomic_bool        LogEnd;

  void LogString (String str)
  {
    if (LogFile) fputs (str.c_str(), LogFile);

    #ifdef _WIN32
    winlog (str);
    #endif
  }

  void LogWorker()
  {
    for (; !LogEnd; this_thread::yield())
    {
      unique_lock<mutex> lck (LogLock);
      LogGate.wait_for (lck, chrono::seconds (1));

      while (!LogStrings.empty())
      {
        LogString (LogStrings.front());
        LogStrings.pop();
      }
    }
  }

  void AddString (String s)
  {
    {
      lock_guard<mutex> _ (LogLock);
      LogStrings.push (s);
    }
    LogGate.notify_all();
  }
}

// -----------------------------------------------------------------------------

namespace Logger
{
  void Log (String message)
  {
    AddString (message + '\n');
  }

  // ---------------------------------------------------------------------------

  void Break (String message)
  {
    AddString (message + '\n');
  }

  // ---------------------------------------------------------------------------

  void Error (String message)
  {
    LogEnd = true;
    LogGate.notify_all();

    LoggerThread.join();

    LogString (message + '\n');

    fclose (LogFile);
    LogFile = nullptr;
  }

  // ---------------------------------------------------------------------------

  void Init()
  {
    LogFile = fopen ("kepler.log", "wt");
#ifndef _DEBUG
    freopen("keplercout.log", "w", stdout);
    freopen("keplercerr.log", "w", stderr);
#endif

    if (!LogFile)
      BREAKPOINT();

    LogEnd = false;

    LoggerThread = std::thread (LogWorker);
  }

  void Free()
  {
    LogEnd = true;
    LogGate.notify_all();

    LoggerThread.join();

    fclose (LogFile);
    LogFile = nullptr;
  }
}

// -----------------------------------------------------------------------------
