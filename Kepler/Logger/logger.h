// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Asynchronys file logging system
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ASYNC_LOGGER_H
#define ASYNC_LOGGER_H

#include "../String/string.h"

#ifdef _MSC_VER

#define BREAKPOINT() do { __debugbreak(); } while(0)

#elif defined __linux

#include <signal.h>
#define BREAKPOINT() do { raise(SIGTRAP); } while(0)

#else

#define BREAKPOINT()

#endif

#ifdef _DEBUG
#define LOG(msg)            do{ Logger::Log(msg); } while(0)
#define LOG_IF(expr, msg)   do{ if(!!(expr)) LOG(msg); } while(0)
#define BREAK(msg)          do{ Logger::Break(msg); BREAKPOINT();  } while(0)
#define BREAK_IF(expr, msg) do{ if(!!(expr)) { Logger::Break(msg); BREAKPOINT(); } } while(0)
#define ERROR(msg)          do{ Logger::Error(msg); BREAKPOINT(); abort(); } while(0)
#define ERROR_IF(expr, msg) do{ if(!!(expr)) { Logger::Error(msg); BREAKPOINT(); abort(); } } while(0)
#else
#define LOG(msg)
#define LOG_IF(expr, msg)
#define BREAK(msg)
#define BREAK_IF(expr, msg)
#define ERROR(msg)
#define ERROR_IF(expr, msg)
#endif


// -----------------------------------------------------------------------------

namespace Logger
{
  void Log (String message);
  void Break (String message);
  void Error (String message);

  void Init();
  void Free();
}

// -----------------------------------------------------------------------------

#endif
