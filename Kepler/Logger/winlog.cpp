// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Adapter function for OutputDebugString
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifdef _WIN32

#include "../String/string.h"
#include <Windows.h>

void winlog (String msg)
{
  std::wstring wide = msg.ToStdWideString();
  OutputDebugStringW (wide.c_str());
}

#endif
