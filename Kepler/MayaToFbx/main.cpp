// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Entry point for the maya to KMO converter command line utility
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include <iostream>
#include <ostream>
#include <string>
#include <maya/MLibrary.h>
#include <maya/MFileIO.h>
#include <maya/MString.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include <setjmp.h>

using namespace std;

bool CheckStatus (const MStatus &status, MString fn_name)
{
  if (!status)
  {
    cout << fn_name << "  " << status.errorString() << endl;
    return false;
  }

  else
    return true;
}


bool Convert (MString fileIn, MString fileOut)
{
  MStatus status;

  status = MFileIO::open (fileIn);

  if (!CheckStatus (status, "MFileIO::open"))
    return false;

  MString exportcmd ("FBXExport -f \"");
  exportcmd += fileOut;
  exportcmd += "\"";

  MString commands[] =
  {
    "FBXExportUpAxis Y",
    "FBXExportTriangulate -v true",
    "FBXExportLights -v true",
    "FBXExportTangents -v true",
    "FBXExportSmoothMesh -v true",
    exportcmd
  };

  for (MString command : commands)
  {
    status = MGlobal::executeCommand (command);

    if (!CheckStatus (status, command))
      return false;
  }

  return true;
}

int main (int argc, const char **argv)
{
  atexit ([] (void) { getc (stdin); });

  printf ("Intializing maya...\n");
  MStatus status = MLibrary::initialize (argv[0], true);

  if (!status)
  {
    printf ("Error initializing maya: ");
    status.perror ("MLibrary::initialize");
    return -1;
  }

  status = MGlobal::executeCommand ("loadPlugin fbxmaya.mll;");

  if (!status)
  {
    printf ("Error loading maya FBX plugin: ");
    status.perror ("MGlobal::executeCommand");
    return -1;
  }

  printf ("Maya initialized\n");

  Convert ("Player Pod.mb", "Player Pod.fbx");

  MLibrary::cleanup();
  return 0;
}
