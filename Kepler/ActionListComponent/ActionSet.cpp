// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Action which runs a set of actions at once
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include <algorithm>

namespace Actions
{
  ActionSet::ActionSet(bool garbageCollected /*= true*/)
    : ActionGroup(garbageCollected)
  {

  }

  void ActionSet::Update(float Dt)
  {
    if (this == nullptr)
      return;

    // Update the first incomplete action in the list
    for (size_t i = 0; i < m_actions.size(); ++i)
      if (!m_actions[i]->Finished())
        m_actions[i]->Update(Dt);

    while (!m_actions.empty() && m_actions.front()->Finished())
      m_actions.pop_front();

    // If there are no incomplete actions, we're done.
    if (m_actions.empty() && m_garbage)
      delete(this);
  }
}
