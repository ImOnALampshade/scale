// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Abstract class for an Action which interpolates a property of an object.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ACTION_PROP_H_
#define ACTION_PROP_H_
#pragma once

namespace Actions
{
  template<typename T>
  class ActionProp : public Action
  {
  public:
    ActionProp(MetaObject target, String prop, T endval, float duration, int easeFunc);

    virtual bool Finished();
    virtual void Cancel();
    virtual void Skip();
    void Update(float Dt);

  protected:
    void Interpolate(float percentDone);

  private:
    MetaObject m_target;
    String m_property;
    T m_endVal;
    float m_duration;
    int m_easeFunc;
    float m_timeSoFar;
    T m_startVal;
    
  };

  template<typename T>
  Actions::ActionProp<T>::ActionProp(MetaObject target, String prop, T endval, float duration, int easeFunc)
    : m_target(target), m_property(prop), m_endVal(endval), m_duration(duration), m_easeFunc(easeFunc),
    m_timeSoFar(0.0f)
  {
    
  }

  template<typename T>
  bool Actions::ActionProp<T>::Finished()
  {
    if (this == nullptr) return true;
    return m_timeSoFar < m_duration;
  }

  template<typename T>
  void Actions::ActionProp<T>::Cancel()
  {
    delete(this);
  }

  template<typename T>
  void Actions::ActionProp<T>::Skip()
  {
    this->Interpolate(1.0f);
    delete(this);
  }

  template<typename T>
  void Actions::ActionProp<T>::Update(float Dt)
  {
    if (m_timeSoFar > m_duration || !m_target.Contents.IsValid())
    {
      delete(this);
      return;
    }
    else if (m_timeSoFar == 0)
    {
      m_startVal = m_target.Meta->Get(m_property, m_target.Contents.operator->()).as<T>();
    }

    m_timeSoFar += Dt;
    float percentDone = 0;
    switch (m_easeFunc)
    {
      // Currently, only linear ease is supported.
    case 0:
      percentDone = (m_timeSoFar / m_duration);
      break;
    default:
      percentDone = (m_timeSoFar / m_duration);
      break;
    }
    percentDone = glm::clamp(percentDone, 0.f, 1.f);
    Interpolate(percentDone);
  }

  //template<>
  //void Actions::ActionProp<Quat>::Interpolate(float percentDone)
  //{
  //  Quat targetVal = m_startVal.slerp(m_endVal, percentDone);
  //  m_target.GetMeta()->Set(m_property, m_target.Value.operator->(), &targetVal);
  //}

  template<typename T>
  void Actions::ActionProp<T>::Interpolate(float percentDone)
  {
    T targetVal = (m_startVal * (1 - percentDone)) + (m_endVal * percentDone);
    m_target.Meta->Set(m_property, m_target.Contents.operator->(), &targetVal);
  }
}

#endif