// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Action which calls a function, then reports completion.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ACTION_CALL_H_
#define ACTION_CALL_H_
#pragma once

// NYI

namespace Actions
{
  class ActionCall : Action
  {
    //ActionCall();

    virtual bool Finished();
    virtual void Cancel();
    virtual void Skip();
    virtual void Update(float Dt);

    static void CreateMeta();
  };
}

#endif