// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Action which runs a set of actions at once
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ACTION_SET_H_
#define ACTION_SET_H_
#pragma once


namespace Actions
{
  class ActionSet : public ActionGroup
  {
  public:
    ActionSet(bool garbageCollected = true);

    virtual void Update(float Dt);
  };
}

#endif