// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "load_factories.h"
#include "../KeplerCore/compositefactory.h"

using namespace Actions;

void ActionList::LoadFactories()
{
  CREATE_FACTORY(Actions);
}

void ActionList::CreateMeta()
{
  ActionsComponent::CreateMeta();
  Action::CreateMeta();
  ActionGroup::CreateMeta();
}
