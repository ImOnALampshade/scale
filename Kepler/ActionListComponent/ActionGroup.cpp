// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Abstract class for an Action which runs a set of actions.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------+


#include "stdafx.h"

namespace Actions
{
  bool ActionGroup::Finished()
  {
    // Actions die as soon as they are completed.
    if (this == nullptr)
      return true;
    else return m_actions.empty();
  }

  void ActionGroup::Cancel()
  {
    // Actions die as soon as they are completed.
    // A completed action may still be canceled,
    // it just doesn't do anything.
    if (this == nullptr)
      return;

    for (Handle<Action> action : m_actions)
      action->Cancel();

    if (m_garbage)
      delete(this);
    else
      m_actions.clear();
  }

  void ActionGroup::Skip()
  {
    // Actions die as soon as they are completed.
    // A completed action may still be skipped,
    // it just doesn't do anything.
    if (this == nullptr)
      return;

    for (Handle<Action> action : m_actions)
      if(action.IsValid())
        action->Skip();
    m_actions.clear();

    if (m_garbage)
      delete(this);
    else
      m_actions.clear();
  }

  void ActionGroup::Add(Handle<Action> action)
  {
    // Revival: A completed action ActionGroup can have an action added
    // to it, which will bring it back to life.
    if (this == nullptr)
    {
      LOG("Action revival is not yet implemented");
      return;
    }

    m_actions.push_back(action);
  }

  Handle<Action> ActionGroup::AddPropertyFloat(MetaObject target, String prop, float endVal, float duration, int ease)
  {
    Action *ret = new ActionProp<float>(target, prop, endVal, duration, ease);
    Add(ret->GetHandle());
    return ret->GetHandle();
  }

  Handle<Action> ActionGroup::AddPropertyVec3(MetaObject target, String prop, Vec3 endVal, float duration, int ease)
  {
    Action *ret = new ActionProp<Vec3>(target, prop, endVal, duration, ease);
    Add(ret->GetHandle());
    return ret->GetHandle();
  }

  Handle<Action> ActionGroup::AddPropertyVec4(MetaObject target, String prop, Vec4 endVal, float duration, int ease)
  {
    Action *ret = new ActionProp<Vec4>(target, prop, endVal, duration, ease);
    Add(ret->GetHandle());
    return ret->GetHandle();
  }
  
  Handle<Action> ActionGroup::AddPropertyColor(MetaObject target, String prop, Vec4 endVal, float duration, int ease)
  {
    Action *ret = new ActionProp<Vec4>(target, prop, endVal, duration, ease);
    Add(ret->GetHandle());
    return ret->GetHandle();
  }

  Handle<Action> ActionGroup::AddPropertyQuat(MetaObject target, String prop, Quat endVal, float duration, int ease)
  {
    Action *ret = new ActionProp<Quat>(target, prop, endVal, duration, ease);
    Add(ret->GetHandle());
    return ret->GetHandle();
  }

  Handle<ActionGroup> ActionGroup::AddSequence()
  {
    Action *ret = new ActionSequence();
    Add(ret->GetHandle());
    return *reinterpret_cast<Handle<ActionGroup> *>(&ret->GetHandle());
  }

  Handle<ActionGroup> ActionGroup::AddSet()
  {
    Action *ret = new ActionSet();
    Add(ret->GetHandle());
    return *reinterpret_cast<Handle<ActionGroup> *>(&ret->GetHandle());
  }

  Handle<ActionGroup> ActionGroup::GetHandleGroup()
  {
    Handle<Action> ret_ = this->GetHandle();
    Handle<ActionGroup> ret = *reinterpret_cast<Handle<ActionGroup> *>(&ret_);
    return ret;
  }

  void ActionGroup::CreateMeta()
  {
    META_INHERIT(ActionGroup, "Actions", "Action");
    META_HANDLE(ActionGroup);

    META_ADD_METHOD(ActionGroup, AddPropertyFloat);
    META_ADD_METHOD(ActionGroup, AddPropertyVec3);
    META_ADD_METHOD(ActionGroup, AddPropertyVec4);
    META_ADD_METHOD(ActionGroup, AddPropertyColor);
    META_ADD_METHOD(ActionGroup, AddPropertyQuat);

    META_FINALIZE(ActionGroup);
  }

}
