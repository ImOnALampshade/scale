// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Abstract class for an Action which runs a set of actions.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ACTION_GROUP_H_
#define ACTION_GROUP_H_
#pragma once


namespace Actions
{
  class ActionGroup : public Action
  {
  public:
    virtual bool Finished();
    virtual void Cancel();
    virtual void Skip();
    virtual void Update(float Dt) = 0;

    void Add(Handle<Action> action);

    Handle<Action> AddPropertyFloat(MetaObject target, String prop, float endVal, float duration, int ease);
    Handle<Action> AddPropertyVec3 (MetaObject target, String prop, Vec3 endVal, float duration, int ease);
    Handle<Action> AddPropertyVec4 (MetaObject target, String prop, Vec4 endVal, float duration, int ease);
    Handle<Action> AddPropertyColor(MetaObject target, String prop, Vec4 endVal, float duration, int ease);
    Handle<Action> AddPropertyQuat (MetaObject target, String prop, Quat endVal, float duration, int ease);
    Handle<ActionGroup> AddSequence();
    Handle<ActionGroup> AddSet();

    Handle<ActionGroup> GetHandleGroup();

    static void CreateMeta();

  protected:
    ActionGroup(bool garbageCollected = true) : m_garbage(garbageCollected) { }
    // Set to true if this sequence should be disposed after all its members are complete
    // false otherwise.
    bool m_garbage;

    std::deque<Handle<Action>> m_actions;
  };
}

#endif