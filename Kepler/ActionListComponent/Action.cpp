//
//  Interface class for actions.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
using namespace Actions;

namespace Actions
{
  void Action::CreateMeta()
  {
    META_CREATE(Action, "Actions");
    META_HANDLE(Action);

    META_ADD_PROP_READONLY(Action, Finished);
    META_ADD_METHOD(Action, Skip);
    META_ADD_METHOD(Action, Cancel);

    META_FINALIZE(Action);
  }

  Handle<Action> Action::GetHandle()
  {
    return ::Handle<Action>(m_handle);
  }

  Action::~Action()
  {
    // Set the handle's pointer to null (No one can use it anymore) and
    // release our reference to it
    m_handle->ptr = nullptr;
    --m_handle->refcount;

    if (m_handle->refcount == 0)
      delete m_handle;
  }

  Action::Action()
    : m_handle(new HandleInner<Action>(this))
  {

  }

}