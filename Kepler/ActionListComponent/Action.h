// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Interface class for actions.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef I_ACTION_H_
#define I_ACTION_H_
#pragma once

namespace Actions
{
  class Action
  {
  public:
    Action();

    virtual bool Finished() = 0;
    virtual void Cancel() = 0;
    virtual void Skip() = 0;
    virtual void Update(float Dt) = 0;

    static void CreateMeta();
    
    Handle<Action> GetHandle();

    virtual ~Action();
  private:
    HandleInner<Action> *m_handle;
  };
}

#endif