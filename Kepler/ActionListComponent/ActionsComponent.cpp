// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Component for action lists.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"

namespace Actions
{
  ActionsComponent::ActionsComponent()
    : m_set(false), m_sequence(false)
  {

  }

  void ActionsComponent::Initialize(const Json::Object &jsonData)
  {
    Owner()->RegisterObserver("LogicUpdate",
      std::function<void(float) >([this](float Dt){this->update(Dt); }));
  }

  ::Handle<ActionGroup> ActionsComponent::Set()
  {
    return m_set.GetHandleGroup();
  }

  ::Handle<ActionGroup> ActionsComponent::Sequence()
  {
    return m_sequence.GetHandleGroup();
  }

  void ActionsComponent::update(float Dt)
  {
    m_sequence.Update(Dt);
    m_set.Update(Dt);
  }

  META_REGISTER_FUNCTION(Actions)
  {
    META_INHERIT(ActionsComponent, "Kepler", "Component");
    META_HANDLE(ActionsComponent);

    META_ADD_METHOD(ActionsComponent, Sequence);
    META_ADD_METHOD(ActionsComponent, Set);

    // Call this to finalize the metatype for a component.
    META_FINALIZE_PTR(ActionsComponent, MetaPtr);
  }
}