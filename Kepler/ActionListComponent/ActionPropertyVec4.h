// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Abstract class for an Action which interpolates a property of an object.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ACTION_PROP_VEC4_H_
#define ACTION_PROP_VEC4_H_
#pragma once

namespace Actions
{
  class ActionPropVec4 : public Action
  {
  public:
    ActionPropVec4(KeplerPyHandle target, String prop, float endval, float duration, int easeFuc);

    virtual bool Finished();
    virtual void Cancel();
    virtual void Skip();
    void Update(float Dt);
  private:
    KeplerPyHandle m_target;
    String m_property;
    float m_startVal;
    float m_endval;
    float m_duration;
    float m_timeSoFar;
    int m_easeFunc;

  };
}

#endif