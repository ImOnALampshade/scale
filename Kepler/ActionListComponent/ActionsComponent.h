// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Component for action lists.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef ACTIONS_COMPONENT_H
#define ACTIONS_COMPONENT_H
#pragma once

#include "../KeplerCore/component.h"

namespace Actions
{
  class ActionsComponent : public Component
  {
  public:
    ActionsComponent();

    ::Handle<ActionGroup> Set();
    ::Handle<ActionGroup> Sequence();

    virtual void Initialize(const Json::Object &jsonData);

    COMPONENT_META
  private:
    void update(float Dt);

    ActionSequence m_sequence;
    ActionSet m_set;
  };
}

#endif
