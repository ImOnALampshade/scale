// -----------------------------------------------------------------------------
//  Author: Timothy Chapman
//
//  Action which runs a set of actions in sequence.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"


namespace Actions
{

  ActionSequence::ActionSequence(bool garbageCollected /* = true */)
    : ActionGroup(garbageCollected)
  {

  }

  void ActionSequence::Update(float Dt)
  {
    if (this == nullptr)
      return;

    // Update the first incomplete action in the list
    while(!m_actions.empty())
      if (m_actions.front().IsValid())
      {
      m_actions.front()->Update(Dt);
        break;
      }
      else
      {
        m_actions.pop_front();
      }

    // If there are no incomplete actions, we're done.
    if (m_actions.empty() && m_garbage)
      delete(this);
  }
}
