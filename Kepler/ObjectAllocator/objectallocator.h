// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  An object allocator which allocates pages of memory to speed up
//  memory management.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef OBJECTALLOCATOR_H
#define OBJECTALLOCATOR_H

#include <cstddef>

using std::size_t;

#define OA_ALLOCATE() Allocate( __LINE__, __FILE__ )
#define OA_DEALLOCATE(Object) Deallocate(Object, __LINE__, __FILE__ )
// -----------------------------------------------------------------------------

class ObjectAllocator
{
public:

  //this is neat, strongly typed enum. courtesy of c++ 11
  enum MemoryPattern : unsigned char
  {
    Unallocated = 0xEE,
    Allocated   = 0xBB,
    Freed       = 0xFF,
    Padding     = 0xDD,
    Alignment   = 0xAA
  };

// -----------------------------------------------------------------------------

  struct Config
  {
    Config(void) : ObjectSize(0), ObjectsPerPage(0), Alignment(0), PadBytes(0),
                    DebugOn(false) {};

    size_t ObjectSize;     // Size of allocated objects in bytes
    size_t ObjectsPerPage; // Objects to store per page.
    size_t Alignment;      // Alignment of object data.
    size_t PadBytes;       // Number of bytes before and after each object.
    bool   DebugOn;        // Whether or not debug code is run.
  };

// -----------------------------------------------------------------------------

  struct Stats
  {
    Stats(void) : PageSize(0), FreeObjects(0), ObjectsInUse(0), PagesInUse(0),
                  MostObjects(0), Allocations(0), Deallocations(0) {};

    size_t   PageSize;        // Size of a page including all headers, padding, etc.
    unsigned FreeObjects;     // Number of objects on the free list
    unsigned ObjectsInUse;    // Number of objects in use by client
    unsigned PagesInUse;      // Number of pages allocated
    unsigned MostObjects;     // Most objects in use by client at one time
    unsigned Allocations;     // Total requests to allocate memory
    unsigned Deallocations;   // Total requests to free memory
    unsigned ObjectTotalSize; // Total size of object (padding/alignment and all)
  };


  // -----------------------------------------------------------------------------
  //A class to make header manipulation a little easier.
  class Page
  {
  public:
    Page(size_t Alignment);

    Page* NextPage();
    void  NextPage(Page* NextPage);
    size_t AlignmentBytes() const;
    char* PageStart();

  private:
    size_t m_AlignmentBytes;
    Page* m_NextPage; //the pointer to the memory
  };


private:
  // -----------------------------------------------------------------------------
  struct MemoryBlockSizes
  {
    size_t TotalPadBytes;
    size_t BlockFront;
    size_t BlockRear;
    size_t LeftAlign;
    size_t InterAlign;
  };

  // -----------------------------------------------------------------------------
  struct ByteOffsets
  {
    size_t Data;
    size_t FrontPadding;
    size_t RearPadding;
    size_t Alignment;
    size_t LeftAlignment;
  };

// -----------------------------------------------------------------------------
//A class to make header manipulation a little easier.
  class Header
  {
  public:
    char const* FilePath() const;
    void FilePath(char const* FilePath);
    unsigned LineNumber() const;
    void LineNumber(unsigned LineNumber);
    bool InUse() const;
    void InUse(bool InUse);

  private:
    char const* m_FilePath;
    unsigned    m_LineNumber;
    bool        m_InUse;
  };


// -----------------------------------------------------------------------------

  struct GenericList
  {
    GenericList *m_Next;
  };

  //Object allocator members
  Page*            m_PageList;
  GenericList*     m_FreeList;
  Config           m_Config;
  Stats            m_Stats;
  MemoryBlockSizes m_ByteSizeOf;
  ByteOffsets      m_OffsetTo;
  bool             m_Initialized;


public:
  ObjectAllocator();
  ObjectAllocator(const Config & Configuration);
  ~ObjectAllocator();

  void Initialize(const Config & Configuration);
  void* Allocate(unsigned LineNumber, char const* FilePath);
  void Deallocate(void * Object, unsigned LineNumber, char const* FilePath);
  Config const& Configuration();
  Stats  const& Statistics();

private:
  // Make private to prevent copy construction and assignment
  ObjectAllocator(const ObjectAllocator &);
  ObjectAllocator &operator=(const ObjectAllocator &);

  //helper functions
  void AllocateNewPage();
  void AddToFreeList( char* MemoryBlock );
  char* PullMemBlock();
  bool AddressInBounds( char const* Page, char const* Address ) const;
  bool AddressInPages(char const* Address) const;
  bool IsMemoryBlockFree(char const* MemoryBlock) const;

};

#endif