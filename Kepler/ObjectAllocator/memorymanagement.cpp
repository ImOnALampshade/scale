//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "memorymanagement.h"


namespace Utilities
{
  namespace MemoryManagement
  {

    unsigned int NextPowerOf2(unsigned int val)
    {
      unsigned int p = 1;
      if( val && !( val & ( val - 1 ) ) )
      {
        return val;
      }

      while( p < val )
      {
        p <<= 1;
      }

      return p;
    }
    
    unsigned GetAlignmentPadding(unsigned Alignment, unsigned CurrentSize)
    {
      //zero alignment means zero padding.
      if( Alignment == 0 )
      {
        return 0;
      }

      return (Alignment - ( CurrentSize % Alignment )) % Alignment;
    }

    bool CheckSignatureBlock(char const* Location, size_t Size, unsigned char Signature)
    {
      unsigned char const *sigLoc = reinterpret_cast<unsigned char const *>(Location);

      for (unsigned i = 0; i < Size; i += 1)
      {
        if (*(sigLoc + i) != Signature) return false;
      }

      return true;
    }

  }

}