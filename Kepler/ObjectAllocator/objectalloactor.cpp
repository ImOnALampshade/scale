// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  An object allocator which allocates pages of memory to speed up
//  memory management.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include <exception>
#include <cstring>
#include <cassert>

#include "objectallocator.h"
#include "memorymanagement.h"
#include "../String/string.h"

using namespace Utilities::MemoryManagement;

ObjectAllocator::ObjectAllocator()
  : m_PageList (nullptr), m_FreeList (nullptr), m_Config (Config()),
    m_Stats (Stats()), m_ByteSizeOf (MemoryBlockSizes()), m_OffsetTo (ByteOffsets()),
    m_Initialized (false)
{

}

ObjectAllocator::ObjectAllocator (const Config &Configuration)
  : m_PageList (nullptr), m_FreeList (nullptr), m_Config (Configuration),
    m_Stats (Stats()), m_ByteSizeOf (MemoryBlockSizes()), m_OffsetTo (ByteOffsets()),
    m_Initialized (false)
{
  Initialize (Configuration);
}

//TODO: Cant delete pages one at a time because freed objects are still in free_list
// when free OA using DebugOn == false, it checks free list, but finds 0xfeefee addresses
//because it has previously deleted a page, and hadnt removed the objects on that
//page from the free list.
ObjectAllocator::~ObjectAllocator()
{
  if (!m_Initialized)
  {
    return; // nothing to do here if it was never initialized.
  }


  Page *currentPage = m_PageList;

  //get offset to first memory block.
  size_t offsetToFirstBlock = m_OffsetTo.LeftAlignment + m_ByteSizeOf.LeftAlign;

  //iterate through pages and delete them all.
  while (currentPage != nullptr)
  {
    Page *nextPage = currentPage->NextPage();

// -----------------------------------------------------------------------------
// If debug macro is defined, then we check every object to make sure it is freed.
// -----------------------------------------------------------------------------
    #ifdef _DEBUG
    char *memoryBlock = currentPage->PageStart() + offsetToFirstBlock;

    for (unsigned int i = 0; i < m_Config.ObjectsPerPage; i += 1)
    {
      Header *header = reinterpret_cast<Header *> (memoryBlock);

      if (IsMemoryBlockFree (memoryBlock) == false)
      {
        String errorMessage =
          String::Format (
            "Error in deconstructor. Object allocated in file: %s, Line: %d\n\
         Object is still allocated. Deallocate all objects before destroying OA.\n",
            header->FilePath(), header->LineNumber()
          );

        throw std::exception (errorMessage.CStr());
      }

      //if OA debug mode is off, then we have to walk the free list when checking for free objects
      //because of this we need to pull off these objects as we check for them.
      //otherwise objects that were on freed pages will still be in the list.
      if (m_Config.DebugOn == false)
      {
        //find object in free list, route around it.
        GenericList *objectToSkip = m_FreeList;
        GenericList *previousObject = m_FreeList;

        while (objectToSkip != reinterpret_cast<GenericList *> (memoryBlock))
        {
          previousObject = objectToSkip;
          objectToSkip = objectToSkip->m_Next;

          if (objectToSkip->m_Next == nullptr)
          {
            break;
          }
        }

        if (objectToSkip == m_FreeList)
        {
          //take object off the front of the list
          m_FreeList = objectToSkip->m_Next;
        }

        else
        {
          //remove object from in list.
          previousObject->m_Next = objectToSkip->m_Next;
        }

        m_Stats.FreeObjects -= 1;
      }

      memoryBlock += m_Stats.ObjectTotalSize;
    }

    #endif
// -----------------------------------------------------------------------------

    delete[] reinterpret_cast<char *> (currentPage);
    currentPage = nextPage;
  }
}

void ObjectAllocator::Initialize (const Config &Configuration)
{

  assert (m_Initialized == false);

  //assume the new configuration.
  m_Config = Configuration;

  //headers only on if debug on.
  size_t headerSize = 0;

  if (m_Config.DebugOn)
  {
    headerSize = sizeof (Header);
  }

  else
  {
    m_Config.PadBytes = 0;
  }

  //caclulate memory block offsets.
  m_OffsetTo.FrontPadding = headerSize;
  m_OffsetTo.Data = m_OffsetTo.FrontPadding + m_Config.PadBytes;
  m_OffsetTo.RearPadding = m_OffsetTo.Data + m_Config.ObjectSize;
  m_OffsetTo.Alignment = m_OffsetTo.RearPadding + m_Config.PadBytes;

  m_OffsetTo.LeftAlignment = 0; // so now the page start is where left align would be.

  //Calculate memory block chunk sizes.
  m_ByteSizeOf.TotalPadBytes = m_Config.PadBytes * 2;
  m_ByteSizeOf.BlockFront = headerSize + m_Config.PadBytes;
  m_ByteSizeOf.LeftAlign = GetAlignmentPadding (m_Config.Alignment, m_ByteSizeOf.BlockFront);
  m_ByteSizeOf.InterAlign = GetAlignmentPadding (m_Config.Alignment, m_OffsetTo.Alignment);
  m_ByteSizeOf.BlockRear = m_Config.PadBytes + m_ByteSizeOf.InterAlign;
  m_Stats.ObjectTotalSize = m_ByteSizeOf.BlockFront + m_Config.ObjectSize + m_ByteSizeOf.BlockRear;

  /*
  size of the page is the pointer to the next page, plus the total size
  of a memory block times the number of total possible objects in a page.
  plus the size of alignment in order to have space to correct for the starting address
  of the page.
  */
  m_Stats.PageSize = sizeof (Page)+m_ByteSizeOf.LeftAlign
                     + (m_Stats.ObjectTotalSize * m_Config.ObjectsPerPage)
                     + m_Config.Alignment; // this space is used to line up page address

  //OA is now good to go.
  m_Initialized = true;
}

/******************************************************************************/
/*!
    \brief
      Retrieves a chunk of memory from the memory pages and returns it.

    \return
      Returns an object of the size specified in the configuration structure.
      Returns nullptr if there is no memory pages available, and one could
      not be created.
*/
/******************************************************************************/
void *ObjectAllocator::Allocate (unsigned LineNumber, char const *FilePath)
{
  assert (m_Initialized == true);
  //attempt to pull memory block.
  char *clientsObject = PullMemBlock();

  //if it failed, try allocating a new page and pulling again.
  if (clientsObject == nullptr)
  {
    AllocateNewPage();
    clientsObject = PullMemBlock(); //if it fails again here its just returns nullptr
  }

  //Allocations is more like allocation "attempts".
  m_Stats.Allocations += 1;

  if (m_Config.DebugOn)
  {
    //get a pointer to the beggining of the memory block and cast it to a header.
    Header *blockHeader = reinterpret_cast<Header *> (clientsObject - m_OffsetTo.Data);
    //Mark it as in use, set the line number and file it was called from.
    blockHeader->InUse (true);
    blockHeader->LineNumber (LineNumber);
    blockHeader->FilePath (FilePath);
  }


  return clientsObject;
}

/******************************************************************************/
/*!
    \brief
      Returns memory allocated by the memory manager back to the memory pages.

    \param Object
      Object is a pointer to memory allocated by the ObjectAllocator.
      Giving something else here will royaly screw things up.
*/
/******************************************************************************/
void ObjectAllocator::Deallocate (void *Object, unsigned LineNumber, char const *FilePath)
{
  assert (m_Initialized == true);

  if (Object == nullptr) return;  //ignore null pointer.

  //Get pointer to memoryblock start.
  char *memoryBlockData = reinterpret_cast<char *> (Object);
  char *memoryBlock = memoryBlockData - m_OffsetTo.Data;



  if (m_Config.DebugOn)
  {
    Header *blockHeader = reinterpret_cast<Header *> (memoryBlock);

    //check that the address is in a page.
    if (!AddressInPages (memoryBlockData))
    {
      String errorMessage =
        String::Format
        (
          "Error on deallocate, in file: %s, Line: %d\n\
         Object being freed is not in the object allocator.\n\
         Address Given: %p\n", FilePath, LineNumber, Object);

      throw std::exception (errorMessage.CStr());
    }

    //check signature blocks
    if (!CheckSignatureBlock (memoryBlock + m_OffsetTo.FrontPadding, m_Config.PadBytes, MemoryPattern::Padding)
        || !CheckSignatureBlock (memoryBlock + m_OffsetTo.RearPadding,  m_Config.PadBytes, MemoryPattern::Padding))
    {
      String errorMessage = String::Format ("Error on deallocate, in file: %s, Line: %d:\nObject being freed has courrpted pad bytes.\n", FilePath, LineNumber);
      //if either of these fail it should throw an exception.
      throw std::exception (errorMessage.CStr());
    }

    //check for double free
    if (IsMemoryBlockFree (memoryBlock))
    {
      String errorMessage =
        String::Format
        (
          "Error on deallocate, in file: %s, line: %d\nObject being freed has already been freed at\n"\
          "file: %s, line: %d\n", FilePath, LineNumber, blockHeader->FilePath(), blockHeader->LineNumber()
        );

      //if either of these fail it should throw an exception.
      throw std::exception (errorMessage.CStr());
    }

    //all checks completed succuesfully, changing header values next.
    blockHeader->InUse (false);
    blockHeader->FilePath (FilePath);
    blockHeader->LineNumber (LineNumber);

  }

  //Add it back to the free list.
  AddToFreeList (memoryBlock);

  //And increase our deallocations.
  m_Stats.Deallocations += 1;

}

/******************************************************************************/
/*!
    \brief
      Returns the configuration parameters.

    \return
      Returns the configuration parameters.
*/
/******************************************************************************/
ObjectAllocator::Config const &ObjectAllocator::Configuration()
{
  assert (m_Initialized == true);
  return m_Config;
}

/******************************************************************************/
/*!
    \brief
      Returns the stats of the object allocator.

    \return
      Returns the stats of the object allocator.
*/
/******************************************************************************/
ObjectAllocator::Stats const &ObjectAllocator::Statistics()
{
  assert (m_Initialized == true);
  return m_Stats;
}

/******************************************************************************/
/*!
   \brief
      Allocates a new page of memory that holds an amount of objects specified
      in the config.
*/
/******************************************************************************/
void ObjectAllocator::AllocateNewPage()
{

  //allocate new page
  char *newPage = nullptr;
  newPage = new char[m_Stats.PageSize];

  Page *memoryPage = new (newPage) Page (m_Config.Alignment);

  if (m_Config.DebugOn && newPage != nullptr)
  {
    //set unallocated pattern to block
    std::memset (memoryPage->PageStart(), 0, m_Stats.PageSize - memoryPage->AlignmentBytes());
    //set align pattern to the left align.
    std::size_t alignBytes = memoryPage->AlignmentBytes() - sizeof (Page);
    std::memset (memoryPage->PageStart() - alignBytes, MemoryPattern::Alignment, m_ByteSizeOf.LeftAlign + alignBytes);
  }

  if (newPage != nullptr)
  {
    /*
      The next bit adds the page to the page list.
    */
    m_Stats.PagesInUse += 1;

    //point page to the next page.
    memoryPage->NextPage (m_PageList);
    //the new page becomes the end of the page list.
    m_PageList = memoryPage;

    /*
      Now we need to divide up the page into blocks and add them to the
      free list.
    */

    //get offset to first memory block.
    size_t offsetToFirstBlock = m_OffsetTo.LeftAlignment + m_ByteSizeOf.LeftAlign;
    //get that as a pointer
    char *memoryBlock = memoryPage->PageStart() + offsetToFirstBlock;

    //loop through all memory blocks and add them to the free list.
    for (unsigned int i = 0; i < m_Config.ObjectsPerPage; i += 1)
    {
      m_Stats.ObjectsInUse += 1; // (do this cause AddToFreeList subtracts from this)
      AddToFreeList (memoryBlock);
      memoryBlock += m_Stats.ObjectTotalSize;
    }

  }
}

/******************************************************************************/
/*!
    \brief
      Adds an object back to the free list.

    \param MemoryBlock
      The memory block to add back to the free list.

*/
/******************************************************************************/
void ObjectAllocator::AddToFreeList (char *MemoryBlock)
{
  m_Stats.FreeObjects += 1;
  m_Stats.ObjectsInUse -= 1;

  //Set in use to false. And other debug data.
  if (m_Config.DebugOn)
  {
    Header *blockHeader = reinterpret_cast<Header *> (MemoryBlock);

    //write in pad bytes
    std::memset (MemoryBlock + m_OffsetTo.FrontPadding, MemoryPattern::Padding, m_Config.PadBytes);
    std::memset (MemoryBlock + m_OffsetTo.RearPadding, MemoryPattern::Padding, m_Config.PadBytes);

    //write in unallocated or freed pattern
    // if its not in use (meaning it was not in use, and not in the free list) then it is unallocated
    if (!blockHeader->InUse())
    {
      std::memset (MemoryBlock + m_OffsetTo.Data, MemoryPattern::Unallocated, m_Config.ObjectSize);
    }

    else // else if it is in use and being added to the free list, then it is being freed.
    {
      blockHeader->InUse (false);
      std::memset (MemoryBlock + m_OffsetTo.Data, MemoryPattern::Freed, m_Config.ObjectSize);
    }

    //write in alignment pattern.
    std::memset (MemoryBlock + m_OffsetTo.Alignment, MemoryPattern::Alignment, m_ByteSizeOf.InterAlign);

  }

  /*
    Add this memory block to the free list at the front.
  */
  //get pointer to where the data in the memory block is stored
  char *MemoryBlockData = MemoryBlock + m_OffsetTo.Data;
  GenericList *freeObject = reinterpret_cast<GenericList *> (MemoryBlockData);
  //point this data at the current data on the freelist
  freeObject->m_Next = m_FreeList;
  //this data now becomes the data at the top of the freelist.
  m_FreeList = freeObject;



}

/******************************************************************************/
/*!
    \brief
      A helper function which attempts to retrieve some memory from the memory
      pages.

    \return
      Returns nullptr if it fails. Otherwise it returns the address
      of the new memoryblock where the DATA is to be stored. (Pull mem block
      gives a pointer to the beginning address where the client will put data.
      to either side of that is potentially padding and headers)

*/
/******************************************************************************/
char *ObjectAllocator::PullMemBlock()
{
  if (m_FreeList != nullptr)
  {
    //grab object off top of free list
    GenericList *objectToGive = m_FreeList;

    //free list will now point to next object on the free list.
    m_FreeList = objectToGive->m_Next;


    /*
      Keep track of some stats...
    */
    m_Stats.FreeObjects -= 1;
    m_Stats.ObjectsInUse += 1;

    if (m_Stats.ObjectsInUse > m_Stats.MostObjects)
    {
      m_Stats.MostObjects = m_Stats.ObjectsInUse;
    }


    //cast and get memory block start
    char *memoryBlock = reinterpret_cast<char *> (objectToGive) - m_OffsetTo.Data;

    if (m_Config.DebugOn)
    {
      Header *blockHeader = reinterpret_cast<Header *> (memoryBlock);
      blockHeader->InUse (true);

      //write in pad bytes
      std::memset (memoryBlock + m_OffsetTo.FrontPadding, MemoryPattern::Padding, m_Config.PadBytes);
      std::memset (memoryBlock + m_OffsetTo.RearPadding, MemoryPattern::Padding, m_Config.PadBytes);
      //write in allocated pattern
      std::memset (memoryBlock + m_OffsetTo.Data, MemoryPattern::Allocated, m_Config.ObjectSize);
      //write in alignment pattern.
      std::memset (memoryBlock + m_OffsetTo.Alignment, MemoryPattern::Alignment, m_ByteSizeOf.InterAlign);
    }

    //pass it along as char*
    return reinterpret_cast<char *> (objectToGive);
  }

  else
  {
    //the free list is empty, so there is nothing to return.
    return nullptr;
  }
}


// -------------------------------------------------------------------------------------------------
// Checks if an address lies within the given page.
// -------------------------------------------------------------------------------------------------
bool ObjectAllocator::AddressInBounds (char const *Page, char const *Address) const
{
  //get address of the first bit of data.
  char const *dataStart = Page + m_OffsetTo.LeftAlignment + m_ByteSizeOf.LeftAlign + m_OffsetTo.Data;

  //Check if is between start and end of page, and also that it aligns with an object on that page.
  return (Address >= dataStart && Address < Page + m_Stats.PageSize
          && (Address - dataStart) % m_Stats.ObjectTotalSize == 0);
}

// -------------------------------------------------------------------------------------------------
// Looks through the pages of memory to see if the address is in them.
// -------------------------------------------------------------------------------------------------
bool ObjectAllocator::AddressInPages (char const *Address) const
{
  Page *currentPage = m_PageList;

  while (currentPage != nullptr)
  {
    if (AddressInBounds (reinterpret_cast<char *> (currentPage->PageStart()), Address))
    {
      return true;
    }


    currentPage = currentPage->NextPage();

  }


  return false;
}

// -------------------------------------------------------------------------------------------------
// Checks to see if the paticular memory block is currently free. (good for checking double free)
// -------------------------------------------------------------------------------------------------
bool ObjectAllocator::IsMemoryBlockFree (char const *MemoryBlock) const
{
  //if it is not in use then it is free.
  if (m_Config.DebugOn)
  {
    //with debug on we just check the headers.
    return !reinterpret_cast<Header const *> (MemoryBlock)->InUse();
  }

  else
  {
    //if its not on we have to check the free list.
    //in general this function will not be called if debug is off.
    GenericList const *currentObj = m_FreeList;
    GenericList const *testObject = reinterpret_cast<GenericList const *> (MemoryBlock + m_OffsetTo.Data);

    while (currentObj != nullptr)
    {
      if (testObject == currentObj)
      {
        return true;
      }

      currentObj = currentObj->m_Next;
    }

    //if we walked the entire list and didnt find it then its not free!
    return false;
  }
}

// -----------------------------------------------------------------------------
// Object Allocator Header Code
// -----------------------------------------------------------------------------


char const *ObjectAllocator::Header::FilePath() const
{
  return m_FilePath;
}

void ObjectAllocator::Header::FilePath (char const *FilePath)
{
  m_FilePath = FilePath;
}

unsigned ObjectAllocator::Header::LineNumber() const
{
  return m_LineNumber;
}

void ObjectAllocator::Header::LineNumber (unsigned LineNumber)
{
  m_LineNumber = LineNumber;
}

bool ObjectAllocator::Header::InUse() const
{
  return m_InUse;
}


void ObjectAllocator::Header::InUse (bool InUse)
{
  m_InUse = InUse;
}

// -----------------------------------------------------------------------------
// Object Allocator Page Code
// -----------------------------------------------------------------------------

ObjectAllocator::Page::Page (std::size_t Alignment)
  : m_AlignmentBytes (0), m_NextPage (nullptr)
{
  using namespace Utilities::MemoryManagement;

  //get memory offset to start of page data
  uintptr_t addressOffset = reinterpret_cast<uintptr_t> (this);
  addressOffset += sizeof (*this);
  //set initial alignment
  m_AlignmentBytes = GetAlignmentPadding (Alignment, addressOffset);
  //add on size of this structure.
  m_AlignmentBytes += sizeof (*this);

}

ObjectAllocator::Page *ObjectAllocator::Page::NextPage()
{
  return m_NextPage;
}

void  ObjectAllocator::Page::NextPage (Page *NextPage)
{
  m_NextPage = NextPage;
}

size_t ObjectAllocator::Page::AlignmentBytes() const
{
  return m_AlignmentBytes;
}

char *ObjectAllocator::Page::PageStart()
{
  return reinterpret_cast<char *> (this) + m_AlignmentBytes;
}