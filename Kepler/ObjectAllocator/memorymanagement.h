// -------------------------------------------------------------------------------------------------
// Helper functions for memory management.
// Reese Jones
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -------------------------------------------------------------------------------------------------

#ifndef MEMORY_MANAGEMENT_H
#define MEMORY_MANAGEMENT_H

namespace Utilities
{
  namespace MemoryManagement
  {


// -------------------------------------------------------------------------------------------------
// Get next power of two
// -------------------------------------------------------------------------------------------------
    unsigned int NextPowerOf2( unsigned int val );

// -------------------------------------------------------------------------------------------------
// A function to figure out how much padding you need to make "Offset" end on an alignment
// that matches "Alignment". so if you have something that is 17bytes and need alignment 4 it would
// return 3.
// -------------------------------------------------------------------------------------------------
    unsigned GetAlignmentPadding( unsigned Alignment, unsigned Offset );


// -------------------------------------------------------------------------------------------------
// Checks a location in memory for "Size" amount of bytes to see if each byte matches "Signature"
// -------------------------------------------------------------------------------------------------
    bool CheckSignatureBlock(char const* Location, size_t Size, unsigned char Signature);
  }

}

#endif