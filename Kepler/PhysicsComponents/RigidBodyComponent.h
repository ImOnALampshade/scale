// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  The RigidBody Component
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// --------------------------------------------------------------------------------
#include "../KeplerCore/component.h"
#include "../glminclude.h"
#include "../KeplerCore/transformcomponent.h"
#include "../KeplerMath/KeplerMath.h"
#ifndef RIGIDBODY_H
#define RIGIDBODY_H


const float FORCE_INFINITE_LIFE = -10.0f;


class CollisionVolumeComponent;
class RigidBodyComponent : public Component
{
public:
  void Initialize(const Json::Object &jsonData);
  void Update(void);
  void Free();

  void Awake(void);
  //Forces-----------------------------
  void UpdateForces(float DeltaTime);

  void ApplyForce(Vec3 Force, float lifetime);
  void ApplyTorque(Vec3 Torque, float lifetime);
  void ApplyForceAtPoint(Vec3 Force, Vec3 WorldPoint, float lifeime);
  void ApplyForceAtOffsetVector(Vec3 Force, Vec3 WorldR, float lifetime);



  void AddImpulseLinear(Vec3 arg);
  void AddImpulseRot(Vec3 arg);
  void ApplyImpulses(void);

  void AddVel(Vec3 arg);
  void AddRotVel(Vec3 arg);

  //Getters/setters--------------------
  void GravityDir(Vec3 arg);
  Vec3 GravityDir(void);

  void GravityMag(float arg);
  float GravityMag(void);

  void Acceleration(Vec3 arg);
  Vec3 Acceleration(void);

  void Velocity(Vec3 arg);
  Vec3 Velocity(void);

  void Position(Vec3 arg);
  Vec3 Position(void);

  void RotAcceleration(Vec3 arg);
  Vec3 RotAcceleration(void);

  void RotVelocity(Vec3 arg);
  Vec3 RotVelocity(void);

  void Orientation(Quat arg);
  Quat Orientation(void);

  void InertiaTensor(Mat3 arg);
  Mat3 InertiaTensor();

  Mat3 InvInertia(void);
  float InvMass(void);

  void Mass(float arg);
  float Mass(void);

  void Elasticity(float arg);
  float Elasticity(void);

  void GhostMode(bool arg);
  bool GhostMode(void);

  void TheCollisionVolume(::Handle<CollisionVolumeComponent> arg);
  ::Handle<CollisionVolumeComponent> TheCollisionVolume(void);

  COMPONENT_META
private:
  //linear terms-----------------------
  glm::vec3 m_Accel;
  glm::vec3 m_Vel;
  glm::vec3 m_Pos;
  
  //Rotational Terms-------------------
  glm::vec3 m_RotAccel;
  glm::vec3 m_RotVel;
  glm::quat m_Orientation;

  //Inertial Terms---------------------
  float m_Mass;
  float m_InvMass;
  glm::mat3 m_Inertia;
  glm::mat3 m_InvInertia;

  glm::mat3 m_Inertia_World;
  glm::mat3 m_InvInertia_World;

  //Collision Terms--------------------
  bool m_IsColliding;
  float m_Elasticity;
  bool m_GhostMode;

  ::Handle<TransformComponent> m_Transform;
  ::Handle<CollisionVolumeComponent> m_CV;

  //message handler
  void SetTheCV(::Handle<CollisionVolumeComponent> arg);

  struct InternalForce
  {
    glm::vec3 LinearForce;
    glm::vec3 Torque;
    float Lifetime;
  };

  std::vector<InternalForce> m_Forces;

  
  glm::vec3 m_GravityDir;
  float m_GravityMag;

  glm::vec3 m_LinImpulseAccum;
  glm::vec3 m_RotImpulseAccum;
};

#endif