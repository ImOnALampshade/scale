//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "load_factories.h"
#include "PhysicsSystem.h"
#include "RigidBodyComponent.h"
#include "CollisionVolume.h"
#include "../KeplerCore/compositefactory.h"

void PhysicsComponents::LoadFactories()
{
  CREATE_FACTORY(PhysicsSystem);
  CREATE_FACTORY(RigidBody);

  CREATE_FACTORY(Collision_OBB);
  CREATE_FACTORY(Collision_AABB);
  CREATE_FACTORY(Collision_Sphere);
  CREATE_FACTORY(Collision_Plane);

}

void PhysicsComponents::CreateMeta()
{
  PhysicsSystemComponent::CreateMeta();
  RigidBodyComponent::CreateMeta();
  Collision_SphereComponent::CreateMeta();
  Collision_PlaneComponent::CreateMeta();
  Collision_OBBComponent::CreateMeta();
  Collision_AABBComponent::CreateMeta();
}
