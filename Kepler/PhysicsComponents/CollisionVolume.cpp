//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "CollisionVolume.h"
#include "RigidBodyComponent.h"
CollisionVolumeComponent::CollisionVolumeComponent(ShapeType CollisionType)
  :m_CollisionType(CollisionType), m_CollisionID(0)
{

}

void CollisionVolumeComponent::InitMessages(void)
{
  m_Transform = Owner()->GetComponent("Transform").as<TransformComponent>();
  Owner()->DispatchUp("CollisionVolumeCreate", Handle().as<CollisionVolumeComponent>());
  RegisterMessageProc("RigidBodyCreate", std::function<void(::Handle<RigidBodyComponent>)>(std::bind(&CollisionVolumeComponent::SetTheRB, this, std::placeholders::_1)));
}

void CollisionVolumeComponent::Initialize(const Json::Object &jsonData)
{
}
//-----------------------------------------
AABB_struct   CollisionVolumeComponent::CalculateAABB(void)
{
  AABB_struct localTmp;
  localTmp.m_HalfLengths = glm::vec3(0, 0, 0);
  localTmp.m_Pos = glm::vec3(0, 0, 0);
  return localTmp;
}
Sphere_struct CollisionVolumeComponent::CalculateBoundingSphere(void)
{
  Sphere_struct tmp;
  tmp.m_Pos = glm::vec3(0, 0, 0);
  tmp.m_Radius = 0.0f;
  return tmp;
}

float CollisionVolumeComponent::CalculateMass(float)
{
  //default to infinite
  return 0;
}
glm::mat3 CollisionVolumeComponent::CalculateInertia(float)
{
  //default to infinite
  return glm::mat3(0,0,0,0,0,0,0,0,0);
}

//-------------------------------------------

glm::vec3 CollisionVolumeComponent::Position(void)
{
  if (m_RigidBody.IsValid())
    return m_RigidBody->Position();
  else
    return m_Transform->Position();
}
glm::mat3 CollisionVolumeComponent::GetRotationMtx(void)
{
  glm::quat theQuat;
  if (m_RigidBody.IsValid())
    theQuat = m_RigidBody->Orientation();
  else
    theQuat = m_Transform->Rotation();


  return glm::mat3_cast(theQuat);
}

::Handle<RigidBodyComponent> CollisionVolumeComponent::Body(void)
{
  return m_RigidBody;
}
void CollisionVolumeComponent::Body(::Handle<RigidBodyComponent> TheBody)
{
  m_RigidBody = TheBody;
}

ShapeType CollisionVolumeComponent::GetCollisionType(void)
{
  return m_CollisionType;
}

unsigned char CollisionVolumeComponent::CollisionID(void)
{
  return m_CollisionID;
}
void CollisionVolumeComponent::CollisionID(unsigned char arg)
{
  m_CollisionID = arg;
}

//------------------------------------------
glm::mat4 CollisionVolumeComponent::GetTransform(void)
{
  glm::mat4 Translation;
  glm::mat4 Rotate;

  if (m_RigidBody.IsValid())
  {
    glm::translate(Translation, m_RigidBody->Position());
    Rotate = glm::mat4_cast(m_RigidBody->Orientation());
  }
  else
  {
    glm::translate(Translation, m_Transform->Position());
    Rotate = glm::mat4_cast(m_Transform->Rotation());
  }
  return Translation * Rotate;
}
glm::mat4 CollisionVolumeComponent::GetInvTransform(void)
{
  glm::mat4 Transform = GetTransform();
  return glm::inverse(Transform);
}

void CollisionVolumeComponent::SetTheRB(::Handle<RigidBodyComponent> arg)
{
  m_RigidBody = arg;
  m_RigidBody->TheCollisionVolume(Handle().as<CollisionVolumeComponent>());
}

//-------------------------------------------------------------------
//OBB

Collision_OBBComponent::Collision_OBBComponent(float HalfWidth, float HalfHeight, float HalfDepth)
  :CollisionVolumeComponent(ShapeType::OBB), m_HalfWidth(HalfWidth),
   m_HalfHeight(HalfHeight), m_HalfDepth(HalfDepth)
{}

glm::mat3 Collision_OBBComponent::CalculateInertia(float Mass)
{
  float height = 2 * m_HalfHeight;
  float width = 2 * m_HalfWidth;
  float depth = 2 * m_HalfDepth;
  glm::mat3 inertia;
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      inertia[i][j] = 0;

  inertia[0][0] = height * height + depth * depth;
  inertia[1][1] = width * width + depth * depth;
  inertia[2][2] = width * width + height * height;

  inertia *= Body()->Mass() / 12.0f;
  return inertia;
}

void Collision_OBBComponent::Initialize(const Json::Object &jsonData)
{
  InitMessages();
  HalfWidth((float)jsonData.Locate("half_width", 1.0f).as<Json::Number>());
  HalfHeight((float)jsonData.Locate("half_height", 1.0f).as<Json::Number>());
  HalfDepth((float)jsonData.Locate("half_depth", 1.0f).as<Json::Number>());
  CollisionID((unsigned int)jsonData.Locate("collision_id", 0).as<Json::Number>());
}

//------------------------------------------------
AABB_struct   Collision_OBBComponent::CalculateAABB(void)
{
  glm::vec3 length = glm::vec3(m_HalfWidth, m_HalfHeight, m_HalfDepth);
  float mag = glm::length(length);

  AABB_struct output;
  output.m_HalfLengths = glm::vec3(mag, mag, mag);
  output.m_Pos = Position();
  return output;
}
Sphere_struct Collision_OBBComponent::CalculateBoundingSphere(void)
{
  glm::vec3 length = glm::vec3(m_HalfWidth, m_HalfHeight, m_HalfDepth);
  float mag = glm::length(length);

  Sphere_struct output;
  output.m_Radius = mag;
  output.m_Pos = Position();
  return output;
}

//------------------------------------------------
float Collision_OBBComponent::HalfWidth(void)
{
  return m_HalfWidth;
}
float Collision_OBBComponent::HalfHeight(void)
{
  return m_HalfHeight;
}
float Collision_OBBComponent::HalfDepth(void)
{
  return m_HalfDepth;
}

void Collision_OBBComponent::HalfWidth(float arg)
{
  m_HalfWidth = arg;
}
void Collision_OBBComponent::HalfHeight(float arg)
{
  m_HalfHeight = arg;
}
void Collision_OBBComponent::HalfDepth(float arg)
{
  m_HalfDepth = arg;
}

//----------------------------------------------------------------------------------
//Sphere

Collision_SphereComponent::Collision_SphereComponent(float Radius)
  :CollisionVolumeComponent(ShapeType::SPHERE), m_Radius(Radius)
{}

void Collision_SphereComponent::Initialize(const Json::Object &jsonData)
{
  InitMessages();
  Radius((float)jsonData.Locate("radius", 1.0).as<Json::Number>());
  CollisionID((unsigned int)jsonData.Locate("collision_id", 0).as<Json::Number>());
}

glm::mat3 Collision_SphereComponent::CalculateInertia(float Mass)
{

  glm::mat3 inertia;
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      inertia[i][j] = 0;

  inertia[0][0] = Radius() * Radius();
  inertia[1][1] = Radius() * Radius();
  inertia[2][2] = Radius() * Radius();

  inertia *= Body()->Mass() * 2.0f / 5.0f;
  return inertia;
}
//------------------------------------------------
AABB_struct   Collision_SphereComponent::CalculateAABB(void)
{
  AABB_struct output;
  output.m_HalfLengths = glm::vec3(m_Radius, m_Radius, m_Radius);
  output.m_Pos = Position();
  return output;
}
Sphere_struct Collision_SphereComponent::CalculateBoundingSphere(void)
{
  Sphere_struct output;
  output.m_Pos = Position();
  output.m_Radius = m_Radius;
  return output;
}

//------------------------------------------------
float Collision_SphereComponent::Radius(void)
{
  return m_Radius;
}
void  Collision_SphereComponent::Radius(float arg)
{
  m_Radius = arg;
}
//--------------------------------------------------------------
// AABB
Collision_AABBComponent::Collision_AABBComponent(float HalfWidth, float HalfHeight, float HalfDepth)
  :CollisionVolumeComponent(ShapeType::AABB), m_HalfWidth(HalfWidth), 
   m_HalfHeight(HalfHeight), m_HalfDepth(HalfDepth)
{}

void Collision_AABBComponent::Initialize(const Json::Object &jsonData)
{
  InitMessages();
  HalfWidth((float)jsonData.Locate("half_width", 1.0).as<Json::Number>());
  HalfHeight((float)jsonData.Locate("half_height", 1.0).as<Json::Number>());
  HalfDepth((float)jsonData.Locate("half_depth", 1.0).as<Json::Number>());
  CollisionID((unsigned char)jsonData.Locate("collision_id", 0).as<Json::Number>());
}

glm::mat3 Collision_AABBComponent::CalculateInertia(float Mass)
{
  float height = 2 * m_HalfHeight;
  float width = 2 * m_HalfWidth;
  float depth = 2 * m_HalfDepth;
  glm::mat3 inertia;
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      inertia[i][j] = 0;

  inertia[0][0] = height * height + depth * depth;
  inertia[1][1] = width * width + depth * depth;
  inertia[2][2] = width * width + height * height;

  inertia *= Body()->Mass() / 12.0f;
  return inertia;
}
//------------------------------------------------
AABB_struct Collision_AABBComponent::CalculateAABB(void)
{
  AABB_struct output;
  output.m_Pos = Position();
  output.m_HalfLengths = glm::vec3(m_HalfWidth, m_HalfHeight, m_HalfDepth);
  return output;
}
Sphere_struct Collision_AABBComponent::CalculateBoundingSphere(void)
{
  glm::vec3 corner(m_HalfWidth, m_HalfHeight, m_HalfDepth);
  Sphere_struct output;
  output.m_Pos = Position();
  output.m_Radius = glm::length(corner);
  return output;
}

//------------------------------------------------
void  Collision_AABBComponent::HalfWidth(float arg)
{
  m_HalfWidth = arg;
}
void  Collision_AABBComponent::HalfHeight(float arg)
{
  m_HalfHeight = arg;
}
void  Collision_AABBComponent::HalfDepth(float arg)
{
  m_HalfDepth = arg;
}
float Collision_AABBComponent::HalfWidth(void)
{
  return m_HalfWidth;
}
float Collision_AABBComponent::HalfHeight(void)
{
  return m_HalfHeight;
}
float Collision_AABBComponent::HalfDepth(void)
{
  return m_HalfDepth;
}

//-----------------------------------------------------------
//Plane
Collision_PlaneComponent::Collision_PlaneComponent(glm::vec3 Normal, float Width, float Height)
  :CollisionVolumeComponent(ShapeType::PLANE), m_Normal(Normal), m_Width(Width), m_Height(Height)
{
  glm::normalize(m_Normal);
}

void Collision_PlaneComponent::Initialize(const Json::Object &jsonData)
{
  InitMessages();
  bool found = jsonData.CallOnValue("normal", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_Normal[i] = float(l[i].as<Json::Number>());
  });

  if (!found)
    m_Normal = glm::vec3(0, 1, 0);

  glm::normalize(m_Normal);
}

float Collision_PlaneComponent::GetOffsetValue()
{
  glm::vec3& pos = Position();
  return pos.x * m_Normal.x +
         pos.y * m_Normal.y +
         pos.z * m_Normal.z;
         
}

//------------------------------------------------
// used for BroadPhase
AABB_struct   Collision_PlaneComponent::CalculateAABB(void)
{
  float length = glm::length(glm::vec3(m_Width, m_Height, 0));
  AABB_struct output;
  output.m_Pos = Position();
  output.m_HalfLengths = glm::vec3(length, length, length);
  return output;
}
Sphere_struct Collision_PlaneComponent::CalculateBoundingSphere(void)
{
  float length = glm::length(glm::vec3(m_Width, m_Height, 0));
  Sphere_struct output;
  output.m_Pos = Position();
  output.m_Radius = length;
  return output;
}

//------------------------------------------------
// getters/setters
void Collision_PlaneComponent::Normal(Vec3 arg)
{
  m_Normal = arg;
}
Vec3 Collision_PlaneComponent::Normal(void)
{
  return m_Normal;
}

META_REGISTER_FUNCTION(Collision_AABB)
{
  META_INHERIT(Collision_AABBComponent, "Kepler", "Component");
  META_HANDLE(Collision_AABBComponent);

  META_ADD_PROP(Collision_AABBComponent, HalfWidth);
  META_ADD_PROP(Collision_AABBComponent, HalfHeight);
  META_ADD_PROP(Collision_AABBComponent, HalfDepth);
  META_ADD_PROP(Collision_AABBComponent, CollisionID);

  META_FINALIZE_PTR(Collision_AABBComponent, MetaPtr);
}
META_REGISTER_FUNCTION(Collision_Sphere)
{
  META_INHERIT(Collision_SphereComponent, "Kepler", "Component");
  META_HANDLE(Collision_SphereComponent);

  META_ADD_PROP(Collision_SphereComponent, Radius);
  META_ADD_PROP(Collision_SphereComponent, CollisionID);

  META_FINALIZE_PTR(Collision_SphereComponent, MetaPtr);
}
META_REGISTER_FUNCTION(Collision_OBB)
{
  META_INHERIT(Collision_OBBComponent, "Kepler", "Component");
  META_HANDLE(Collision_OBBComponent);

  META_ADD_PROP(Collision_OBBComponent, HalfWidth);
  META_ADD_PROP(Collision_OBBComponent, HalfHeight);
  META_ADD_PROP(Collision_OBBComponent, HalfDepth);
  META_ADD_PROP(Collision_OBBComponent, CollisionID);

  META_FINALIZE_PTR(Collision_OBBComponent, MetaPtr);
}
META_REGISTER_FUNCTION(Collision_Plane)
{
  META_INHERIT(Collision_PlaneComponent, "Kepler", "Component");
  META_HANDLE(Collision_PlaneComponent);

  META_ADD_PROP(Collision_PlaneComponent, Normal);
  META_ADD_PROP(Collision_PlaneComponent, CollisionID);

  META_FINALIZE_PTR(Collision_PlaneComponent, MetaPtr);
}
