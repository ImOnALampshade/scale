// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  Contact object, used in collision resolution
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef CONTACT_H
#define CONTACT_H
#include <vector>
#include "../glminclude.h"
#include "../KeplerCore/component.h"
class RigidBodyComponent;
class Contact
{
public:
  glm::vec3 ContactNormal;
  glm::vec3 ContactPoint;
  ::Handle<RigidBodyComponent> A;
  ::Handle<RigidBodyComponent> B;
  float PenetrationDepth;
  float Restitution;

  bool A_NoRotate;
  bool B_NoRotate;
};

typedef std::vector<Contact> ContactList;
#endif