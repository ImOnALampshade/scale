// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  Component for a physics system in a space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------
#include "PhysicsSystem.h"
#include <algorithm>
#include "RigidBodyComponent.h"
#include "CollisionEngine.h"
#include "../KeplerCore/transformcomponent.h"

void PhysicsSystemComponent::Initialize(const Json::Object &jsonData)
{
  
  RegisterMessageProc("PhysicsUpdate", std::function<void(float)>(std::bind(&PhysicsSystemComponent::Update, this, std::placeholders::_1)));
  RegisterMessageProc("RigidBodyCreate", std::function<void(::Handle<RigidBodyComponent>) >(std::bind(&PhysicsSystemComponent::AddRigidBody, this, std::placeholders::_1)));
  RegisterMessageProc("CollisionVolumeCreate", std::function<void(::Handle<CollisionVolumeComponent>) >(std::bind(&PhysicsSystemComponent::AddCollisionVolume, this, std::placeholders::_1)));

  m_Accum = 0.0f;
  m_FrameRate = 60.0f;
  m_IsPaused = false;
  m_CollisionEngine = new CollisionEngine(this);


}
void PhysicsSystemComponent::Free()
{
  delete m_CollisionEngine;
}

void PhysicsSystemComponent::Update(float Dt)
{
  float Deltatime = Dt;
  static bool firstFrame = true;

  if (firstFrame)
  {
    firstFrame = false;
    return;
  }

  if (!m_IsPaused)
  {
    m_Accum += Deltatime;
  }

  while (m_Accum >= 1.0f / m_FrameRate)
  {
    //if more than 4 timesteps has passed
    if (m_Accum >= 4.0f / m_FrameRate)
    {
      //assume breakpoint, reset counter
      m_Accum = 0.0f;
      break;
    }

    AdvOneStep();
    m_Accum -= 1.0f / m_FrameRate;
  }

  for (size_t i = 0; i < m_RigidBodies.size(); ++i)
    m_RigidBodies[i]->Update();

}
void PhysicsSystemComponent::AdvOneStep(void)
{
  PruneRigidBodies();
  PruneCollisionVolumes();

  Integrate();

  m_Contacts.clear();
  m_CollisionEngine->DetectCollisions();
  ResolveContacts();
  //reslove constraints
}


void PhysicsSystemComponent::LoadCollisionLayerTable(String filename)
{
  m_CollisionEngine->LoadCollisionLayerTable(filename);
}

void PhysicsSystemComponent::Integrate(void)
{
  float timeStep = 1 / (1 * m_FrameRate);

  for (size_t i = 0; i < m_RigidBodies.size(); ++i)
  {
    ::Handle<TransformComponent> transform = m_RigidBodies[i]->Owner()->GetComponent<TransformComponent>();

    if (transform.IsValid())
    {
      m_RigidBodies[i]->Position(transform->Position());
      m_RigidBodies[i]->Orientation(transform->Rotation());
    }

    m_RigidBodies[i]->UpdateForces(timeStep);
    //get linear values
    glm::vec3 accel = m_RigidBodies[i]->Acceleration();
    glm::vec3 vel = m_RigidBodies[i]->Velocity();
    glm::vec3 pos = m_RigidBodies[i]->Position();

    //integrate linear values
    pos = pos + vel * timeStep + accel * timeStep * timeStep;
    vel = vel + accel * timeStep;


    //get nonlinear values
    glm::vec3 rotAccel = m_RigidBodies[i]->RotAcceleration();
    glm::vec3 rotVel = m_RigidBodies[i]->RotVelocity();
    glm::quat rotPos = m_RigidBodies[i]->Orientation();

    //integrate nonlinear values -> a little extra work to rotate the quaternion
    rotVel = rotVel + rotAccel * timeStep;
    glm::vec3 RotOffset = rotVel * timeStep + rotAccel * timeStep * timeStep;
    float Angle = glm::length(RotOffset) * 180 / 3.14f; // to  radians
    glm::normalize(RotOffset);

    //make sure there is a non zero rotation
    if (Angle <= -0.0001 || Angle >= 0.0001)
      rotPos = glm::rotate(rotPos, Angle, RotOffset);

    //store all of the values
    m_RigidBodies[i]->Velocity(vel);
    m_RigidBodies[i]->Position(pos);
    m_RigidBodies[i]->RotVelocity(rotVel);
    m_RigidBodies[i]->Orientation(rotPos);
  }
}

void PhysicsSystemComponent::AddRigidBody(::Handle<RigidBodyComponent> arg)
{
  m_RigidBodies.push_back(arg);
}
void PhysicsSystemComponent::PruneRigidBodies(void)
{
  m_RigidBodies.erase(
    std::remove_if(m_RigidBodies.begin(), m_RigidBodies.end(), [](::Handle<RigidBodyComponent> arg)->bool {
    return !arg.IsValid();
  }),
    m_RigidBodies.end()
    );
}

void PhysicsSystemComponent::AddCollisionVolume(::Handle<CollisionVolumeComponent> arg)
{
  m_CollisionEngine->AddCVolume(arg);
}
void PhysicsSystemComponent::PruneCollisionVolumes(void)
{
  m_CollisionEngine->PruneCollisionVolumes();
}

void PhysicsSystemComponent::AddContact(Contact arg)
{
  m_Contacts.push_back(arg);
}


void PhysicsSystemComponent::ResolveContacts(void)
{

  int size = m_Contacts.size();

  for (int i = 0; i < size; ++i)
  {
    Contact &CurrentContact = m_Contacts[i];

    ::Handle<RigidBodyComponent> A = CurrentContact.A;
    ::Handle<RigidBodyComponent> B = CurrentContact.B;
    if (glm::length(CurrentContact.ContactNormal) == 0.0)
      CurrentContact.ContactNormal = glm::vec3(0, 1, 0);

    if (!A.IsValid() && !B.IsValid())
      continue;

    //rigidbody v no rigidbody is a ghost collision now
    if (!A.IsValid() || !B.IsValid())
      continue;

    //if (!A.IsValid())
    //{
    //  A = B;
    //  B = nullptr;
    //  CurrentContact.ContactNormal *= -1;
    //}
    float Elasticity;

    //min
    if (B.IsValid())
    {
      Elasticity = (A->Elasticity() > B->Elasticity()) ? B->Elasticity() : A->Elasticity();
    }

    else
      Elasticity = A->Elasticity();




    //special case for constraints
    if (CurrentContact.Restitution != -1.0f)
      Elasticity = CurrentContact.Restitution;

    //get change of basis
    glm::mat3 ContactToWorldRot = CreateOrthoNormalBasis(CurrentContact.ContactNormal);

    //get the inverse rotation
    glm::mat3 WorldToContactRot = ContactToWorldRot;
    WorldToContactRot = glm::inverse(ContactToWorldRot);

    //relative contact pos
    glm::vec3 RelContactPosA = CurrentContact.ContactPoint - A->Position();
    glm::vec3 RelContactPosB;

    if (B.IsValid())
      RelContactPosB = CurrentContact.ContactPoint - B->Position();

    //LocalVelocities
    glm::vec3 LocalContactVelA = glm::cross(A->RotVelocity(), RelContactPosA);
    LocalContactVelA += glm::vec3(A->Velocity());
    LocalContactVelA = WorldToContactRot * LocalContactVelA;


    glm::vec3 LocalContactVelB;

    if (B.IsValid())
    {
      LocalContactVelB = glm::cross(B->RotVelocity(), RelContactPosB);
      LocalContactVelB += glm::vec3(B->Velocity());
      LocalContactVelB = WorldToContactRot * LocalContactVelB;
    }

    //contact velocity
    glm::vec3 ContactVelocity = LocalContactVelA - LocalContactVelB;

    if (ContactVelocity.x > 0)
    {
      ContactVelocity.x = 0;
      //RestingContacts++;
      //continue;
    }

    //calculate desired delta vel
    float DesiredDeltaVelocity = -ContactVelocity.x * (1 + Elasticity);
    glm::vec3 LocalNormal = WorldToContactRot * CurrentContact.ContactNormal;


    if (abs(DesiredDeltaVelocity) < .5)
    {
      if (DesiredDeltaVelocity > 0)
        DesiredDeltaVelocity += 0.5;

      else
        DesiredDeltaVelocity -= .5;
    }

    //dvel per unit impulse for A
    glm::vec3 dVelWorlPerUnitImpulse = glm::cross(RelContactPosA, CurrentContact.ContactNormal);
    dVelWorlPerUnitImpulse = A->InvInertia() * dVelWorlPerUnitImpulse;

    if (CurrentContact.A_NoRotate)
      dVelWorlPerUnitImpulse = glm::vec3(0, 0, 0);

    dVelWorlPerUnitImpulse = glm::cross(dVelWorlPerUnitImpulse, RelContactPosA);

    //delta velocity per unit impuse along contact normal
    float dVelPUI = glm::dot(dVelWorlPerUnitImpulse, CurrentContact.ContactNormal);
    dVelPUI += A->InvMass();

    if (B.IsValid())
    {
      //dvel per unit impulse for B
      dVelWorlPerUnitImpulse = glm::cross(RelContactPosB, CurrentContact.ContactNormal);
      dVelWorlPerUnitImpulse = B->InvInertia() * dVelWorlPerUnitImpulse;
      dVelWorlPerUnitImpulse = glm::cross(dVelWorlPerUnitImpulse, RelContactPosB);

      if (CurrentContact.B_NoRotate)
        dVelWorlPerUnitImpulse = glm::vec3(0, 0, 0);

      //delta velocity per unit impuse along contact normal
      dVelPUI += glm::dot(dVelWorlPerUnitImpulse, CurrentContact.ContactNormal);
      dVelPUI += B->InvMass();
    }

    //calculate and transform impulse
    glm::vec3 Impulse;
    Impulse.x = DesiredDeltaVelocity / dVelPUI;
    assert(Impulse.x == Impulse.x);
    Impulse = ContactToWorldRot * Impulse;

    //apply impulse to A
    glm::vec3 VelChange = Impulse * A->InvMass();
    glm::vec3 ImpulsiveTorque = glm::cross(RelContactPosA, Impulse);
    glm::vec3 RotChange = A->InvInertia() * ((PI / 180.0f) * ImpulsiveTorque);
    A->AddImpulseLinear(VelChange);

    if (!CurrentContact.A_NoRotate)
      A->AddImpulseRot(RotChange);



    //LocalVelocities
    LocalContactVelA = glm::cross(A->RotVelocity(), RelContactPosA);
    LocalContactVelA += glm::vec3(A->Velocity());
    LocalContactVelA = WorldToContactRot * LocalContactVelA;
    float ActualDeltaVel = glm::length(LocalContactVelA);

    //apply impulse to B
    //Impulse = -Impulse;
    if (B.IsValid())
    {
      VelChange = -Impulse * B->InvMass();
      ImpulsiveTorque = glm::cross(RelContactPosB, Impulse);
      RotChange = B->InvInertia() * ((PI / 180.0f) * ImpulsiveTorque);
      B->AddImpulseLinear(VelChange);

      if (!CurrentContact.B_NoRotate)
        B->AddImpulseRot(RotChange);
    }

    const float percent = 0.4f; //0.2 to o.8
    const float slop = 0.1f; //0.01 to 0.1
    float totalInvMass = A->InvMass();

    if (B.IsValid())
      totalInvMass += B->InvMass();

    glm::vec3 Correction = CurrentContact.ContactNormal * percent * (CurrentContact.PenetrationDepth /
      (totalInvMass));

    if (CurrentContact.PenetrationDepth - slop > 0)
    {
      A->Position(A->Position() + Correction* A->InvMass());

      if (B.IsValid())
        B->Position(B->Position() - Correction* B->InvMass());
    }
  }


  for (size_t i = 0; i < m_RigidBodies.size(); ++i)
    m_RigidBodies[i]->ApplyImpulses();

}


float PhysicsSystemComponent::Clamp(float min, float max, float value)
{
  if (value > max)
    return max;

  else if (value < min)
    return min;

  else
    return value;
};

glm::mat3 PhysicsSystemComponent::CreateOrthoNormalBasis(glm::vec3 &X_Input)
{
  //asume world y axis (change if needed)
  glm::vec3 YTryOne = glm::vec3(0, 1, 0);
  glm::vec3 ZTryOne = glm::cross(X_Input, YTryOne);
  ZTryOne = glm::normalize(ZTryOne);
  bool one = true;
  //glm::vec3 Y_Output = glm::vec3(0, 1, 0);
  //glm::vec3 Z_Output = glm::cross(X_Input, Y_Output);
  glm::vec3 YTryTwo;
  glm::vec3 ZTryTwo;
  //check that X & Y are not parallel
  if (ZTryOne != ZTryOne)
  {
    //try X axis
    YTryTwo = glm::vec3(1, 0, 0);
    ZTryTwo = glm::cross(X_Input, YTryTwo); 
    one = false;
  }

  
  glm::vec3 RealY1 = glm::cross(ZTryOne, X_Input);
  glm::vec3 RealY2 = glm::cross(ZTryTwo, X_Input);

  glm::vec3 Y_Output;
  glm::vec3 Z_Output;
  if (one)
  {
    X_Input = glm::normalize(X_Input);
    Y_Output = glm::normalize(RealY1);
    Z_Output = glm::normalize(ZTryOne);
  }
  else
  {

    X_Input = glm::normalize(X_Input);
    Y_Output = glm::normalize(RealY2);
    Z_Output = glm::normalize(ZTryTwo);
  }
  

  // may need to be transposed, must test
  return glm::mat3(X_Input, Y_Output, Z_Output);
}

bool PhysicsSystemComponent::CmpFloats(float a, float b, float epsilon)
{
  return (a < (b + epsilon)) && (a >(b - epsilon));
}


//----------------------------------------------------------------------------------
void  PhysicsSystemComponent::FrameRate(float arg)
{
  m_FrameRate = arg;
}
float PhysicsSystemComponent::FrameRate(void)
{
  return m_FrameRate;
}
void  PhysicsSystemComponent::PauseFrameRate(void)
{
  m_IsPaused = true;
}
void  PhysicsSystemComponent::ResumeFrameRate(void)
{
  m_IsPaused = false;
}


META_REGISTER_FUNCTION(PhysicsSystem)
{
  META_INHERIT(PhysicsSystemComponent, "Kepler", "Component");
  META_HANDLE(PhysicsSystemComponent);

  META_ADD_METHOD(PhysicsSystemComponent, AdvOneStep);
  META_ADD_METHOD(PhysicsSystemComponent, PauseFrameRate);
  META_ADD_METHOD(PhysicsSystemComponent, ResumeFrameRate);
  META_ADD_METHOD(PhysicsSystemComponent, LoadCollisionLayerTable);

  META_ADD_PROP(PhysicsSystemComponent, FrameRate);

  META_FINALIZE_PTR(PhysicsSystemComponent, MetaPtr);
}