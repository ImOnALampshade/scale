// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  The RigidBody Component
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// --------------------------------------------------------------------------------
#include "../KeplerCore/component.h"
#include "../glminclude.h"
#include "../KeplerCore/transformcomponent.h"
#ifndef COLLISIONVOLUME_H
#define COLLISIONVOLUME_H
class RigidBodyComponent;
struct AABB_struct
{
  glm::vec3 m_Pos;
  glm::vec3 m_HalfLengths;
};

struct Sphere_struct
{
  glm::vec3 m_Pos;
  float m_Radius = 0.0f;
};

enum ShapeType
{
  AABB,
  OBB,
  SPHERE,
  PLANE
};

class CollisionVolumeComponent : public Component
{
public:
  CollisionVolumeComponent(ShapeType CollisionType);

  //-----------------------------------------
  //virtual functions
  virtual void Initialize(const Json::Object &jsonData);
  //for broadphase
  virtual AABB_struct CalculateAABB(void);
  virtual Sphere_struct CalculateBoundingSphere(void);

  //for inertial values 
  float CalculateMass(float density);
  virtual glm::mat3 CalculateInertia(float Mass); 


  //-------------------------------------------
  //getters/setters
  
  glm::vec3 Position(void);
  glm::mat3 GetRotationMtx(void);

  ::Handle<RigidBodyComponent> Body(void);
  void Body(::Handle<RigidBodyComponent> TheBody);

  ShapeType GetCollisionType(void);

  unsigned char CollisionID(void);
  void CollisionID(unsigned char);

  //------------------------------------------
  //transformation getters, from rigid body data
  glm::mat4 GetTransform(void);
  glm::mat4 GetInvTransform(void);
  
  //Hey look ma! I actually found a use for this
protected: 
  void InitMessages(void);
private:
  ::Handle<RigidBodyComponent> m_RigidBody;
  ::Handle<TransformComponent> m_Transform;
  ShapeType m_CollisionType;
  //message handler
  void SetTheRB(::Handle<RigidBodyComponent> arg);
  unsigned char m_CollisionID;

};

//----------------------------------------------------------------------------------

class Collision_OBBComponent : public CollisionVolumeComponent
{
public:
  Collision_OBBComponent(float HalfWidth = 0, float HalfHeight = 0, float HalfDepth = 0);
  virtual void Initialize(const Json::Object &jsonData);
  virtual glm::mat3 CalculateInertia(float Mass);

  //------------------------------------------------
  // used for BroadPhase
  virtual AABB_struct CalculateAABB(void);
  virtual Sphere_struct CalculateBoundingSphere(void);

  //------------------------------------------------
  // getters/setters
  float HalfWidth(void);
  float HalfHeight(void);
  float HalfDepth(void);
  
  void HalfWidth(float arg);
  void HalfHeight(float arg);
  void HalfDepth(float arg);

  COMPONENT_META
private: 
  float m_HalfWidth;
  float m_HalfHeight;
  float m_HalfDepth;

};


//----------------------------------------------------------------------------------

class Collision_SphereComponent : public CollisionVolumeComponent
{
public:

  Collision_SphereComponent(float Radius = 0);
  void Initialize(const Json::Object &jsonData);
  virtual glm::mat3 CalculateInertia(float Mass);
  //------------------------------------------------
  // used for BroadPhase
  virtual AABB_struct CalculateAABB(void);
  virtual Sphere_struct CalculateBoundingSphere(void);
 
  
  //------------------------------------------------
  // getters/setters
  float Radius(void);
  void Radius(float arg);
  
  COMPONENT_META
private:
  float m_Radius;
};

//----------------------------------------------------------------------------------

class Collision_AABBComponent : public CollisionVolumeComponent
{
public:
  Collision_AABBComponent(float HalfWidth = 0, float HalfHeight = 0, float HalfDepth = 0);
  void Initialize(const Json::Object &jsonData);
  virtual glm::mat3 CalculateInertia(float Mass);
  //------------------------------------------------
  // used for BroadPhase
  virtual AABB_struct CalculateAABB(void);
  virtual Sphere_struct CalculateBoundingSphere(void);

  //------------------------------------------------
  // getters/setters
  void HalfWidth(float arg); 
  void HalfHeight(float arg);
  void HalfDepth(float arg);
  float HalfWidth(void);
  float HalfHeight(void);
  float HalfDepth(void);

  COMPONENT_META
private:
  float m_HalfWidth;
  float m_HalfHeight;
  float m_HalfDepth;

};

//----------------------------------------------------------------------------------

class Collision_PlaneComponent : public CollisionVolumeComponent
{
public:
  Collision_PlaneComponent(glm::vec3 Normal = glm::vec3(0,0,0), float Width = 0, float Height = 0);

  //-----------------------------------------
  //virtual functions
  virtual void Initialize(const Json::Object &jsonData);

  float GetOffsetValue();

  //------------------------------------------------
  // used for BroadPhase
  virtual AABB_struct CalculateAABB(void);
  virtual Sphere_struct CalculateBoundingSphere(void);

  //------------------------------------------------
  // getters/setters
  void Normal(Vec3 arg);
  Vec3 Normal(void);

  COMPONENT_META
private:
  glm::vec3 m_Normal;
  //width and height of zero means infinite
  float m_Width;
  float m_Height;
};


//-----------------------------------------------
#endif