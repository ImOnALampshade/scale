// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  Component for a physics system in a space
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PHYSICSSYSTEM_H
#define PHYSICSSYTEM_H
#include "../KeplerCore/component.h"
#include "../glminclude.h"
#include "Contact.h"
class RigidBodyComponent;
class CollisionVolumeComponent;
class CollisionEngine;
class PhysicsSystemComponent : public Component
{
public:
  void Initialize(const Json::Object &jsonData);
  void Free();
  void Update(float Dt);

  void AdvOneStep(void);
  void FrameRate(float arg);
  float FrameRate(void);
  void PauseFrameRate(void);
  void ResumeFrameRate(void);
  void AddContact(Contact arg);
  void LoadCollisionLayerTable(String filename);

  COMPONENT_META
private: 

  std::vector<::Handle<RigidBodyComponent>> m_RigidBodies;
  std::vector<::Handle<CollisionVolumeComponent>> m_CVs;
  std::vector<Contact> m_Contacts;

  void AddRigidBody(::Handle<RigidBodyComponent> arg);
  void PruneRigidBodies(void);
  void AddCollisionVolume(::Handle<CollisionVolumeComponent> arg);
  void PruneCollisionVolumes(void);

  void Integrate(void);
  /***************Collision detection***********/
  CollisionEngine * m_CollisionEngine;


  /*****************************Misc Math************/

  glm::mat3 CreateOrthoNormalBasis(glm::vec3& X_Input);
  bool CmpFloats(float a, float b, float epsilon);
  float Clamp(float min, float max, float value);

  /***************Collision Resolution**********/
  //void ResolveContactsPart(Contact& CurrentContact);

  void ResolveContacts(void);

  /***************Constraints********************/
  //void ResolveConstraints(void);

  float m_FrameRate;
  float m_Accum;
  bool m_IsPaused;
};


#endif