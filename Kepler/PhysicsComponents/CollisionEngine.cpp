// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  CollisionEngine, part of Physics System
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------
#include "CollisionEngine.h"
#include "Contact.h"
#include "CollisionVolume.h"
#include "PhysicsSystem.h"
#include "RigidBodyComponent.h"
#include <algorithm>

#include <fstream>
#include <sstream>

CollisionEngine::CollisionEngine(PhysicsSystemComponent * physSystem)
{
  m_phySystem = physSystem;

 
  ClearCollisionTable();
  //LoadCollisionLayerTable("CollisionLayerTables/TestTable.txt");
}

void CollisionEngine::AddCollisionLayerRule(unsigned char ID1, unsigned char ID2, unsigned char Rule)
{
  assert(ID1 < 100);
  assert(ID2 < 100);
  assert(Rule < COLLISION_LAYER_MODE::SIZE);

  m_CollisionLayerMatrix[ID1][ID2] = Rule;
  m_CollisionLayerMatrix[ID2][ID1] = Rule;
}

void CollisionEngine::ClearCollisionTable(void)
{
  for (int i = 0; i < 100; ++i)
    for (int j = 0; j < 100; ++j)
      m_CollisionLayerMatrix[i][j] = COLLISION_LAYER_MODE::COLLIDE;
}
void CollisionEngine::LoadCollisionLayerTable(String filename)
{
   ClearCollisionTable();
  std::ifstream InputFile(filename.CStr());
  assert(InputFile.is_open());
  while (!InputFile.eof())
  {
    std::string Line;
    getline(InputFile, Line);

    if (Line[0] == '#' || Line == "" || Line == " ")
      continue;

    std::string Token;
    std::stringstream LineStream(Line);
    LineStream >> Token;

    unsigned int arg1;
    unsigned int arg2;

    if (!(LineStream >> arg1 && LineStream >> arg2))
    {
      printf("Error reading Line in collision table file\n");
      printf("File : %s, Line %s\n", filename.c_str(), Line.c_str());
      continue;
    }

    if (Token == "COLLIDE:")
    {
      AddCollisionLayerRule(arg1, arg2, COLLISION_LAYER_MODE::COLLIDE);
    }
    else if (Token == "NOCOLLIDE:")
    {
      AddCollisionLayerRule(arg1, arg2, COLLISION_LAYER_MODE::NO_COLLIDE);
    }
    else if (Token == "GHOST:")
    {
      AddCollisionLayerRule(arg1, arg2, COLLISION_LAYER_MODE::GHOST);
    }

  }
}
void CollisionEngine::DispatchCollisionMsg(::Handle<CollisionVolumeComponent> A, ::Handle<CollisionVolumeComponent> B)
{
  A->Owner()->Dispatch("OnCollide", B->Owner()->Handle());
  B->Owner()->Dispatch("OnCollide", A->Owner()->Handle());
}

void CollisionEngine::AddCVolume(::Handle<CollisionVolumeComponent>arg)
{
  m_CVolumes.push_back(arg);
}
void CollisionEngine::PruneCollisionVolumes(void)
{
  m_CVolumes.erase(
    std::remove_if(m_CVolumes.begin(), m_CVolumes.end(), [](::Handle<CollisionVolumeComponent> arg)->bool {
    return !arg.IsValid();
  }),
    m_CVolumes.end()
    );
}
void CollisionEngine::DetectCollisions(void)
{
  for (unsigned int i = 0; i < m_CVolumes.size(); ++i)
  {
    for (unsigned int j = i; j < m_CVolumes.size(); ++j)
    {
      // no broad phase
      if (j != i)
      {
        NarrowPhase(m_CVolumes[i], m_CVolumes[j]);
      }
    }
  }
}
void CollisionEngine::NarrowPhase(::Handle<CollisionVolumeComponent> A, ::Handle<CollisionVolumeComponent> B)
{
  //check if this layer doesn't collide
  if (m_CollisionLayerMatrix[A->CollisionID()][B->CollisionID()] == COLLISION_LAYER_MODE::NO_COLLIDE)
    return;

  switch (A->GetCollisionType())
  {
  case ShapeType::AABB:
    switch (B->GetCollisionType())
    {
    case ShapeType::AABB:
      CheckCollisionAABBvAABB(A->Handle().as<Collision_AABBComponent>(),
                              B->Handle().as<Collision_AABBComponent>());
      break;
    case ShapeType::OBB:
      CheckCollisionOBBvAABB(B->Handle().as<Collision_OBBComponent>(),
                             A->Handle().as<Collision_AABBComponent>());
      break;
    case ShapeType::PLANE:
      CheckCollisionAABBvPlane(A->Handle().as<Collision_AABBComponent>(),
                               B->Handle().as<Collision_PlaneComponent>());
      break;
    case ShapeType::SPHERE:
      CheckCollisionSpherevAABB(B->Handle().as<Collision_SphereComponent>(),
                                A->Handle().as<Collision_AABBComponent> ());
      break;
    }
    break;
  case ShapeType::OBB:
    switch (B->GetCollisionType())
    {
    case ShapeType::AABB:
      CheckCollisionOBBvAABB(A->Handle().as<Collision_OBBComponent>(),
                             B->Handle().as<Collision_AABBComponent>());
      break;
    case ShapeType::OBB:
      CheckCollisionOBBvOBB(A->Handle().as<Collision_OBBComponent>(),
                            B->Handle().as<Collision_OBBComponent>());
      break;
    case ShapeType::PLANE:
      CheckCollisionOBBvPlane(A->Handle().as<Collision_OBBComponent> (),
                              B->Handle().as<Collision_PlaneComponent>());
      break;
    case ShapeType::SPHERE:
      CheckCollisionSpherevOBB(B->Handle().as<Collision_SphereComponent>(),
                               A->Handle().as<Collision_OBBComponent>());
      break;
    }
    break;
  case ShapeType::PLANE:
    switch (B->GetCollisionType())
    {
    case ShapeType::AABB:
      CheckCollisionAABBvPlane(B->Handle().as<Collision_AABBComponent>(),
                               A->Handle().as<Collision_PlaneComponent>());
      break;
    case ShapeType::OBB:
      CheckCollisionOBBvPlane(B->Handle().as<Collision_OBBComponent> (),
                              A->Handle().as<Collision_PlaneComponent>());
      break;
    case ShapeType::PLANE:
      //no plane v plane collision
      break;
    case ShapeType::SPHERE:
      CheckCollisionSpherevPlane(B->Handle().as<Collision_SphereComponent >(),
                                 A->Handle().as<Collision_PlaneComponent> ());
      break;
    }
    break;
  case ShapeType::SPHERE:
    switch (B->GetCollisionType())
    {
    case ShapeType::AABB:
      CheckCollisionSpherevAABB(A->Handle().as<Collision_SphereComponent> (),
                                B->Handle().as<Collision_AABBComponent>   ());
      break;
    case ShapeType::OBB:
      CheckCollisionSpherevOBB(A->Handle().as<Collision_SphereComponent>(),
                               B->Handle().as<Collision_OBBComponent>());
      break;
    case ShapeType::PLANE:
      CheckCollisionSpherevPlane(A->Handle().as<Collision_SphereComponent>(),
                                 B->Handle().as<Collision_PlaneComponent> ());
      break;
    case ShapeType::SPHERE:
      CheckCollisionSpherevSphere(A->Handle().as<Collision_SphereComponent>(),
                                  B->Handle().as<Collision_SphereComponent>());
      break;
    }
    break;
  }
}

void CollisionEngine::CheckCollisionSpherevSphere(::Handle<Collision_SphereComponent>  A, ::Handle<Collision_SphereComponent>  B)
{
  glm::vec3 APos = A->Position();
  glm::vec3 BPos = B->Position();
  glm::vec3 Displacement = BPos - APos;

  float Distance = glm::length(Displacement);

  float CombinedRadius = (A)->Radius() +
                         (B)->Radius();

  if (Distance > CombinedRadius)
  {
    //no collision
    return;
  }

  //if collision we can respond to
  if (A->Body().IsValid() && B->Body().IsValid() &&
    !A->Body()->GhostMode() && !B->Body()->GhostMode()
    && (m_CollisionLayerMatrix[A->CollisionID()][B->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
  {

    Contact NewContact;
    NewContact.A = A->Body();
    NewContact.B = B->Body();
    glm::normalize(Displacement);
    NewContact.ContactNormal = -Displacement;
    NewContact.PenetrationDepth = CombinedRadius - Distance;
    NewContact.ContactPoint = APos + Displacement * ((A)->Radius() - NewContact.PenetrationDepth);
    NewContact.Restitution = -1.0f;
    m_phySystem->AddContact(NewContact);
  }
  
  DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());

}
void CollisionEngine::CheckCollisionSpherevOBB(::Handle<Collision_SphereComponent>  A, ::Handle<Collision_OBBComponent>  B)
{

  glm::vec3 BoxPos = B->Position();
  glm::mat3 BoxRot = B->GetRotationMtx();
  glm::mat3 InvBoxRot = glm::inverse(BoxRot);
  glm::vec3 SpherePos = A->Position();

  //convert to box's model space
  SpherePos = SpherePos - BoxPos;
  SpherePos = InvBoxRot * SpherePos;


  //do aabb v sphere collision test
  glm::vec3 ClampedPosition;
  ClampedPosition.x = Clamp(-B->HalfWidth(), B->HalfWidth(), SpherePos.x);
  ClampedPosition.y = Clamp(-B->HalfHeight(), B->HalfHeight(), SpherePos.y);
  ClampedPosition.z = Clamp(-B->HalfDepth(), B->HalfDepth(), SpherePos.z);

  glm::vec3 Displacement = ClampedPosition - SpherePos;
  float Distance = glm::length(Displacement);

  //check for collision
  if (Distance > A->Radius())
    return;

  //convert results back to world space
  SpherePos = BoxRot * SpherePos;
  SpherePos = SpherePos + BoxPos;

  ClampedPosition = BoxRot * ClampedPosition;
  ClampedPosition = ClampedPosition + BoxPos;


  //if collision we can respond to
  if (A->Body().IsValid() && B->Body().IsValid() &&
    !A->Body()->GhostMode() && !B->Body()->GhostMode()
    && (m_CollisionLayerMatrix[A->CollisionID()][B->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
  {
    //generate contact
    Contact NewContact;
    NewContact.A = A->Body();
    NewContact.B = B->Body();
    NewContact.ContactNormal = SpherePos - ClampedPosition;
    glm::normalize(NewContact.ContactNormal);
    NewContact.PenetrationDepth = A->Radius() - Distance;
    NewContact.Restitution = -1.0f;
    m_phySystem->AddContact(NewContact);
  }

  DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());
}
void CollisionEngine::CheckCollisionSpherevAABB(::Handle<Collision_SphereComponent>  A, ::Handle<Collision_AABBComponent>  B)
{


  glm::vec3 AABBPos = B->Position();
  glm::vec3 SpherePos = A->Position();

  glm::vec3 ClampedPosition;
  ClampedPosition.x = Clamp(AABBPos.x - B->HalfWidth(),  AABBPos.x + B->HalfWidth(), SpherePos.x);
  ClampedPosition.y = Clamp(AABBPos.y - B->HalfHeight(), AABBPos.y + B->HalfHeight(), SpherePos.y);
  ClampedPosition.z = Clamp(AABBPos.z - B->HalfDepth(),  AABBPos.z + B->HalfDepth(), SpherePos.z);

  glm::vec3 Displacement = ClampedPosition - SpherePos;
  float Distance = glm::length(Displacement);

  //check for collision
  if (Distance > A->Radius())
    return;


  //if collision we can respond to
  if (A->Body().IsValid() && B->Body().IsValid() &&
    !A->Body()->GhostMode() && !B->Body()->GhostMode()
    && (m_CollisionLayerMatrix[A->CollisionID()][B->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
  {
    Contact NewContact;
    NewContact.A = A->Body();
    NewContact.B = B->Body();
    NewContact.ContactNormal = SpherePos - ClampedPosition;
    glm::normalize(NewContact.ContactNormal);
    NewContact.ContactPoint = ClampedPosition;
    NewContact.PenetrationDepth = A->Radius() - Distance;
    NewContact.Restitution = -1.0f;
    NewContact.A_NoRotate = true;
    NewContact.B_NoRotate = true;
    m_phySystem->AddContact(NewContact);
  }

  DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());
}


void CollisionEngine::CheckCollisionSpherevPlane(::Handle<Collision_SphereComponent>  A, ::Handle<Collision_PlaneComponent>  B)
{
  //plane equation
  float Distance = glm::dot(B->Normal(), A->Position()) - B->GetOffsetValue() - A->Radius();

  if (Distance >= 0)
    return;

  //if collision we can respond to
  if (A->Body().IsValid() && B->Body().IsValid() &&
    !A->Body()->GhostMode() && !B->Body()->GhostMode()
    && (m_CollisionLayerMatrix[A->CollisionID()][B->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
  {
    Contact NewContact;
    NewContact.A = A->Body();
    NewContact.B = nullptr;
    NewContact.ContactNormal = B->Normal();
    NewContact.PenetrationDepth = -Distance;
    NewContact.ContactPoint = A->Position() - B->Normal() * (Distance + A->Radius());
    NewContact.Restitution = -1.0f;
    m_phySystem->AddContact(NewContact);
  }
  DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());
}

void CollisionEngine::CheckCollisionOBBvOBB(::Handle<Collision_OBBComponent>  A, ::Handle<Collision_OBBComponent>  B)
{
  //TODO: Actual Collision mesh for cubes!!!!!
  ContactList Contacts;

  auto calcRadi = [](glm::vec3 axis, ::Handle<Collision_OBBComponent>  A, ::Handle<Collision_OBBComponent>  B, glm::vec3 AOne, glm::vec3 ATwo, glm::vec3 AThree,
    glm::vec3 BOne, glm::vec3 BTwo, glm::vec3 BThree)
  {
    float radii;
    AOne *= A->HalfWidth();
    ATwo *= A->HalfHeight();
    AThree *= A->HalfDepth();

    BOne *= B->HalfWidth();
    BTwo *= B->HalfHeight();
    BThree *= B->HalfDepth();

    radii = abs(glm::dot(AOne,(axis))) +
            abs(glm::dot(ATwo,(axis))) +
            abs(glm::dot(AThree,(axis))) +
            abs(glm::dot(BOne,(axis))) +
            abs(glm::dot(BTwo,(axis))) +
            abs(glm::dot(BThree,axis));
    return radii;
  };

  //generate each vertex for A
  glm::vec4 AVertices[8] = {
    glm::vec4(-A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth(), 1)
  };

  //generate each vertex for B
  glm::vec4 BVertices[8] = {
    glm::vec4(-B->HalfWidth(), -B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(-B->HalfWidth(), -B->HalfHeight(), +B->HalfDepth(), 1),
    glm::vec4(-B->HalfWidth(), +B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(-B->HalfWidth(), +B->HalfHeight(), +B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), -B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), -B->HalfHeight(), +B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), +B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), +B->HalfHeight(), +B->HalfDepth(), 1)

  };

  glm::mat4 ATransform = A->GetTransform();
  glm::mat4 BTransform = B->GetTransform();
  for (int i = 0; i < 8; ++i)
  {
    //convert points to world space
    AVertices[i] = ATransform * AVertices[i];
    BVertices[i] = BTransform * BVertices[i];
  }

  int Lines[12][2] =
  {
    { 0, 1 },
    { 0, 2 },
    { 0, 4 },
    { 1, 3 },
    { 1, 5 },
    { 2, 6 },
    { 2, 3 },
    { 3, 7 },
    { 4, 6 },
    { 4, 5 },
    { 5, 7 },
    { 6, 7 }
  };

  
  glm::mat3 ARotate = A->GetRotationMtx();
  glm::mat3 BRotate = B->GetRotationMtx();
  glm::vec3 Translation = B->Position() - A->Position();

  int index = -1;
  float distance = -1;


  glm::vec3 axis[15];

  axis[0] = ARotate * glm::vec3(1, 0, 0); //object A basis
  axis[1] = ARotate * glm::vec3(0, 1, 0); //object A basis
  axis[2] = ARotate * glm::vec3(0, 0, 1); //object A basis
  axis[3] = BRotate * glm::vec3(1, 0, 0); //object B basis
  axis[4] = BRotate * glm::vec3(0, 1, 0); //object B basis
  axis[5] = BRotate * glm::vec3(0, 0, 1); //object B basis
  axis[6] =  glm::cross(axis[0],axis[3]); // U0xV0
  axis[7] =  glm::cross(axis[0],axis[4]); // U0xV1
  axis[8] =  glm::cross(axis[0],axis[5]); // U0xV2
  axis[9] =  glm::cross(axis[1],axis[3]); // U1xV0
  axis[10] = glm::cross(axis[1],axis[4]); // U1xV1
  axis[11] = glm::cross(axis[1],axis[5]); // U1xV2
  axis[12] = glm::cross(axis[2],axis[3]); // U2xV0
  axis[13] = glm::cross(axis[2],axis[4]); // U2xV1
  axis[14] = glm::cross(axis[2],axis[5]); // U2xV2
  index = -1;


  float depth = 100000000;
  float TransDotBasis;
  float ProjectedDistance;
  for (int i = 0; i < 15; ++i)
  {
    glm::normalize(axis[i]);
    TransDotBasis = glm::dot(Translation,axis[i]);
    ProjectedDistance = calcRadi(axis[i], A, B, axis[0], axis[1], axis[2], axis[3], axis[4], axis[5]);
    if (abs(TransDotBasis) > abs(ProjectedDistance))
      return;
    if (depth > abs(ProjectedDistance) - abs(TransDotBasis) && abs(ProjectedDistance) - abs(TransDotBasis) > 0.0001f)
    {
      index = i;
      depth = abs(ProjectedDistance) - abs(TransDotBasis);

      
    }
  }

  DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());
  
  // edge-edge
  if (index > 5)
  {
    //take normal to box's coordinate frame

    //start at box center
    //check sign of normal, add corresponding half extent

    //gives corner
  }
  else //something face
  {
    
  }

}
void CollisionEngine::CheckCollisionOBBvAABB(::Handle<Collision_OBBComponent> A, ::Handle<Collision_AABBComponent>  B)
{
  //TODO: this case -> same as OBBvOBB
  //TODO: Actual Collision mesh for cubes!!!!!
  ContactList Contacts;

  auto calcRadi = [](glm::vec3 axis, ::Handle<Collision_OBBComponent>  A, ::Handle<Collision_AABBComponent>  B, glm::vec3 AOne, glm::vec3 ATwo, glm::vec3 AThree,
    glm::vec3 BOne, glm::vec3 BTwo, glm::vec3 BThree)
  {
    float radii;
    AOne *= A->HalfWidth();
    ATwo *= A->HalfHeight();
    AThree *= A->HalfDepth();

    BOne *= B->HalfWidth();
    BTwo *= B->HalfHeight();
    BThree *= B->HalfDepth();

    radii = abs(glm::dot(AOne, (axis))) +
      abs(glm::dot(ATwo, (axis))) +
      abs(glm::dot(AThree, (axis))) +
      abs(glm::dot(BOne, (axis))) +
      abs(glm::dot(BTwo, (axis))) +
      abs(glm::dot(BThree, axis));
    return radii;
  };

  //generate each vertex for A
  glm::vec4 AVertices[8] = {
    glm::vec4(-A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth(), 1)
  };

  //generate each vertex for B
  glm::vec4 BVertices[8] = {
    glm::vec4(-B->HalfWidth(), -B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(-B->HalfWidth(), -B->HalfHeight(), +B->HalfDepth(), 1),
    glm::vec4(-B->HalfWidth(), +B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(-B->HalfWidth(), +B->HalfHeight(), +B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), -B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), -B->HalfHeight(), +B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), +B->HalfHeight(), -B->HalfDepth(), 1),
    glm::vec4(+B->HalfWidth(), +B->HalfHeight(), +B->HalfDepth(), 1)

  };

  glm::mat4 ATransform = A->GetTransform();
  glm::mat4 BTransform(1, 0, 0, 0,
                       0, 1, 0, 0,
                       0, 0, 1, 0,
                       0, 0, 0, 1);
  BTransform = glm::translate(BTransform, B->Position());

  for (int i = 0; i < 8; ++i)
  {
    //convert points to world space
    AVertices[i] = ATransform * AVertices[i];
    BVertices[i] = BTransform * BVertices[i];
  }

  int Lines[12][2] =
  {
    { 0, 1 },
    { 0, 2 },
    { 0, 4 },
    { 1, 3 },
    { 1, 5 },
    { 2, 6 },
    { 2, 3 },
    { 3, 7 },
    { 4, 6 },
    { 4, 5 },
    { 5, 7 },
    { 6, 7 }
  };


  glm::mat3 ARotate = A->GetRotationMtx();
  glm::vec3 Translation = B->Position() - A->Position();

  int index = -1;
  float distance = -1;


  glm::vec3 axis[15];

  axis[0] = ARotate * glm::vec3(1, 0, 0); //object A basis
  axis[1] = ARotate * glm::vec3(0, 1, 0); //object A basis
  axis[2] = ARotate * glm::vec3(0, 0, 1); //object A basis
  axis[3] = glm::vec3(1, 0, 0); //object B basis
  axis[4] = glm::vec3(0, 1, 0); //object B basis
  axis[5] = glm::vec3(0, 0, 1); //object B basis
  axis[6] = glm::cross(axis[0], axis[3]); // U0xV0
  axis[7] = glm::cross(axis[0], axis[4]); // U0xV1
  axis[8] = glm::cross(axis[0], axis[5]); // U0xV2
  axis[9] = glm::cross(axis[1], axis[3]); // U1xV0
  axis[10] = glm::cross(axis[1], axis[4]); // U1xV1
  axis[11] = glm::cross(axis[1], axis[5]); // U1xV2
  axis[12] = glm::cross(axis[2], axis[3]); // U2xV0
  axis[13] = glm::cross(axis[2], axis[4]); // U2xV1
  axis[14] = glm::cross(axis[2], axis[5]); // U2xV2
  index = -1;


  float depth = 100000000;
  float TransDotBasis;
  float ProjectedDistance;
  for (int i = 0; i < 15; ++i)
  {
    glm::normalize(axis[i]);
    TransDotBasis = glm::dot(Translation, axis[i]);
    ProjectedDistance = calcRadi(axis[i], A, B, axis[0], axis[1], axis[2], axis[3], axis[4], axis[5]);
    if (abs(TransDotBasis) > abs(ProjectedDistance))
      return;
    if (depth > abs(ProjectedDistance) - abs(TransDotBasis) && abs(ProjectedDistance) - abs(TransDotBasis) > 0.0001f)
    {
      index = i;
      depth = abs(ProjectedDistance) - abs(TransDotBasis);


    }
  }

  DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());
}
void CollisionEngine::CheckCollisionOBBvPlane(::Handle<Collision_OBBComponent>  A, ::Handle<Collision_PlaneComponent>  B)
{

  //generate each vertex
  glm::vec4 Vertices[8] = {
    glm::vec4(-A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(-A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth(), 1),
    glm::vec4(+A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth(), 1)
  };

  //calculate transform
  glm::mat4 Translate = glm::translate(glm::mat4(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1), A->Position());
  glm::mat4 Rotate =glm::mat4(A->GetRotationMtx());

  glm::mat4 Transform = Translate * Rotate;

  for (int i = 0; i < 8; ++i)
  {
    //transform each vertex
    Vertices[i] = Transform * Vertices[i];

    //plane equation
    float Distance = glm::dot(B->Normal(), Vertices[i].xyz()) - B->GetOffsetValue();

    //check if on wrong side
    if (Distance >= 0)
      continue;


    //if collision we can respond to
    if (A->Body().IsValid() && B->Body().IsValid() &&
      !A->Body()->GhostMode() && !B->Body()->GhostMode()
      && (m_CollisionLayerMatrix[A->CollisionID()][B->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
    {
      // generate contact
      Contact NewContact;
      NewContact.A = A->Body();
      NewContact.B = nullptr;// B->Body();
      NewContact.ContactNormal = B->Normal();
      NewContact.PenetrationDepth = -Distance;
      NewContact.ContactPoint = Vertices[i].xyz();
      NewContact.Restitution = -1.0f;
      m_phySystem->AddContact(NewContact);
    }
  }


}
void CollisionEngine::CheckCollisionAABBvAABB(::Handle<Collision_AABBComponent>  A, ::Handle<Collision_AABBComponent>  B)
{

  //generate each vertex for B
  glm::vec3 BVertices[8] = {
    glm::vec3(-B->HalfWidth(), -B->HalfHeight(), -B->HalfDepth()),
    glm::vec3(-B->HalfWidth(), -B->HalfHeight(), +B->HalfDepth()),
    glm::vec3(-B->HalfWidth(), +B->HalfHeight(), -B->HalfDepth()),
    glm::vec3(-B->HalfWidth(), +B->HalfHeight(), +B->HalfDepth()),
    glm::vec3(+B->HalfWidth(), -B->HalfHeight(), -B->HalfDepth()),
    glm::vec3(+B->HalfWidth(), -B->HalfHeight(), +B->HalfDepth()),
    glm::vec3(+B->HalfWidth(), +B->HalfHeight(), -B->HalfDepth()),
    glm::vec3(+B->HalfWidth(), +B->HalfHeight(), +B->HalfDepth())
  };

  glm::vec3 AVertices[8] = {
    glm::vec3(-A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(-A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth()),
    glm::vec3(-A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(-A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth())
  };

  glm::vec3 APosition = A->Position();
  glm::vec3 BPosition = B->Position();

  bool collision = false;

  for (int i = 0; i < 8; ++i)
  {

    //find and generate contacts, point by point

    if (CheckCollisionPointforAABBvAABB(BVertices[i] - APosition + B->Position(), A, B))
      collision = true;

    if (CheckCollisionPointforAABBvAABB(AVertices[i] + APosition - B->Position(), B, A))
      collision = true;

  }

  if (collision)
    DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());

}
bool CollisionEngine::CheckCollisionPointforAABBvAABB(glm::vec3 Point, ::Handle<Collision_AABBComponent>  A, ::Handle<Collision_AABBComponent>   PointAABB)
{
  float tempdepth;
  float depthX;
  float depthY;
  float depthZ;


  //X axis
  if (Point.x > A->HalfWidth())
    return false;

  depthX = A->HalfWidth() - Point.x;

  if (Point.x < -A->HalfWidth())
    return false;

  tempdepth = Point.x + A->HalfWidth();
  if (tempdepth < depthX)
    depthX = tempdepth;


  //Y axis
  if (Point.y > A->HalfHeight())
    return false;

  depthY = A->HalfHeight() - Point.y;

  if (Point.y < -A->HalfHeight())
    return false;

  tempdepth = Point.y + A->HalfHeight();
  if (tempdepth < depthY)
    depthY = tempdepth;

  //Z axis
  if (Point.z > A->HalfDepth())
    return false;

  depthZ = A->HalfDepth() - Point.z;

  if (Point.z < -A->HalfDepth())
    return false;

  tempdepth = Point.z + A->HalfDepth();
  if (tempdepth < depthY)
    depthY = tempdepth;

  glm::vec3 ContactNormal;
  float depth;


  //find penetration of least depth
  if (depthX < depthY)
  {
    if (depthZ < depthX)
    {
      depth = depthZ;
      if (Point.z > 0)
        ContactNormal = glm::vec3(0, 0, 1);
      else
        ContactNormal = glm::vec3(0, 0, -1);
    }
    else
    {
      depth = depthX;
      if (Point.x > 0)
        ContactNormal = glm::vec3(1, 0, 0);
      else
        ContactNormal = glm::vec3(-1, 0, 0);
    }
  }
  else
  {
    if (depthZ < depthY)
    {
      depth = depthZ;
      if (Point.z > 0)
        ContactNormal = glm::vec3(0, 0, 1);
      else
        ContactNormal = glm::vec3(0, 0, -1);
    }
    else
    {
      depth = depthY;
      if (Point.x > 0)
        ContactNormal = glm::vec3(0, 1, 0);
      else
        ContactNormal = glm::vec3(0, -1, 0);
    }
  }

  //if collision we can respond to
  //if collision we can respond to
  if (A->Body().IsValid() && PointAABB->Body().IsValid() &&
    !A->Body()->GhostMode() && !PointAABB->Body()->GhostMode()
    && (m_CollisionLayerMatrix[A->CollisionID()][PointAABB->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
  {
    //generate contact
    Contact NewContact;
    NewContact.A = A->Body();
    NewContact.B = PointAABB->Body();
    NewContact.Restitution = -1.0f;
    NewContact.PenetrationDepth = depth;
    NewContact.ContactNormal = -ContactNormal;
    NewContact.ContactPoint = Point;
    NewContact.A_NoRotate = true;
    NewContact.B_NoRotate = true;

    m_phySystem->AddContact(NewContact);
  }
  return true;
}

void CollisionEngine::CheckCollisionAABBvPlane(::Handle<Collision_AABBComponent>  A, ::Handle<Collision_PlaneComponent>  B)
{

  //generate each vertex
  glm::vec3 Vertices[8] = {
    glm::vec3(-A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(-A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth()),
    glm::vec3(-A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(-A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), -A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), -A->HalfHeight(), +A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), +A->HalfHeight(), -A->HalfDepth()),
    glm::vec3(+A->HalfWidth(), +A->HalfHeight(), +A->HalfDepth())
  };

  //calculate transform
  glm::vec3 APos = A->Position();
  bool collision = false;

  for (int i = 0; i < 8; ++i)
  {
    //transform each vertex
    Vertices[i] = Vertices[i] + APos;

    //plane equation
    float Distance = glm::dot(B->Normal(),Vertices[i]) - B->GetOffsetValue();

    //check if on wrong side
    if (Distance >= 0)
      continue;

    //if collision we can respond to
    if (A->Body().IsValid() && B->Body().IsValid() &&
      !A->Body()->GhostMode() && !B->Body()->GhostMode()
      && (m_CollisionLayerMatrix[A->CollisionID()][B->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
    {
      // generate contact
      Contact NewContact;
      NewContact.A = A->Body();
      NewContact.B = nullptr;
      NewContact.ContactNormal = B->Normal();

      NewContact.PenetrationDepth = -Distance;
      NewContact.ContactPoint = A->Position();
      NewContact.Restitution = -1.0f;
      m_phySystem->AddContact(NewContact);
    }
    collision = true;
  }

  if (collision)
    DispatchCollisionMsg(A->Handle().as<CollisionVolumeComponent>(), B->Handle().as<CollisionVolumeComponent>());
}
void CollisionEngine::CheckCollisionPlanevPlane(::Handle<Collision_PlaneComponent>  A, ::Handle<Collision_PlaneComponent>  B)
{}

bool CollisionEngine::CheckCollisionPointvOBBNoContact(glm::vec4 Point, ::Handle<Collision_OBBComponent>  A)
{


  glm::mat4 Transform = A->GetTransform();
  glm::mat4 InvTransform = A->GetInvTransform();

  //convert point to Box model space
  Point = InvTransform * Point;

  //X axis
  if (Point.x > A->HalfWidth())
    return false;

  if (Point.x < -A->HalfWidth())
    return false;

  //Y axis
  if (Point.y > A->HalfHeight())
    return false;

  if (Point.y < -A->HalfHeight())
    return false;

  //Z axis
  if (Point.z > A->HalfDepth())
    return false;

  if (Point.z < -A->HalfDepth())
    return false;

  //penetrating on all axis
  return true;

}
void CollisionEngine::CheckCollisionPointforAABBvOBB(glm::vec3 Point, ::Handle<Collision_AABBComponent>  A, ::Handle<Collision_OBBComponent>  PointOBB)
{
  float tempdepth;
  float depthX;
  float depthY;
  float depthZ;


  //X axis
  if (Point.x > A->HalfWidth())
    return;

  depthX = A->HalfWidth() - Point.x;

  if (Point.x < -A->HalfWidth())
    return;

  tempdepth = Point.x + A->HalfWidth();
  if (tempdepth < depthX)
    depthX = tempdepth;


  //Y axis
  if (Point.y > A->HalfHeight())
    return;

  depthY = A->HalfHeight() - Point.y;

  if (Point.y < -A->HalfHeight())
    return;

  tempdepth = Point.y + A->HalfHeight();
  if (tempdepth < depthY)
    depthY = tempdepth;

  //Z axis
  if (Point.z > A->HalfDepth())
    return;

  depthZ = A->HalfDepth() - Point.z;

  if (Point.z < -A->HalfDepth())
    return;

  tempdepth = Point.z + A->HalfDepth();
  if (tempdepth < depthY)
    depthY = tempdepth;

  glm::vec3 ContactNormal;
  float depth;


  //find penetration of least depth
  if (depthX < depthY)
  {
    if (depthZ < depthX)
    {
      depth = depthZ;
      if (Point.z > 0)
        ContactNormal = glm::vec3(0, 0, 1);
      else
        ContactNormal = glm::vec3(0, 0, -1);
    }
    else
    {
      depth = depthX;
      if (Point.x > 0)
        ContactNormal = glm::vec3(1, 0, 0);
      else
        ContactNormal = glm::vec3(-1, 0, 0);
    }
  }
  else
  {
    if (depthZ < depthY)
    {
      depth = depthZ;
      if (Point.z > 0)
        ContactNormal = glm::vec3(0, 0, 1);
      else
        ContactNormal = glm::vec3(0, 0, -1);
    }
    else
    {
      depth = depthY;
      if (Point.x > 0)
        ContactNormal = glm::vec3(0, 1, 0);
      else
        ContactNormal = glm::vec3(0, -1, 0);
    }
  }

  //if collision we can respond to
  if (A->Body().IsValid() && PointOBB->Body().IsValid() &&
    !A->Body()->GhostMode() && !PointOBB->Body()->GhostMode()
    && (m_CollisionLayerMatrix[A->CollisionID()][PointOBB->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
  {
    //generate contact
    Contact NewContact;
    NewContact.A = A->Body();
    NewContact.B = PointOBB->Body();
    NewContact.Restitution = -1.0f;
    NewContact.PenetrationDepth = depth;
    NewContact.ContactNormal = ContactNormal;
    NewContact.ContactPoint = Point;

    m_phySystem->AddContact(NewContact);
  }
}
bool CollisionEngine::CheckCollisionPointvAABBNoContact(glm::vec3 Point, ::Handle<Collision_AABBComponent>  A)
{

  //X axis
  if (Point.x > A->HalfWidth())
    return false;

  if (Point.x < -A->HalfWidth())
    return false;

  //Y axis
  if (Point.y > A->HalfHeight())
    return false;

  if (Point.y < -A->HalfHeight())
    return false;

  //Z axis
  if (Point.z > A->HalfDepth())
    return false;

  if (Point.z < -A->HalfDepth())
    return false;

  //penetrating on all axis
  return true;
}

void CollisionEngine::CheckCollisionPointforOBBvAABB(glm::vec4 Point, ::Handle<Collision_OBBComponent>  A, ::Handle<Collision_AABBComponent>  PointAABB)
{
  //calculate transform


  glm::mat4 Transform = A->GetTransform();
  glm::mat4 InvTransform = A->GetInvTransform();

  //convert point to Box model space
  Point = InvTransform * Point;

  float tempdepth;
  float depthX;
  float depthY;
  float depthZ;


  //X axis
  if (Point.x > A->HalfWidth())
    return;

  depthX = A->HalfWidth() - Point.x;

  if (Point.x < -A->HalfWidth())
    return;

  tempdepth = Point.x + A->HalfWidth();
  if (tempdepth < depthX)
    depthX = tempdepth;


  //Y axis
  if (Point.y > A->HalfHeight())
    return;

  depthY = A->HalfHeight() - Point.y;

  if (Point.y < -A->HalfHeight())
    return;

  tempdepth = Point.y + A->HalfHeight();
  if (tempdepth < depthY)
    depthY = tempdepth;

  //Z axis
  if (Point.z > A->HalfDepth())
    return;

  depthZ = A->HalfDepth() - Point.z;

  if (Point.z < -A->HalfDepth())
    return;

  tempdepth = Point.z + A->HalfDepth();
  if (tempdepth < depthY)
    depthY = tempdepth;

  glm::vec3 ContactNormal;
  float depth;


  //find penetration of least depth
  if (depthX < depthY)
  {
    if (depthZ < depthX)
    {
      depth = depthZ;
      if (Point.z > 0)
        ContactNormal = glm::vec3(0, 0, 1);
      else
        ContactNormal = glm::vec3(0, 0, -1);
    }
    else
    {
      depth = depthX;
      if (Point.x > 0)
        ContactNormal = glm::vec3(1, 0, 0);
      else
        ContactNormal = glm::vec3(-1, 0, 0);
    }
  }
  else
  {
    if (depthZ < depthY)
    {
      depth = depthZ;
      if (Point.z > 0)
        ContactNormal = glm::vec3(0, 0, 1);
      else
        ContactNormal = glm::vec3(0, 0, -1);
    }
    else
    {
      depth = depthY;
      if (Point.x > 0)
        ContactNormal = glm::vec3(0, 1, 0);
      else
        ContactNormal = glm::vec3(0, -1, 0);
    }
  }

  //convert data back to world space
  ContactNormal = (Transform * glm::vec4(ContactNormal,0)).xyz();
  Point = Transform * Point;


  //if collision we can respond to
  //if collision we can respond to
  if (A->Body().IsValid() && PointAABB->Body().IsValid() &&
    !A->Body()->GhostMode() && !PointAABB->Body()->GhostMode()
    && (m_CollisionLayerMatrix[A->CollisionID()][PointAABB->CollisionID()] != COLLISION_LAYER_MODE::GHOST))
  {
    //generate contact
    Contact NewContact;
    NewContact.A = A->Body();
    NewContact.B = PointAABB->Body();
    NewContact.Restitution = -1.0f;
    NewContact.PenetrationDepth = depth;
    NewContact.ContactNormal = ContactNormal;
    NewContact.ContactPoint = Point.xyz();

    m_phySystem->AddContact(NewContact);
  }

}


float CollisionEngine::GetDistanceBetweenEdges(glm::vec3 A_One,
  glm::vec3 A_Two,
  glm::vec3 B_One,
  glm::vec3 B_Two,
  glm::vec3 * A_Closest_Output,
  glm::vec3 * B_Closest_Output,
  bool * Clamped)
{
  float slop = 0.0;
  //must solve for s and t in parametric equations
  //p = p_0 + s * u and P = P_0 + t * v
  //for closest points, 
  glm::vec3 AVec = A_Two - A_One;
  glm::vec3 BVec = B_Two - B_One;
  glm::vec3 W_One = A_One - B_One;

  //Avec is u, BVec is V
  float a = glm::dot(AVec,AVec);
  float b = glm::dot(AVec,BVec);
  float c = glm::dot(BVec,BVec);
  float d = glm::dot(AVec,W_One);
  float e = glm::dot(BVec,W_One);

  //check for parrallel lines
  float denominator = a * c - b * b;
  if (denominator < 0 + 0.001 && denominator > 0 - 0.001)
  {
    //solve parallel lines,
    //take s as zero, solve for t
    float t = e / c;

    glm::vec3 ClosestPointB = B_One + BVec * t;

    float distance = glm::length(A_One - ClosestPointB);

    if (A_Closest_Output)
      *A_Closest_Output = A_One;

    if (B_Closest_Output)
      *B_Closest_Output = ClosestPointB;

    return distance;

  }

  float s = (b * e - c * d) / denominator;
  float t = (a * e - b * d) / denominator;

  s = Clamp(0, 1, s);
  t = Clamp(0, 1, t);

  if (Clamped)
  {
    if (s >= 1 - slop || t >= 1 - slop || s <= 0 + slop || t <= 0 + slop)
      *Clamped = true;
    else
      *Clamped = false;
  }

  glm::vec3 ClosestPointA = A_One + AVec * s;
  glm::vec3 ClosestPointB = B_One + BVec * t;


  float distance = glm::length(ClosestPointA - ClosestPointB);

  if (A_Closest_Output)
    *A_Closest_Output = ClosestPointA;

  if (B_Closest_Output)
    *B_Closest_Output = ClosestPointB;

  return distance;


}


float CollisionEngine::Clamp(float min, float max, float value)
{
  if (value > max)
    return max;
  else if (value < min)
    return min;
  else
    return value;
};

bool CollisionEngine::CmpFloats(float a, float b, float epsilon)
{
  return (a < (b + epsilon)) && (a >(b - epsilon));
}
