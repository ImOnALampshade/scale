// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  CollisionEngine, part of Physics System
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef COLLISION_ENGINE_H
#define COLLISION_ENGINE_H
#include "../glminclude.h"
#include "Contact.h"
class Body;
class CollisionVolumeComponent;
class Collision_SphereComponent;
class Collision_PlaneComponent;
class Collision_AABBComponent;
class Collision_OBBComponent;
class PhysicsSystemComponent;
class CollisionEngine
{
public:
  CollisionEngine(PhysicsSystemComponent * physSystem);
  void DetectCollisions();
  void AddCVolume(::Handle<CollisionVolumeComponent> arg);
  void CollisionEngine::PruneCollisionVolumes(void);

  //id must be between 0 and 99, rule 0 - collide, 1 - no collide, 2 - ghost
  void AddCollisionLayerRule(unsigned char ID1, unsigned char ID2, unsigned char Rule);
  void LoadCollisionLayerTable(String filename);
  void ClearCollisionTable(void);
private:
  void DispatchCollisionMsg(::Handle<CollisionVolumeComponent> A, ::Handle<CollisionVolumeComponent> B);

  void NarrowPhase(::Handle<CollisionVolumeComponent> A, ::Handle<CollisionVolumeComponent> B);
  void CheckCollisionSpherevSphere(::Handle<Collision_SphereComponent> A, ::Handle<Collision_SphereComponent> B);
  void CheckCollisionSpherevOBB(::Handle<Collision_SphereComponent> A, ::Handle<Collision_OBBComponent> B);
  void CheckCollisionSpherevAABB(::Handle<Collision_SphereComponent> A, ::Handle<Collision_AABBComponent> B);
  void CheckCollisionSpherevPlane(::Handle<Collision_SphereComponent> A, ::Handle<Collision_PlaneComponent> B);

  bool CheckCollisionPointvOBBNoContact(glm::vec4 Point, ::Handle<Collision_OBBComponent> A);
  void CheckCollisionPointforAABBvOBB(glm::vec3 Point, ::Handle<Collision_AABBComponent> A, ::Handle<Collision_OBBComponent> PointOBB);
  bool CheckCollisionPointvAABBNoContact(glm::vec3 Point, ::Handle<Collision_AABBComponent> A);
  bool CheckCollisionPointforAABBvAABB(glm::vec3 Point, ::Handle<Collision_AABBComponent> B, ::Handle<Collision_AABBComponent>  A);
  void CheckCollisionPointforOBBvAABB(glm::vec4 Point, ::Handle<Collision_OBBComponent> A, ::Handle<Collision_AABBComponent> PointAABB);


  void CheckCollisionOBBvOBB(::Handle<Collision_OBBComponent> A, ::Handle<Collision_OBBComponent> B);
  void CheckCollisionOBBvAABB(::Handle<Collision_OBBComponent> A, ::Handle<Collision_AABBComponent> B);
  void CheckCollisionOBBvPlane(::Handle<Collision_OBBComponent> A, ::Handle<Collision_PlaneComponent> B);

  void CheckCollisionAABBvAABB(::Handle<Collision_AABBComponent> A, ::Handle<Collision_AABBComponent> B);
  void CheckCollisionAABBvPlane(::Handle<Collision_AABBComponent> A, ::Handle<Collision_PlaneComponent> B);

  void CheckCollisionPlanevPlane(::Handle<Collision_PlaneComponent> A, ::Handle<Collision_PlaneComponent> B);

  float GetDistanceBetweenEdges(glm::vec3 A_One,
    glm::vec3 A_Two,
    glm::vec3 B_One,
    glm::vec3 B_Two,
    glm::vec3 * A_Closest_Output,
    glm::vec3 * B_Closest_Output,
    bool * Clamped);



  float Clamp(float min, float max, float value);
  bool CollisionEngine::CmpFloats(float a, float b, float epsilon);

  std::vector<::Handle<CollisionVolumeComponent> > m_CVolumes;
  PhysicsSystemComponent * m_phySystem;

  enum COLLISION_LAYER_MODE
  {
    COLLIDE,
    NO_COLLIDE,
    GHOST, 
    SIZE
  };
  unsigned char m_CollisionLayerMatrix[100][100];
};


#endif