// -----------------------------------------------------------------------------
//  Author: Cole Ingram
//
//  The RigidBody Component
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// --------------------------------------------------------------------------------

#include "RigidBodyComponent.h"
#include "CollisionVolume.h"

void RigidBodyComponent::Initialize(const Json::Object &jsonData)
{
  m_Transform = Owner()->GetComponent(TransformComponent::Name).as<TransformComponent>();

  //BREAK_IF(m_Transform == nullptr, "Cannot have a rigidbody if there is no transform");

  Position(m_Transform->Position());
  Orientation(m_Transform->Rotation());
  double Ghost = jsonData.Locate("ghost", 0.0).as<Json::Number>();

  if (Ghost)
    m_GhostMode = true;
  else
    m_GhostMode = false;

  Mass((float)jsonData.Locate("mass", 1.0).as<Json::Number>());
  Elasticity((float)jsonData.Locate("elasticity", 1.0).as<Json::Number>());

  if (m_Elasticity > 1.0f)
    m_Elasticity = 1.0f;
  if (m_Elasticity < 0)
    m_Elasticity = 0.0f;

  bool found = jsonData.CallOnValue("velocity", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_Vel[i] = float(l[i].as<Json::Number>());
  });

  if (!found)
    m_Vel = glm::vec3(0);

  found = jsonData.CallOnValue("rot_velocity", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_RotVel[i] = float(l[i].as<Json::Number>());
  });

  if (!found)
    m_RotVel = glm::vec3(0);



  Owner()->DispatchUp("RigidBodyCreate", Handle().as<RigidBodyComponent>());
  RegisterMessageProc("CollisionVolumeCreate", std::function<void(::Handle<CollisionVolumeComponent>)>(std::bind(&RigidBodyComponent::SetTheCV, this, std::placeholders::_1)));
  RegisterMessageProc("Awake", std::function<void(void)>(std::bind(&RigidBodyComponent::Awake, this)));


  m_GravityMag = ((float)jsonData.Locate("gravity_magnitude", 0.0).as<Json::Number>());

  found = jsonData.CallOnValue("gravity", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_GravityDir[i] = float(l[i].as<Json::Number>());
  });

  if (!found)
    m_GravityDir = glm::vec3(0, -1, 0);



  InertiaTensor(glm::mat3(1, 0, 0, 0, 1, 0, 0, 0, 1));
  m_LinImpulseAccum = Vec3(0, 0, 0);
  m_RotImpulseAccum = Vec3(0, 0, 0);



}

void RigidBodyComponent::Awake(void)
{
  m_Transform = Owner()->GetComponent(TransformComponent::Name).as<TransformComponent>();
  Position(m_Transform->Position());
  Orientation(m_Transform->Rotation());
}

void RigidBodyComponent::Free()
{
}

void RigidBodyComponent::Update(void)
{
  if (m_CV.IsValid())
  {
    InertiaTensor(m_CV->CalculateInertia(Mass()));
    glm::mat3 Rotate = glm::mat3_cast(Orientation());
    glm::mat3 RotateTrans = glm::transpose(Rotate);

    m_Inertia_World = Rotate * InertiaTensor() * RotateTrans;
    //check if matrix is zero
    bool ZeroMat = true;
    for (int i = 0; i < 3 && ZeroMat; ++i)
    {
      for (int j = 0; j < 3; ++j)
      {
        if (m_Inertia_World[i][j] != 0.0f)
        {
          ZeroMat = false;
          break;
        }
      }
    }
    if (!ZeroMat)
      m_InvInertia_World = glm::inverse(m_Inertia_World);
    else
      m_InvInertia_World = m_Inertia_World;

  }

  //if no transform component
  if (m_Transform.IsValid())
  {
    m_Transform->Position(m_Pos);
    m_Transform->Rotation(m_Orientation);
  }



}

//Forces--------------------------------
void RigidBodyComponent::UpdateForces(float DeltaTime)
{
  glm::vec3 SumOfForces(0, 0, 0);
  glm::vec3 SumOfTorques(0, 0, 0);
  for (int i = m_Forces.size() - 1; i != -1; --i)
  {
    if (m_Forces[i].Lifetime != FORCE_INFINITE_LIFE)
    {
      m_Forces[i].Lifetime -= DeltaTime;
    }

    if (m_Forces[i].Lifetime < 0 && m_Forces[i].Lifetime != FORCE_INFINITE_LIFE)
    {
      m_Forces.erase(m_Forces.begin() + i);
    }
    else
    {
      SumOfForces += m_Forces[i].LinearForce;
      SumOfTorques += m_Forces[i].Torque;
    }

  }

  m_Accel = m_InvMass * SumOfForces;
  //gravity special case
  m_Accel += m_GravityDir * m_GravityMag;

  m_RotAccel = m_InvInertia_World * SumOfTorques;
}

void RigidBodyComponent::ApplyForce(Vec3 TheForce, float lifetime)
{
  InternalForce newForce;
  newForce.LinearForce = TheForce;
  newForce.Lifetime = lifetime;
  newForce.Torque = glm::vec3(0, 0, 0);
  m_Forces.push_back(newForce);

}
void RigidBodyComponent::ApplyTorque(Vec3 Torque, float lifetime)
{
  InternalForce newForce;
  newForce.LinearForce = glm::vec3(0, 0, 0);
  newForce.Lifetime = lifetime;
  newForce.Torque = Torque;
  m_Forces.push_back(newForce);
}
void RigidBodyComponent::ApplyForceAtPoint(Vec3 TheForce, Vec3 WorldPoint, float lifetime)
{
  glm::vec3 r = WorldPoint - Position();
  InternalForce newForce;
  newForce.LinearForce = TheForce;
  newForce.Torque = glm::cross(r, TheForce);
  newForce.Lifetime = lifetime;
  m_Forces.push_back(newForce);
}
void RigidBodyComponent::ApplyForceAtOffsetVector(Vec3 TheForce, Vec3 WorldR, float lifetime)
{
  InternalForce newForce;
  newForce.LinearForce = TheForce;
  newForce.Torque = glm::cross(WorldR, TheForce);
  newForce.Lifetime = lifetime;
  m_Forces.push_back(newForce);
}


void RigidBodyComponent::AddImpulseLinear(Vec3 arg)
{
  m_LinImpulseAccum += glm::vec3(arg);
}
void RigidBodyComponent::AddImpulseRot(Vec3 arg)
{
  m_RotImpulseAccum += glm::vec3(arg);
}
void RigidBodyComponent::ApplyImpulses(void)
{
  AddVel(m_LinImpulseAccum);
  AddRotVel(m_RotImpulseAccum);
  m_LinImpulseAccum = Vec3(0, 0, 0);
  m_RotImpulseAccum = Vec3(0, 0, 0);
}

void RigidBodyComponent::AddVel(Vec3 arg)
{
  m_Vel += glm::vec3(arg);
}
void RigidBodyComponent::AddRotVel(Vec3 arg)
{
  m_RotVel += glm::vec3(arg);
}

//Getters/setters--------------------

void RigidBodyComponent::GhostMode(bool arg)
{
  m_GhostMode = arg;
}
bool RigidBodyComponent::GhostMode(void)
{
  return m_GhostMode;
}

void RigidBodyComponent::GravityDir(Vec3 arg)
{
  //== works because I assume it was done intentionally
  if (arg.x + arg.y + arg.z != 0.0f)
  {
    arg = glm::normalize(glm::vec3(arg));
  }
  m_GravityDir = arg;
}
Vec3 RigidBodyComponent::GravityDir(void)
{
  return m_GravityDir;
}

void RigidBodyComponent::GravityMag(float arg)
{
  m_GravityMag = arg;
}
float RigidBodyComponent::GravityMag(void)
{
  return m_GravityMag;
}

void RigidBodyComponent::Acceleration(Vec3 arg)
{
  m_Accel = arg;
}
Vec3 RigidBodyComponent::Acceleration(void)
{
  return m_Accel;
}
void RigidBodyComponent::Velocity(Vec3 arg)
{
  m_Vel = arg;
}
Vec3 RigidBodyComponent::Velocity(void)
{
  return m_Vel;
}
void RigidBodyComponent::Position(Vec3 arg)
{
  m_Pos = arg;
}
Vec3 RigidBodyComponent::Position(void)
{
  return m_Pos;
}
void RigidBodyComponent::RotAcceleration(Vec3 arg)
{
  m_RotAccel = arg;
}
Vec3 RigidBodyComponent::RotAcceleration(void)
{
  return m_RotAccel;
}
void RigidBodyComponent::RotVelocity(Vec3 arg)
{
  m_RotVel = arg;
}
Vec3 RigidBodyComponent::RotVelocity(void)
{
  return m_RotVel;
}
void RigidBodyComponent::Orientation(Quat arg)
{
  m_Orientation = arg;
}
Quat RigidBodyComponent::Orientation(void)
{
  return m_Orientation;
}
void RigidBodyComponent::Mass(float arg)
{
  //this should only happen on purpose 
  //(don't care about floating point inaccuracy)
  if (arg == 0.0f)
  {
    m_Mass = 0;
    m_InvMass = 0;
  }
  else
  {
    m_Mass = arg;
    m_InvMass = 1 / arg;
  }
}
float RigidBodyComponent::Mass(void)
{
  return m_Mass;
}

void RigidBodyComponent::InertiaTensor(Mat3 arg)
{

  //check if matrix is zero
  bool ZeroMat = true;
  for (int i = 0; i < 3 && ZeroMat; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      if (arg[i][j] != 0.0f)
      {
        ZeroMat = false;
        break;
      }
    }
  }


  if (ZeroMat)
  {
    m_Inertia = arg;
    m_InvInertia = arg;
  }
  else
  {
    m_Inertia = arg;
    m_InvInertia = glm::inverse(arg);
  }
}
Mat3 RigidBodyComponent::InertiaTensor()
{
  return m_Inertia;
}

Mat3 RigidBodyComponent::InvInertia(void)
{
  return m_InvInertia;
}
float RigidBodyComponent::InvMass(void)
{
  return m_InvMass;
}


void RigidBodyComponent::Elasticity(float arg)
{
  m_Elasticity = arg;
}
float RigidBodyComponent::Elasticity(void)
{
  return m_Elasticity;
}

void RigidBodyComponent::TheCollisionVolume(::Handle<CollisionVolumeComponent> arg)
{
  m_CV = arg;
}
::Handle<CollisionVolumeComponent> RigidBodyComponent::TheCollisionVolume(void)
{
  return m_CV;
}

void RigidBodyComponent::SetTheCV(::Handle<CollisionVolumeComponent> arg)
{
  m_CV = arg;
  m_CV->Body(Handle().as<RigidBodyComponent>());
}

META_REGISTER_FUNCTION(RigidBody)
{
  META_INHERIT(RigidBodyComponent, "Kepler", "Component");
  META_HANDLE(RigidBodyComponent);

  META_ADD_METHOD(RigidBodyComponent, ApplyForce);
  META_ADD_METHOD(RigidBodyComponent, ApplyTorque);
  META_ADD_METHOD(RigidBodyComponent, ApplyForceAtPoint);
  META_ADD_METHOD(RigidBodyComponent, ApplyForceAtOffsetVector);
  META_ADD_METHOD(RigidBodyComponent, AddVel);
  META_ADD_METHOD(RigidBodyComponent, AddRotVel);

  META_ADD_PROP(RigidBodyComponent, GhostMode);
  META_ADD_PROP(RigidBodyComponent, GravityDir);
  META_ADD_PROP(RigidBodyComponent, GravityMag);
  META_ADD_PROP(RigidBodyComponent, Acceleration);
  META_ADD_PROP(RigidBodyComponent, Velocity);
  META_ADD_PROP(RigidBodyComponent, Position);
  META_ADD_PROP(RigidBodyComponent, RotAcceleration);
  META_ADD_PROP(RigidBodyComponent, RotVelocity);
  META_ADD_PROP(RigidBodyComponent, Orientation);
  META_ADD_PROP(RigidBodyComponent, InertiaTensor);
  META_ADD_PROP(RigidBodyComponent, Mass);
  META_ADD_PROP(RigidBodyComponent, Elasticity);
  META_ADD_PROP(RigidBodyComponent, TheCollisionVolume);

  META_FINALIZE_PTR(RigidBodyComponent, MetaPtr);
}
//----------------------------------------------------