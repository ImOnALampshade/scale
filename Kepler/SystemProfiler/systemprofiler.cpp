// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  System profiler keeps track of and logs game performance.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------


#include "systemprofiler.h"
#include "..\KeplerCore\versioninfo.h"
#include <ctime>
#include "../Serializer/csvparser.h"
#include <fstream>

namespace Utilities
{


//------------------------------------------------------------------------------
// SystemProfiler
//------------------------------------------------------------------------------

  SystemProfiler::SystemProfiler (SystemProfiler::Configuration const &Config) :
    m_Configuration (Config), m_SystemInitTime(), m_MemoryUsageLog(),
    m_ProfileData (HashTable<String,SystemProfileData> (Hash::SuperFast)),
    m_Lock (std::mutex())
  {
    //load data previously logged from today
    LoadPreviousLogFile();

    //set starting time of system profiler.
    m_SystemInitTime = std::chrono::high_resolution_clock::now();

  }

  SystemProfiler::~SystemProfiler()
  {
    SaveData (GetLogName());
  }

  void SystemProfiler::LogProcessTime (String const &ProcessName, milliseconds ProcessingTime)
  {

    m_ProfileData[ProcessName.CStr()].RecordProcessTime (ProcessingTime, m_Configuration.RecordInterval);

  }

  void SystemProfiler::LogMemoryUsage (size_t Bytes)
  {

  }

  String SystemProfiler::GetLogName() const
  {
    String logName;
    //variables to get local time
    time_t currentTime;
    struct tm timeinfo;

    //get local time
    std::time (&currentTime);
    localtime_s (&timeinfo, &currentTime);

    //put local time into a c ascii string
    char timeStringBuffer[32];
    strftime (timeStringBuffer, sizeof (timeStringBuffer), "%Y_%m_%d", &timeinfo);

    //put everything into one unicode string
    logName = String::Format ("%sv%i_timedata_", m_Configuration.LogFilePathDestination.CStr(), KEPLER_VERSION_NUM);

    logName.Append (timeStringBuffer, 1);
    logName += ".csv";

    return logName;
  }

  void SystemProfiler::LoadPreviousLogFile()
  {
    std::lock_guard<std::mutex> _ (m_Lock);

    m_ProfileData.Clear();


    Serializer::Value val;
    val = Csv::ReadFile (GetLogName());

    //this wont be constructed if a file was not found.
    if (val.IsConstructed())
    {

      Csv::CsvTable &tabel = val.as<Csv::Table>();

      for (size_t i = 1; i < tabel.GetRowCount(); ++i)
      {
        for (size_t j = 0; j < tabel.GetColumnCount (i) - 1; ++j) //-1 because we dont read the time(tms)
        {
          const char *systemName = tabel.GetValue (0, j).as<Csv::String>().CStr();
          Csv::Number val = tabel.GetValue (i, j).as<Csv::Number>();
          int totalMilliseconds = static_cast<int> (val);
          m_ProfileData[systemName].AddIntervalValue (milliseconds (totalMilliseconds));
        }
      }

    }

    else
    {
      //no file for today. dont load anything.
    }

  }

  void SystemProfiler::SaveData (String const &FileName) const
  {
    //make the table big enough to put all our data in.
    Csv::CsvTable tabel;
    //+1 on Profile Data Size to make units column.
    size_t largestRecordSize = LargestRecord();
    tabel.SetSize (m_ProfileData.Size() + 1, largestRecordSize + 1);

    unsigned int currentColumn = 0;

    for (auto profileData = m_ProfileData.cbegin(); profileData != m_ProfileData.cend(); ++profileData)
    {
      tabel.SetValue (0, currentColumn, profileData->key);
      size_t dataPoints = profileData->value.GetSizeOfDataPoints();

      for (unsigned int i = 0; i < dataPoints; i += 1)
      {
        Csv::Number value = Csv::Number (profileData->value.GetTimeAt (i).count());
        tabel.SetValue (i + 1, currentColumn, value);
      }

      currentColumn += 1;
    }

    //this section of code writes in the units column.
    tabel.SetValue (0, m_ProfileData.Size(), Csv::String ("Time(ms)"));

    for (unsigned int i = 0; i < largestRecordSize; i += 1)
    {
      tabel.SetValue (i + 1, currentColumn, Csv::Number (i * m_Configuration.RecordInterval.count()));
    }

    tabel.Save (FileName);

  }

  size_t SystemProfiler::LargestRecord() const
  {
    size_t largestSize = 0;

    for (auto it = m_ProfileData.cbegin(); it != m_ProfileData.cend(); ++it)
    {
      if (it->value.GetSizeOfDataPoints() > largestSize)
      {
        largestSize = it->value.GetSizeOfDataPoints();
      }
    }

    return largestSize;
  }

//------------------------------------------------------------------------------
// SystemProfileData
//------------------------------------------------------------------------------

  SystemProfileData::SystemProfileData()
  {
    m_LastRecordTime = std::chrono::high_resolution_clock::now();
  }

  void SystemProfileData::AddIntervalValue (milliseconds ProcessTime)
  {
    if (ProcessTime > m_HighestProcessTime)
    {
      m_HighestProcessTime = ProcessTime;
    }

    //add proccess time into process time average and average it.
    m_ProcessTimeAverage = (m_ProcessTimeAverage + ProcessTime) / 2;

    m_ProcessTimeLog.push_back (ProcessTime);
  }

  void SystemProfileData::RecordProcessTime (milliseconds ProcessTime, milliseconds IntervalDuration)
  {
    milliseconds timeDuration = std::chrono::duration_cast<milliseconds> (std::chrono::high_resolution_clock::now() - m_LastRecordTime);

    AddIntervalValue (ProcessTime);

    //see if its been long enough to log the data.
    if (timeDuration.count() >= IntervalDuration.count())
    {

      m_LastRecordTime = std::chrono::high_resolution_clock::now();
    }


  }

  milliseconds const &SystemProfileData::AverageProcessTime() const
  {
    return m_ProcessTimeAverage;
  }

  milliseconds const &SystemProfileData::LargestProcessTime() const
  {
    return m_HighestProcessTime;
  }

  size_t SystemProfileData::GetSizeOfDataPoints() const
  {
    return m_ProcessTimeLog.size();
  }

  milliseconds SystemProfileData::GetTimeAt (unsigned int Interval) const
  {
    return m_ProcessTimeLog[Interval];
  }

}//namespace Utilities