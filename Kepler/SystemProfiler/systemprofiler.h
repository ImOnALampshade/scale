// -----------------------------------------------------------------------------
//  Author: Reese Jones
//
//  System profiler keeps track of and logs game performance.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef SYSTEM_PROFILER_H


#include <vector>
#include "..\HashTable\hashtable.h"
#include "..\HashFunctions\hashfunctions.h"
#include "..\String\string.h"
#include <chrono>
#include <mutex>

namespace Utilities
{




  using std::chrono::milliseconds;

// -----------------------------------------------------------------------------
// SystemProfileData keeps track of the data for a single system.
// -----------------------------------------------------------------------------
  class SystemProfileData
  {
  public:
    SystemProfileData();
// -----------------------------------------------------------------------------
//  Functions to poll for data
// -----------------------------------------------------------------------------

    milliseconds const &AverageProcessTime() const;
    milliseconds const &LargestProcessTime() const;

    size_t GetSizeOfDataPoints() const;
    milliseconds GetTimeAt (unsigned int Interval) const;

    //size_t AverageMemoryUsage();
    //size_t MemoryUsage();
    //size_t MaxMemoryUsage();

// -----------------------------------------------------------------------------
//  Functions to insert data
// -----------------------------------------------------------------------------
    void RecordProcessTime (milliseconds ProcessTime, milliseconds IntervalDuration);
    //void RecordMemoryUsage( size_t MemoryUsage );

    friend class SystemProfiler;

  private:
    void AddIntervalValue (milliseconds ProcessTime);

  private:
    std::chrono::high_resolution_clock::time_point m_LastRecordTime;
    std::vector<milliseconds> m_ProcessTimeLog;
    //std::vector<size_t> m_MemoryUsageLog;

    milliseconds m_ProcessTimeAverage;
    milliseconds m_HighestProcessTime;

  };//class SystemData

// -----------------------------------------------------------------------------
// The system profiler will track the status of various systems. It will log
// Memory and CPU usage over time. It saves this result to a file.
// -----------------------------------------------------------------------------
  class SystemProfiler
  {
  public:
    struct Configuration
    {
      Configuration() :RecordInterval (milliseconds (5000)), LogFilePathDestination (".\\") {}

      milliseconds RecordInterval;
      String LogFilePathDestination;
    };

    SystemProfiler (Configuration const &Config);
    ~SystemProfiler();

    void LogProcessTime (String const &ProcessName, milliseconds ProcessingTime);
    void LogMemoryUsage (size_t Bytes);

    void SaveData (String const &FileName) const;
    void LoadPreviousLogFile();
    String GetLogName() const;

  private:
    size_t LargestRecord() const;




  private:
    Configuration m_Configuration;
    std::chrono::high_resolution_clock::time_point m_SystemInitTime;

    std::vector<size_t> m_MemoryUsageLog;
    HashTable<String, SystemProfileData> m_ProfileData;
    std::mutex m_Lock;


  };//class SystemProfiler

};//namespace Utilities

#endif