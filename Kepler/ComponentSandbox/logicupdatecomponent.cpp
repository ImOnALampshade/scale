// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for distributing logic update messages
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "logicupdatecomponent.h"

void LogicUpdateComponent::Initialize (const Json::Object &jsonData)
{
  RegisterMessageProc ("LogicUpdate", std::function<void (float) > ([this] (float Dt) {
    auto &children = Owner()->GetChildren();

    if (m_paused != 0)
      for (auto &child : children)
        if (child.IsValid())
          child->DispatchDown ("LogicUpdate", Dt);
  }));
}

int LogicUpdateComponent::Paused() const
{
  return m_paused;
}

void LogicUpdateComponent::Paused (int value)
{
  m_paused = value;
}

META_REGISTER_FUNCTION (LogicUpdate)
{
  META_INHERIT (LogicUpdateComponent, "Kepler", "Component");

  // META_HANDLE means that we are binding a pointer to this object, not the object itself
  META_HANDLE (LogicUpdateComponent);

  // Properties can be bound with META_ADD_PROP
  META_ADD_PROP (LogicUpdateComponent, Paused);

  META_FINALIZE_PTR (LogicUpdateComponent, MetaPtr);
}
