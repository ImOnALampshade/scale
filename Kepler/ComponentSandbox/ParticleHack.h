//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#ifndef PARTICLE_SYSTEM_HACK_COMPONENT_H
#define PARTICLE_SYSTEM_HACK_COMPONENT_H

#include "../KeplerCore/component.h"
#include "../glminclude.h"
#include "../KeplerCore/transformcomponent.h"
#include "../ComponentSandbox/ParticlePool.h"

class ParticleSystemHackComponent : public Component
{
public:
  virtual void Initialize(const Json::Object &jsonData);
  virtual void Free();
  void Update(float dt);
  //---------------------------------------------------------
  //setters
  bool IsActive(void);
  void IsActive(bool arg);

  bool UseLocalCoords(void);
  void UseLocalCoords(bool arg);

  int MaxParticles(void);
  void MaxParticles(int arg);

  bool RecycleParticles(void);
  void RecycleParticles(bool arg);

  float EmitterAngle(void);
  void EmitterAngle(float arg);

  float EmitterAngleOffset(void);
  void EmitterAngleOffset(float arg);

  void SetInitSpeed(float min, float max);
  void SetFinalSpeed(float min, float max);
  void SetEmissionRate(float arg);
  void SetParticleLifetime(float min, float max);
  void SetInitColor(Vec3 min, Vec3 max);
  void SetFinalColor(Vec3 min, Vec3 max);
  void SetInitScale(Vec3 min, Vec3 max);
  void SetFinalScale(Vec3 min, Vec3 max);
  void SetInitRotSpeed(float min, float max);
  void SetFinalRotSpeed(float min, float max);
  void SetInitAngle(float min, float max);


  COMPONENT_META
private:
 
  int GetRandNum(int min, int max);
  float GetRandNum(float min, float max);

  float m_EmitterAngle;
  float m_EmitterAngleOffset;
  ParticlePoolComponent::Particle * GenerateParticle(void);
  void GenerateInitialPosAndVel(ParticlePoolComponent::Particle& Particle);

  void EmitParticles(void);
  void UpdateParticle(ParticlePoolComponent::Particle& CurrentParticle);

  bool m_UseEmitterAngle;

  bool m_IsActive;

  float m_InitialSpeedMin;
  float m_InitialSpeedMax;
  
  float m_FinalSpeedMin;
  float m_FinalSpeedMax;

  float m_EmissionRate;
  float m_EmissionRateRemainder;

  float m_ParticleLifetimeMin;
  float m_ParticleLifetimeMax;

  glm::vec3 m_InitialColorMin;
  glm::vec3 m_InitialColorMax;

  glm::vec3 m_FinalColorMin;
  glm::vec3 m_FinalColorMax;

  int m_MaxParticles;
  std::vector<ParticlePoolComponent::Particle> m_Particles;

  glm::vec3 m_InitialScaleMin;
  glm::vec3 m_InitialScaleMax;

  glm::vec3 m_FinalScaleMin;
  glm::vec3 m_FinalScaleMax;

  float m_InitialRotSpeedMin;
  float m_InitialRotSpeedMax;
  
  float m_FinalRotSpeedMin;
  float m_FinalRotSpeedMax;
  
  float m_InitialAngleMin;
  float m_InitialAngleMax;

  bool m_RecycleParticles;
  bool m_UseLocalCoords;

  int m_ParticlesUsed;
};

#endif 