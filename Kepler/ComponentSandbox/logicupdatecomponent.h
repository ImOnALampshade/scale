// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for distributing logic update messages
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef LOGIC_UPDATE_COMPONENT_H
#define LOGIC_UPDATE_COMPONENT_H

#include "../KeplerCore/component.h"

class LogicUpdateComponent : public Component
{
private:
  int m_paused;
public:
  virtual void Initialize (const Json::Object &jsonData);

  int Paused() const;
  void Paused(int value);

  COMPONENT_META
};

#endif
