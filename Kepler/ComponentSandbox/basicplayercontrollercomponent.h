// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for basic player control
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef BASIC_PLAYER_CONTROLLER_COMPONENT_H
#define BASIC_PLAYER_CONTROLLER_COMPONENT_H

#include "../KeplerCore/component.h"

class BasicPlayerControllerComponent : public Component
{
private:

public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  COMPONENT_META
};

#endif
