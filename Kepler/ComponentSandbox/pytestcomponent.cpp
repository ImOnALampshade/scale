// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for distributing logic update messages
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "pytestcomponent.h"

// -----------------------------------------------------------------------------

void PyTestComponent::Initialize (const Json::Object &jsonData)
{
  RegisterMessageProc ("PrintOutSomething", std::function<void (String, String) > (
  [this] (String str1, String str2) {
    Owner()->Dispatch ("SomeMessage", str1, str2);
  }));
}

void PyTestComponent::Free()
{
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (PyTest)
{
  META_INHERIT (PyTestComponent, "Kepler", "Component");
  META_HANDLE (PyTestComponent);

  META_MESSAGE ("PrintOutSomething", String, String);

  META_FINALIZE_PTR (PyTestComponent, MetaPtr);
}
