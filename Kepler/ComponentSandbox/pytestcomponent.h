// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for distributing logic update messages
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef PYTHON_TEST_COMPONENT_H
#define PYTHON_TEST_COMPONENT_H

#include "../KeplerCore/component.h"

class PyTestComponent : public Component
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  COMPONENT_META
};

#endif
