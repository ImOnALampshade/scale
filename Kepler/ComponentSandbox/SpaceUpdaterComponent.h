//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#ifndef SPACE_UPDATER_COMPONENT_H
#define SPACE_UPDATER_COMPONENT_H

#include "../KeplerCore/component.h"

class SpaceUpdaterComponent : public Component
{

public:
  virtual void Initialize(const Json::Object &jsonData);

  void UpdateSpace(void);
  
  int PhysicsPaused() const;
  void PhysicsPaused(int arg);

  int GraphicsPaused() const;
  void GraphicsPaused(int arg);

  int UIPaused() const;
  void UIPaused(int arg);

  int LogicPaused() const;
  void LogicPaused(int arg);
  
  int Paused() const;
  void Paused(int value);
 COMPONENT_META
private:
  int m_paused;
  
  int m_PhysicsPaused;
  int m_GraphicsPaused;
  int m_UIPaused;
  int m_LogicPaused;

 
};

#endif 