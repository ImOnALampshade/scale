//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "SpaceUpdaterComponent.h"
#include "../KeplerCore/game.h"

void SpaceUpdaterComponent::Initialize (const Json::Object &jsonData)
{
  std::function<void (void) > fc = std::function<void() > (std::bind (&SpaceUpdaterComponent::UpdateSpace, this));
  //RegisterMessageProc("Update", fc);
  RegisterMessageProc ("SpaceUpdate", fc);
  m_paused = 0;
  m_UIPaused = 0;
  m_LogicPaused = 0;
  m_PhysicsPaused = 0;
  m_GraphicsPaused = 0;
  //CoreEngine::Game->RegisterObserver (String ("Update"), fc, Owner());
}

void SpaceUpdaterComponent::UpdateSpace (void)
{
  if (!m_paused)
  {
    if (!m_LogicPaused)
      Owner()->DispatchDown ("LogicUpdate", (1.f / 60.0f));

    if (!m_PhysicsPaused)
      Owner()->DispatchDown ("PhysicsUpdate", (1.f / 60.f));
  }
}
//-----------------------------------------------------------

int  SpaceUpdaterComponent::PhysicsPaused() const
{
  return m_PhysicsPaused;
}
void SpaceUpdaterComponent::PhysicsPaused (int arg)
{
  m_PhysicsPaused = arg;
}
int  SpaceUpdaterComponent::GraphicsPaused() const
{
  return m_GraphicsPaused;
}
void SpaceUpdaterComponent::GraphicsPaused (int arg)
{
  m_GraphicsPaused = arg;
}
int  SpaceUpdaterComponent::UIPaused() const
{
  return m_UIPaused;
}
void SpaceUpdaterComponent::UIPaused (int arg)
{
  m_UIPaused = arg;
}
int  SpaceUpdaterComponent::LogicPaused() const
{
  return m_LogicPaused;
}
void SpaceUpdaterComponent::LogicPaused (int arg)
{
  m_LogicPaused = arg;
}
int SpaceUpdaterComponent::Paused() const
{
  return m_paused;
}
void SpaceUpdaterComponent::Paused (int value)
{
  m_paused = value;
}

META_REGISTER_FUNCTION (SpaceUpdater)
{
  META_INHERIT (SpaceUpdaterComponent, "Kepler", "Component");

  // META_HANDLE means that we are binding a pointer to this object, not the object itself
  META_HANDLE (SpaceUpdaterComponent);

  // Properties can be bound with META_ADD_PROP
  META_ADD_PROP (SpaceUpdaterComponent, Paused);
  META_ADD_PROP (SpaceUpdaterComponent, LogicPaused);
  META_ADD_PROP (SpaceUpdaterComponent, PhysicsPaused);
  META_ADD_PROP (SpaceUpdaterComponent, GraphicsPaused);
  META_ADD_PROP (SpaceUpdaterComponent, UIPaused);


  META_FINALIZE_PTR (SpaceUpdaterComponent, MetaPtr);
}
