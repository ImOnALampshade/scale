// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for basic player control
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "basicplayercontrollercomponent.h"
#include "../KeplerCore/transformcomponent.h"
#include <GLFW/glfw3.h>

using namespace glm;

void BasicPlayerControllerComponent::Initialize (const Json::Object &jsonData)
{
}

void BasicPlayerControllerComponent::Free()
{
}

META_REGISTER_FUNCTION (BasicPlayerController)
{
}
