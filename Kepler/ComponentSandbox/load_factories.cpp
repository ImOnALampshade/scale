// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "load_factories.h"
#include "../KeplerCore/compositefactory.h"

#include "basicplayercontrollercomponent.h"
#include "logicupdatecomponent.h"
#include "pytestcomponent.h"
#include "SpaceUpdaterComponent.h"

#include "ParticleHack.h"
#include "ParticlePool.h"

void ComponentSandbox::LoadMeta()
{
  BasicPlayerControllerComponent::CreateMeta();
  LogicUpdateComponent::CreateMeta();
  PyTestComponent::CreateMeta();
  SpaceUpdaterComponent::CreateMeta();
  ParticleSystemHackComponent::CreateMeta();
  ParticlePoolComponent::CreateMeta();
}

void ComponentSandbox::LoadFactories()
{
  CREATE_FACTORY (BasicPlayerController);
  CREATE_FACTORY (LogicUpdate);
  CREATE_FACTORY (PyTest);
  CREATE_FACTORY (SpaceUpdater);
  CREATE_FACTORY (ParticleSystemHack);
  CREATE_FACTORY (ParticlePool);
}
