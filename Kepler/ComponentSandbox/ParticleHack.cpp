//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "ParticleHack.h"
#include "../glminclude.h"
#include "../KeplerCore/compositefactory.h"
#include "../GraphicsComponents/renderablecomponent.h"

void ParticleSystemHackComponent::Initialize(const Json::Object &jsonData)
{
  //Elasticity((float)jsonData.Locate("elasticity", 1.0).as<Json::Number>());

  //serializers


  m_ParticlesUsed = 0;
  m_EmissionRateRemainder = 0;
  m_EmitterAngle          =  jsonData.Locate("EmitterAngle", 360.0).as<Json::Number>(); 
  m_EmitterAngleOffset    =  jsonData.Locate("EmitterAngleOffset", 0.0).as<Json::Number>();
  m_InitialSpeedMin       =  jsonData.Locate("InitalSpeedMin", 1.0).as<Json::Number>();
  m_InitialSpeedMax       =  jsonData.Locate("InitalSpeedMax", 1.0).as<Json::Number>();
  m_FinalSpeedMin         =  jsonData.Locate("FinalSpeedMin", 1.0).as<Json::Number>();
  m_FinalSpeedMax         =  jsonData.Locate("FinalSpeedMax", 1.0).as<Json::Number>();
  m_EmissionRate          =  jsonData.Locate("EmissionRate", 10.0).as<Json::Number>(); 
  m_ParticleLifetimeMin   =  jsonData.Locate("ParticleLifetimeMin", 1.0).as<Json::Number>();
  m_ParticleLifetimeMax   =  jsonData.Locate("ParticleLifetimeMax", 1.0).as<Json::Number>();
  m_InitialRotSpeedMin    =  jsonData.Locate("InitialRotSpeedMin", 0.0).as<Json::Number>();
  m_InitialRotSpeedMax    =  jsonData.Locate("InitialRotSpeedMax", 0.0).as<Json::Number>();
  m_FinalRotSpeedMin      =  jsonData.Locate("FinalRotSpeedMin", 0.0).as<Json::Number>();
  m_FinalRotSpeedMax      =  jsonData.Locate("FinalRotSpeedMax", 0.0).as<Json::Number>();
  m_InitialAngleMin       =  jsonData.Locate("InitialAngleMin", 0.0).as<Json::Number>();
  m_InitialAngleMax       =  jsonData.Locate("InitialAngleMax", 0.0).as<Json::Number>();

  m_MaxParticles = (int)jsonData.Locate("MaxParticles", 256.0).as<Json::Number>();

  if (jsonData.Locate("IsActive", 1).as<Json::Number>())
    m_IsActive = true;
  else
    m_IsActive = false;

  if (jsonData.Locate("RecycleParticles", 1).as<Json::Number>())
    m_RecycleParticles = true;
  else
    m_RecycleParticles = false;


  if (jsonData.Locate("UseLocalCoords", 1).as<Json::Number>())
    m_UseLocalCoords = true;
  else
    m_UseLocalCoords = false;
  
  if (jsonData.Locate("UseEmitterAngle", 1).as<Json::Number>())
    m_UseEmitterAngle = true;
  else
    m_UseEmitterAngle = false;
  
  bool found;
  found = jsonData.CallOnValue("InitialScaleMin", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_InitialScaleMin[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_InitialScaleMin = glm::vec3(1,1,1);

  found = jsonData.CallOnValue("InitialScaleMax", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_InitialScaleMax[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_InitialScaleMax = glm::vec3(1,1,1);

  found = jsonData.CallOnValue("FinalScaleMin", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_FinalScaleMin[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_FinalScaleMin = glm::vec3(1,1,1);

  found = jsonData.CallOnValue("InitialScaleMax", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_FinalScaleMax[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_FinalScaleMax = glm::vec3(1,1,1);



  
  found = jsonData.CallOnValue("InitialColorMin", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_InitialColorMin[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_InitialColorMin = glm::vec3(1, 1, 1);

  found = jsonData.CallOnValue("InitialColorMax", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_InitialColorMax[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_InitialColorMax = glm::vec3(1, 1, 1);

  found = jsonData.CallOnValue("FinalColorMin", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_FinalColorMin[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_FinalColorMin = glm::vec3(1, 1, 1);

  found = jsonData.CallOnValue("FinalColorMax", [this](const Json::List &l) {
    for (unsigned i = 0; i < 3 && i < l.size(); ++i)
      m_FinalColorMax[i] = float(l[i].as<Json::Number>());
  });
  if (!found)
    m_FinalColorMax = glm::vec3(1, 1, 1);


    
  //auto ParticleInstance = jsonData.Locate("Particle").as < Json::String >();
  auto particlePool = Owner()->Space()->GetComponent("ParticlePool").as<ParticlePoolComponent>();
  for (int i = 0; i < m_MaxParticles; ++i)
  {
    m_Particles.push_back( particlePool->GiveParticle() );
  }


  RegisterMessageProc("LogicUpdate", std::function<void(float)>(std::bind(&ParticleSystemHackComponent::Update, this, std::placeholders::_1)));
}


void ParticleSystemHackComponent::Free()
{
  auto pool = Owner()->Space()->GetComponent("ParticlePool").as<ParticlePoolComponent>();
  //return particles back to particle pool
  for (unsigned i = 0; i < m_Particles.size(); ++i)
  {
    pool->TakeParticle(m_Particles[i]);
  }

  //make sure no one else gets at these.
  m_Particles.clear();

}

void ParticleSystemHackComponent::Update(float)
{
  if (m_IsActive)
  {
    EmitParticles();
  }

  unsigned size = m_Particles.size();
  for (unsigned i = 0; i < size; ++i)
  {
    if (m_Particles[i].Active)
      UpdateParticle(m_Particles[i]);
  }
}

void ParticleSystemHackComponent::EmitParticles(void)
{

  const float dt = 1.0 / 60;
  if (!m_RecycleParticles && m_ParticlesUsed > m_MaxParticles)
  {
    m_IsActive = false;
    m_EmissionRateRemainder = 0;
    m_ParticlesUsed = 0;
    return;
  }
  m_EmissionRateRemainder += m_EmissionRate * dt;
  while (m_EmissionRateRemainder > 1.0f)
  {

    m_EmissionRateRemainder -= 1.0f;
    ParticlePoolComponent::Particle * NewParticle = GenerateParticle();
    if (!NewParticle)
      continue;

    if (m_UseLocalCoords)
    {
      ::Handle<TransformComponent> tran = Owner()->GetComponent(TransformComponent::Name).as<TransformComponent>();
      NewParticle->Position = NewParticle->Position - tran->Position();
    }

    m_ParticlesUsed++;
    float initialSpeed = (float)GetRandNum(m_InitialSpeedMin, m_InitialSpeedMax);
    float finalSpeed =   (float)GetRandNum(m_FinalSpeedMin,   m_FinalSpeedMax);
    float initialAngle = (float)GetRandNum(m_InitialAngleMin, m_InitialAngleMax);

   
    NewParticle->InitLifeTime = (float)GetRandNum(m_ParticleLifetimeMin, m_ParticleLifetimeMax);
    NewParticle->CurrentLifeTime = 0.0f;

    
    NewParticle->Acceleration =
      NewParticle->Velocity * (float) (finalSpeed - initialSpeed) / (float) NewParticle->InitLifeTime;
    NewParticle->Velocity = NewParticle->Velocity * (float)initialSpeed;
    




      NewParticle->angle = initialAngle;
  

    NewParticle->RotationalVelocity = 
      GetRandNum(m_InitialRotSpeedMin, m_InitialRotSpeedMax);
    NewParticle->RotationalAccel = 
      (GetRandNum(m_FinalRotSpeedMin, m_FinalRotSpeedMax) -
      NewParticle->RotationalVelocity) / NewParticle->InitLifeTime;


    // all the colors, inerpolated in the ranges with a random increment.
    float Increment = (float)GetRandNum(0.0f, 1.0f);

    NewParticle->InitialColor = m_InitialColorMax * (1 - Increment) + m_InitialColorMin * Increment;
    
    Increment = (float)GetRandNum(0.0f, 1.0f);
    NewParticle->FinalColor = m_FinalColorMax * (1 - Increment) + m_FinalColorMin *(Increment);

    //get the inital and final scales.
    NewParticle->InitialScale.x = (float)GetRandNum(m_InitialScaleMin.x, m_InitialScaleMax.x);
    NewParticle->InitialScale.y = (float)GetRandNum(m_InitialScaleMin.y, m_InitialScaleMax.y);
    NewParticle->InitialScale.z = (float)GetRandNum(m_InitialScaleMin.z, m_InitialScaleMax.z);

    //get the inital and final scales.
    NewParticle->FinalScale.x = (float)GetRandNum(m_FinalScaleMin.x, m_FinalScaleMax.x);
    NewParticle->FinalScale.y = (float)GetRandNum(m_FinalScaleMin.y, m_FinalScaleMax.y);
    NewParticle->FinalScale.z = (float)GetRandNum(m_FinalScaleMin.z, m_FinalScaleMax.z);

  }
  
}
void ParticleSystemHackComponent::UpdateParticle( ParticlePoolComponent::Particle& Part)
{
  const float dt = 1.0 / 60.0;
  Part.CurrentLifeTime += dt;
  if (Part.CurrentLifeTime > Part.InitLifeTime)
  {
    Part.Active = false;
    ::Handle<TransformComponent> PartTran = Part.TheComposite->GetComponent(TransformComponent::Name).as<TransformComponent>();
    PartTran->Scale(Vec3(0,0,0));
    return;
  }

  Part.Position = Part.Position + Part.Velocity * dt;
  Part.Velocity = Part.Velocity + Part.Acceleration * dt;

  Part.angle = Part.angle + Part.RotationalVelocity * dt;
  Part.RotationalVelocity = Part.RotationalVelocity + Part.RotationalAccel * dt;

  float Increment = Part.CurrentLifeTime / Part.InitLifeTime;
  Part.CurrentScale = Part.InitialScale * (1 - Increment) + Part.FinalScale * Increment;
  Part.CurrentColor = Part.InitialColor * (1 - Increment) + Part.FinalColor * Increment;

  if (m_UseLocalCoords)
  {
    ::Handle<TransformComponent> tran = Owner()->GetComponent(TransformComponent::Name).as<TransformComponent>();
    //get parent
    //world = part + Parent
    Part.WorldPosition = Part.Position + tran->Position();
  }
  else
  {
    Part.WorldPosition = Part.Position;
  }


  //update the object values
  //world position
  ::Handle<TransformComponent> PartTran = Part.TheComposite->GetComponent(TransformComponent::Name).as<TransformComponent>();
  PartTran->Position(Part.WorldPosition);
  PartTran->Scale(Part.CurrentScale);
  glm::mat3 Rotation(1, 0, 0, 0, 1, 0, 0, 0, 1);
  PartTran->Rotation(glm::quat_cast(Rotation));
  PartTran->Rotation(glm::rotate(PartTran->Rotation(),(float)Part.angle * 3.14f/180.0f, glm::vec3(0, 0, 1)));
  
  //color- can't use yet
  Part.TheComposite->GetComponent(RenderableComponent::Name).as<RenderableComponent>()->Tint(Part.CurrentColor);

}

ParticlePoolComponent::Particle * ParticleSystemHackComponent::GenerateParticle(void)
{
  for (int i = 0; i < m_MaxParticles; ++i)
  {
    if (!m_Particles[i].Active)
    {
      m_Particles[i].Active = true;
      GenerateInitialPosAndVel(m_Particles[i]);
      return &m_Particles[i];
    }
  }

  return nullptr;
}
void ParticleSystemHackComponent::GenerateInitialPosAndVel(ParticlePoolComponent::Particle& Particle)
{

  ::Handle<TransformComponent> OwnerTran = Owner()->GetComponent(TransformComponent::Name).as<TransformComponent>();
  
 
  glm::vec3 axis = glm::vec3(1, 0, 0);
  glm::quat orientation = OwnerTran->WorldRotation();
  glm::mat3 RotMat = glm::mat3_cast(orientation);
  axis = RotMat * axis;
  float ParentAngle = atan2(axis.y, axis.x);

  if (!m_UseEmitterAngle)
  {
    ParentAngle = 0;
  }
  
  Particle.Position = OwnerTran->WorldPosition();
  float angle = (float)GetRandNum(m_EmitterAngle - m_EmitterAngleOffset, m_EmitterAngle + m_EmitterAngleOffset);
  Particle.Velocity = Vec3(cos(ParentAngle + angle* 3.14/180.0), sin(ParentAngle + angle* 3.14 / 180.0), 0);
  return;
}


float ParticleSystemHackComponent::EmitterAngle(void)
{
  return m_EmitterAngle;
}
void  ParticleSystemHackComponent::EmitterAngle(float arg)
{
  m_EmitterAngle = arg;
}
float ParticleSystemHackComponent::EmitterAngleOffset(void)
{
  return m_EmitterAngleOffset;
}
void  ParticleSystemHackComponent::EmitterAngleOffset(float arg)
{
  m_EmitterAngleOffset = arg;
}

bool ParticleSystemHackComponent::IsActive(void)
{
  return m_IsActive;
}
void ParticleSystemHackComponent::IsActive(bool arg)
{
  m_IsActive = arg;
}
bool ParticleSystemHackComponent::UseLocalCoords(void)
{
  return m_UseLocalCoords;
}
void ParticleSystemHackComponent::UseLocalCoords(bool arg)
{
  m_UseLocalCoords = arg;
}
int  ParticleSystemHackComponent::MaxParticles(void)
{
  return m_MaxParticles;
}
void ParticleSystemHackComponent::MaxParticles(int arg)
{
  m_MaxParticles = arg;
}
bool ParticleSystemHackComponent::RecycleParticles(void)
{
  return m_RecycleParticles;
}
void ParticleSystemHackComponent::RecycleParticles(bool arg)
{
  m_RecycleParticles = arg;
}
void ParticleSystemHackComponent::SetInitSpeed(float min, float max)
{
  m_InitialSpeedMin = min;
  m_InitialSpeedMax = max;
}
void ParticleSystemHackComponent::SetFinalSpeed(float min, float max)
{
  m_FinalSpeedMax = max;
  m_FinalSpeedMin = min;
}
void ParticleSystemHackComponent::SetEmissionRate(float arg)
{
  m_EmissionRate = arg;
}
void ParticleSystemHackComponent::SetParticleLifetime(float min, float max)
{
  m_ParticleLifetimeMin = min;
  m_ParticleLifetimeMax = max;
}
void ParticleSystemHackComponent::SetInitColor(Vec3 min, Vec3 max)
{
  m_InitialColorMin = min;
  m_InitialColorMax = max;
}
void ParticleSystemHackComponent::SetFinalColor(Vec3 min, Vec3 max)
{
  m_FinalColorMax = max;
  m_FinalColorMin = min;
}
void ParticleSystemHackComponent::SetInitScale(Vec3 min, Vec3 max)
{
  m_InitialScaleMin = min;
  m_InitialScaleMax = max;
}
void ParticleSystemHackComponent::SetFinalScale(Vec3 min, Vec3 max)
{
  m_FinalScaleMin = min;
  m_FinalScaleMax = max;
}
void ParticleSystemHackComponent::SetInitRotSpeed(float min, float max)
{
  m_InitialRotSpeedMin = min;
  m_InitialRotSpeedMax = max;
}
void ParticleSystemHackComponent::SetFinalRotSpeed(float min, float max)
{
  m_FinalRotSpeedMin = min;
  m_FinalRotSpeedMax = max;
}
void ParticleSystemHackComponent::SetInitAngle(float min, float max)
{
  m_InitialAngleMin = min;
  m_InitialAngleMax = max;
}



int   ParticleSystemHackComponent::GetRandNum(int min, int max)
{
  unsigned int number = rand() % ((max - min) + 1);
  return min + number;
}
float ParticleSystemHackComponent::GetRandNum(float min, float max)
{
  unsigned int number = rand() % 10001;
  float Variation = number / 10000.0;
  float Range = max - min;

  return min + (Variation * Range);
}



META_REGISTER_FUNCTION(ParticleSystemHack)
{
  META_INHERIT(ParticleSystemHackComponent, "Kepler", "Component");

  // META_HANDLE means that we are binding a pointer to this object, not the object itself
  META_HANDLE(ParticleSystemHackComponent);

  META_ADD_PROP(ParticleSystemHackComponent, IsActive);
  META_ADD_PROP(ParticleSystemHackComponent, UseLocalCoords);
  META_ADD_PROP(ParticleSystemHackComponent, MaxParticles);
  META_ADD_PROP(ParticleSystemHackComponent, RecycleParticles);
  META_ADD_PROP(ParticleSystemHackComponent, EmitterAngle);
  META_ADD_PROP(ParticleSystemHackComponent, EmitterAngleOffset);

  META_ADD_METHOD(ParticleSystemHackComponent, SetInitSpeed);
  META_ADD_METHOD(ParticleSystemHackComponent, SetFinalSpeed);
  META_ADD_METHOD(ParticleSystemHackComponent, SetEmissionRate);
  META_ADD_METHOD(ParticleSystemHackComponent, SetParticleLifetime);
  META_ADD_METHOD(ParticleSystemHackComponent, SetInitColor);
  META_ADD_METHOD(ParticleSystemHackComponent, SetFinalColor);
  META_ADD_METHOD(ParticleSystemHackComponent, SetInitScale);
  META_ADD_METHOD(ParticleSystemHackComponent, SetFinalScale);
  META_ADD_METHOD(ParticleSystemHackComponent, SetInitRotSpeed);
  META_ADD_METHOD(ParticleSystemHackComponent, SetFinalRotSpeed);
  META_ADD_METHOD(ParticleSystemHackComponent, SetInitAngle);

  META_FINALIZE_PTR(ParticleSystemHackComponent, MetaPtr);
}