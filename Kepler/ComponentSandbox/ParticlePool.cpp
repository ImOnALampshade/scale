//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "ParticlePool.h"
#include "../KeplerCore/compositefactory.h"


ParticlePoolComponent::ParticlePoolComponent(): m_Growths(0)
{

}

void ParticlePoolComponent::Initialize(const Json::Object &jsonData)
{
  m_PoolSize = static_cast<size_t>( jsonData.Locate("PoolSize", 1024.0).as<Json::Number>() );

  //load particle compositites all at once.
  GenerateParticles(m_PoolSize);

}

ParticlePoolComponent::Particle ParticlePoolComponent::GiveParticle()
{

  //do we have particles to give out?
  if( m_ParticlePool.size() < 1 )
  { 
    GenerateParticles( m_PoolSize );
  }

  //grab a particle out and return it.
  Particle p;
  p = m_ParticlePool.back();
  m_ParticlePool.pop_back();
 
  return p;

}

void ParticlePoolComponent::TakeParticle(Particle const& particle)
{
  m_ParticlePool.emplace_back( particle );
}

void ParticlePoolComponent::GenerateParticles( unsigned int Count )
{
  //generate count particles.
  for (unsigned i = 0; i < Count; ++i)
  {
    ParticlePoolComponent::Particle newParticle;
    newParticle.Active = false;
    newParticle.CurrentScale = Vec3(0, 0, 0);
    newParticle.TheComposite = CompositeManagement::Factory->CreateComposite("Particle", Owner()->Space());

    m_ParticlePool.push_back(newParticle);
  }

  m_Growths += 1;
}

META_REGISTER_FUNCTION(ParticlePool)
{
  META_INHERIT(ParticlePoolComponent, "Kepler", "Component");

  // META_HANDLE means that we are binding a pointer to this object, not the object itself
  META_HANDLE(ParticlePoolComponent);

  META_FINALIZE_PTR(ParticlePoolComponent, MetaPtr);
}