//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#pragma once
#ifndef PARTICLE_POOL_COMPONENT_H
#define PARTICLE_POOL_COMPONENT_H

#include "../KeplerCore/component.h"
#include "../glminclude.h"
#include "../KeplerCore/transformcomponent.h"

class ParticlePoolComponent : public Component
{
public:
  struct Particle
  {
    bool Active;
    ::Handle<Composite> TheComposite;
    glm::vec3 Position;
    glm::vec3 WorldPosition;
    glm::vec3 Velocity;
    glm::vec3 Acceleration;

    double angle;
    double RotationalVelocity;
    double RotationalAccel;

    double InitLifeTime;
    double CurrentLifeTime;

    glm::vec3 InitialColor;
    glm::vec3 FinalColor;

    glm::vec3 InitialScale;
    glm::vec3 FinalScale;

    glm::vec3 CurrentColor;
    glm::vec3 CurrentScale;

  };

public:
  ParticlePoolComponent();
  virtual void Initialize(const Json::Object &jsonData);
  Particle GiveParticle();
  void     TakeParticle( Particle const& particle);

  COMPONENT_META

private:
  void GenerateParticles( unsigned int Count );


private:
  size_t m_PoolSize;
  size_t m_Growths;
  std::vector<Particle> m_ParticlePool;

};

#endif 