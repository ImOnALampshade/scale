// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  This is the implementation of a Python Interpreter. The command window that
//  launches with the game takes in python code and runs it.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once

#include <iostream>
#include <string>
#include <thread>
#include "../pyinclude.h"

namespace PyConsole
{
  void Init (void);
  void Free (void);

  void RunPyConsole (void);
  void ParsePythonCode (const char *input);


  extern bool PyConsoleRunning;

  extern std::thread PyConsoleThread;
}