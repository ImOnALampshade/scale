// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  This is the implementation of a Python Interpreter. The command window that
//  launches with the game takes in python code and runs it.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "PyConsole.h"

namespace PyConsole
{
  bool PyConsoleRunning;
  std::thread PyConsoleThread;
}

void PyConsole::Init(void)
{
  PyConsoleRunning = false;

  PyConsoleThread = std::thread(RunPyConsole);
}

void PyConsole::Free(void)
{
  PyConsoleRunning = false;

  PyConsoleThread.join();
}

void PyConsole::RunPyConsole(void)
{
  std::string userTypedLine;

  PyConsoleRunning = true;

  while(PyConsoleRunning)
  {
    std::getline(std::cin, userTypedLine);

    //this -2 is for the return char and the null char
    if(userTypedLine[userTypedLine.size() - 2] == ':')
    {
      std::string userTypedLineCont;
      userTypedLineCont[0] = ' ';

      while(userTypedLineCont[0] == ' ')
      {
        std::getline(std::cin, userTypedLineCont);
        userTypedLine += userTypedLineCont;
      }
    }

    PyRun_SimpleString(userTypedLine.c_str());
  }
}


// player.moveto(0, 0, 0)

// if i == 1:

// for i in range(0, 100):
//   spawn_enemy()
//   whatever_else()
// # now it runs

// class foo:

// def bar:

//Py_main