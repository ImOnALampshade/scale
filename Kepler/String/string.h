//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#ifndef KEPLER_STRING_H
#define KEPLER_STRING_H

#include "utf8.h"
#include <codecvt>
#include <locale>
#include <memory>
#include <vector>

// -----------------------------------------------------------------------------

class String
{
public:
  typedef utf8::unchecked::iterator<const char *> iterator;
  static const size_t npos = size_t (-1);

  String ();
  String (const char *);
  String (const char *, size_t len);
  String (const std::string &);
  String (size_t len);
  String (const String &) = default;

  String &operator= (const char *);
  String &operator= (const std::string &);
  String &operator= (const String &) = default;

  String &Assign (const char *, size_t len);
  String &Assign (size_t len);

  bool Empty() const;
  void Clear();

  iterator begin() const;
  iterator end() const;

  char &operator[] (size_t i);
  const char &operator[] (size_t i) const;
  size_t Length() const;

  String operator+ (const char *) const;
  String operator+ (const std::string &) const;
  String operator+ (String) const;
  String operator+ (char) const;

  String &operator+= (const char *);
  String &operator+= (const std::string &);
  String &operator+= (String);
  String &operator+= (char);

  String &Append (const char *, size_t len);

  String SubString (size_t start) const;
  String SubString (size_t start, size_t len) const;

  size_t Find (String str, size_t start = 0) const;
  size_t Find (const char *str, size_t start = 0) const;
  size_t Find (const char *str, size_t start, size_t len) const;
  size_t Find (char32_t c, size_t start = 0) const;

  size_t ReverseFind (String str, size_t start = npos) const;
  size_t ReverseFind (const char *str, size_t start = npos) const;
  size_t ReverseFind (const char *str, size_t start, size_t len) const;
  size_t ReverseFind (char32_t c, size_t start = npos) const;

  String &Replace (size_t pos, size_t len, const char *);
  String &Replace (size_t pos, size_t len, const std::string &);
  String &Replace (size_t pos, size_t len, String);
  String &Replace (size_t pos, size_t len, const char *, size_t bufLen);

  String &Erase (size_t pos, size_t len = npos);

  String &ToUpper();
  String &ToLower();
  String &SwapCase();
  String &Capitalize();

  std::vector<String> Split (String seperator) const;
  std::vector<String> Split (const char *seperator) const;
  std::vector<String> Split (const char *seperator, size_t len) const;
  std::vector<String> Split (char32_t c);

  String &ReplaceAll (String find, String replace);
  String &ReplaceAll (const char *find, String replace);
  String &ReplaceAll (String find, const char *replace);
  String &ReplaceAll (const char *find, const char *replace);

  String &StripTrailingWhitespace();

  template<typename Iterable>
  String Join (const Iterable &collection);

  template<typename ForwardIter>
  String Join (ForwardIter begin, ForwardIter end) const;

  const std::string &GetUnderlying() const;
  std::string ToStdString() const;
  std::wstring ToStdWideString() const;
  const char *c_str() const;
  const char *CStr() const;

  static String Format (const char *format, ...);
  static String LoadFile (String filename);

  void Aquire();

  friend std::ostream &operator<< (std::ostream &, String);

private:
  typedef std::string underlying_string;

  std::shared_ptr<underlying_string> m_str;

  static std::wstring_convert<std::codecvt_utf8<wchar_t> > m_utf8Conv;
};

// -----------------------------------------------------------------------------

std::ostream &operator<< (std::ostream &, String);
std::wostream &operator<< (std::wostream &, String);

String operator+ (const char *, String);
String operator+ (const std::string &, String);

String ToString (int val);
String ToString (long val);
String ToString (long long val);
String ToString (unsigned val);
String ToString (unsigned long val);
String ToString (unsigned long long val);
String ToString (float val);
String ToString (double val);
String ToString (long double val);
String ToString (String val);

bool operator== (String, String);
bool operator== (String, const char *);
bool operator== (const char *, String);
bool operator!= (String, String);
bool operator!= (String, const char *);
bool operator!= (const char *, String);
bool operator>= (String, String);
bool operator<= (String, String);
bool operator> (String, String);
bool operator< (String, String);

// -----------------------------------------------------------------------------

template<typename Iterable>
String String::Join (const Iterable &collection)
{
  return Join (collection.begin(), collection.end());
}

template<typename ForwardIter>
String String::Join (ForwardIter begin, ForwardIter end) const
{
  String total ("");

  if (begin == end)
    return total;

loop_start:

  total += ToString (*begin);

  if (++begin != end)
  {
    total += *this;
    goto loop_start;
  }

  return total;
}

// -----------------------------------------------------------------------------

#endif
