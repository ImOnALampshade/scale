//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include "string.h"
#include <cstring>
#include <cstdarg>
#include <fstream>

using namespace std;

wstring_convert<codecvt_utf8<wchar_t> > String::m_utf8Conv;

// -----------------------------------------------------------------------------

String::String() :
  m_str (make_shared<underlying_string> (""))
{
}

String::String (const char *str) :
  m_str (make_shared<underlying_string> (str))
{
}

String::String (const char *str, size_t len) :
  m_str (make_shared<underlying_string> (str, len))
{
}

String::String (const std::string &str) :
  m_str (make_shared<underlying_string> (str))
{
}

String::String (size_t len) :
  m_str (make_shared<underlying_string> (len, '\0'))
{
}

// -----------------------------------------------------------------------------

String &String::operator= (const char *str)
{
  m_str = make_shared<underlying_string> (str);
  return *this;
}

String &String::operator= (const std::string &str)
{
  m_str = make_shared<underlying_string> (str);
  return *this;
}

String &String::Assign (const char *str, size_t len)
{
  m_str = make_shared<underlying_string> (str, len);
  return *this;
}

String &String::Assign (size_t len)
{
  m_str = make_shared<underlying_string> (len, '\0');
  return *this;
}

// -----------------------------------------------------------------------------

bool String::Empty() const
{
  return m_str->empty();
}

void String::Clear()
{
  m_str = make_shared<underlying_string> ("");
}

// -----------------------------------------------------------------------------

String::iterator String::begin() const
{
  return iterator (m_str->c_str());
}

String::iterator String::end() const
{
  return iterator (m_str->c_str() + m_str->length());
}

// -----------------------------------------------------------------------------

char &String::operator[] (size_t i)
{
  return m_str->operator[] (i);
}

const char &String::operator[] (size_t i) const
{
  return m_str->operator[] (i);
}

// -----------------------------------------------------------------------------

size_t String::Length() const
{
  return m_str->length();
}

// -----------------------------------------------------------------------------

String String::operator+ (const char *str) const
{
  String result (*this);
  result.Aquire();
  result += str;
  return result;
}

String String::operator+ (const std::string &str) const
{
  String result (*this);
  result.Aquire();
  *result.m_str.get() += str;
  return result;
}

String String::operator+ (String str) const
{
  String result (*this);
  result.Aquire();
  result += str;
  return result;
}

String String::operator+ (char c) const
{
  String result (*this);
  result.Aquire();
  result += c;
  return result;
}

// -----------------------------------------------------------------------------

String &String::operator+= (const char *str)
{
  Aquire();
  *m_str.get() += str;
  return *this;
}

String &String::operator+= (const std::string &str)
{
  Aquire();
  *m_str.get() += str;
  return *this;
}

String &String::operator+= (String str)
{
  Aquire();
  *m_str.get() += *str.m_str.get();
  return *this;
}

String &String::operator+= (char c)
{
  Aquire();
  *m_str.get() += c;
  return *this;
}

// -----------------------------------------------------------------------------

String &String::Append (const char *str, size_t len)
{
  Aquire();
  m_str->append (str, len);
  return *this;
}

// -----------------------------------------------------------------------------

String String::SubString (size_t start) const
{
  return String (m_str->substr (start));
}

String String::SubString (size_t start, size_t len) const
{
  return String (m_str->substr (start, len));
}

// -----------------------------------------------------------------------------

size_t String::Find (String str, size_t start) const
{
  return m_str->find (*str.m_str.get(), start);
}

size_t String::Find (const char *str, size_t start) const
{
  return m_str->find (str, start);
}

size_t String::Find (const char *str, size_t start, size_t len) const
{
  return m_str->find (str, start, len);
}

size_t String::Find (char32_t c, size_t start) const
{
  return m_str->find (c, start);
}

// -----------------------------------------------------------------------------

size_t String::ReverseFind (String str, size_t start) const
{
  return m_str->rfind (*str.m_str.get(), start);
}

size_t String::ReverseFind (const char *str, size_t start) const
{
  return m_str->rfind (str, start);
}

size_t String::ReverseFind (const char *str, size_t start, size_t len) const
{
  return m_str->rfind (str, start, len);
}

size_t String::ReverseFind (char32_t c, size_t start) const
{
  return m_str->rfind (c, start);
}

// -----------------------------------------------------------------------------

String &String::Replace (size_t pos, size_t len, const char *str)
{
  Aquire();
  m_str->replace (pos, len, str);
  return *this;
}

String &String::Replace (size_t pos, size_t len, const std::string &str)
{
  Aquire();
  m_str->replace (pos, len, str);
  return *this;
}

String &String::Replace (size_t pos, size_t len, String str)
{
  Aquire();
  m_str->replace (pos, len, *str.m_str.get());
  return *this;
}

String &String::Replace (size_t pos, size_t len, const char *str, size_t bufLen)
{
  Aquire();
  m_str->replace (pos, len, str, bufLen);
  return *this;
}

// -----------------------------------------------------------------------------

String &String::Erase (size_t pos, size_t len)
{
  Aquire();
  m_str->erase (pos, len);
  return *this;
}

// -----------------------------------------------------------------------------

String &String::ToUpper()
{
  Aquire();

  for (char &c : *m_str.get())
    c = toupper (c);

  return *this;
}

String &String::ToLower()
{
  Aquire();

  for (char &c : *m_str.get())
    c = tolower (c);

  return *this;
}

String &String::SwapCase()
{
  Aquire();

  for (char &c : *m_str.get())
  {
    if (islower (c))
      c -= 32;

    else if (isupper (c))
      c += 32;
  }

  return *this;
}

String &String::Capitalize()
{
  if (Empty())
    return *this;

  Aquire();
  m_str->front() = toupper (m_str->front());

  for (size_t i = 1; i < m_str->length(); ++i)
    (*m_str.get()) [i] = tolower ( (*m_str.get()) [i]);

  return *this;
}

vector<String> String::Split (String seperator) const
{
  vector<String> list;

  size_t start = 0, end;

  while ( (end = Find (seperator, start)) != npos)
  {
    String substr = SubString (start, end - start);
    list.emplace_back (substr);
    start = end + 1;
  }

  list.emplace_back (SubString (start));

  return list;
}

vector<String> String::Split (const char *seperator) const
{
  vector<String> list;

  size_t start = 0, end;

  while ( (end = Find (seperator, start)) != npos)
  {
    String substr = SubString (start, end - start);
    list.emplace_back (substr);
    start = end + 1;
  }

  list.emplace_back (SubString (start));

  return list;
}

vector<String> String::Split (const char *seperator, size_t len) const
{
  vector<String> list;

  size_t start = 0, end;

  while ( (end = Find (seperator, start, len)) != npos)
  {
    String substr = SubString (start, end - start);
    list.emplace_back (substr);
    start = end + 1;
  }

  list.emplace_back (SubString (start));

  return list;
}

vector<String> String::Split (char32_t c)
{
  vector<String> list;

  size_t start = 0, end;

  while ( (end = Find (c, start)) != npos)
  {
    String substr = SubString (start, end - start);
    list.emplace_back (substr);
    start = end + 1;
  }

  list.emplace_back (SubString (start));

  return list;
}

String &String::ReplaceAll (String find, String replace)
{
  Aquire();
  size_t start = 0;

  while ( (start = Find (find, start)) != npos)
    Replace (start, find.Length(), replace);

  return *this;
}

String &String::ReplaceAll (const char *find, String replace)
{
  Aquire();
  size_t start = 0;
  size_t findLen = strlen (find);

  while ( (start = Find (find, start)) != npos)
    Replace (start, findLen, replace);

  return *this;
}

String &String::ReplaceAll (String find, const char *replace)
{
  Aquire();
  size_t start = 0;

  while ( (start = Find (find, start)) != npos)
    Replace (start, find.Length(), replace);

  return *this;
}

String &String::ReplaceAll (const char *find, const char *replace)
{
  Aquire();
  size_t start = 0;
  size_t findLen = strlen (find);

  while ( (start = Find (find, start)) != npos)
    Replace (start, findLen, replace);

  return *this;
}

String &String::StripTrailingWhitespace()
{
  Aquire();

  size_t start = m_str->find_first_not_of (" \t\n");
  m_str->erase (0, start);

  size_t end = m_str->find_last_of (" \t\n");
  m_str->erase (end - 1, std::string::npos);

  return *this;
}

// -----------------------------------------------------------------------------

const std::string &String::GetUnderlying() const
{
  return *m_str.get();
}

std::string String::ToStdString() const
{
  return *m_str.get();
}

std::wstring String::ToStdWideString() const
{
  return m_utf8Conv.from_bytes (c_str());
}

const char *String::c_str() const
{
  return m_str->c_str();
}

const char *String::CStr() const
{
  return m_str->c_str();
}

// -----------------------------------------------------------------------------

String String::Format (const char *format, ...)
{
  va_list args;
  va_start (args, format);

  // Calculate how large of a buffer to allocate
  int l = vsnprintf (nullptr, 0, format, args);
  unique_ptr<char> buf (new char[l + 1]);

  // Print the string into the buffer
  vsprintf (buf.get(), format, args);

  va_end (args);

  // Return the buffer as a String object
  return String (buf.get());
}

// -----------------------------------------------------------------------------

String String::LoadFile (String filename)
{
  String file;
  std::ifstream is (filename.c_str());

  for (std::string line; getline (is, line);)
  {
    file += line;
    file += "\n";
  }

  return file;
}

// -----------------------------------------------------------------------------

void String::Aquire()
{
  // If not a unique shared pointer, make a new shared pointer, pointing at a
  // copy of the current string.
  if (!m_str.unique())
    m_str = make_shared<underlying_string> (*m_str.get());
}

// -----------------------------------------------------------------------------

std::ostream &operator<< (std::ostream &os, String str)
{
  return os << str.c_str();
}

std::wostream &operator<< (std::wostream &os, String str)
{
  return os << str.c_str();
}

// -----------------------------------------------------------------------------

String operator+ (const char *lhs, String rhs)
{
  String result (lhs);
  result += rhs;
  return result;
}

String operator+ (const std::string &lhs, String rhs)
{
  String result (lhs);
  result += rhs;
  return result;
}

// -----------------------------------------------------------------------------

String ToString (int val)                { return String::Format ("%d",   val); }
String ToString (long val)               { return String::Format ("%ld",  val); }
String ToString (long long val)          { return String::Format ("%lld", val); }
String ToString (unsigned val)           { return String::Format ("%u",   val); }
String ToString (unsigned long val)      { return String::Format ("%lu",  val); }
String ToString (unsigned long long val) { return String::Format ("%llu", val); }
String ToString (float val)              { return String::Format ("%g",   val); }
String ToString (double val)             { return String::Format ("%g",   val); }
String ToString (long double val)        { return String::Format ("%Lg",  val); }
String ToString (String val)             { return val; }

// -----------------------------------------------------------------------------

bool operator== (String lhs, String rhs)
{
  return strcmp(lhs.c_str(), rhs.c_str()) == 0;
}
bool operator== (String lhs, const char* rhs)
{
  return strcmp(lhs.c_str(), rhs) == 0;
}
bool operator== (const char* lhs, String rhs)
{
  return strcmp(lhs, rhs.c_str()) == 0;
}

bool operator!= (String lhs, String rhs)
{
  return strcmp(lhs.c_str(), rhs.c_str()) != 0;
}
bool operator!= (String lhs, const char* rhs)
{
  return strcmp(lhs.c_str(), rhs) != 0;
}
bool operator!= (const char* lhs, String rhs)
{
  return strcmp(lhs, rhs.c_str()) != 0;
}

bool operator>= (String lhs, String rhs)
{
  return strcmp (lhs.c_str(), rhs.c_str()) >= 0;
}

bool operator<= (String lhs, String rhs)
{
  return strcmp (lhs.c_str(), rhs.c_str()) <= 0;
}

bool operator> (String lhs, String rhs)
{
  return strcmp (lhs.c_str(), rhs.c_str()) > 0;
}

bool operator< (String lhs, String rhs)
{
  return strcmp (lhs.c_str(), rhs.c_str()) < 0;
}

// -----------------------------------------------------------------------------
