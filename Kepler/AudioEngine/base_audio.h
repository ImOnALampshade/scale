// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Base audio calss that each audio component type inherits from. provieds simple
//    FMOD debuging and the static pointer to the game engine that all components
//    must have access to.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once

#include <stdio.h>

#include "FMOD/fmod.hpp"
#include "FMOD/fmod_studio.hpp"
#include "FMOD/fmod_errors.h"
#include "../KeplerCore/component.h"
#include "../KeplerCore/composite.h"
#include "../HashTable/hashtable.h"
#include "../HashFunctions/hashfunctions.h"
#include "../String/string.h"

struct Position
{
  Position (void) : x (0), y (0), z (0) {}
  Position (float x, float y, float z) : x (x), y (y), z (z) {}

  float x;
  float y;
  float z;
};

struct AudioMessage
{
  float value;
  String name;
  bool state;
};

class BaseAudio : public Component
{
public:
  virtual void Initialize (const Json::Object &jsonData) = 0;
  virtual void Free() = 0;

  void ErrorCheck (FMOD_RESULT result);

  static BaseAudio *m_audioEngine;

  static int TEST;

private:



};

namespace Utilities
{
  namespace Hash
  {
    std::uint32_t IntHash (const int &);
  }
}
