// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Base audio calss that each audio component type inherits from. provieds simple
//    FMOD debuging and the static pointer to the game engine that all components 
//    must have access to.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "base_audio.h"

BaseAudio* BaseAudio::m_audioEngine = nullptr;
int BaseAudio::TEST = 123;

void BaseAudio::ErrorCheck(FMOD_RESULT result)
{
  if(result != FMOD_OK)
  {
    printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
  }
}

namespace Utilities
{
  namespace Hash
  {
    std::uint32_t IntHash(const int &val)
    {
      return val;
    }
  }
}
