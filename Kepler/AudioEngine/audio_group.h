// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Audio groups, used only internally by the engine. A fancy HashTable
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once

#include "base_audio.h"

class AudioComponent;

class AudioComponentGroup : public BaseAudio
{
public:

  virtual void Initialize(const Json::Object &jsonData) {}
  virtual void Free() {}

  AudioComponentGroup(void);
  AudioComponentGroup(String GroupName);
  ~AudioComponentGroup(void);

  void AddComponent(AudioComponent*);
  void RemoveComponent(AudioComponent*);

  int ComponentCount(void);


  static const StringHasher testHash;

  HashTable<int, AudioComponent*> m_components;

  String m_groupName;

  float m_groupVolume;
  bool m_groupPauseState;

};
