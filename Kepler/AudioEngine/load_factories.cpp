// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Simple load function for our engines internal use. Initialized all components
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "load_factories.h"
#include "audio_component.h"
#include "audioEngine.h"


void AudioInit::LoadFactories()
{
  CREATE_FACTORY (AudioEngine);
  CREATE_FACTORY (Audio);


}

void AudioInit::CreateMeta()
{
  AudioComponent::CreateMeta();
  AudioEngineComponent::CreateMeta();
}