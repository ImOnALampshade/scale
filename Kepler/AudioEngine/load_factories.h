// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Simple load function for our engines internal use. Initialized all components
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once
#include "../KeplerCore/compositefactory.h"

namespace AudioInit
{
  void LoadFactories();

  void CreateMeta();
}
