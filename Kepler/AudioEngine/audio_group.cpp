// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Audio groups, used only internally by the engine. A fancy HashTable
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "audio_group.h"
#include "audio_component.h"


//just a group of pointers to objects, think map, with the components id as first, and pointer to it, second


AudioComponentGroup::AudioComponentGroup(void) : m_components(Utilities::Hash::IntHash)
{
  //exit(8);
}

AudioComponentGroup::AudioComponentGroup(String groupName) : m_components(Utilities::Hash::IntHash)
{
  m_groupName = groupName;
  m_groupVolume = 1.0f;
  m_groupPauseState = false;
}

AudioComponentGroup::~AudioComponentGroup(void)
{
  //empty body
}


void AudioComponentGroup::AddComponent(AudioComponent* component)
{
  if(m_components.Find(component->GetID()) == m_components.end())
  {
    m_components.Insert(component->GetID(), component);
  }
}

void AudioComponentGroup::RemoveComponent(AudioComponent* component)
{
  if(m_components.Find(component->GetID()) != m_components.end())
  {
    m_components.Erase(component->GetID());
  }
}


int AudioComponentGroup::ComponentCount(void)
{
  return m_components.Size();
}

