// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  each thing in the game that is to make sound should have one of these.
//    Allows for sounds to be played and controlled from a spesific object.
//    One instance of this should be declared the listener.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once

#include "base_audio.h"
#include "audioEngine.h"

class AudioComponent : public BaseAudio
{
  friend class AudioEngineComponent;


public:
  AudioComponent (void);

  void Initialize (const Json::Object &jsonData);
  void Free();

  void Initialize (void);

  void PlayLongSound (String);
  void PlayShortSound (String SoundName);
  void Pause (String SoundName, bool PauseState);
  void PauseAll (bool PauseState);
  void Stop(String SoundName);
  void StopAll();
  void StopButLetFinish(String);
  void UpdatePosition (float x, float y, float z);
  void MakeMeTheListener (void);
  void SetSoundVolume (float, String);
  void SetObjectVolume (float);
  void CheckLoopingSounds(void);
  
  
  void Pitch(float);
  float Pitch() const;
  void Volume(float);
  float Volume() const;


  int GetID (void);

  void CreateAndJoinGroup (String);
  void JoinGroup (String);
  void LeaveGroup (String);
  void LeaveAllGroups (void);

  COMPONENT_META

private:

  Position m_position;

  AudioEngineComponent *m_engine;
  HashTable<String, FMOD::Studio::EventInstance *> m_instanceTable;

  int m_componentID;

  float m_objectVolume;
  float m_objectPitch;

  HashTable<String, int> m_previousTimelinePosition;
};
