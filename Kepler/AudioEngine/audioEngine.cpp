// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Incharge of keeping tack of all sound id's and groups. Alows for some
//    overall controll of things like volume and pause state.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "audioEngine.h"
#include <iostream>

//run on thread component

AudioEngineComponent::AudioEngineComponent (void) : m_idTable (Utilities::Hash::SuperFast), m_groups (Utilities::Hash::SuperFast)
{
  m_system = nullptr;

  m_listenerObject = nullptr;

  BaseAudio::TEST = 99;

  m_nextFreeID = 0;
}

void AudioEngineComponent::Initialize (const Json::Object &jsonData)
{
  m_audioEngine = this;

  // Create the Studio m_system object.
  ErrorCheck (FMOD::Studio::System::create (&m_system, FMOD_VERSION));

  // Initialize FMOD Studio, which will also initialize FMOD Low Level
  ErrorCheck (m_system->initialize (512, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, 0));

  //Sets the listener to 0. this get updated only when an object is set as the listener
  SetListenerPosittion (0.0, 0.0, 0.0);

  //sets up the master bank
  FMOD::Studio::Bank *masterBank = NULL;
  ErrorCheck (m_system->loadBankFile ("Master Bank.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &masterBank));

  FMOD::Studio::Bank *StringsBank = NULL;
  ErrorCheck (m_system->loadBankFile ("Master Bank.Strings.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &StringsBank));

  //sets up other banks (AudioBank.bank only in this instance)
  FMOD::Studio::Bank *ambienceBank = nullptr;
  ErrorCheck (m_system->loadBankFile ("AudioBank.bank", FMOD_STUDIO_LOAD_BANK_NORMAL, &ambienceBank));

  //get the id of each event in the given file and store it in a table
  //open file
  FILE *file;
  fopen_s (&file, "events.txt", "rb");

  if (!file)
  {
    printf ("events.txt failed to open");
  }

  //read in all event names and fill table with the names and the associared id from fmod
  //no names longer than 253 permited
  char tempEventNameBuffer[255];

  while (!feof (file))
  {
    fgets (tempEventNameBuffer, 255, file);
    //fgets(tempEventNameBuffer, 255, file);

    //break at end of file or blank line
    if (tempEventNameBuffer[0] == '\n' || tempEventNameBuffer[0] == '\0')
      break;

    //strip extra newline and null from read in name
    //REWORK
    for (unsigned i = 0; i < strlen (tempEventNameBuffer); ++i)
      //for(unsigned i = 0; i < strlen(tempEventNameBuffer); ++i)
    {
      if (tempEventNameBuffer[i] == '\n' || tempEventNameBuffer[i] == '\r')
      {
        tempEventNameBuffer[i] = '\0';
      }
    }

    FMOD::Studio::ID loopingAmbienceID = { 0 };
    String eventName = "event:/";
    eventName += tempEventNameBuffer;
    ErrorCheck (m_system->lookupID (eventName.CStr(), &loopingAmbienceID));

    //adds name and ID of event to table
    m_idTable.Insert (tempEventNameBuffer, loopingAmbienceID);
  }

  fclose (file);

  //RegisterMessageProc ("LogicUpdate",         std::function<void(float)>([&] (float Dt)        { Update();           std::cout << "HEREISH" << std::endl;  }));
  RegisterMessageProc ("Update",              std::function<void()>( [this] ()   {  Update(); }));
  RegisterMessageProc ("SetMasterVolume",     std::function<void(float)>( [this] (float value)     { SetMasterVolume (value); }));
  RegisterMessageProc ("SetMasterAudioPause", [this] (void *state)     { SetMasterPause ( (reinterpret_cast<AudioMessage *> (state))->state);  });
  RegisterMessageProc ("WindowUnfocus",       std::function<void()>( [this] (void)  { SetMasterPause (true); }));
  RegisterMessageProc ("WindowFocus",         std::function<void()>( [this] (void)  { SetMasterPause (false); }));
  RegisterMessageProc ("CreateAudioGroup",    [this] (void *groupName) { CreateGroup ( (reinterpret_cast<AudioMessage *> (groupName))->name);  });
  RegisterMessageProc ("DestroyAudioGroup",   [this] (void *groupName) { DestroyGroup ( (reinterpret_cast<AudioMessage *> (groupName))->name); });
  RegisterMessageProc ("SetAudioGroupVolume", [this] (void *groupName) {
    SetGroupVolume ( (reinterpret_cast<AudioMessage *> (groupName))->name,
                     (reinterpret_cast<AudioMessage *> (groupName))->value);
  });
  RegisterMessageProc ("SetAudioGroupPause",  [this] (void *groupName) {
    PauseGroup ( (reinterpret_cast<AudioMessage *> (groupName))->name,
                 (reinterpret_cast<AudioMessage *> (groupName))->state);
  });

}


void AudioEngineComponent::Free()
{
  //empty body
}

//updates the listener to the propper location and updates all running audio events
void AudioEngineComponent::Update (void)
{
  if (m_listenerObject)
  {
    SetListenerPosittion (m_listenerObject->m_position);
  }

  //update each audio object in the EndingSounds group
  if (m_groups.Size() > 0 &&
      m_groups.Find ("EndingSounds") != m_groups.end() &&
      m_groups["EndingSounds"].ComponentCount() > 0)
  {
    for (auto tempObjects : m_groups["EndingSounds"].m_components)
    {
      tempObjects.value->CheckLoopingSounds();
    }
  }
  
  ErrorCheck (m_system->update());
}

void AudioEngineComponent::Shutdown (void)
{
  ErrorCheck (m_system->release());
}

void AudioEngineComponent::SetListenerPosittion (Position listenerPos)
{
  FMOD_3D_ATTRIBUTES attributes = { { 0 } };

  attributes.forward.z = 1.0f;
  attributes.up.y = 1.0f;

  attributes.position.x = listenerPos.x;
  attributes.position.y = listenerPos.y;
  attributes.position.z = listenerPos.z;

  ErrorCheck (m_system->setListenerAttributes (&attributes));
}

void AudioEngineComponent::SetListenerPosittion (float x, float y, float z)
{
  FMOD_3D_ATTRIBUTES attributes = { { 0 } };

  attributes.forward.z = 1.0f;
  attributes.up.y = 1.0f;

  attributes.position.x = x;
  attributes.position.y = y;
  attributes.position.z = z;

  ErrorCheck (m_system->setListenerAttributes (&attributes));
}

void AudioEngineComponent::SetMasterVolume (float volume)
{
  FMOD::Studio::ID masterBusID;
  ErrorCheck (m_system->lookupID ("bus:/", &masterBusID)); // the master bus path is "/"

  //FMOD::Studio::MixerStrip* masterBus;
  FMOD::Studio::Bus *masterBus;
  //ErrorCheck(m_system->getMixerStrip(&masterBusID, FMOD_STUDIO_LOAD_PROHIBITED, &masterBus));
  ErrorCheck (m_system->getBusByID (&masterBusID, &masterBus));

  masterBus->setFaderLevel (volume);
}

void AudioEngineComponent::SetMasterPause (bool pauseState)
{
  FMOD::Studio::ID masterBusID;
  ErrorCheck (m_system->lookupID ("bus:/", &masterBusID)); // the master bus path is "/"

  //FMOD::Studio::MixerStrip* masterBus;
  FMOD::Studio::Bus *masterBus;
  //ErrorCheck(m_system->getMixerStrip(&masterBusID, FMOD_STUDIO_LOAD_PROHIBITED, &masterBus));
  ErrorCheck (m_system->getBusByID (&masterBusID, &masterBus));

  masterBus->setPaused (pauseState);
}


int AudioEngineComponent::GetNextFreeID (void)
{
  return m_nextFreeID++;
}


void AudioEngineComponent::CreateGroup (String groupName)
{
  m_groups.Insert (groupName, AudioComponentGroup (groupName));
}

void AudioEngineComponent::DestroyGroup (String groupName)
{
  m_groups.Erase (groupName);
}

void AudioEngineComponent::AddToGroup (String groupName, AudioComponent *component)
{
  m_groups[groupName].AddComponent (component);
  component->SetObjectVolume (m_groups[groupName].m_groupVolume);
  component->PauseAll (m_groups[groupName].m_groupPauseState);
}

void AudioEngineComponent::RemoveFromGroup (String groupName, AudioComponent *component)
{
  m_groups[groupName].RemoveComponent (component);
}

void AudioEngineComponent::RemoveFromAllGroups (AudioComponent *component)
{
  if (m_groups.Size() > 0)
  {
    for (auto tempGroups : m_groups)
    {
      m_groups[tempGroups.key].RemoveComponent (component);
      //tempGroups.second.RemoveComponent(component);
    }
  }
}


void AudioEngineComponent::SetGroupVolume (String groupName, float volume)
{
  m_groups[groupName].m_groupVolume = volume;

  if (m_groups.Size() > 0 &&
      m_groups.Find (groupName) != m_groups.end() &&
      m_groups[groupName].ComponentCount() > 0)
  {
    for (auto tempObjects : m_groups[groupName].m_components)
    {
      tempObjects.value->SetObjectVolume (volume);
    }
  }
}

void AudioEngineComponent::PauseGroup (String groupName, bool state)
{
  m_groups[groupName].m_groupPauseState = state;

  if (m_groups.Size() > 0 &&
      m_groups.Find (groupName) != m_groups.end() &&
      m_groups[groupName].ComponentCount() > 0)
  {
    for (auto tempObjects : m_groups[groupName].m_components)
    {
      tempObjects.value->PauseAll (state);
    }
  }
}

META_REGISTER_FUNCTION (AudioEngine)
{
  META_INHERIT (AudioEngineComponent, "Kepler", "Component");

  // META_HANDLE means that we are binding a pointer to this object, not the object itself
  META_HANDLE (AudioEngineComponent);

  META_ADD_METHOD(AudioEngineComponent, SetMasterPause);
  META_ADD_METHOD(AudioEngineComponent, SetMasterVolume);
  
  META_FINALIZE_PTR (AudioEngineComponent, MetaPtr);
}
