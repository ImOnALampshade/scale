// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  each thing in the game that is to make sound should have one of these.
//    Allows for sounds to be played and controlled from a spesific object.
//    One instance of this should be declared the listener.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "audio_component.h"
#include "../KeplerCore/game.h"

AudioComponent::AudioComponent (void) : m_instanceTable (Utilities::Hash::SuperFast), 
                                        m_previousTimelinePosition(Utilities::Hash::SuperFast)
{
  m_position.x = 0;
  m_position.y = 0;
  m_position.z = 0;

  m_engine = nullptr;
}

void AudioComponent::Initialize (const Json::Object &jsonData)
{
  m_engine = (AudioEngineComponent *) m_audioEngine;

  CoreEngine::Game->GetComponent<AudioEngineComponent>()->CreateGroup ("THISISAGROUP");

  m_componentID = CoreEngine::Game->GetComponent<AudioEngineComponent>()->GetNextFreeID();

  m_objectVolume = 1.0f;

  RegisterMessageProc ("MakeThisTheListener",     [&] (void *)           { MakeMeTheListener();                                                     });
  RegisterMessageProc ("PlayLongSound",           [&] (void *soundName) { PlayLongSound ( (reinterpret_cast<AudioMessage *> (soundName))->name);  });
  RegisterMessageProc ("PlayShortSound",          [&] (void *soundName) { PlayShortSound ( (reinterpret_cast<AudioMessage *> (soundName))->name);  });
  RegisterMessageProc ("PauseSingleAudio",        [&] (void *soundName) {
    Pause ( (reinterpret_cast<AudioMessage *> (soundName))->name,
            (reinterpret_cast<AudioMessage *> (soundName))->state);
  });
  RegisterMessageProc ("PauseAllAudio",           [&] (void *state)     { PauseAll ( (reinterpret_cast<AudioMessage *> (state))->state);     });
  RegisterMessageProc ("UpdateAudioPosition",     [&] (void *position)  {
    UpdatePosition ( (reinterpret_cast<Position *> (position))->x,
                     (reinterpret_cast<Position *> (position))->y,
                     (reinterpret_cast<Position *> (position))->z);
  });
  RegisterMessageProc ("SetSoundVolume",          [&] (void *soundName) {
    SetSoundVolume ( (reinterpret_cast<AudioMessage *> (soundName))->value,
                     (reinterpret_cast<AudioMessage *> (soundName))->name);
  });
  RegisterMessageProc ("SetObjectVolume",         [&] (void *value)     { SetObjectVolume ( (reinterpret_cast<AudioMessage *> (value))->value);     });
  RegisterMessageProc ("CreateAndJoinAudioGroup", [&] (void *groupName) { CreateAndJoinGroup ( (reinterpret_cast<AudioMessage *> (groupName))->name);  });
  RegisterMessageProc ("JoinAudioGroup",          [&] (void *groupName) { JoinGroup ( (reinterpret_cast<AudioMessage *> (groupName))->name);  });
  RegisterMessageProc ("LeaveAudioGroup",         [&] (void *groupName) { LeaveGroup ( (reinterpret_cast<AudioMessage *> (groupName))->name);  });
  RegisterMessageProc ("LeaveAllAudioGroups",     [&] (void *)           { LeaveAllGroups();                                                        });
  //RegisterMessageProc("KeyEvent", [&](void* input){ PlayLongSound("LeviathanSounds"); });
}

void AudioComponent::Free (void)
{
  //empty body
  StopAll();
}

void AudioComponent::PlayLongSound (String SoundName)
{
  if (m_instanceTable.Find (SoundName) == m_instanceTable.end())
  {
    FMOD::Studio::EventDescription *longSoundDescription = NULL;
    ErrorCheck (CoreEngine::Game->GetComponent<AudioEngineComponent>()->m_system->getEventByID (&CoreEngine::Game->GetComponent<AudioEngineComponent>()->m_idTable[SoundName], &longSoundDescription));

    FMOD::Studio::EventInstance *longSoundInstance = NULL;
    ErrorCheck (longSoundDescription->createInstance (&longSoundInstance));

    m_instanceTable.Insert (SoundName, longSoundInstance);
  }

  ErrorCheck (m_instanceTable[SoundName]->setVolume (m_objectVolume));

  ErrorCheck (m_instanceTable[SoundName]->start());
}

//play fire and forget sounds
void AudioComponent::PlayShortSound (String SoundName)
{
  FMOD::Studio::EventDescription *shortSoundDescription = NULL;
  ErrorCheck (CoreEngine::Game->GetComponent<AudioEngineComponent>()->m_system->getEventByID (&CoreEngine::Game->GetComponent<AudioEngineComponent>()->m_idTable[SoundName], &shortSoundDescription));

  FMOD::Studio::EventInstance *shortSoundInstance = NULL;
  ErrorCheck (shortSoundDescription->createInstance (&shortSoundInstance));

  ErrorCheck (shortSoundInstance->setVolume (m_objectVolume));

  ErrorCheck (shortSoundInstance->start());

  ErrorCheck (shortSoundInstance->release());
}

void AudioComponent::Pause (String SoundName, bool PauseState)
{
  if(m_instanceTable.Find(SoundName) != m_instanceTable.end())
  {
    ErrorCheck (m_instanceTable[SoundName]->setPaused (PauseState));
  }
}

void AudioComponent::PauseAll (bool PauseState)
{
  if(m_instanceTable.Size() != 0)
  {
    for (auto m_instanceTableIter : m_instanceTable)
    {
      ErrorCheck (m_instanceTableIter.value->setPaused (PauseState));
    }
  }
}

void AudioComponent::Stop(String SoundName)
{
  if(m_instanceTable.Find(SoundName) != m_instanceTable.end())
  {
    ErrorCheck(m_instanceTable[SoundName]->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT));
  }
}

void AudioComponent::StopAll()
{
  if (m_instanceTable.Size() != 0)
  {
    for (auto m_instanceTableIter : m_instanceTable)
    {
      ErrorCheck(m_instanceTableIter.value->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT));
    }
  }
}

void AudioComponent::StopButLetFinish(String SoundName)
{
  if(!m_instanceTable.Contains(SoundName))
  {
    return;
  }

  if(!m_previousTimelinePosition.Contains(SoundName))
  {
    int tempTime = 0;
    ErrorCheck(m_instanceTable[SoundName]->getTimelinePosition(&tempTime));
    m_previousTimelinePosition.Insert(SoundName, tempTime);

    CreateAndJoinGroup("EndingSounds");
    //JoinGroup("EndingSounds");
  }
  else
  {
    ErrorCheck(m_instanceTable[SoundName]->getTimelinePosition(&m_previousTimelinePosition[SoundName]));
  }
}

void AudioComponent::UpdatePosition (float x, float y, float z)
{
  m_position.x = x;
  m_position.y = y;
  m_position.z = z;

  if (m_instanceTable.Size() > 0)
  {
    //update all audio instances to here as well
    FMOD_3D_ATTRIBUTES attributes = { { 0 } };
    attributes.forward.z = 1.0f;
    attributes.up.y = 1.0f;

    attributes.position.x = x;
    attributes.position.y = y;
    attributes.position.z = z;

    for (auto instance : m_instanceTable)
    {
      ErrorCheck (instance.value->set3DAttributes (&attributes));
    }
  }

}

void AudioComponent::MakeMeTheListener (void)
{
  CoreEngine::Game->GetComponent<AudioEngineComponent>()->m_listenerObject = this;
}

void AudioComponent::SetSoundVolume (float volume, String SoundName)
{
  ErrorCheck (m_instanceTable[SoundName]->setVolume (volume));
}

void AudioComponent::SetObjectVolume (float volume)
{
  m_objectVolume = volume;

  if (m_instanceTable.Size() > 0)
  {
    for (auto instanceIter : m_instanceTable)
    {
      instanceIter.value->setVolume (m_objectVolume);
    }
  }
}

void AudioComponent::CheckLoopingSounds(void)
{
  if(!m_instanceTable.Size())
  {
    return;
  }

  int count = m_previousTimelinePosition.Size();
  int doneSoFar = 0;
  for( auto m_timelineIter : m_previousTimelinePosition)
  {
    int currentTime = 0;
  
    ErrorCheck(m_instanceTable[m_timelineIter.key]->getTimelinePosition(&currentTime));
  
    if(currentTime < m_timelineIter.value)
    {
      Stop(m_timelineIter.key);
      ++doneSoFar;
    }
    else
    {
      //StopButLetFinish(m_timelineIter.key);
      m_timelineIter.value = currentTime;
    }
  }

  if(doneSoFar == count)
  {
    m_previousTimelinePosition.Clear();
  }
}

void AudioComponent::Pitch(float pitch)
{
  m_objectPitch = pitch;

  if (m_instanceTable.Size() > 0)
  {
    for (auto instanceIter : m_instanceTable)
    {
      instanceIter.value->setPitch(m_objectPitch);
    }
  }
}

float AudioComponent::Pitch() const
{
  return m_objectPitch;
}

void AudioComponent::Volume(float volume)
{
  m_objectVolume = volume;

  if (m_instanceTable.Size() > 0)
  {
    for (auto instanceIter : m_instanceTable)
    {
      instanceIter.value->setVolume(m_objectVolume);
    }
  }
}

float AudioComponent::Volume() const
{
  return m_objectVolume;
}


int AudioComponent::GetID (void)
{
  return m_componentID;
}


void AudioComponent::CreateAndJoinGroup (String groupName)
{
  CoreEngine::Game->GetComponent<AudioEngineComponent>()->CreateGroup (groupName);
  JoinGroup (groupName);
}

void AudioComponent::JoinGroup (String groupName)
{
  CoreEngine::Game->GetComponent<AudioEngineComponent>()->AddToGroup (groupName, this);
}

void AudioComponent::LeaveGroup (String groupName)
{
  CoreEngine::Game->GetComponent<AudioEngineComponent>()->RemoveFromGroup (groupName, this);
}

void AudioComponent::LeaveAllGroups (void)
{
  CoreEngine::Game->GetComponent<AudioEngineComponent>()->RemoveFromAllGroups (this);
}

META_REGISTER_FUNCTION (Audio)
{
  META_INHERIT (AudioComponent, "Kepler", "Component");

  // META_HANDLE means that we are binding a pointer to this object, not the object itself
  META_HANDLE (AudioComponent);

  META_ADD_METHOD (AudioComponent, PlayLongSound);
  META_ADD_METHOD (AudioComponent, PlayShortSound);
  META_ADD_METHOD (AudioComponent, Pause);
  META_ADD_METHOD (AudioComponent, PauseAll);
  META_ADD_METHOD (AudioComponent, Stop);
  META_ADD_METHOD (AudioComponent, StopAll);
  META_ADD_METHOD (AudioComponent, StopButLetFinish);
  META_ADD_METHOD (AudioComponent, SetSoundVolume);
  META_ADD_METHOD (AudioComponent, SetObjectVolume);
  META_ADD_METHOD (AudioComponent, MakeMeTheListener);

  META_ADD_METHOD (AudioComponent, GetID);

  META_ADD_PROP(AudioComponent, Volume);
  META_ADD_PROP(AudioComponent, Pitch);

  META_ADD_METHOD (AudioComponent, CreateAndJoinGroup);
  META_ADD_METHOD (AudioComponent, JoinGroup);
  META_ADD_METHOD (AudioComponent, LeaveGroup);
  META_ADD_METHOD (AudioComponent, LeaveAllGroups);


  META_FINALIZE_PTR (AudioComponent, MetaPtr);
}