// -----------------------------------------------------------------------------
//  Author: Drew Jacobsen
//
//  Incharge of keeping tack of all sound id's and groups. Alows for some
//    overall controll of things like volume and pause state.
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#pragma once

#include <fstream>
#include <utility>
#include "base_audio.h"
#include "audio_group.h"
#include "audio_component.h"
#include <cstddef>

class AudioEngineComponent : public BaseAudio
{
  friend class AudioComponent;
  friend class AudioComponentGroup;

public:
  AudioEngineComponent (void);

  void Initialize (const Json::Object &jsonData);
  void Free();

  void Initialize (void);

  void Update (void);
  void Shutdown (void);

  void SetListenerPosittion (Position);
  void SetListenerPosittion (float x, float y, float z);

  void SetMasterVolume (float);
  void SetMasterPause (bool);

  int GetNextFreeID (void);

  void CreateGroup (String);
  void DestroyGroup (String);
  void AddToGroup (String, AudioComponent *);
  void RemoveFromGroup (String, AudioComponent *);
  void RemoveFromAllGroups (AudioComponent *);

  void SetGroupVolume (String, float);
  void PauseGroup (String, bool state);

  COMPONENT_META

private:
  FMOD::Studio::System *m_system;
  FMOD_RESULT m_result;

  //audioComponent sets the m_listenerObject with the MakeMeTheListener() fuction
  //audioEngine updates the listeners position bassed on the m_listenerObject's position
  AudioComponent *m_listenerObject;

  //id table is the id of each sound, not an instance of it
  //these id's are used to create instances of sounds by the components
  HashTable<String, FMOD::Studio::ID> m_idTable;

  //each component will get an id from here (incremented at that time)
  //so each component has a unique id for the grouping system to use
  int m_nextFreeID;

  //this is just a list of groups that exist
  HashTable<String, AudioComponentGroup> m_groups;

};