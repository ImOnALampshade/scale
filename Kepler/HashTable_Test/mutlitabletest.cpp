//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <catch.hpp>
#include "../HashTable/multihashtable.h"
#include "../HashFunctions/hashfunctions.h"

TEST_CASE ("MutliTable: Basic insertion")
{
  MultiHashTable<String, int> table (Utilities::Hash::SuperFast);

  SECTION ("Insert single items")
  {
    table.Insert ("one", 1);
    table.Insert ("two", 2);
    table.Insert ("three", 3);

    CHECK (table.Contains ("one") == 1);
    CHECK (table.Contains ("two") == 1);
    CHECK (table.Contains ("three") == 1);
    CHECK (table.Contains ("four") == 0);
  }

  SECTION ("Insert multiple items")
  {
    table.Insert ("thing", 1);
    table.Insert ("thing", 2);
    table.Insert ("other", 5);
    table.Insert ("other", 27);

    CHECK (table.Contains ("thing") == 2);
    CHECK (table.Contains ("other") == 2);
    CHECK (table.Contains ("not_there") == 0);
  }
}

TEST_CASE ("MutliTable: Remove items")
{
  MultiHashTable<String, int> table (Utilities::Hash::SuperFast);


  SECTION ("Remove single item")
  {
    table.Insert ("one", 1);
    table.Insert ("two", 1);
    table.Insert ("three", 1);

    CHECK (table.Erase ("one") == 1);
    CHECK (table.Contains ("one") == 0);
  }

  SECTION ("Remove multiple items")
  {
    table.Insert ("thing", 1);
    table.Insert ("thing", 2);
    table.Insert ("other", 5);
    table.Insert ("other", 27);

    CHECK (table.Erase ("thing") == 2);
    CHECK (table.Contains ("thing") == 0);
    CHECK (table.Contains ("other") == 2);
  }
}

TEST_CASE ("MultiTable: Find items")
{

  SECTION ("No growth")
  {
    MultiHashTable<String, int> table (Utilities::Hash::Zero);

    table.Insert ("abcd", 1);
    table.Insert ("abcd", 2);
    table.Insert ("abcd", 3);
    table.Insert ("asdf", 7);
    table.Insert ("abcd", 4);
    table.Insert ("abcd", 5);

    unsigned count = 0;
    unsigned sum = 0;

    for (auto it = table.Find ("abcd"); it != table.end() && it->key == "abcd"; ++it)
    {
      CHECK (it->key == "abcd");
      ++count;
      sum += it->value;
    }

    CHECK (count == 5);
    CHECK (sum == (1 + 2 + 3 + 4 + 5));
  }
  SECTION ("Growth with real hash")
  {
    MultiHashTable<String, int> table (Utilities::Hash::Simple);

    table.Insert ("abcd", 1);
    table.Insert ("abcd", 2);
    table.Insert ("abcd", 3);
    table.Insert ("dcba", 7);
    table.Insert ("dcba", 4000);
    table.Insert ("dcba", 4000);
    table.Insert ("dcba", 4000);
    table.Insert ("dcba", 4000);
    table.Insert ("dcba", 4000);
    table.Insert ("dcba", 4000);
    table.Insert ("abcd", 4);
    table.Insert ("abcd", 5);
    table.Insert ("grbg", 93);
    table.Insert ("grbg", 42);
    table.Insert ("grbg", 97);
    table.Insert ("grbg", 93);
    table.Insert ("grbg", 93);
    table.Insert ("grbg", 93);
    table.Insert ("grbg", 93);
    table.Insert ("grbg", 93);
    table.Insert ("grbg", 93);

    unsigned count = 0;
    unsigned sum = 0;

    for (auto it = table.Find ("abcd"); it != table.end() && it->key == "abcd"; ++it)
    {
      CHECK (it->key == "abcd");
      ++count;
      sum += it->value;
    }

    CHECK (count == 5);
    CHECK (sum == (1 + 2 + 3 + 4 + 5));
  }
}