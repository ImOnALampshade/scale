//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#include <catch.hpp>
#include "../HashTable/hashtable.h"
#include "../HashFunctions/hashfunctions.h"

TEST_CASE ("Basic insertion")
{
  HashTable<String, int> table (Utilities::Hash::FNV1);

  SECTION ("Inserting a number")
  {
    table.Insert ("one", 1);

    CHECK (table.Contains ("one"));
    CHECK (!table.Contains ("two"));
  }

  SECTION ("Inserting several numbers")
  {
    table.Insert ("zero",  0);
    table.Insert ("one",   1);
    table.Insert ("two",   2);
    table.Insert ("three", 3);
    table.Insert ("four",  4);
    table.Insert ("five",  5);
    table.Insert ("six",   6);
    table.Insert ("seven", 7);
    table.Insert ("eight", 8);
    table.Insert ("nine",  9);

    CHECK (table.Contains ("four"));
    CHECK (!table.Contains ("eleven"));
  }
}

TEST_CASE ("Collisions")
{
  HashTable<String, int> table (Utilities::Hash::Zero);

  SECTION ("Inserting a number")
  {
    table.Insert ("one", 1);

    CHECK (table.Contains ("one"));
    CHECK (!table.Contains ("two"));
  }

  SECTION ("Inserting several numbers")
  {
    table.Insert ("zero", 0);
    table.Insert ("one", 1);
    table.Insert ("two", 2);
    table.Insert ("three", 3);
    table.Insert ("four", 4);
    table.Insert ("five", 5);
    table.Insert ("six", 6);
    table.Insert ("seven", 7);
    table.Insert ("eight", 8);
    table.Insert ("nine", 9);

    CHECK (table.Contains ("four"));
    CHECK (!table.Contains ("eleven"));
  }
}

TEST_CASE ("Operator[]")
{
  HashTable<String, int> table1 (Utilities::Hash::FNV1);
  HashTable<String, int> table2 (Utilities::Hash::Zero);

  SECTION ("Low-collision")
  {
    table1["one"] = 5;
    table1["two"] = 37;
    table1["three"] = 19;
    table1["four"] = 27;

    CHECK (table1["one"] == 5);
    CHECK (table1["two"] == 37);
    CHECK (table1["three"] == 19);
    CHECK (table1["four"] == 27);
  }

  SECTION ("Collisions")
  {
    table2["one"] = 5;
    table2["two"] = 37;
    table2["three"] = 19;
    table2["four"] = 27;

    CHECK (table2["one"] == 5);
    CHECK (table2["two"] == 37);
    CHECK (table2["three"] == 19);
    CHECK (table2["four"] == 27);
  }

  SECTION ("Access")
  {
    table2["two"] = 27;

    for (unsigned i = 0; i < 5; ++i)
      table2["one"]++;

    table2["three"] = 91;

    CHECK (table2["one"] == 5);
    CHECK (table2["two"] == 27);
    CHECK (table2["three"] == 91);

    CHECK (table2.Size() == 3);

    CHECK (!table2.IsEmpty());
    table2.Clear();
    CHECK (table2.IsEmpty());
  }
}

#include <random>
#include <iostream>

using namespace std;

String RandomString (unsigned len)
{
  static unsigned inc = 0;
  mt19937 random (inc++);

  String result;

  for (unsigned i = 0; i < len; ++i)
  {
    char c = (random() % 26) + 'a';
    result += c;
  }

  return result;
}

TEST_CASE ("Iteration")
{
  HashTable<String, int> table (Utilities::Hash::Zero);

  table.Insert ("Hello", 5);

  for (unsigned i = 0; i < 50; ++i)
  {
    String key = RandomString (10);
    table[key] = i;
  }

  SECTION ("Looping")
  {
    unsigned total = 0;

    for (auto &data : table)
    {
      ++total;
      cout << total << ": " << data.key << " => " << data.value << endl;
    }

    CHECK (total == table.Size());
  }

  SECTION ("Finding")
  {
    auto it = table.Find ("Hello");

    REQUIRE (it != table.end());

    CHECK (it->key == "Hello");
    CHECK (it->value == 5);
  }
}