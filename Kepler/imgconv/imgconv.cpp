// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Converts an image file from one file type to another (As long as OpenIL
//  supports both file types)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include <IL/il.h>
#include <stdlib.h>

int main (int argc, const char **argv)
{
  ILuint img;
  ILboolean itsallok;

  const char *infile  = argv[1];
  const char *outfile = argv[2];

  if (argc < 3)
  {
    printf ("usage: imgconv <infile> <outfile>\n");
    return -1;
  }

  ilInit();
  ilGenImages (1, &img);
  ilBindImage (img);

  itsallok = ilLoadImage (infile);

  if (itsallok == IL_FALSE)
  {
    printf ("Failed to load image file %s\n", infile);
    return -1;
  }

  itsallok  = ilSaveImage (outfile);

  if (itsallok == IL_FALSE)
  {
    printf ("Failed to save image file %s\n", outfile);
    return -1;
  }

  return 0;
}
