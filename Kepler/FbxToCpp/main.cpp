// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Converts an FBX file to a C++ header file
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "../kmo/scene.h"

using namespace glm;

int main (int argc, char **argv)
{
  Scene s;
  s.LoadFbx ("sphere.fbx");

  FILE *file = fopen ("output.cpp", "wt");

  if (!file)
    return -1;

  fprintf (file, "unsigned VertexCount   = %u;\n", s.Vertices.size());
  fprintf (file, "unsigned TriangleCount = %u;\n", s.Triangles.size());

  fprintf (file, "float VertexData[] =\n{\n");

  for (Scene::Vertex &vertex : s.Vertices)
  {
    vec3 c = vertex.Coordinate;
    vec2 t = vertex.Texcoord;
    vec3 n = vertex.Normal;

    fprintf (file, "  %g, %g, %g, %g, %g, %g, %g, %g,\n",
             c.x, c.y, c.z,
             t.x, t.y,
             n.x, n.y, n.z);
  }

  fprintf (file, "};\n\n");

  fprintf (file, "unsigned Indicies[] = \n{\n");

  for (Scene::Triangle &tri : s.Triangles)
  {
    int i1 = tri.Vertices[0];
    int i2 = tri.Vertices[1];
    int i3 = tri.Vertices[2];

    fprintf (file, "  %i, %i, %i,\n",
             i1, i2, i3);
  }

  fprintf (file, "}\n\n");
  fclose (file);
}
