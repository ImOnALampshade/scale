// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef AWESOMIUM_SPACE_COMPONENT_H
#define AWESOMIUM_SPACE_COMPONENT_H

#include "../OpenGLHelpers/glinclude.h"
#include "../OpenGLHelpers/shader.h"
#include "../GraphicsComponents/basicrenderedspacecomponent.h"
#include "awesomiumoglsurface.h"
#include "keplercompositebinding.h"

class AwesomiumSpaceComponent : public BasicRenderedSpaceComponent
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();
  virtual void Render();

  void URL (String url);

  COMPONENT_META

private:
  Awesomium::WebView *m_view;
  OpenGL::Shader *m_shader;
  KeplerMethodHandler m_methodHandler;
};

#endif
