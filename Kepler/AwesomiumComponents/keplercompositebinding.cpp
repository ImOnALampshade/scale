// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Awesomium bindings for the composites
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "../KeplerCore/compositefactory.h"
#include "../KeplerCore/composite.h"
#include "../KeplerCore/messagemeta.h"
#include "awesomiumstringconv.h"
#include "keplercompositebinding.h"

using namespace Awesomium;
using namespace std;

// -----------------------------------------------------------------------------

static Serializer::Value ConvertToJsonVal (const JSValue &val)
{
  if (val.IsInteger())
    return Json::Number (val.ToInteger());

  if (val.IsDouble())
    return Json::Number (val.ToDouble());

  if (val.IsString())
    return Json::String (AwesomiumToKepler (val.ToString()));

  if (val.IsNull())
    return Json::Null();

  if (val.IsBoolean())
    return Json::Boolean (val.ToBoolean());

  if (val.IsUndefined())
    return Json::Null();

  if (val.IsArray())
  {
    const JSArray &arr = val.ToArray();
    Json::List list (arr.size());

    for (unsigned i = 0; i < arr.size(); ++i)
      list[i] = ConvertToJsonVal (arr[i]);

    return list;
  }

  if (val.IsObject())
  {
    const JSObject &obj = val.ToObject();
    Json::Object map (Utilities::Hash::SuperFast);

    JSArray properties = obj.GetPropertyNames();

    for (unsigned i = 0; i < properties.size(); ++i)
    {
      WebString name = properties[i].ToString();
      String key = AwesomiumToKepler (name);

      Serializer::Value val = ConvertToJsonVal (obj.GetProperty (name));

      map.Insert (key, val);
    }

    return map;
  }

  return Json::Value();
}

static JSValue ConvertFromJsonVal (const Json::Value &val)
{
  if (val.IsType<Json::Number>())
    return JSValue (val.as<Json::Number>());

  if (val.IsType<Json::String>())
    return JSValue (KeplerToAwesomium (val.as<Json::String>()));

  if (val.IsType<Json::Boolean>())
    return JSValue (val.as<Json::Boolean>());

  if (val.IsType<Json::Null>())
    return JSValue::Null();

  if (val.IsType<Json::Object>())
  {
    JSObject obj;
    val.as<Json::Object>().ForEach ([&obj] (const Json::Object::Payload &data) {

      WebString key = KeplerToAwesomium (data.key);
      obj.SetProperty (key, ConvertFromJsonVal (data.value));
    });

    return obj;
  }

  if (val.IsType<Json::List>())
  {
    JSArray arr;

    for (const Json::Value &val : val.as<Json::List>())
      arr.Push (ConvertFromJsonVal (val));

    return arr;
  }

  return JSValue::Undefined();
}

static DynamicVariant ConvertFromJSValue (const JSValue &val)
{
  if (val.IsInteger())
    return val.ToInteger();

  if (val.IsDouble())
    return val.ToDouble();

  if (val.IsString())
    return AwesomiumToKepler (val.ToString());

  if (val.IsArray())
  {
    Json::List list;
    const JSArray &arr = val.ToArray();

    for (unsigned i = 0; i < arr.size(); ++i)
      list.emplace_back (ConvertToJsonVal (arr[i]));

    return list;
  }

  if (val.IsObject())
  {
    Json::Object map (Utilities::Hash::SuperFast);
    const JSObject &obj = val.ToObject();

    JSArray properties = obj.GetPropertyNames();

    for (unsigned i = 0; i < properties.size(); ++i)
    {
      WebString name = properties[i].ToString();
      String key = AwesomiumToKepler (name);

      Serializer::Value val = ConvertToJsonVal (obj.GetProperty (name));

      map.Insert (key, val);
    }

    return map;
  }

  return DynamicVariant();
}

static JSValue ConvertToJSValue (const DynamicVariant &var)
{
  if (!var.IsConstructed())
    return JSValue::Null();

  if (var.IsType<Json::Number>())
    return JSValue (var.as<Json::Number>());

  if (var.IsType<Json::String>())
    return JSValue (KeplerToAwesomium (var));

  if (var.IsType<Json::Boolean>())
    return JSValue (var.as<Json::Boolean>());

  if (var.IsType<Json::List>())
  {
    JSArray arr;

    for (const DynamicVariant &item : var.as<Json::List>())
      arr.Push (ConvertToJSValue (item));

    return arr;
  }

  if (var.IsType<Json::Object>())
  {
    JSObject obj;

    var.as<Json::Object>().ForEach ([&obj] (const Json::Object::Payload &data) {
      obj.SetProperty (KeplerToAwesomium (data.key), ConvertToJSValue (data.value));
    });

    return obj;
  }

  return JSValue::Undefined();
}

// -----------------------------------------------------------------------------

KeplerMethodHandler::KeplerMethodHandler () :
  m_methods (Utilities::Hash::SuperFast),
  m_isLoaded (false)
{
  m_methods["dispatchOnGame"] = [] (const JSArray &args, ::Handle<Component> component, WebView *) {
    Tuple kepArgs (args.size() - 1);

    for (unsigned i = 1; i < args.size(); ++i)
      kepArgs[i - 1] = ConvertFromJSValue (args[i]);

    String name = AwesomiumToKepler (args[0].ToString());

    MessageMeta *meta = MessageMeta::Get (name);

    if (meta)
      meta->Dispatch (CoreEngine::Game, kepArgs);

    else
      CoreEngine::Game->DispatchTuple (name, kepArgs);
  };

  m_methods["dispatchOnSpace"] = [] (const JSArray &args, ::Handle<Component> component, WebView *) {
    Tuple kepArgs (args.size() - 1);

    for (unsigned i = 1; i < args.size(); ++i)
      kepArgs[i - 1] = ConvertFromJSValue (args[i]);

    String name = AwesomiumToKepler (args[0].ToString());

    MessageMeta *meta = MessageMeta::Get (name);

    if (meta)
      meta->Dispatch (component->Owner(), kepArgs);

    else
      component->Owner()->DispatchTuple (name, kepArgs);
  };

  m_methods["registerForMessage"] = [] (const JSArray &args, ::Handle<Component> component, WebView *caller) {

    String name = AwesomiumToKepler (args[0].ToString());
    WebString invokeName = args[1].ToString();

    component->Owner()->RegisterObserver (name, std::function<void (const Tuple &) > (
    [caller, invokeName] (const Tuple & args) {

      // Retrieve the global 'window' object from the page
      JSValue window = caller->ExecuteJavascriptWithResult (WSLit ("window"), WSLit (""));

      JSArray awesomiumArgs;

      for (const DynamicVariant &var : args)
        awesomiumArgs.Push (ConvertToJSValue (var));

      window.ToObject().Invoke (invokeName, awesomiumArgs);
    }), nullptr, Composite::OBSERVER_PASS_TUPLE);
  };

  m_methods["log"] = [] (const JSArray &args, ::Handle<Component> component, WebView *caller) {
    String msg = AwesomiumToKepler (args[0].ToString()) + "\n";

    LOG (msg);
    cout << msg;
  };

  m_methods["doneLoading"] = [this] (const JSArray &args, ::Handle<Component> component, WebView *caller) {
    m_isLoaded = true;
  };
}

void KeplerMethodHandler::Initialize (Awesomium::WebView *view, Handle<Component> component)
{
  m_component = component;

  JSValue keplerVal = view->CreateGlobalJavascriptObject (WSLit ("kepler"));
  BREAK_IF (!keplerVal.IsObject(), "Wasn't an object");

  JSObject &kepler = keplerVal.ToObject();

  kepler.SetCustomMethod (WSLit ("dispatchOnGame"), false);
  kepler.SetCustomMethod (WSLit ("dispatchOnSpace"), false);
  kepler.SetCustomMethod (WSLit ("registerForMessage"), false);
  kepler.SetCustomMethod (WSLit ("log"), false);
  kepler.SetCustomMethod (WSLit ("doneLoading"), false);
}

void KeplerMethodHandler::OnMethodCall (Awesomium::WebView *caller,
                                        unsigned int remote_object_id,
                                        const Awesomium::WebString &method_name,
                                        const Awesomium::JSArray &args)
{
  m_methods.CallOnValue (AwesomiumToKepler (method_name),
  [this, &args, caller] (std::function<void (const JSArray &, Handle<Component>, WebView *) > fn) {
    fn (args, m_component, caller);
  });
}

// -----------------------------------------------------------------------------

void KeplerMethodHandler::IsLoaded (bool loaded) { m_isLoaded = loaded; }
bool KeplerMethodHandler::IsLoaded() const { return m_isLoaded; }

// -----------------------------------------------------------------------------
