// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "load_factories.h"
#include "../KeplerCore/compositefactory.h"

#include "awesomiummanagercomponent.h"
#include "awesomiumspacecomponent.h"

void AwesomiumComponents::LoadMeta()
{
  AwesomiumManagerComponent::CreateMeta();
  AwesomiumSpaceComponent::CreateMeta();
}

void AwesomiumComponents::LoadFactories()
{
  CREATE_FACTORY (AwesomiumManager);
  CREATE_FACTORY (AwesomiumSpace);
}
