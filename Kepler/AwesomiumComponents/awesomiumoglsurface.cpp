// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "../Logger/logger.h"
#include "awesomiumoglsurface.h"

// -----------------------------------------------------------------------------

AwesomeiumGLTextureSurface::AwesomeiumGLTextureSurface (unsigned width, unsigned height) :
  m_xOffset (0),
  m_yOffset (0),
  m_width (width),
  m_height (height)
{
  glGenTextures (1, &m_texture);
  glBindTexture (GL_TEXTURE_2D, m_texture);
  glTexStorage2D (GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
}

AwesomeiumGLTextureSurface::~AwesomeiumGLTextureSurface()
{
  glDeleteTextures (1, &m_texture);
}

// -----------------------------------------------------------------------------

void AwesomeiumGLTextureSurface::Paint (unsigned char *data, int bytesPerRow, const Awesomium::Rect &srcRect, const Awesomium::Rect &destRect)
{
  unsigned char *dataStart = data + ( (bytesPerRow * srcRect.y) + (4 * srcRect.x));

  auto myData = std::make_unique<unsigned char[]> (destRect.width * destRect.height * 4);

  for (unsigned y = 0; y < destRect.height; ++y)
  {
    memcpy (myData.get() + 4 * destRect.width * y, dataStart, 4 * destRect.width);
    dataStart += bytesPerRow;
  }

  glBindTexture (GL_TEXTURE_2D, m_texture);
  glTexSubImage2D (GL_TEXTURE_2D, 0,
                   m_xOffset + destRect.x, m_yOffset + destRect.y,
                   srcRect.width, srcRect.height,
                   GL_BGRA, GL_UNSIGNED_BYTE,
                   myData.get());
}

void AwesomeiumGLTextureSurface::Scroll (int dx, int dy, const Awesomium::Rect &clipRect)
{
  BREAK ("Don't have scrolling webpages!");
  //BREAK_IF (clipRect.width != m_width, "Wrong clip rect");
  //BREAK_IF (clipRect.height != m_height, "Wrong clip rect");
}

// -----------------------------------------------------------------------------

void AwesomeiumGLTextureSurface::BindTexture()
{
  glBindTexture (GL_TEXTURE_2D, m_texture);
}

// -----------------------------------------------------------------------------

Awesomium::Surface *AwesomeiumGLTextureSurface::Factory::CreateSurface (Awesomium::WebView *view, int width, int height)
{
  return new AwesomeiumGLTextureSurface (width, height);
}

void AwesomeiumGLTextureSurface::Factory::DestroySurface (Awesomium::Surface *surface)
{
  delete static_cast<AwesomeiumGLTextureSurface *> (surface);
}

// -----------------------------------------------------------------------------
