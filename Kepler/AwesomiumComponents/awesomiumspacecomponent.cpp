// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "awesomiumspacecomponent.h"
#include "awesomiumstringconv.h"
#include "keplercompositebinding.h"
#include "../GraphicsComponents/windowcomponent.h"
#include "../KeplerCore/game.h"
#include "../GraphicsComponents/renderedgamecomponent.h"
#include "../GraphicsComponents/shaderloadercomponent.h"
#include <thread>

using namespace std;

// -----------------------------------------------------------------------------

static Awesomium::WebKeyboardEvent CreateKeyEvent (int key, int action, int mods)
{
  Awesomium::WebKeyboardEvent e;

  switch (action)
  {
  case GLFW_REPEAT:
    e.modifiers |= Awesomium::WebKeyboardEvent::Modifiers::kModIsAutorepeat;

  // Fallthrough to set that it was a key down

  case GLFW_PRESS:
    e.type = Awesomium::WebKeyboardEvent::kTypeKeyDown;
    break;

  case GLFW_RELEASE:
    e.type = Awesomium::WebKeyboardEvent::kTypeKeyUp;
    break;
  }

  if (mods | GLFW_MOD_ALT)
    e.modifiers |= Awesomium::WebKeyboardEvent::Modifiers::kModAltKey;

  if (mods | GLFW_MOD_CONTROL)
    e.modifiers |= Awesomium::WebKeyboardEvent::Modifiers::kModControlKey;

  if (mods | GLFW_MOD_SHIFT)
    e.modifiers |= Awesomium::WebKeyboardEvent::Modifiers::kModShiftKey;

  if (mods | GLFW_MOD_SUPER)
    e.modifiers |= Awesomium::WebKeyboardEvent::Modifiers::kModMetaKey;

  e.virtual_key_code = key;

  char *id = e.key_identifier;
  Awesomium::GetKeyIdentifierFromVirtualKeyCode (key, &id);

  e.is_system_key = false;

  return e;
}

static Awesomium::WebKeyboardEvent CreateKeyEvent (unsigned c)
{
  Awesomium::WebKeyboardEvent e;

  e.type = Awesomium::WebKeyboardEvent::kTypeChar;

  if (isupper (c))
    e.modifiers |= Awesomium::WebKeyboardEvent::kModShiftKey;


  switch (c)
  {
  case GLFW_KEY_BACKSPACE:
    e.virtual_key_code = Awesomium::KeyCodes::AK_BACK;
    break;

  default:
    e.virtual_key_code = c;
    break;
  }

  char *id = e.key_identifier;
  Awesomium::GetKeyIdentifierFromVirtualKeyCode (c, &id);

  memcpy (e.text, &c, sizeof e.text);
  memcpy (e.unmodified_text, &c, sizeof e.text);

  e.is_system_key = false;

  return e;
}

// -----------------------------------------------------------------------------

void AwesomiumSpaceComponent::Initialize (const Json::Object &jsonData)
{
  glm::ivec2 size = CoreEngine::Game->GetComponent<WindowComponent>()->Size();

  m_view = Awesomium::WebCore::instance()->CreateWebView (size.x, size.y);

  auto ShaderLoader = Owner()->GetComponent<ShaderLoaderComponent>();
  BREAK_IF (!ShaderLoader.IsValid(), "No shader loader in awesomium space");
  m_shader = ShaderLoader->GetProgram ("awesomium");

  BREAK_IF (!m_shader, "No awesomium shader!");

  glUseProgram (m_shader->GetProgram());
  m_shader->SetUniform ("ui", 0);
  m_shader->SetUniform ("size", size);

  m_view->set_js_method_handler (&m_methodHandler);
  m_view->SetTransparent (true);
  m_view->Focus();

  String path = jsonData.Locate ("URL", String());
  m_methodHandler.Initialize (m_view, Handle());
  URL (path);

  BasicRenderedSpaceComponent::Initialize (jsonData);

  CoreEngine::Game->RegisterObserver ("MouseMove", std::function<void (float, float) > (
  [this] (float x, float y) {
    m_view->InjectMouseMove (int (x), int (y));
  }), Owner());

  CoreEngine::Game->RegisterObserver ("MouseDown", std::function<void (int, int) > (
  [this] (int button, int mods) {
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
      m_view->InjectMouseDown (Awesomium::MouseButton::kMouseButton_Left);
      break;

    case GLFW_MOUSE_BUTTON_RIGHT:
      m_view->InjectMouseDown (Awesomium::MouseButton::kMouseButton_Right);
      break;

    case GLFW_MOUSE_BUTTON_MIDDLE:
      m_view->InjectMouseDown (Awesomium::MouseButton::kMouseButton_Middle);
      break;
    }
  }), Owner());

  CoreEngine::Game->RegisterObserver ("MouseUp", std::function<void (int, int) > (
  [this] (int button, int modes) {
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
      m_view->InjectMouseUp (Awesomium::MouseButton::kMouseButton_Left);
      break;

    case GLFW_MOUSE_BUTTON_RIGHT:
      m_view->InjectMouseUp (Awesomium::MouseButton::kMouseButton_Right);
      break;

    case GLFW_MOUSE_BUTTON_MIDDLE:
      m_view->InjectMouseUp (Awesomium::MouseButton::kMouseButton_Middle);
      break;
    }
  }), Owner());

  CoreEngine::Game->RegisterObserver ("MouseScroll", std::function<void (float, float) > (
  [this] (float x, float y) {
    m_view->InjectMouseWheel (int (x), int (y));
  }), Owner());

  CoreEngine::Game->RegisterObserver ("KeyEvent", std::function<void (int, int, int) > (
  [this] (int key, int action, int mods) {
    //Awesomium::WebKeyboardEvent e = CreateKeyEvent (key, action, mods);
    //m_view->InjectKeyboardEvent (e);
  }), Owner());

  CoreEngine::Game->RegisterObserver ("KeyTyped", std::function<void (unsigned) > (
  [this] (unsigned c) {
    //Awesomium::WebKeyboardEvent e = CreateKeyEvent (c);
    //m_view->InjectKeyboardEvent (e);
  }), Owner());

  CoreEngine::Game->RegisterObserver ("WindowFocus", std::function<void() > (
  [this]() {
    m_view->Focus();
  }), Owner());

  CoreEngine::Game->RegisterObserver ("WindowUnfocus", std::function<void() > (
  [this]() {
    m_view->Unfocus();
  }), Owner());

  CoreEngine::Game->RegisterObserver ("WindowResize", std::function<void (unsigned, unsigned) > (
  [this] (unsigned w, unsigned h) {
    m_view->Resize (w, h);
  }), Owner());
}

void AwesomiumSpaceComponent::Free()
{
  m_view->Destroy();

  BasicRenderedSpaceComponent::Free();
}

// -----------------------------------------------------------------------------

void AwesomiumSpaceComponent::Render()
{
  // Get the surface from the webview and bind the texture
  AwesomeiumGLTextureSurface *surface = static_cast<AwesomeiumGLTextureSurface *> (m_view->surface());

  surface->BindTexture();

  // Bind the shader
  glUseProgram (m_shader->GetProgram());

  // Render the quad
  auto RenderedGame = CoreEngine::Game->GetComponent<RenderedGameComponent>();

  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  RenderedGame->BindQuad();
  RenderedGame->DrawQuad();

  glDisable (GL_BLEND);
}

// -----------------------------------------------------------------------------

void AwesomiumSpaceComponent::URL (String url)
{
  static const char *workingMacro = "$(working)";
  static size_t macroLen = strlen (workingMacro);

  size_t i = url.Find (workingMacro, 0);

  if (i != String::npos)
    url.Replace (i, macroLen, _getcwd (NULL, 0));

  m_methodHandler.IsLoaded (false);
  m_view->LoadURL (Awesomium::WebURL (KeplerToAwesomium (url)));
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (AwesomiumSpace)
{
  META_INHERIT (AwesomiumSpaceComponent, "Kepler", "Component");

  META_ADD_PROP_WRITEONLY (AwesomiumSpaceComponent, URL);

  META_FINALIZE_PTR (AwesomiumSpaceComponent, MetaPtr);
}
