// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Awesomium string converion (UTF8 to UTF16)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "awesomiumstringconv.h"

using namespace std;

String AwesomiumToKepler (const Awesomium::WebString &str)
{
  unsigned len = str.ToUTF8 (nullptr, 0);
  String out (len);

  // This const cast is safe (String was just created, no one else is pointing to
  // it. Also, the length is going to be correct because it was just made with
  // that length
  str.ToUTF8 (const_cast<char *> (out.c_str()), len);

  return out;
}

Awesomium::WebString KeplerToAwesomium (const String &str)
{
  return Awesomium::WebString::CreateFromUTF8 (str.c_str(), str.Length());
}
