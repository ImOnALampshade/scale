// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef AWESOMIUM_OPENGL_SURFACE_H
#define AWESOMIUM_OPENGL_SURFACE_H

#include "../OpenGLHelpers/glinclude.h"

// -----------------------------------------------------------------------------

class AwesomeiumGLTextureSurface : public Awesomium::Surface
{
public:
  AwesomeiumGLTextureSurface (unsigned width, unsigned height);
  AwesomeiumGLTextureSurface (const AwesomeiumGLTextureSurface &) = delete;
  AwesomeiumGLTextureSurface &operator= (const AwesomeiumGLTextureSurface &) = delete;
  ~AwesomeiumGLTextureSurface();

  virtual void  Paint (unsigned char *data, int bytesPerRow, const Awesomium::Rect &srcRect, const Awesomium::Rect &destRect);
  virtual void  Scroll (int dx, int dy, const Awesomium::Rect &clipRect);

  void BindTexture();

  class Factory : public Awesomium::SurfaceFactory
  {
  public:
    virtual Awesomium::Surface  *CreateSurface (Awesomium::WebView *view, int width, int height);
    virtual void  DestroySurface (Awesomium::Surface *surface);
  };

private:
  int m_xOffset;
  int m_yOffset;
  unsigned m_width;
  unsigned m_height;
  GLuint m_texture;
};

// -----------------------------------------------------------------------------

#endif
