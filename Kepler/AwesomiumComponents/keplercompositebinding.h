// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Awesomium bindings for the composites
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef KEPLER_COMPOSITE_BINDING_H
#define KEPLER_COMPOSITE_BINDING_H

#include "../KeplerCore/component.h"
#include <Awesomium/WebView.h>

class KeplerMethodHandler : public Awesomium::JSMethodHandler
{
public:
  KeplerMethodHandler ();

  void Initialize (Awesomium::WebView *view, ::Handle<Component> component);

  virtual void OnMethodCall (Awesomium::WebView *caller,
                             unsigned int remote_object_id,
                             const Awesomium::WebString &method_name,
                             const Awesomium::JSArray &args);

  virtual Awesomium::JSValue OnMethodCallWithReturnValue (Awesomium::WebView *caller,
                                                          unsigned int remote_object_id,
                                                          const Awesomium::WebString &method_name,
                                                          const Awesomium::JSArray &args)
  {
    return Awesomium::JSValue::Undefined();
  }

  void IsLoaded (bool loaded);
  bool IsLoaded() const;

private:
  HashTable<String, std::function<void (const Awesomium::JSArray &, ::Handle<Component>, Awesomium::WebView *) >> m_methods;
  ::Handle<Component> m_component;
  bool m_isLoaded;
};

#endif
