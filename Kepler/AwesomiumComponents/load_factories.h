// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Function for loading factories into the engine
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef AWESOMIUM_COMPONENTS_LOAD_FACTORIES_H
#define AWESOMIUM_COMPONENTS_LOAD_FACTORIES_H

namespace AwesomiumComponents
{
  void LoadMeta();
  void LoadFactories();
}

#endif
