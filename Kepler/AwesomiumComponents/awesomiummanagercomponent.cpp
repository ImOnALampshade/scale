// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for managing awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#include "stdafx.h"
#include "awesomiummanagercomponent.h"
#include "awesomiumstringconv.h"
#include "awesomiumoglsurface.h"

using namespace std;

// -----------------------------------------------------------------------------

void AwesomiumManagerComponent::Initialize (const Json::Object &jsonData)
{
  Awesomium::WebConfig config;
  Awesomium::WebPreferences prefs;

  // Always set log level to verbose, default port is 8000
  config.log_level = Awesomium::kLogLevel_Verbose;
  config.remote_debugging_port = 8000;

  // Get the user agent from the json if there is one
  jsonData.CallOnValue ("UserAgent", [&config] (Json::String agent) {
    config.user_agent = KeplerToAwesomium (agent);
  });

  // Get the debugger port from the json if there is one (Uses 8000 if there isn't)
  jsonData.CallOnValue ("DebuggingPort", [&config] (Json::Number port) {
    config.remote_debugging_port = int (port);
  });

  // Get the debugging host from the json if there is one
  jsonData.CallOnValue ("DebuggingHost", [&config] (Json::String host) {
    config.remote_debugging_host = KeplerToAwesomium (host);
  });

  // Enable the following options by default no matter what
  const char *defaultOpts[] =
  {
    "enable-gamepad",
    "disable-fullscreen",
    "disable-gpu-vsync"
  };

  for (const char *opt : defaultOpts)
    config.additional_options.Push (Awesomium::WSLit (opt));

  // Enable any other options in the json
  jsonData.CallOnValue ("AdditionalOptions", [&config] (const Json::List &opts) {
    for (String opt : opts)
      config.additional_options.Push (KeplerToAwesomium (opt));
  });

  prefs.enable_web_gl = true;     // WebGL might be need, enable that
  prefs.enable_web_audio = false; // Playing audio would stomp on the audio engine. No.
  prefs.user_stylesheet = Awesomium::WSLit (
                            "html {background: transparent !important; } "
                            "body {overflow: hidden; }"
                          );

  Awesomium::WebString path;

  jsonData.CallOnValue ("CachePath", [&path] (Json::String path_) {
    path = KeplerToAwesomium (path_);
  });

  Awesomium::WebCore::instance()->Initialize (config);

  m_session = Awesomium::WebCore::instance()->CreateWebSession (path, prefs);

  m_surfaceFactory = new AwesomeiumGLTextureSurface::Factory;
  Awesomium::WebCore::instance()->set_surface_factory (m_surfaceFactory);

  RegisterMessageProc ("UIUpdate", function<void() > (
  []() {
    Awesomium::WebCore::instance()->Update();
  }));
}

void AwesomiumManagerComponent::Free()
{
  delete m_surfaceFactory;
  m_session->Release();
  Awesomium::WebCore::Shutdown();
}

// -----------------------------------------------------------------------------

META_REGISTER_FUNCTION (AwesomiumManager)
{
  META_INHERIT (AwesomiumManagerComponent, "Kepler", "Component");

  META_FINALIZE_PTR (AwesomiumManagerComponent, MetaPtr);
}
