//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.

#ifndef AWESOMIUM_STDAFX_H
#define AWESOMIUM_STDAFX_H

#ifdef _WIN32

// First: Let's get Windows.h
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#undef ERROR

#endif

#include <Awesomium/WebCore.h>
#include <Awesomium/WebConfig.h>
#include <Awesomium/WebView.h>
#include <Awesomium/WebString.h>
#include <Awesomium/WebSession.h>
#include <Awesomium/STLHelpers.h>
#include <Awesomium/JSObject.h>
#include <Awesomium/Surface.h>
#include <Awesomium/BitmapSurface.h>

#include <direct.h>
#include <iostream>
#include <iostream>
#include <codecvt>

#include "../Logger/logger.h"

#endif