// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Component for managing awesomium stuff
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef AWESOMIUM_MANAGER_COMPONENT_H
#define AWESOMIUM_MANAGER_COMPONENT_H

#include "../KeplerCore/component.h"
#include "awesomiumoglsurface.h"

class AwesomiumManagerComponent : public Component
{
public:
  virtual void Initialize (const Json::Object &jsonData);
  virtual void Free();

  COMPONENT_META

private:
  Awesomium::WebSession *m_session;
  AwesomeiumGLTextureSurface::Factory *m_surfaceFactory;
};

#endif
