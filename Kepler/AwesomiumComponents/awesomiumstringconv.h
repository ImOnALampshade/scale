// -----------------------------------------------------------------------------
//  Author: Howard Hughes
//
//  Awesomium string converion (UTF8 to UTF16)
//
//  Copyright (C) 2014 DigiPen Institute of Technology.
//  Reproduction or disclosure of this file or its contents without
//  the prior written consent of DigiPen Institute of Technology is
//  prohibited.
// -----------------------------------------------------------------------------

#ifndef AWESOMIUM_STRING_CONVERSION_H
#define AWESOMIUM_STRING_CONVERSION_H

#include "../String/string.h"

String AwesomiumToKepler (const Awesomium::WebString &str);
Awesomium::WebString KeplerToAwesomium (const String &str);

#endif
